<?php

class ContactController extends SecureController   {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	function getResourceForACL() {
		return "Contact";
	}
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	function getActionforACL() {
        $action = strtolower($this->getRequest()->getActionName()); 
        if($action == 'create'){
        	return "index";
        }
		if($action == 'list'){
			return "view";			
		}
		
		return parent::getActionforACL(); 
	}
	function createAction(){
		parent::createAction();
	}
	function indexAction(){
		
	}
	function updateAction(){
		
	}
	function viewAction(){
		
	}
	function listAction(){
		
	}
}