<?php

class MilestoneController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Milestone";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
		if($action == "index" || $action == "update" || $action == "create" || $action == "edit" || $action == "update" || $action == "progress" || $action == "processprogress" || $action == "processmilestones"){
	 		return "index";
	 	}
	 	if($action == "view" || $action == 'listsearch' || $action == 'listsubmit'){
	 		return "list";
	 	}
		return parent::getActionforACL();
    }
    function processmilestonesAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	 
    	$formvalues = $this->_getAllParams(); // debugMessage($this->_getAllParams());
    	$milestone_collection = new Doctrine_Collection(Doctrine_Core::getTable("Milestone"));
    	
    	for($x = 4; $x >= 0; $x--){
    		$data['id'] = $this->_getParam('level'.$x.'_id');
    		$data['name'] = $this->_getParam('level'.$x.'_name');
    		$data['level'] = $this->_getParam('level'.$x);
    		$data['programid'] = $this->_getParam('programid');
    		$data['type'] = $this->_getParam('level'.$x.'_type');
    		
    		// debugMessage($data);
    		$milestone = new Milestone();
    		if(!isArrayKeyAnEmptyString('id', $data)){
    			$milestone->populate($data['id']);
    		}
    		$milestone->processPost($data);
    		if(!$milestone->hasError()){
    			$milestone_collection->add($milestone);
    		}
    	}
    	// debugMessage($milestone_collection->toArray());
    	if($milestone_collection->count() > 0){
    		$milestone_collection->save();
    	}
    	$session->setVar(SUCCESS_MESSAGE, "Successfully updated."); // debugMessage('passed');
    	$this->_helper->redirector->gotoUrl($this->view->baseUrl('milestone/list'));
    }
    function progressAction(){
    	
    }
    function processprogressAction(){
    	 
    }
}
