<?php

/**
 * Controls access to only users who have logged in and checks the access control for the different pages 
 * 
 */
class SecureController extends IndexController  {
	
	public function init() {
		// initialize the parent controller 
		parent::init();
		$session = SessionWrapper::getInstance(); 
		// check whether the user is logged in
		if (isEmptyString($session->getVar('userid'))) {
	         // clear the session
			$this->_helper->redirector->gotoSimpleAndExit("login", "user", $this->getRequest()->getModuleName(), array('redirecturl' => encode(Zend_Controller_Front::getInstance()->getRequest()->getRequestUri())));
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 9;
			$audit_values['module'] = 1;
			$audit_values['usecase'] = '1.2';
			$audit_values['transactiontype'] = USER_LOGOUT;
			$audit_values['status'] = "Y";
			$audit_values['transactiondetails'] = "Auto logout detected. Session expired for User ".getUserNamesFromID(getUserID(), getCompanyID())." (ID ".getUserID().") ";
			$this->notify(new sfEvent($this, USER_LOGIN, $audit_values));
		}
		
		$cache = Zend_Registry::get('cache');
		// load the acl instance
		$acl = getACLInstance(); 
		
		/* $test = var_export($acl->checkPermission($this->getResourceForACL(), $this->getActionforACL()));
		debugMessage('resource is '.$this->getResourceForACL()." action ".$this->getActionforACL()." test is ".$test); exit; */
		if (!$acl->checkPermission($this->getResourceForACL(), $this->getActionforACL())) { 
			$resource = $this->getResourceForACL();
			$resourcename = getAclResourceName($resource);
			$resourceid = getAclResourceID($resource);
			
			$action = $this->getActionforACL(); // debugMessage('action: '.$this->getActionforACL());
			$actionname = getActionNameforACL($action, $resourceid);
			if(isEmptyString($actionname)){
				$actionname = $action;
			}
			
			// redirect to the access denied page 
			$viewurl = $this->view->viewurl;
			// save to audit
			$usecase = '1.17';
			$module = '1';
			$type = USER_ACCESSDENIED;
			$details = "Access denied to interface at '".$viewurl."'. No permissions detected for action '".$actionname."' on resource '".$resourcename."' ";
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 15;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "N";
			// debugMessage($audit_values);
			$this->notify(new sfEvent($this, $type, $audit_values));
			
			// exit;
			$this->_helper->redirector->gotoUrl($this->view->baseUrl("index/accessdenied"));
		}
	}
	
	/**
	 * Get the name of the resource being accessed 
	 *
	 * @return String 
	 */
	function getResourceForACL() {
		return strtolower($this->getRequest()->getControllerName()); 
	}
	
	/**
	 * Get the name of the action being accessed 
	 *
	 * @return String 
	 */
	function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		if($action == 'listsearch' || $action == 'listsubmit'){
			return "list";
		}
		if($action == 'create'){
			return "index";
		} 
		if($action == 'edit'){
			return "update";
		}
		return $action; 
	}
}
