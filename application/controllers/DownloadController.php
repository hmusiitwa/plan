<?php

class DownloadController extends IndexController {
        /**
	 * The default action - show the home page
	 */
	public function indexAction() {
		// automatic file mime type handling
		$filename = decode($this->_getParam('filename')); //debugMessage($filename);
		$full_path = decode($this->_getParam('path')); //debugMessage($full_path); 
		// exit();
		// file headers to force a download
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    // to handle spaces in the file names 
	    header("Content-Disposition: attachment; filename=\"$filename\"");
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    readfile($full_path);
 
	    // disable layout and view
	    $this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
	}
	
	function pdfAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit();
		
		// debugMessage($formvalues);
		$cvsdata = $formvalues['csv_text'];
		$separator = '\t';
		
		$currenttime = time();
		$filename = $currenttime.'.pdf';
		// debugMessage($ignorecolumns);
		
		$data = stripcslashes($cvsdata); // debugMessage($data);
		$rows = array_remove_empty(parse_csv($data, $separator));
		if($this->_getParam('nodecode') == 1){
			foreach ($rows as $k => $line){
				// debugMessage($line);
				$rows[$k] = explode("\t", $line[0]);
			}
			// debugMessage($rows); exit;
			$pdf_html = build_table($rows); // die($pdf_html);
			$timestamp = time().rand(100, 5000);
			$temp_file_pdf = ($timestamp).".pdf";
			$path = BASE_PATH.DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR.$temp_file_pdf; // deb
				
			$mpdfpath = BASE_PATH.DIRECTORY_SEPARATOR.'application'.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php'; // debugMessage('pdf '.$mpdfpath);
			ini_set('memory_limit','128M');
			require_once($mpdfpath);
				
			$mpdf = new mPDF('win-1252','A4');
			$mpdf->useOnlyCoreFonts = true;    // false is default
			$mpdf->SetProtection(array('print'));
			$mpdf->SetTitle(getAppName(). "- PDF Download");
			$mpdf->SetAuthor(getCompanyName());
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->WriteHTML($pdf_html);
			$mpdf->Output($temp_file_pdf, 'I');
			exit;
		}
	}
	
	function excelAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); // exit();
		$title = $this->_getParam('reporttitle');
	
		// debugMessage($formvalues);
		if($this->_getParam('nodecode') == 1){
			$cvsdata = $formvalues['csv_text']; 
			// $htm = csvToTable($cvsdata); echo ($htm); exit();
		} else {
			$cvsdata = decode($formvalues['csv_text']); // echo ($cvsdata); exit();
		}
		
		$cvsdata = str_replace('"--"', '', $cvsdata);
		$separator = stringContains(';', $cvsdata) ? ";" : ",";
		if($this->_getParam('nodecode') == 1){
			$separator = '\t';
		}
		
		if(!isEmptyString($title)){
			$title = str_replace(', ',' ',$title);
			$cvsdata = $title."\r\n".$cvsdata;
		}
		
		$currenttime = time();
		$filename = $currenttime.'.xlsx';
		$ignorecolumns = array();
		if(!isEmptyString($this->_getParam('ignorecolumns'))){
			$ignorecolumns = array_unique(array_remove_empty(explode(',', $this->_getParam('ignorecolumns'))));
		}
		// debugMessage($ignorecolumns);
		
		$data = stripcslashes($cvsdata); // debugMessage($data);
		$rows = array_remove_empty(parse_csv($data, $separator)); 
		if($this->_getParam('nodecode') == 1){
			foreach ($rows as $k => $line){
				// debugMessage($line);
				$rows[$k] = explode("\t", $line[0]);
			}
		}
		
		if(is_array($rows)){
			if(!isArrayKeyAnEmptyString(0, $rows)){
				$row1keys = array_keys($rows[0]); // debugMessage($row1keys); // exit;
			}
				
			foreach ($rows as $key => $value){
				if($key > 0){
					$rowxkeys = array_keys($rows[$key]);
					$diff  = array_diff($row1keys, $rowxkeys);  // debugMessage($diff);
					foreach ($diff as $t => $mskey) {
						if(isArrayKeyAnEmptyString($mskey, $value)){
							$rows[$key][$mskey] = NULL;
						}
					}
				}
				foreach ($value as $k => $avalue){
					$rows[$key][$k] = trim($rows[$key][$k]);
					$num = str_replace(',', '', $rows[$key][$k]);
					if(is_numeric($num) && stringContains(',', $rows[$key][$k])){
						$rows[$key][$k] = $num;
					}
					if(stringContains('href', $avalue) || stringContains('span', $avalue) || stringContains('div', $avalue)){
						// debugMessage('string exits '.$k.' ~ '.$v);
						$rows[$key][$k] = NULL;
					}
					// debugMessage(trim($rows[$key][$k]));
				}
				if(!isEmptyString($this->_getParam('ignorecolumns'))){
					foreach ($ignorecolumns as $k => $val){
						unset($rows[$key][$val]);
					}
				}
				ksort($rows[$key]);
			}
		}
		
		if(!isArrayKeyAnEmptyString(0, $rows)){
			$keepkeys = $this->_getParam('keepkeys') == '1' ? true : false;
			foreach ($rows[0] as $key => $value){
				if(!$keepkeys){
					if(isEmptyString($value)){
						unset($rows[0][$key]);
					}
				}
			}
		}
		// debugMessage($rows); exit;
		// $rows = $rows[0]; // debugMessage($rows); exit;
		// $rows = array(1,2,3,4,5,6);
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->fromArray($rows, null, 'A1');
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle(isEmptyString($this->_getParam('title')) ? 'ExportData' : $this->_getParam('title'));
		$totalStyle = array(
				'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '336699')
				),
				'font'  => array(
						'bold'  => true,
						'color' => array('rgb' => 'FFFFFF')
				),
				'alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP, 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);
		
		// Set AutoSize for name and email fields
		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			// $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(50);
			$objPHPExcel->getActiveSheet()->getStyle($col.'1')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle($col.'1:'.$col.''.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle($col.'1:'.$col.''.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		}
		
		// exit;
		if($this->_getParam('source') == 'framework'){
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
			
			$objPHPExcel->getActiveSheet()->getStyle('B1:B'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C1:c'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
		}
		if($this->_getParam('source') == 'indicator'){
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		}
		
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.$objPHPExcel->getActiveSheet()->getHighestDataColumn().'1')->applyFromArray($totalStyle); 
		// exit;
		// header('Content-Type: application/vnd.ms-excel');
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		header('Content-Description: File Transfer');
		
		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('php://output');
	}
}
