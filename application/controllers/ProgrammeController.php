<?php

class ProgrammeController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Framework";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	if($action == "resultsframe"){
	 		return 'viewresults';
	 	}
	 	if($action == "logframe" || $action == "countobjectives" || $action == "countgoals" || $action == "populateobj" || $action == "populateframe"){
	 		return 'viewlogical';
	 	}
	 	if($action == "addcontent" || $action == "processcontent"){
	 		return 'addresults';
	 	}
		return parent::getActionforACL();
    }
    function logframeAction(){}
    function resultsframeAction(){}
    function addcontentAction(){
    	 
    }
    function processcontentAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit;
    	
    	$entity = new ContentFrame(); 
    	if($this->_getParam('level') == 1){
    		$entity = new Program();
    		$formvalues['name'] = $formvalues['title'];
    		$formvalues['code'] = $formvalues['refno'];
    	}
    	if($this->_getParam('level') == 4){
    		$entity = new Activity();
    	}
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	
    	if(!isEmptyString($id)){
    		$entity->populate($id);
    		$formvalues['lastupdatedby'] = $session->getVar('userid');
    	} else {
    		$formvalues['createdby'] = getUserID();
    	}
    	// debugMessage($formvalues);
    	$entity->processPost($formvalues); // debugMessage($entity->toArray()); 
    	// debugMessage('errors are '.$entity->getErrorStackAsString()); exit();
    	
    	if($entity->hasError()){
    		// $session->setVar(ERROR_MESSAGE, $entity->getErrorStackAsString());
    		$resultarray = array(
    				'id' => '',
    				'name' => '',
    				'result' => 'error',
    				'message' => $entity->getErrorStackAsString()
    		);
    	} else {
    		try {
    			$entity->save(); // debugMessage($entity->toArray());
    			if(!isEmptyString($id)){
    				$entity->afterUpdate();
    			} else {
    				$entity->afterSave();
    			}
    			$resultarray = array(
    					'id' => $entity->getID(),
    					'result' => 'success',
    					'message' => 'Successfully saved'
    			);
    			
    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
    		} catch (Exception $e) {
    			//$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
    			$resultarray = array(
    					'id' => '',
    					'result' => 'error',
    					'message' => $e->getMessage()
    			);
    		}
    	}
    	die(json_encode($resultarray));
    }
	
    function countobjectivesAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$countoutcomes = count(getObjectives($this->_getParam('type'), $this->_getParam('projectid'))); //
    	$data = array();
    	$data['count'] = $countoutcomes;
    	$data['nextref'] = $countoutcomes+1;
    	echo json_encode($data);
    }
    function countgoalsAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	 
    	$countoutcomes = count(getGoals($this->_getParam('type'), $this->_getParam('projectid'))); //
    	$data = array();
    	$data['count'] = $countoutcomes;
    	$data['nextref'] = $countoutcomes+1;
    	echo json_encode($data);
    }
    function populateobjAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$program = new Program();
    	$program->populate($this->_getParam('programid'));
    	$data = array();
    	$data['id'] = $program->getID();
    	$data['code'] = $program->getCode();
    	$countoutcomes = count(getOutcomes($this->_getParam('type'), $this->_getParam('programid'), $this->_getParam('projectid'))); //
    	$data['nextref'] = $program->getCode().'.'.($countoutcomes+1);
    	//debugMessage($data);
    	echo json_encode($data);
    }
    function populateframeAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	 
    	$content = new ContentFrame();
    	$content->populate($this->_getParam('outcomeid'));
    	$data = array();
    	$data['id'] = $content->getID();
    	$data['refno'] = $content->getRefno();
    	$countoutputs = count(getOutputs($this->_getParam('type'), $this->_getParam('outcomeid'), $this->_getParam('projectid'))); //
    	$data['nextref'] = $content->getRefno().'.'.($countoutputs+1);
    	// debugMessage($data);
    	echo json_encode($data);
    }
}
