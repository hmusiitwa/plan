<?php

class VariablesController extends SecureController   {
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * Return the Application Settings since we need to make the url more friendly
	 *
	 * @return String
	 */
	function getResourceForACL() {
		$action = strtolower($this->getRequest()->getActionName());
		return 'Variables';
	}
	/**
	 * Get the name of the resource being accessed 
	 *
	 * @return String 
	 */
	function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		$reportactions = getActionsResources('6');
		foreach($reportactions as $aid => $actiondetails){
			if($action == $actiondetails['slug']) {
				return $actiondetails['slug'];
			}
		}
		if($action == 'processvariables'){
			return 'index';
		}
		parent::getActionforACL();
		// return 'index';
	}
	
	function farmertypeAction(){}
	function vulgroupsAction(){}
	function specialgrpsAction(){}
	function entgroupsAction(){}
	function entcategoriesAction(){}
	function seasontypeAction(){}
	function contractstatusAction(){}
	function procmethodsAction(){}
	function indexAction(){
		
	}
	
	function processvariablesAction(){
		$session = SessionWrapper::getInstance(); 
     	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); // exit;
		if(isArrayKeyAnEmptyString('noreload', $formvalues)){
			$hasnoreload = false; 	
		} else {
			$hasnoreload = true;
		}
		
		$haserror = false;
		if(isArrayKeyAnEmptyString('value', $formvalues) && !$hasnoreload){
			$haserror = true;
			$session->setVar(ERROR_MESSAGE, 'Error: No value specified for addition');
			$session->setVar(FORM_VALUES, $formvalues); 
			$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_SUCCESS)));
		}
		
		$type_ext = '';
		$alias = '';
		if(isArrayKeyAnEmptyString('id', $formvalues)){
			if(!isArrayKeyAnEmptyString('alias', $formvalues)){
				$alias = $formvalues['alias'];
			}
			if($alias == 'undefined'){
				$alias = '';
			}
		} else {
			if(!isArrayKeyAnEmptyString('alias', $formvalues)){
				$alias = decode($formvalues['alias']);
			}
			if($alias == 'undefined'){
				$alias = '';
			}
		}
		
		// exit;
		// debugMessage()
		switch ($formvalues['lookupid']){
			case 20:
				$programunit = new ProgramUnit();
				
				$index = '';
				if($hasnoreload){
					$value  = trim($formvalues['value']);
				} else {
					$value  = addslashes(decode(trim($formvalues['value'])));
				}
				$dataarray = array('id' => $formvalues['id'], 'name' => $value);
				if(!isEmptyString($alias)){
					$dataarray['refno'] = $alias;
				}
				if(!isArrayKeyAnEmptyString('id', $dataarray)){
					$dataarray['lastupdatedby'] = getUserID();
				} else {
					$dataarray['createdby'] = getUserID();
				}
				
				if(!isArrayKeyAnEmptyString('id', $formvalues)){
					$programunit->populate($formvalues['id']);
					$beforesave = $programunit->toArray(); // debugMessage($beforesave);
				}
				// unset($dataarray['id']);
				$programunit->processPost($dataarray);
				
				$result = array('id'=>'', 'name'=>'', 'error' => '');
				if($programunit->hasError()){
					$haserror = true;
					$session->setVar(ERROR_MESSAGE, $programunit->getErrorStackAsString());
					$session->setVar(FORM_VALUES, $formvalues);
					$result['error'] = $programunit->getErrorStackAsString();
				} else {
					try {
						$programunit->save();
						if(isEmptyString($programunit->getRefno()) || $programunit->getRefno() == 'Auto'){
							$programunit->setRefno('PU'.($programunit->getID() + 100));
							$programunit->save();
						}
						if(!$hasnoreload){
							$url = '';
							$module = '0';
							if(isArrayKeyAnEmptyString('id', $formvalues)){
								$session->setVar(SUCCESS_MESSAGE, "Successfully saved");
								$type = SYSTEM_ADDVARIABLE;
								$usecase = '0.1';
								$details = 'Added PROGRAM_UNIT ['.$programunit->getName().']';
							} else {
								$session->setVar(SUCCESS_MESSAGE, "Successfully updated");
								$type = SYSTEM_UPDATEVARIABLE;
								$usecase = '0.2';
								$details = 'Updated PROGRAM_UNIT ['.$programunit->getName().']';
								$prejson = json_encode($beforesave);
								$programunit->clearRelated();
								$after = $programunit->toArray(); // debugMessage($after);
								$postjson = json_encode($after); // debugMessage($postjson);
								$diff = array_diff($beforesave, $after);  // debugMessage($diff);
								$jsondiff = json_encode($diff); // debugMessage($jsondiff);
							}
								
							$browser = new Browser();
							$audit_values = $session->getVar('browseraudit');
							$audit_values['module'] = $module;
							$audit_values['usecase'] = $usecase;
							$audit_values['transactiontype'] = $type;
							$audit_values['status'] = "Y";
							$audit_values['userid'] = $session->getVar('userid');
							$audit_values['transactiondetails'] = $details;
							$audit_values['url'] = '';
							if(!isArrayKeyAnEmptyString('id', $formvalues)){
								$audit_values['isupdate'] = 1;
								$audit_values['prejson'] = $prejson;
								$audit_values['postjson'] = $postjson;
								$audit_values['jsondiff'] = $jsondiff;
							}
							// debugMessage($audit_values);
							$this->notify(new sfEvent($this, $type, $audit_values));
						}
						$result = array('id'=>$programunit->getID(), 'name'=>$programunit->getName(), 'alias'=>$programunit->getRefno(), 'error'=>'');
					} catch (Exception $e) {
						$session->setVar(ERROR_MESSAGE, $e->getMessage()."<br />".$programunit->getErrorStackAsString());
						$session->setVar(FORM_VALUES, $formvalues);
					}
				}
				break;
			default:
				$lookupvalue = new LookupTypeValue();
				$lookuptype = new LookupType();
				$lookuptype->populate($formvalues['lookupid']);
					
				$index = '';
				if($hasnoreload){
					$index = $lookuptype->getNextInsertIndex();
					$value  = trim($formvalues['value']);
				} else {
					if(!isArrayKeyAnEmptyString('index', $formvalues)){
						$index = $formvalues['index'];
					} else {
						$index = $lookuptype->getNextInsertIndex();
					}
					$value  = addslashes(decode(trim($formvalues['value'])));
				}
				
				$dataarray = array('id' => $formvalues['id'],
									'lookuptypeid' => $formvalues['lookupid'], 
									'lookuptypevalue' => $index, 
									'lookupvaluedescription' => $value,
									'alias' => $alias,
									'createdby' => $session->getVar('userid')
							);
				// debugMessage($dataarray); // exit();
				if(!isArrayKeyAnEmptyString('id', $formvalues)){
					$lookupvalue->populate($formvalues['id']);
					$beforesave = $lookupvalue->toArray(); // debugMessage($beforesave);
				}
				// unset($dataarray['id']);
				$lookupvalue->processPost($dataarray);
				/* debugMessage($lookupvalue->toArray());
		    	debugMessage('errors are '.$lookupvalue->getErrorStackAsString()); exit(); */
				
		    	$result = array('id'=>'', 'name'=>'', 'error' => '');
				if($lookupvalue->hasError()){
					$haserror = true;
					$session->setVar(ERROR_MESSAGE, $lookupvalue->getErrorStackAsString());
					$session->setVar(FORM_VALUES, $formvalues);
					$result['error'] = $lookupvalue->getErrorStackAsString();
				} else {
					try {
						$lookupvalue->save();
						if(!$hasnoreload){
							$url = '';
							$module = '0';
							if(isArrayKeyAnEmptyString('id', $formvalues)){
								$session->setVar(SUCCESS_MESSAGE, "Successfully saved");
								$type = SYSTEM_ADDVARIABLE;
								$usecase = '0.1';
								$details = 'Variable - <b>'.$lookupvalue->getlookupvaluedescription().' </b>('.$lookupvalue->getLookupType()->getdisplayname().') addded';
							} else {
								$session->setVar(SUCCESS_MESSAGE, "Successfully updated");
								$type = SYSTEM_UPDATEVARIABLE;
								$usecase = '0.2';
								$details = 'Variable - <b>'.$lookupvalue->getlookupvaluedescription().' </b>('.$lookupvalue->getLookupType()->getdisplayname().') updated';
								$prejson = json_encode($beforesave);
								$lookupvalue->clearRelated();
								$after = $lookupvalue->toArray(); // debugMessage($after);
								$postjson = json_encode($after); // debugMessage($postjson);
								$diff = array_diff($beforesave, $after);  // debugMessage($diff);
								$jsondiff = json_encode($diff); // debugMessage($jsondiff);
							}
							
							$browser = new Browser();
							$audit_values = $session->getVar('browseraudit');
							$audit_values['module'] = $module;
							$audit_values['usecase'] = $usecase;
							$audit_values['transactiontype'] = $type;
							$audit_values['status'] = "Y";
							$audit_values['userid'] = $session->getVar('userid');
							$audit_values['transactiondetails'] = $details;
							$audit_values['url'] = '';
							if(!isArrayKeyAnEmptyString('id', $formvalues)){
								$audit_values['isupdate'] = 1;
								$audit_values['prejson'] = $prejson;
								$audit_values['postjson'] = $postjson;
								$audit_values['jsondiff'] = $jsondiff;
							}
							// debugMessage($audit_values);
							$this->notify(new sfEvent($this, $type, $audit_values));
						}
						$result = array('id'=>$lookupvalue->getlookuptypevalue(), 'name'=>$lookupvalue->getlookupvaluedescription(), 'alias'=>$lookupvalue->getalias(), 'error'=>'');
					} catch (Exception $e) {
						$session->setVar(ERROR_MESSAGE, $e->getMessage()."<br />".$lookupvalue->getErrorStackAsString());
						$session->setVar(FORM_VALUES, $formvalues);
					}
				}
				break;
		}
		
		$successurl = decode($this->_getParam(URL_SUCCESS)); // debugMessage('success - '.$successurl);
		if(!$hasnoreload){
			$this->_helper->redirector->gotoUrl($successurl);	
		} else {
			echo json_encode($result);
		}
	}
}

