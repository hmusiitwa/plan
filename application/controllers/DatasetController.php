<?php

class DatasetController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Dataset";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	if($action == "edit" || $action == "update" || $action == "section" || $action == "setgroup" || $action == "managegroup" || $action == "processgroup" || $action == "processsection" || $action == "movesection" || $action == "greyfields" || $action == "processgreyfields" || $action == "projectsavesuccess"){
	 		return "index";
	 	}
	 	if($action == "view" || $action == 'listsearch' || $action == 'listsubmit'){
	 		return "list";
	 	}
		return parent::getActionforACL();
    }
    
    function sectionAction(){}
    
    function setgroupAction(){}
    
    function managegroupAction(){}
    
    function processgroupAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	
    	$entity = new UserCategory();
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	
    	if(!isEmptyString($id)){
    		$entity->populate($id);
    		$formvalues['lastupdatedby'] = $session->getVar('userid');
    		$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
    	} else {
    		$formvalues['createdby'] = $session->getVar('userid');
    	}
    	
    	$entity->processPost($formvalues); // exit;
    	/* debugMessage($entity->toArray());
    	debugMessage('errors are '.$entity->getErrorStackAsString()); exit(); */
    	
    	if($entity->hasError()){
    		$resultarray = array(
    				'id' => '',
    				'name' => '',
    				'result' => 'error',
    				'message' => 'Error: '.$entity->getErrorStackAsString()
    		);
    	} else {
    		try {
    			$entity->save(); // debugMessage($payment->toArray());
    			$resultarray = array(
    				'id' => $entity->getID(),
    				'name' => $entity->getName(),
    				'result' => 'success',
    				'message' => 'Successfully saved'
    			);
    		} catch (Exception $e) {
    			$resultarray = array(
    				'id' => '',
    				'name' => '',
    				'result' => 'error',
    				'message' => 'Error: '.$e->getMessage()
    			);
    		}
    	}
    	
    	echo json_encode($resultarray);
    }
    
    function processsectionAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$conn = Doctrine_Manager::connection();
    	
    	$entity = new DatasetSection();
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	 
    	if(!isEmptyString($id)){
    		$entity->populate($id);
    		$formvalues['lastupdatedby'] = $session->getVar('userid');
    		$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
    	} else {
    		$formvalues['createdby'] = $session->getVar('userid');
    	}
    	 
    	$entity->processPost($formvalues); // exit;
    	/* debugMessage($entity->toArray());
    	debugMessage('errors are '.$entity->getErrorStackAsString());exit(); */
    	 
    	if($entity->hasError()){
    		$session->setVar(ERROR_MESSAGE, $entity->getErrorStackAsString());
    	} else {
    		try {
    			if(isEmptyString($entity->getPosition())){
    				$maxposition = $conn->fetchOne("select max(position) from data_set_section where datasetid = '".$entity->getDatasetID()."'");
    				$entity->setPosition($maxposition + 1);
    			}
    			$entity->save(); // debugMessage($payment->toArray());
    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
    		} catch (Exception $e) {
    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
    		}
    	}
    }
    
    function movesectionAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$conn = Doctrine_Manager::connection();
    	
    	$section = new DatasetSection();
    	$section->populate($formvalues['id']); //debugMessage($section->toArray());
    	$positionbefore = $section->getPosition();
    	
    	$replaceid = $conn->fetchOne("select id from data_set_section where datasetid = '".$section->getDatasetID()."' AND position > '".$positionbefore."' order by position asc limit 1 "); debugMessage($replaceid);
    	if($this->_getParam('direction') == 'up'){
    		$replaceid = $conn->fetchOne("select id from data_set_section where datasetid = '".$section->getDatasetID()."' AND position < '".$positionbefore."' order by position desc limit 1 "); debugMessage($replaceid);
    	}
    	$newsection = new DatasetSection();
    	$newsection->populate($replaceid); //debugMessage($newsection->toArray());
    	
    	try {
    		$section->setPosition($newsection->getPosition());
    		$section->setlastupdatedate(DEFAULT_DATETIME);
    		
    		$newsection->setPosition($positionbefore);
    		
    		$section->save();
    		$newsection->save();
    		$session->setVar(SUCCESS_MESSAGE, "Successfully changed position");
    	} catch (Exception $e) {
    		$session->setVar(ERROR_MESSAGE, "Error changing section position. ".$e->getMessage());
    	}
    	
    	$successurl = decode($this->_getParam(URL_SUCCESS)); debugMessage($successurl);
    	$this->_helper->redirector->gotoUrl($successurl);
    }
    
    function greyfieldsAction(){
    	
    }
    
    function processgreyfieldsAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	 
    	$entity = new DatasetSection();
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	$entity->populate($id);
    	$formvalues['lastupdatedby'] = $session->getVar('userid');
    	$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
    	
    	$attrlist = array();
    	if(!isArrayKeyAnEmptyString('attributes', $formvalues)){
    		foreach($formvalues['attributes'] as $key => $element){
    			foreach($element as $pkey => $option){
    				if(!is_array($option)){
    					if($option == '0'){
    						$attrlist[] = $key.'.'.$pkey;
    					}
    				} else {
    					foreach($option as $ckey => $coption){
    						if(is_array($coption)){
    							foreach($coption as $mkey => $moption){
    								if($moption == '0'){
    									$attrlist[] = $key.'.'.$pkey.'.'.$ckey.'.'.$mkey;
    								}
    							}
    						} else {
    							if($coption == '0'){
    								$attrlist[] = $key.'.'.$pkey.'.'.$ckey;
    							}
    						}
    					}
    				}
    			}
    		}
    	}
    	// debugMessage($attrlist);
    		if(count($attrlist) > 0){
    			$formvalues['greyelements'] = implode(',', $attrlist);
    		} else {
    			$formvalues['greyelements'] = NULL;
    		}
    		$entity->processPost($formvalues);
    		if($entity->hasError()){
    			$session->setVar(ERROR_MESSAGE, "Error saving changes to disabled attributes. ".$entity->getErrorStackAsString());
    		} else {
	    		try {
	    			$entity->save();
	    			$session->setVar(SUCCESS_MESSAGE, "Successfully saved changes");
	    		} catch (Exception $e) {
	    			$session->setVar(ERROR_MESSAGE, "Error saving changes to disabled attributes. ".$e->getMessage());
	    		}
    		}
    }
    
    function projectsavesuccessAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams();
    	debugMessage($formvalues);
    	 
    	$successurl = decode($this->_getParam(URL_SUCCESS));
    	if(!isEmptyString($this->_getParam('id'))){
    		$successurl .= '/did/'.$this->_getParam('id');
    	}
    	debugMessage($successurl);
    	$this->_helper->redirector->gotoUrl($successurl);
    }
}
