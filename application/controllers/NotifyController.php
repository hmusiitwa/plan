<?php

class NotifyController extends SecureController  {
	
	/**
	 * Override unknown actions to enable ACL checking
	 *
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		if($action == "xx") {
			// return ACTION_CREATE;
		}
		return parent::getActionforACL();
	}
}