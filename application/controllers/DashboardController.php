<?php

class DashboardController extends SecureController  {
	
	function getResourceForACL(){
		return "Navigation";
	}
	/**
	 * @see SecureController::getActionforACL()
	 * 
	 * The dashboard can only be viewed, however the default is create for the index.phtml file. 
	 *
	 * @return String
	 */
	function getActionforACL() {
		return "userdashboard"; 
	}
	
	public function init()    {
		parent::init();
	
		$current_timestamp = strtotime('now'); $now_iso = date('Y-m-d H:i:s', $current_timestamp); $this->view->now_iso = $now_iso; //debugMessage('now '.$now_iso.'-'.$current_timestamp);
		$onehourago_timestamp = strtotime('-1 hour'); $onehourago_iso = date('Y-m-d H:i:s', $onehourago_timestamp );
		$this->view->onehourago_iso = $onehourago_iso; $this->view->onehourago_timestamp = $onehourago_timestamp;// debugMessage('now '.$onehourago_iso.'-'.$onehourago_timestamp);
		$sixhourago_timestamp = strtotime('-6 hour'); $sixhourago_iso = date('Y-m-d H:i:s', $sixhourago_timestamp);
		$this->view->sixhourago_iso = $sixhourago_iso; $this->view->sixhourago_timestamp = $sixhourago_timestamp;
		$twelvehourago_timestamp = strtotime('-12 hour'); $twelvehourago_iso = date('Y-m-d H:i:s', $twelvehourago_timestamp);
		$this->view->twelvehourago_timestamp = $twelvehourago_timestamp; $this->view->twelvehourago_iso = $twelvehourago_iso;
	
		// debugMessage($logged_today_sql);
		$today_iso = date('Y-m-d'); $today = changeMySQLDateToPageFormat($today_iso);  $this->view->today_iso = $today_iso; //debugMessage('today '.$today_iso);
		$today_iso_short = date('M j', $current_timestamp);
	
		$yestday_iso = date('Y-m-d', strtotime('1 day ago')); $yestday = changeMySQLDateToPageFormat($yestday_iso); $this->view->yestday_iso = $yestday_iso; //debugMessage('yesterday '.$yestday_iso);
		$yestday_iso_short = date('M j', strtotime($yestday_iso));
		$weekday = date("N");
	
		// monday of week
		$mondaythisweek_iso = date('Y-m-d', strtotime('monday this week')); $mondaythisweek = changeMySQLDateToPageFormat($mondaythisweek_iso);
		if($weekday == 1){
			$mondaythisweek_iso = $today_iso;
			$mondaythisweek = $today;
		}
		if($weekday == 7){
			$mondaythisweek_iso = date('Y-m-d', strtotime('monday last week'));
			$mondaythisweek = changeMySQLDateToPageFormat($mondaythisweek_iso);
		}
		$this->view->mondaythisweek_iso = $mondaythisweek_iso; // debugMessage('monday this week '.$mondaythisweek_iso);
	
		// sunday of week
		$sundaythisweek_iso = date('Y-m-d', strtotime('sunday this week')); $sundaythisweek = changeMySQLDateToPageFormat($sundaythisweek_iso);
		if($weekday == 1){
			$sundaythisweek_iso = date('Y-m-d', strtotime('today + 6 days')); $sundaythisweek = changeMySQLDateToPageFormat($sundaythisweek_iso);
		}
		if($weekday == 7){
			$sundaythisweek_iso = $today_iso; $sundaythisweek = $today;
		}
		$this->view->sundaythisweek_iso = $sundaythisweek_iso; // debugMessage('sunday this week '.$sundaythisweek_iso);
	
		// monday last week
		$mondaylastweek_iso = date('Y-m-d', strtotime('-7 days', strtotime($mondaythisweek_iso))); // debugMessage('monday last week '.$mondaylastweek_iso);
		$this->view->mondaylastweek_iso = $mondaylastweek_iso;
		// sunday last week
		$sundaylastweek_iso = date('Y-m-d', strtotime('-7 days', strtotime($sundaythisweek_iso)));  // debugMessage('sunday last week '.$sundaylastweek_iso);
		$this->view->sundaylastweek_iso = $sundaylastweek_iso;
	
		// monday next week
		$mondaynextweek_iso = date('Y-m-d', strtotime('+7 days', strtotime($mondaythisweek_iso))); // debugMessage('monday last week '.$mondaylastweek_iso);
		$this->view->mondaynextweek_iso = $mondaynextweek_iso;
		// sunday next week
		$sundaynextweek_iso = date('Y-m-d', strtotime('+7 days', strtotime($sundaythisweek_iso)));  // debugMessage('sunday last week '.$sundaylastweek_iso);
		$this->view->sundaynextweek_iso = $sundaynextweek_iso;
	
		// firstday this month
		$firstdayofthismonth_iso = getFirstDayOfCurrentMonth(); //debugMessage('1st day this month '.$firstdayofthismonth_iso);
		$this->view->firstdayofthismonth_iso = $firstdayofthismonth_iso;
		// lastday this month
		$lastdayofthismonth_iso = getLastDayOfCurrentMonth(); //debugMessage('last day this month '.$lastdayofthismonth_iso);
		$this->view->lastdayofthismonth_iso = $lastdayofthismonth_iso;
	
		// firstday last month
		$firstdayoflastmonth_iso = getFirstDayOfMonth(date('m')-1, date('Y')); //debugMessage('1st day last month '.$firstdayoflastmonth_iso);
		$this->view->firstdayoflastmonth_iso = $firstdayoflastmonth_iso;
		// lastday last month
		$lastdayoflastmonth_iso = getLastDayOfMonth(date('m')-1, date('Y')); //debugMessage('last day last month '.$lastdayoflastmonth_iso);
		$this->view->lastdayoflastmonth_iso = $lastdayoflastmonth_iso;
	
		// firstday 2 month ago
		$firstdayof2monthago_iso = getFirstDayOfMonth(date('m')-2, date('Y')); //debugMessage('1st day 2 month ago '.$firstdayof2monthago_iso);
		$this->view->firstdayof2monthago_iso = $firstdayof2monthago_iso;
		// lastday 2 month ago
		$lastdayof2monthago_iso = getLastDayOfMonth(date('m')-2, date('Y')); //debugMessage('last day last month '.$lastdayof2monthago_iso);
		$this->view->lastdayof2monthago_iso = $lastdayof2monthago_iso;
	
		// firstday 3 month ago
		$firstdayof3monthago_iso = getFirstDayOfMonth(date('m')-3, date('Y')); //debugMessage('1st day 3 month ago '.$firstdayof3monthago_iso);
		$this->view->firstdayof3monthago_iso = $firstdayof3monthago_iso;
		// lastday 3 month ago
		$lastdayof3monthago_iso = getLastDayOfMonth(date('m')-3, date('Y')); //debugMessage('last day last month '.$lastdayof3monthago_iso);
		$this->view->lastdayof3monthago_iso = $lastdayof3monthago_iso;
	
		// firstday 4 month ago and lastday 4 month ago
		$firstdayof4monthago_iso = getFirstDayOfMonth(date('m')-4, date('Y')); $this->view->firstdayof4monthago_iso = $firstdayof4monthago_iso;
		$lastdayof4monthago_iso = getLastDayOfMonth(date('m')-4, date('Y')); $this->view->lastdayof4monthago_iso = $lastdayof4monthago_iso;
	
		// firstday 5 month ago and lastday 5 month ago
		$firstdayof5monthago_iso = getFirstDayOfMonth(date('m')-5, date('Y')); $this->view->firstdayof5monthago_iso = $firstdayof5monthago_iso;
		$lastdayof5monthago_iso = getLastDayOfMonth(date('m')-5, date('Y')); $this->view->lastdayof5monthago_iso = $lastdayof5monthago_iso;
		
		// firstday 6 month ago and lastday 6 month ago
		$firstdayof6monthago_iso = getFirstDayOfMonth(date('m')-6, date('Y')); $this->view->firstdayof6monthago_iso = $firstdayof6monthago_iso;
		$lastdayof6monthago_iso = getLastDayOfMonth(date('m')-6, date('Y')); $this->view->lastdayof6monthago_iso = $lastdayof6monthago_iso;
	
		// firstday this year
		$firstdayofyear_iso = getFirstDayOfMonth(1, date('Y')); //debugMessage('1st day this year '.$firstdayofyear_iso);
		$this->view->firstdayofyear_iso = $firstdayofyear_iso;
		// lastday this year
		$lastdayofyear_iso = getLastDayOfMonth(12, date('Y')); //debugMessage('last day this year '.$lastdayofyear_iso);
		$this->view->lastdayofyear_iso = $lastdayofyear_iso;
		// first day of month one year ago
		$startofmonth_oneyearago = getFirstDayOfMonth(date('m', strtotime('1 year ago')), date('Y', strtotime('1 year ago')));
		$this->view->startofmonth_oneyearago = $startofmonth_oneyearago;
	
		$firstsystemday_iso = '2013-01-01';
		$this->view->firstsystemday_iso = $firstsystemday_iso;
	}
	
	public function testAction(){}
	
	public function savestateAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		
		$entity = new UserAccount();
		$entity->populate($formvalues['id']);
		$entity->processPost($formvalues); // debugMessage($entity->toArray());
		
		$entity->setdashbordconfig($formvalues['dashbordconfig']);
		$entity->save();
		
		$dashbordconfig = objectToArray(json_decode($formvalues['dashbordconfig'])); // debugMessage($dashbordconfig);
		if(count($dashbordconfig) > 0){
			$dash_collection = new Doctrine_Collection(Doctrine_Core::getTable("UserDashboard"));
			foreach ($dashbordconfig as $config){
				$dash = new UserDashboard();
				$dash->populate($config['id']);  //debugMessage($dash->toArray());
				if(!isEmptyString($dash->getID())){
					$dash->setgridconfig(json_encode($config));
					$dash_collection->add($dash);
				}
			}
			// debugMessage($dash_collection->toArray());
			if($dash_collection->count() > 0){
				try {
					$dash_collection->save();
					$result = array('result' => 'success', 'msg' => 'Saved'/* , 'obj' => $dash_collection->toArray() */);
				} catch (Exception $e) {
					$result = array('result' => 'fail', 'msg' => $e->getMessage());
				}
			} else {
				$result = array('result' => 'fail', 'msg' => 'No data detected');
			}
		} else {
			$result = array('result' => 'fail', 'msg' => 'No data detected');
		}
		
		echo json_encode($result);
	}

	function projectsnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function projectsclosedstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function indicatornewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function resultsforapprovalstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function datasetsnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function issuesnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function issuesclosedstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function risksnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function risksclosedstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function attributesnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function categorynewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function categoryoptionnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function categorygroupnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function directorynewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function documentnewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
	function usernewstatsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$result = array();
		// debugMessage($result);
		echo json_encode($result);
	}
}

?>