<?php

class ReportController extends SecureController   {
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * Return the Application Settings since we need to make the url more friendly
	 *
	 * @return String
	 */
	function getResourceForACL() {
		$action = strtolower($this->getRequest()->getActionName());
		if ($action == "index" || $action == "view" || $action == "test" || $action == "testdata" || $action == "mps" || $action == "mpsdata" || $action == "reportsearch" || $action == "results" || $action == "resultsdata" || $action == "savestate"   || $action == "project" || $action == "projectdata" || $action == "indicator" || $action == "indicatordata" || $action == "csindicator" || $action == "csindicatordata" || $action == "widgetdetails" || $action == "processwidget" || $action == "sharewidget" || $action == "processsharewidget") {
			return "Profile";
		}
		return 'Report';
	}
	/**
	 * Get the name of the resource being accessed 
	 *
	 * @return String 
	 */
	function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		if($action == "index" || $action == "test" || $action == "testdata" || $action == "mps" || $action == "mpsdata" || $action == "savestate" || $action == "recodata" || $action == "reportsearch" || $action == "results" || $action == "resultsdata" || $action == "project" || $action == "projectdata" || $action == "indicator" || $action == "indicatordata" || $action == "csindicator" || $action == "csindicatordata" || $action == "widgetdetails" || $action == "processwidget" || $action == "sharewidget" || $action == "processsharewidget") {
			return 'view';
		}
		
		$reportactions = getActionsResources('11');
		foreach($reportactions as $aid => $actiondetails){
			if($action == $actiondetails['slug']) {
				return $actiondetails['slug'];
			}
		}
		
		parent::getActionforACL();
	}
	
	public function init()    {
		parent::init();
	
		$current_timestamp = strtotime('now'); $now_iso = date('Y-m-d H:i:s', $current_timestamp); $this->view->now_iso = $now_iso; //debugMessage('now '.$now_iso.'-'.$current_timestamp);
		$onehourago_timestamp = strtotime('-1 hour'); $onehourago_iso = date('Y-m-d H:i:s', $onehourago_timestamp );
		$this->view->onehourago_iso = $onehourago_iso; $this->view->onehourago_timestamp = $onehourago_timestamp;// debugMessage('now '.$onehourago_iso.'-'.$onehourago_timestamp);
		$sixhourago_timestamp = strtotime('-6 hour'); $sixhourago_iso = date('Y-m-d H:i:s', $sixhourago_timestamp);
		$this->view->sixhourago_iso = $sixhourago_iso; $this->view->sixhourago_timestamp = $sixhourago_timestamp;
		$twelvehourago_timestamp = strtotime('-12 hour'); $twelvehourago_iso = date('Y-m-d H:i:s', $twelvehourago_timestamp);
		$this->view->twelvehourago_timestamp = $twelvehourago_timestamp; $this->view->twelvehourago_iso = $twelvehourago_iso;
	
		// debugMessage($logged_today_sql);
		$today_iso = date('Y-m-d'); $today = changeMySQLDateToPageFormat($today_iso);  $this->view->today_iso = $today_iso; //debugMessage('today '.$today_iso);
		$today_iso_short = date('M j', $current_timestamp);
	
		$yestday_iso = date('Y-m-d', strtotime('1 day ago')); $yestday = changeMySQLDateToPageFormat($yestday_iso); $this->view->yestday_iso = $yestday_iso; //debugMessage('yesterday '.$yestday_iso);
		$yestday_iso_short = date('M j', strtotime($yestday_iso));
		$weekday = date("N");
	
		// monday of week
		$mondaythisweek_iso = date('Y-m-d', strtotime('monday this week')); $mondaythisweek = changeMySQLDateToPageFormat($mondaythisweek_iso);
		if($weekday == 1){
			$mondaythisweek_iso = $today_iso;
			$mondaythisweek = $today;
		}
		if($weekday == 7){
			$mondaythisweek_iso = date('Y-m-d', strtotime('monday last week'));
			$mondaythisweek = changeMySQLDateToPageFormat($mondaythisweek_iso);
		}
		$this->view->mondaythisweek_iso = $mondaythisweek_iso; // debugMessage('monday this week '.$mondaythisweek_iso);
	
		// sunday of week
		$sundaythisweek_iso = date('Y-m-d', strtotime('sunday this week')); $sundaythisweek = changeMySQLDateToPageFormat($sundaythisweek_iso);
		if($weekday == 1){
			$sundaythisweek_iso = date('Y-m-d', strtotime('today + 6 days')); $sundaythisweek = changeMySQLDateToPageFormat($sundaythisweek_iso);
		}
		if($weekday == 7){
			$sundaythisweek_iso = $today_iso; $sundaythisweek = $today;
		}
		$this->view->sundaythisweek_iso = $sundaythisweek_iso; // debugMessage('sunday this week '.$sundaythisweek_iso);
	
		// monday last week
		$mondaylastweek_iso = date('Y-m-d', strtotime('-7 days', strtotime($mondaythisweek_iso))); // debugMessage('monday last week '.$mondaylastweek_iso);
		$this->view->mondaylastweek_iso = $mondaylastweek_iso;
		// sunday last week
		$sundaylastweek_iso = date('Y-m-d', strtotime('-7 days', strtotime($sundaythisweek_iso)));  // debugMessage('sunday last week '.$sundaylastweek_iso);
		$this->view->sundaylastweek_iso = $sundaylastweek_iso;
	
		// monday next week
		$mondaynextweek_iso = date('Y-m-d', strtotime('+7 days', strtotime($mondaythisweek_iso))); // debugMessage('monday last week '.$mondaylastweek_iso);
		$this->view->mondaynextweek_iso = $mondaynextweek_iso;
		// sunday next week
		$sundaynextweek_iso = date('Y-m-d', strtotime('+7 days', strtotime($sundaythisweek_iso)));  // debugMessage('sunday last week '.$sundaylastweek_iso);
		$this->view->sundaynextweek_iso = $sundaynextweek_iso;
	
		// firstday this month
		$firstdayofthismonth_iso = getFirstDayOfCurrentMonth(); //debugMessage('1st day this month '.$firstdayofthismonth_iso);
		$this->view->firstdayofthismonth_iso = $firstdayofthismonth_iso;
		// lastday this month
		$lastdayofthismonth_iso = getLastDayOfCurrentMonth(); //debugMessage('last day this month '.$lastdayofthismonth_iso);
		$this->view->lastdayofthismonth_iso = $lastdayofthismonth_iso;
	
		// firstday last month
		$firstdayoflastmonth_iso = getFirstDayOfMonth(date('m')-1, date('Y')); //debugMessage('1st day last month '.$firstdayoflastmonth_iso);
		$this->view->firstdayoflastmonth_iso = $firstdayoflastmonth_iso;
		// lastday last month
		$lastdayoflastmonth_iso = getLastDayOfMonth(date('m')-1, date('Y')); //debugMessage('last day last month '.$lastdayoflastmonth_iso);
		$this->view->lastdayoflastmonth_iso = $lastdayoflastmonth_iso;
	
		// firstday 2 month ago
		$firstdayof2monthago_iso = getFirstDayOfMonth(date('m')-2, date('Y')); //debugMessage('1st day 2 month ago '.$firstdayof2monthago_iso);
		$this->view->firstdayof2monthago_iso = $firstdayof2monthago_iso;
		// lastday 2 month ago
		$lastdayof2monthago_iso = getLastDayOfMonth(date('m')-2, date('Y')); //debugMessage('last day last month '.$lastdayof2monthago_iso);
		$this->view->lastdayof2monthago_iso = $lastdayof2monthago_iso;
	
		// firstday 3 month ago
		$firstdayof3monthago_iso = getFirstDayOfMonth(date('m')-3, date('Y')); //debugMessage('1st day 3 month ago '.$firstdayof3monthago_iso);
		$this->view->firstdayof3monthago_iso = $firstdayof3monthago_iso;
		// lastday 3 month ago
		$lastdayof3monthago_iso = getLastDayOfMonth(date('m')-3, date('Y')); //debugMessage('last day last month '.$lastdayof3monthago_iso);
		$this->view->lastdayof3monthago_iso = $lastdayof3monthago_iso;
	
		// firstday 4 month ago and lastday 4 month ago
		$firstdayof4monthago_iso = getFirstDayOfMonth(date('m')-4, date('Y')); $this->view->firstdayof4monthago_iso = $firstdayof4monthago_iso;
		$lastdayof4monthago_iso = getLastDayOfMonth(date('m')-4, date('Y')); $this->view->lastdayof4monthago_iso = $lastdayof4monthago_iso;
	
		// firstday 5 month ago and lastday 5 month ago
		$firstdayof5monthago_iso = getFirstDayOfMonth(date('m')-5, date('Y')); $this->view->firstdayof5monthago_iso = $firstdayof5monthago_iso;
		$lastdayof5monthago_iso = getLastDayOfMonth(date('m')-5, date('Y')); $this->view->lastdayof5monthago_iso = $lastdayof5monthago_iso;
		
		// firstday 6 month ago and lastday 6 month ago
		$firstdayof6monthago_iso = getFirstDayOfMonth(date('m')-6, date('Y')); $this->view->firstdayof6monthago_iso = $firstdayof6monthago_iso;
		$lastdayof6monthago_iso = getLastDayOfMonth(date('m')-6, date('Y')); $this->view->lastdayof6monthago_iso = $lastdayof6monthago_iso;
	
		// firstday this year
		$firstdayofyear_iso = getFirstDayOfMonth(1, date('Y')); //debugMessage('1st day this year '.$firstdayofyear_iso);
		$this->view->firstdayofyear_iso = $firstdayofyear_iso;
		// lastday this year
		$lastdayofyear_iso = getLastDayOfMonth(12, date('Y')); //debugMessage('last day this year '.$lastdayofyear_iso);
		$this->view->lastdayofyear_iso = $lastdayofyear_iso;
		// first day of month one year ago
		$startofmonth_oneyearago = getFirstDayOfMonth(date('m', strtotime('1 year ago')), date('Y', strtotime('1 year ago')));
		$this->view->startofmonth_oneyearago = $startofmonth_oneyearago;
	
		$firstsystemday_iso = '2013-01-01';
		$this->view->firstsystemday_iso = $firstsystemday_iso;
	}
	
	function smsAction(){}
	function widgetdetailsAction(){
		
	}
	function processwidgetAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		 
		$entity = new UserDashboard();
		$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
		 
		if(!isEmptyString($id)){
			$entity->populate($id);
			$formvalues['lastupdatedby'] = $session->getVar('userid');
			$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
		} else {
			$formvalues['createdby'] = $session->getVar('userid');
		}
		 
		$entity->processPost($formvalues); // exit;
		/* debugMessage($entity->toArray());
		debugMessage('errors are '.$entity->getErrorStackAsString()); exit(); */
		 
		if($entity->hasError()){
			$resultarray = array(
					'id' => '',
					'title' => '',
					'notes' => '',
					'type' => '',
					'result' => 'error',
					'message' => 'Error: '.$entity->getErrorStackAsString()
			);
		} else {
			try {
				$entity->save(); // debugMessage($payment->toArray());
				$resultarray = array(
					'id' => $entity->getID(),
					'title' => $entity->getTitle(),
					'notes' => $entity->getNotes(),
					'type' => $entity->getType(),
					'result' => 'success',
					'message' => 'Successfully saved'
				);
			} catch (Exception $e) {
				$resultarray = array(
					'id' => '',
					'title' => '',
					'notes' => '',
					'type' => '',
					'result' => 'error',
					'message' => 'Error: '.$e->getMessage()
				);
			}
		}
		 
		echo json_encode($resultarray);
	}
	function sharewidgetAction(){}
	function processsharewidgetAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit;
			
		$entity = new UserDashboard();
		$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
			
		if(!isEmptyString($id)){
			$entity->populate($id);
			$formvalues['lastupdatedby'] = $session->getVar('userid');
			$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
		} else {
			$formvalues['createdby'] = $session->getVar('userid');
		}
			
		$entity->processPost($formvalues); // exit;
		/* debugMessage($entity->toArray());
		debugMessage('errors are '.$entity->getErrorStackAsString()); exit(); */
			
		if($entity->hasError()){
			$resultarray = array(
					'id' => '',
					'title' => '',
					'notes' => '',
					'shareids' => '',
					'type' => '',
					'result' => 'error',
					'message' => 'Error: '.$entity->getErrorStackAsString()
			);
		} else {
			try {
				$entity->save(); // debugMessage($payment->toArray());
				$resultarray = array(
						'id' => $entity->getID(),
						'title' => $entity->getTitle(),
						'notes' => $entity->getNotes(),
						'type' => $entity->getType(),
						'shareids' => $entity->getShareIDs(),
						'result' => 'success',
						'message' => 'Successfully saved'
				);
			} catch (Exception $e) {
				$resultarray = array(
						'id' => '',
						'title' => '',
						'notes' => '',
						'shareids' => '',
						'type' => '',
						'result' => 'error',
						'message' => 'Error: '.$e->getMessage()
				);
			}
		}
		echo json_encode($resultarray);
	}
	
	function audittrailAction(){
	
	}
	function savestateAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit;
		$session = SessionWrapper::getInstance();
		
		$data = array(
			'userid' => getUserID(),
			'createdby' => getUserID()
		);
		
		$url = '';
		if(!isEmptyString($this->_getParam('urlconfig'))){
			$url = decode($this->_getParam('urlconfig'));
			$data['urlconfig'] = $url;
			$session->setVar('urlconfig', $url);
		}
		$config = '';
		if(!isEmptyString($this->_getParam('serialconfig'))){
			$config = stripMultipleSpaces($this->_getParam('serialconfig')); // debugMessage($config); exit;
			$data['serialconfig'] = decode($config);
			$session->setVar('serialconfig', $config);
		}
		if(!isArrayKeyAnEmptyString('datasetid', $formvalues)){
			$data['attrconfig'] = array('datasetid' => arrayToCommaString($formvalues['datasetid']));
			if(!isArrayKeyAnEmptyString('attrid', $formvalues)){
				$data['attrconfig'] = json_encode(array('datasetid' => arrayToCommaString($formvalues['datasetid']), 'attrid' => arrayToCommaString($formvalues['attrid'])));
			}
		}
		if(!isEmptyString($this->_getParam('type'))){
			$data['type'] = $this->_getParam('type');
		}
		// debugMessage($data); exit;
		$dash = new UserDashboard();
		$dash->processPost($data); // debugMessage($dash->toArray()); exit;
		 
		if($dash->hasError()){
			// debugMessage('Err '.$dash->getErrorStackAsString());
			$result =array('result'=>'fail', 'message' => $dash->getErrorStackAsString());
		} else {
			try {
				$dash->save();
				$result = array('result'=>'success', 'id'=>$dash->getID(), 'message' => 'Successfully pinned to dashboard');
				$session->setVar('serialurl', $url);
				$session->setVar('serialconfig', $config);
			} catch (Exception $e) {
				// debugMessage('Exception: '.$e->getMessage());
				$result =array('result'=>'fail', 'message' => $e->getMessage());
			}
		}
		
		echo json_encode($result);
	}
	function testAction(){
	
	}
	function testdataAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		// header('Content-Type: text/plain');
		
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
		header('Access-Control-Allow-Headers', 'Content-Type');
		header('Content-Type: text/plain');
		
		$server = "localhost";
		$username = "dev";
		$password = "dev";
		$dbname = "infotd";
		mysql_connect($server, $username, $password);
		mysql_select_db($dbname);
		$query = "SELECT t.* from testdata t where t.id between 0 AND 100000 order by t.datecollected"; // debugMessage($query); exit;
		$query = "select 
p.name as Market,
c.name as Commodity,
d.retailprice as `RP`, 
d.wholesaleprice as `WP`,
d.datecollected as `DateCollected`,
concat('Week ', WEEK(d.datecollected)) as `Week`
from price_details d 
INNER JOIN price_submission s on d.submissionid = s.id
inner join pricesource p on d.sourceid = p.id 
inner join commodity c on d.commodityid = c.id
where TO_DAYS(d.datecollected) > TO_DAYS('2010-01-01') AND d.pricecategoryid = 2
order by d.datecollected limit 300000 ";
		$result = mysql_query($query);
		
		require_once("flexmonster-compressor.php");
		Compressor::compressMySql($result);
		
		mysql_free_result($result);
	}
	function resultsAction(){}
	function resultsdataAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		header('Content-Type: text/plain');
		
		$conn = Doctrine_Manager::connection(); 
		$custom_query = ""; $datasets_array = array(); $attr_array = array(); $attr_cats = array();
		$datasets = commaStringToArray($this->_getParam('datasetid'));
		if(!isEmptyString($this->_getParam('datasetid'))){
			$custom_query .= " AND d.datasetid IN(".$this->_getParam('datasetid').") ";
		}
		$attr_array = commaStringToArray($this->_getParam('attrid'));
		if(!isEmptyString($this->_getParam('attrid'))){
			$custom_query .= " AND d.elementid IN(".$this->_getParam('attrid').") ";
			foreach ($attr_array as $attid) {
				$attb = new Attribute();
				$attb->populate($attid); // debugMessage($attb->toArray());
				$attr_cats[$attid]['options'] = $attb->getCategoryOptionList();  
			}
			// debugMessage($attr_cats);
		}
		$query = "SELECT
		t.`name` as Dataset,
		concat(a.refno, ': ', a.name) as `Attribute`,
		GROUP_CONCAT(`p`.`name` ORDER BY FIND_IN_SET(p.id, d.optionids) SEPARATOR ' -> ') AS `Category`,
		d.result as `Result`,
(CASE 
		WHEN t.periodtype = '4' OR t.periodtype = '5' OR t.periodtype = '6' OR t.periodtype = '7' OR t.periodtype = '8' 
			THEN CONCAT(DATE_FORMAT(CONCAT(LEFT(s.period, 7), '-01'), '%b'), ' - ', DATE_FORMAT(CONCAT(RIGHT(s.period, 7), '-01'), '%b %Y'))
		WHEN t.periodtype = '9' THEN s.period
			ELSE DATE_FORMAT(s.period, '%Y-%m-%d')
		END) AS `Period`,
		m.name as `Milestone`,
		LEFT(s.period, 4) as `Year`,
		(CASE 
		WHEN t.periodtype = '4' OR t.periodtype = '5' OR t.periodtype = '6' OR t.periodtype = '7' OR t.periodtype = '8' 
			THEN CONCAT(LEFT(s.period, 7), '-01')
		WHEN t.periodtype = '9' THEN CONCAT(LEFT(s.period, 4), '-01-01')
			ELSE DATE_FORMAT(s.period, '%Y-%m-%d')
END) AS `Period Start`,
		d.optionids	as optionids,
		d.id,
		d.datasetid as datasetid,
		d.elementid as attributeid,
		t.periodtype as periodtype,
		a.categorytype,
		a.category,
		a.categorygroup,
		a.valuetype,
		a.optiontype as optiontype,	
		a.optionid as optionid,
		a.systemattr as systemattr,
		a.systemattr_values as systemattr_values,
		t.groupattr,
		s.groupattr_val,
		g.name as g_name,
		g.optiontype as g_optiontype,	
		g.optionid as g_optionid,
		g.systemattr as g_systemattr,
		g.systemattr_values as g_systemattr_values,
		a.refno as `Attr_Refno`,
		s.period as periodinfo,
		CONCAT_WS('*', d.datasetid, s.period, s.groupattr_val) as periodkey
		from data_set_result_detail d 
		INNER JOIN data_set_result_summary s on d.resultid = s.id
		INNER JOIN data_set t on d.datasetid = t.id
		INNER JOIN data_attribute a on d.elementid = a.id
		LEFT JOIN content_milestone m on s.milestoneid = m.id
		LEFT JOIN data_category_option p on find_in_set(p.id, d.optionids) > 0
		LEFT JOIN data_attribute g on t.groupattr = g.id
		where d.id <> ''  ".$custom_query."
		group by d.id
		order by s.period desc"; // debugMessage($query);
		$result = $conn->fetchAll($query); //debugMessage($result);
		
		$csv = "";
		$reportdata = array(); $unique_dst_data = array(); $unique_attr_data = array(); $attr_grp_type = array(); $category_results = array();
		foreach($result as $key => $line){
			$reportdata[$key]["Dataset"] = $line['Dataset'];
			if(count($datasets) <= 1){
				unset($reportdata[$key]['Dataset']);
			}
			
			$reportdata[$key]["Attribute"] = $line['Attribute'];
			$reportdata[$key]["Category"] = isEmptyString($line['Category']) ? 'N/A' : $line['Category'];
			$reportdata[$key]["Period"] = $line['Period'];
			$reportdata[$key]["Milestone"] = $line['Milestone'];
			$reportdata[$key]["Year"] = $line['Year'];
			if(!isEmptyString($line["groupattr"]) && !isEmptyString($line['groupattr_val'])){
				if(isArrayKeyAnEmptyString($line["datasetid"], $unique_dst_data)){
					$options = getConfigOptionList($line['g_optiontype'], $line['g_optionid'], $line['g_systemattr'], $line['g_systemattr_values']); // debugMessage($options);
					$unique_dst_data[$line["datasetid"]] = $options;
				}
			
				$grpk = isArrayKeyAnEmptyString($line['groupattr_val'], $unique_dst_data[$line['datasetid']]) ? '' : $unique_dst_data[$line['datasetid']][$line['groupattr_val']]; // debugMessage($grpk);
				// debugMessage($line['g_name']);
				$reportdata[$key][$line['g_name']] = $unique_dst_data[$line['datasetid']][$line['groupattr_val']];
			}
			$attr_value = $line['Result'];
			
			if($line['valuetype'] == C_P){ // percent field
				$attr_value = formatNum($line['Result']);
			}
			if($line['valuetype'] == C_C){ //  gps coordinates
				$attr_value_html = ''; $attr_value_lat = ''; $attr_value_lng = '';
				$listarray = explode(',', $line['Result']);
				if(!isArrayKeyAnEmptyString('0', $listarray)){
					$attr_value .= 'Lat='.$listarray[0];
				}
				if(!isArrayKeyAnEmptyString('1', $listarray)){
					$attr_value .= ', Lng='.$listarray[1];
				}
			}
			if($line['valuetype'] == C_YN){ // Yes no
				if($line['Result'] == 1){
					$attr_value = 'Yes';
				} else {
					$attr_value = 'No';
				}
			}
			if($line['valuetype'] == C_Y){ // Yes only
				if($line['Result'] == 1){
					$attr_value = '<i class="fa fa-check"></i>';
				} else {
					$attr_value = '<i class="fa fa-remove"></i>';
				}
			}
			if($line['valuetype'] == C_D){ //  date only
				$attr_value = changeMySQLDateToPageFormat($line['Result']);
			}
			if($line['valuetype'] == C_TM){ //  time only
				$attr_value = formatTimeCustom($line['Result']);
			}
			if($line['valuetype'] == C_DT){ // date time
				$attr_value = formatDateAndTime($line['Result'], false);
			}
			if($line['valuetype'] == C_LS){ // single choice select
				if(isArrayKeyAnEmptyString($line['attributeid'], $unique_attr_data)){
					$optionlist = getConfigOptionList($line['optiontype'], $line['optionid'], $line['systemattr'], $line['systemattr_values']); // debugMessage($options);
					$unique_attr_data[$line['attributeid']] = $optionlist;
				}
				$optionvalue = '';
				if(!isArrayKeyAnEmptyString($line['attributeid'], $unique_attr_data)){
					$optionvalue = isArrayKeyAnEmptyString($line['Result'], $unique_attr_data[$line['attributeid']]) ? '' : $unique_attr_data[$line['attributeid']][$line['Result']];
				}
				$attr_value = $optionvalue;
			}
			if($line['valuetype'] == C_LM){ // multiple choice select
				$listarray = explode(',', $line['Result']);
				$list_options = array();
				if(isArrayKeyAnEmptyString($line['attributeid'], $unique_attr_data)){
					$optionlist = getConfigOptionList($line['optiontype'], $line['optionid'], $line['systemattr'], $line['systemattr_values']); // debugMessage($options);
					$unique_attr_data[$line['attributeid']] = $optionlist;
				}
				$optionvalue = '';
				if(!isArrayKeyAnEmptyString($line['attributeid'], $unique_attr_data)){
					$optionlist = $unique_attr_data[$line['attributeid']];
					foreach($listarray as $anoption){
						$list_options[] = isArrayKeyAnEmptyString($anoption, $optionlist) ? '-' : $optionlist[$anoption];
					}
					$attr_value = createHTMLCommaListFromArray(array_remove_empty($list_options), ', ');
				}
			}
			$reportdata[$key]["Result"] = $attr_value;
			// $csv .= escapedimplode(",", $reportdata[$key])."\r\n";			
		}
		// debugMessage($reportdata); exit;
		//$csv = escapedimplode(",", array_keys(current($reportdata)))."\r\n".$csv;
		//$csv = "___ocsv2___2.211/2.211\r\n".$csv; 
		//echo $csv;
		echo json_encode(array_values($reportdata));
	}
	function projectAction(){}
	function projectdataAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$conn = Doctrine_Manager::connection(); 
		$custom_query = ""; $datasets_array = array(); $attr_array = array(); $attr_cats = array();
		$projects = commaStringToArray($this->_getParam('projectid'));
		if(!isEmptyString($this->_getParam('projectid'))){
			$custom_query .= " AND q.id IN(".$this->_getParam('projectid').") ";
		}
		
		$query = "SELECT
		q.`title` as Project, 
		q.projectno as `Project No#`, 
		q.acctno as `FAD No#`, 
		IF(q.type = '1', 'Grant', 'Sponsorship') as `Type`, 
		lc.lookupvaluedescription as `Category`, 
		ls.lookupvaluedescription as `Status`, 
		lp.lookupvaluedescription as `Phase`,
		q.startdate as `Start Date`, 
		q.enddate as `End Date`,
		q.cost as `Budget`, 
		GROUP_CONCAT(g.name separator ', ') as Program, 
		GROUP_CONCAT(u.name separator ', ') as `Program Unit` 
		from project q 
		LEFT JOIN lookuptypevalue lc on q.category = lc.lookuptypevalue AND lc.lookuptypeid = 5 
		LEFT JOIN lookuptypevalue ls on q.`status` = ls.lookuptypevalue AND ls.lookuptypeid = 7 
		LEFT JOIN lookuptypevalue lp on q.programid = lp.lookuptypevalue AND lp.lookuptypeid = 19 
		LEFT JOIN program g on FIND_IN_SET(g.id, q.programid) > 0 
		LEFT JOIN program_unit u on FIND_IN_SET(u.id, q.program_units) > 0 
		LEFT JOIN location l on FIND_IN_SET(l.id, q.program_locations) > 0 
		
		where q.id <> ''  ".$custom_query."
		group by q.id
		order by q.startdate desc"; // debugMessage($query); exit;
		$result = $conn->fetchAll($query); //debugMessage($result);
		
		$reportdata = array();
		foreach($result as $key => $line){
			$reportdata[$key] = $line;
		}
		// debugMessage($reportdata);
		echo json_encode(array_values($reportdata));
	
	}
	function indicatorAction(){}
	function indicatordataAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$conn = Doctrine_Manager::connection(); 
		$custom_query = ""; 
		$projects = commaStringToArray($this->_getParam('indicatorid'));
		if(!isEmptyString($this->_getParam('indicatorid'))){
			$custom_query .= " AND q.id IN(".$this->_getParam('indicatorid').") ";
		}
		
		$query = "SELECT q.id from indicator q 
		where q.id <> ''  ".$custom_query."
		group by q.id
		order by q.id asc"; // debugMessage($query); 
		$result = $conn->fetchAll($query); // debugMessage($result);
		// exit;
		$reportdata = array();
		foreach($result as $line){
			// debugMessage($line['id']);
			$indicatorinfo = getIndicatorProgressDetails($line['id']);
		}
		// debugMessage($reportdata);
		// echo json_encode(array_values($reportdata));
	
	}
	
	function csindicatorAction(){
	}
	function csindicatordataAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$conn = Doctrine_Manager::connection();
		$custom_query = "";
		$projects = commaStringToArray($this->_getParam('indicatorid'));
		if(!isEmptyString($this->_getParam('indicatorid'))){
			$custom_query .= " AND q.id IN(".$this->_getParam('indicatorid').") ";
		}
	
		$query = "SELECT q.id from indicator q
		where q.id <> ''  ".$custom_query."
		group by q.id
		order by q.id asc"; // debugMessage($query);
		$result = $conn->fetchAll($query); // debugMessage($result);
		// exit;
		$reportdata = array();
		foreach($result as $line){
			// debugMessage($line['id']);
			$indicatorinfo = getIndicatorProgressDetails($line['id']);
		}
		// debugMessage($reportdata);
		// echo json_encode(array_values($reportdata));
	
	}
    public function reportsearchAction() {
    	// debugMessage($this->getRequest()->getQuery());
    	// debugMessage($this->_getAllParams());
    	$action = $this->_getParam('page');
    	if(!isEmptyString($this->_getParam('pageaction'))){
    		$action = $this->_getParam('pageaction');
    	}
    	//exit();
    	if(!isEmptyString($action)){
    		$this->_helper->redirector->gotoSimple($action, $this->getRequest()->getControllerName(), $this->getRequest()->getModuleName(), array_remove_empty(array_merge_maintain_keys($this->_getAllParams(), $this->getRequest()->getQuery())));
    	}
    }
}

