 <?php

class DataentryController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Data Entry";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	if($action == "view" || $action == 'listsearch' || $action == 'listsubmit' || $action == 'listsubmit' || $action == 'togglestatus' || $action == "updatestatus" || $action == "processresults" || $action == "processcost"){
	 		return "list";
	 	}
	 	
		return parent::getActionforACL();
    }
    
    function togglestatusAction() {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$session = SessionWrapper::getInstance();
    	$this->_translate = Zend_Registry::get("translate");
    
    	$formvalues['id'] = $id = $formvalues['id'];
    
    	$result = new DatasetResult();
    	$result->populate($id);
    	
    	$result->setStatus($this->_getParam('status'));
    	if($this->_getParam('status') == 2){
    		$result->setdatesubmitted(NULL);
    		$result->setdateapproved(NULL);
    		$result->setapprovedbyid(NULL);
    		if($result->getDataset()->isMultiStep()){
    			$result->setapprovedbyid1(NULL);
    			$result->setapprovedbyid2(NULL);
    			$result->setapprovedbyid3(NULL);
    			$result->setdateapproved1(NULL);
    			$result->setdateapproved2(NULL);
    			$result->setdateapproved3(NULL);
    			$result->setreason1(NULL);
    			$result->setreason2(NULL);
    			$result->setreason3(NULL);
    			$result->setstatus1(NULL);
    			$result->setstatus2(NULL);
    			$result->setstatus3(NULL);
    		}
    	}
    	if($this->_getParam('status') == 2){
    		$result->setdatesubmitted(DEFAULT_DATETIME);
    	}
    	
    	try {
    		$result->save();
    		$session->setVar(SUCCESS_MESSAGE, $this->_translate->translate($formvalues[SUCCESS_MESSAGE]));
    	} catch (Exception $e) {
    		// debugMessage('error '.$e->getMessage());
    		$session->setVar(ERROR_MESSAGE, $e->getMessage());
    	}
    	// exit();
    	$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
    }
    
    function updatestatusAction() {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$session = SessionWrapper::getInstance();
    	$this->_translate = Zend_Registry::get("translate");
    
    	$formvalues['id'] = $id = $formvalues['id'];
    	$formvalues['dateapproved'] = date("Y-m-d H:i:s", strtotime('now'));
    	$formvalues['approvedbyid'] = $userid = $session->getVar('userid');
    	// debugMessage($formvalues); // exit();
    
    	$stepuser = 1;
    
    	$result = new DatasetResult();
    	$result->populate($id);
    
    	if($result->getDataset()->isSingleStep()){
    		$result->setStatus($formvalues['status']);
    		$result->setDateApproved(DEFAULT_DATETIME);
    		$result->setApprovedByID($session->getVar('userid'));
    		if(!isArrayKeyAnEmptyString('reason', $formvalues)){
    			$result->setReason("<br/>Rejected with remarks: ".$formvalues['reason']);
    		}
    	}
    	if($result->getDataset()->isMultiStep()){
    		if(($result->getDataset()->getFirstApprover() == $userid || isAdmin()) && isEmptyString($result->getCurrentStep())){
    			$result->setStatus1($formvalues['status']);
    			$result->setDateApproved1(DEFAULT_DATETIME);
    			$result->setApprovedByID1($userid);
    			if(!isArrayKeyAnEmptyString('reason', $formvalues)){
    				$result->setReason1("<br/>Rejected with remarks: ".$formvalues['reason']);
    			}
    			$stepuser = 1;
    		} elseif(($result->getDataset()->getSecondApprover() == $userid || isAdmin()) && $result->getCurrentStep() == 1) {
    			$result->setStatus2($formvalues['status']);
    			$result->setDateApproved2(DEFAULT_DATETIME);
    			$result->setApprovedByID2($userid);
    			if(!isArrayKeyAnEmptyString('reason', $formvalues)){
    				$result->setReason2("<br/>Rejected with remarks: ".$formvalues['reason']);
    			}
    			$stepuser = 2;
    
    		} elseif(($result->getDataset()->getThirdApprover() == $userid || isAdmin()) && $result->getCurrentStep() == 2) {
    			$result->setStatus3($formvalues['status']);
    			$result->setDateApproved3(DEFAULT_DATETIME);
    			$result->setApprovedByID3($userid);
    			if(!isArrayKeyAnEmptyString('reason', $formvalues)){
    				$result->setReason3("<br/>Rejected with remarks: ".$formvalues['reason']);
    			}
    			$stepuser = 3;
    		}
    			
    		// check if approving last step
    		if($result->getDataset()->getApprovalSteps() == 2){
    			if(!isEmptyString($result->getStatus1()) && !isEmptyString($result->getStatus2())){
    				$result->setStatus($formvalues['status']);
    				$result->setDateApproved(DEFAULT_DATETIME);
    				$result->setApprovedByID($session->getVar('userid'));
    				if(!isArrayKeyAnEmptyString('reason', $formvalues)){
    					$result->setComments("<br/>Rejected with remarks: ".$formvalues['reason']);
    				}
    			}
    		}
    		if($result->getDataset()->getApprovalSteps() == 3){
    			if(!isEmptyString($result->getStatus1()) && !isEmptyString($result->getStatus2()) && !isEmptyString($result->getStatus3())){
    				$result->setStatus($formvalues['status']);
    				$result->setDateApproved(DEFAULT_DATETIME);
    				$result->setApprovedByID($session->getVar('userid'));
    				if(!isArrayKeyAnEmptyString('reason', $formvalues)){
    					$result->setComments("<br/>Rejected with remarks: ".$formvalues['reason']);
    				}
    			}
    		}
    		$result->setCurrentStep($stepuser);
    	}
    	/*
    		debugMessage($result->toArray());
    	exit; */
    	try {
    		$result->save();
    		$result->afterApprove($formvalues['status'], $stepuser);
    		$session->setVar(SUCCESS_MESSAGE, $this->_translate->translate($formvalues[SUCCESS_MESSAGE]));
    	} catch (Exception $e) {
    		// debugMessage('error '.$e->getMessage());
    		$session->setVar(ERROR_MESSAGE, $e->getMessage());
    	}
    	// exit();
    	$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
    }
    
    function processcostAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$conn = Doctrine_Manager::connection();
    
    	$formvalues = $this->_getAllParams();
    	// debugMessage($formvalues); // exit();
    	$successurl = stripURL(decode($this->_getParam(URL_SUCCESS))); // debugMessage($successurl);
    
    	$entity = new DatasetResult();
    	$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
    
    	if(!isEmptyString($id)){
    		$entity->populate($id);
    		$formvalues['id'] = $id;
    		$formvalues['lastupdatedby'] = getUserID();
    		$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
    	} else {
    		$formvalues['datecreated'] = DEFAULT_DATETIME;
    		$formvalues['createdby'] = getUserID();
    		$formvalues['submittedbyid'] = getUserID();
    	}
    	
    	$x = 1;
    	$str = ''; $str_array = array(); $data = array();
    	if(!isArrayKeyAnEmptyString('details', $formvalues)){
    		foreach ($formvalues['details'] as $projectid => $lineinfo) {
    			// debugMessage($lineinfo);
    			for($i = 1; $i <= $lineinfo['noofoutlines']; $i++){
    				$data[$x]['projectid'] = $lineinfo['projectid'];
    				$data[$x]['datasetid'] = $formvalues['datasetid'];
    				if($lineinfo['noofoutlines'] == 1){
    					$data[$x]['poid'] = $lineinfo['poid'];
    					$data[$x]['annualbudget'] = $lineinfo['annualbudget'];
    					$data[$x]['periodbudget'] = $lineinfo['periodbudget'];
    					$data[$x]['cost_1'] = $lineinfo['cost_1'];
    					$data[$x]['cost_2'] = $lineinfo['cost_2'];
    					$data[$x]['cost_3'] = $lineinfo['cost_3'];
    				} else {
    					$data[$x]['poid'] = $lineinfo['poid_'.$i];
    					$data[$x]['annualbudget'] = $lineinfo['annualbudget_'.$i];
    					$data[$x]['periodbudget'] = $lineinfo['periodbudget_'.$i];
    					$data[$x]['cost_1'] = $lineinfo['cost_1_'.$i];
    					$data[$x]['cost_2'] = $lineinfo['cost_2_'.$i];
    					$data[$x]['cost_3'] = $lineinfo['cost_3_'.$i];
    				}
    				$x++;
    			}
    		}
    	}
    	// debugMessage($data);
    	if(count($data) > 0){
    		$formvalues['costdetails'] = $data;
    		unset($formvalues['details']);
    	}
    	// exit;
    	$entity->processPost($formvalues);
    	/* debugMessage($entity->toArray());
    	debugMessage('errors are '.$entity->getErrorStackAsString());
    	exit; */
    	
    	if($entity->hasError()){
    		$session->setVar('errorcontent', $data);
    		$session->setVar(ERROR_MESSAGE, $entity->getErrorStackAsString());
    		// debugMessage('err '.$entity->getErrorStackAsString());
    		$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)));
    	} else {
    		try {
    			if($this->_getParam('status') == 1){
    				$entity->setdatesubmitted(NULL);
    				$entity->setdateapproved(NULL);
    				$entity->setapprovedbyid(NULL);
    			}
    			if($this->_getParam('status') == 2){
    				$entity->setdatesubmitted(NULL);
    				$entity->setdateapproved(NULL);
    				$entity->setapprovedbyid(NULL);
    				if($entity->getDataset()->isMultiStep()){
    					$entity->setapprovedbyid1(NULL);
    					$entity->setapprovedbyid2(NULL);
    					$entity->setapprovedbyid3(NULL);
    					$entity->setdateapproved1(NULL);
    					$entity->setdateapproved2(NULL);
    					$entity->setdateapproved3(NULL);
    					$entity->setreason1(NULL);
    					$entity->setreason2(NULL);
    					$entity->setreason3(NULL);
    					$entity->setstatus1(NULL);
    					$entity->setstatus2(NULL);
    					$entity->setstatus3(NULL);
    				}
    			}
    			if($this->_getParam('status') == 3 && $this->_getParam('status_old') != 3){
    				$entity->setdatesubmitted(DEFAULT_DATETIME);
    				$entity->setdateapproved(DEFAULT_DATETIME);
    				$entity->setapprovedbyid(isEmptyString($session->getVar('userid')) ? DEFAULT_ID : $session->getVar('userid'));
    			}
    			$entity->save(); // debugMessage($payment->toArray());
    			$entity->afterSave();
    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
    			$successurl .= '/'.encode($entity->getID());
    		} catch (Exception $e) {
    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
    			$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)));
    		}
    	}
    	// exit;
    	// debugMessage($successurl); exit();
    	$this->_helper->redirector->gotoUrl($successurl);
    	
    }
    function processresultsAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$conn = Doctrine_Manager::connection();
    	 
    	$formvalues = $this->_getAllParams();
    	// debugMessage($formvalues); // exit();
    	$successurl = stripURL(decode($this->_getParam(URL_SUCCESS))); // debugMessage($successurl);
    
    	$entity = new DatasetResult();
    	$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
    
    	if(!isEmptyString($id)){
    		$entity->populate($id);
    		$formvalues['id'] = $id;
    		$formvalues['lastupdatedby'] = $session->getVar('userid');
    		$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
    	} else {
    		$formvalues['datecreated'] = DEFAULT_DATETIME;
    		$formvalues['createdby'] = $session->getVar('userid');
    	}
    	
    	$i = 1;
    	$str = ''; $str_array = array(); $data = array(); $batchattributes = array();
    	if(!isArrayKeyAnEmptyString('attribute', $formvalues)){
    		foreach ($formvalues['attribute'] as $attrid => $lineattribute) {
    			$hasbatch = false;
    			if(is_array($lineattribute) && !isArrayKeyAnEmptyString('1', $lineattribute)){
    				$hasbatch = true;
    				foreach ($lineattribute as $k => $linedata){
    					$batchattributes[$attrid.'*'.$k] = $linedata;
    				}
    				continue;
    			}
    			$key = $i;
    			$datafield = 'value';
    			$vtype = $lineattribute['type'];
    			if($vtype == C_C){
    				$datafield = 'value_lat';
    				$datafield2 = 'value_lng';
    			}
    			if($vtype == C_DT){ // datetime
    				$datafield = 'value_date';
    				$datafield2 = 'value_time';
    			}
    			if($vtype == C_LS){
    				$datafield = 'value_old';
    			}
    			if($vtype == C_LM){ // datetime
    				$datafield = 'value_old';
    			}
    			// debugMessage($lineattribute);
    			if(!isArrayKeyAnEmptyString($datafield, $lineattribute)){
    				if(!is_array($lineattribute[$datafield])){ // debugMessage($attrid.' ~ '.$lineattribute[$datafield]);
    					$data[$key]['datasetid'] = $this->_getParam('datasetid');
    					$data[$key]['elementid'] = $attrid;
    					$data[$key]['type'] = $vtype;
    					$data[$key]['optionids'] = NULL;
    					$data[$key]['result'] = $lineattribute[$datafield];
    						
    					if($vtype == C_C){ // cordinates
    						$data[$key]['result'] = $lineattribute[$datafield];
    						if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    							$data[$key]['result'] = $lineattribute[$datafield].','.$lineattribute[$datafield2];
    						}
    					}
    					if($vtype == C_D){ // date
    						$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield]));
    					}
    					if($vtype == C_TM){ // time
    						$data[$key]['result'] = date('H:i:s', strtotime($lineattribute[$datafield]));
    					}
    					if($vtype == C_DT){ // datetime
    						$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield]));
    						if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    							$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield])).' '.date('H:i:s', strtotime($lineattribute[$datafield2]));
    						}
    					}
    					if($vtype == C_Y || $vtype == C_YN){
    						if(isArrayKeyAnEmptyString($datafield, $lineattribute)){
    							$data[$key]['result'] = 0;
    						}
    					}
    					if($vtype != C_LS && $vtype != C_LM){
    						$i++;
    					}
    				} else {
    					// debugMessage($lineattribute);
    					foreach ($lineattribute[$datafield] as $akey => $option1){
    						// debugMessage($akey);
    						if(!is_array($option1)){
    							// debugMessage($vtype.' ~ '.$akey.' ~ '.$option1);
    							$key = $i;
    							$data[$key]['datasetid'] = $this->_getParam('datasetid');
    							$data[$key]['elementid'] = $attrid;
    							$data[$key]['type'] = $vtype;
    							$data[$key]['optionids'] = $akey;
    							$data[$key]['result'] = $option1;
    								
    							if($vtype == C_C){ // cordinates
    								$data[$key]['result'] = $lineattribute[$datafield][$akey];
    								if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    									$data[$key]['result'] = $lineattribute[$datafield][$akey].','.$lineattribute[$datafield2][$akey];
    								}
    							}
    							if($vtype == C_D){ // date
    								$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    							}
    							if($vtype == C_TM){ // time
    								$data[$key]['result'] = date('H:i:s', strtotime($lineattribute[$datafield][$akey]));
    							}
    							if($vtype == C_DT){ // datetime
    								$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    								if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    									$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey])).' '.date('H:i:s', strtotime($lineattribute[$datafield2][$akey]));
    								}
    							}
    							if($vtype == C_Y || $vtype == C_YN){
    								if(isArrayKeyAnEmptyString($akey, $lineattribute[$datafield])){
    									$data[$key]['result'] = 0;
    								}
    							}
    							$i++;
    						} else {
    							foreach ($option1 as $bkey => $option2){
    								
    								if($bkey == $datafield){
    									foreach ($option2 as $ckey => $option3){
    										
    										if(!is_array($option3)){
    											$key = $i;
    											$data[$key]['datasetid'] = $this->_getParam('datasetid');
    											$data[$key]['elementid'] = $attrid;
    											$data[$key]['type'] = $vtype;
    											$data[$key]['optionids'] = $akey.','.$ckey;
    											$data[$key]['result'] = $option3;
    												
    											if($vtype == C_C){ // cordinates
    											 	$data[$key]['result'] = $lineattribute[$datafield][$akey];
	    											if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
	    												$data[$key]['result'] = $lineattribute[$datafield][$akey].','.$lineattribute[$datafield2][$akey];
	    											}
    											}
    												
    											if($vtype == C_D){ // date
    												$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    											}
    											if($vtype == C_TM){ // time
    												$data[$key]['result'] = date('H:i:s', strtotime($lineattribute[$datafield][$akey]));
    											}
    											if($vtype == C_DT){ // datetime
    												$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
	    											if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
	    												$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey])).' '.date('H:i:s', strtotime($lineattribute[$datafield2][$akey]));
	    											}
    											}
    											if($vtype == C_Y || $vtype == C_YN){
	    											if(isArrayKeyAnEmptyString($akey, $lineattribute[$datafield])){
	    												$data[$key]['result'] = 0;
	    											}
    											}
    											$i++;
    										} else {
    											// if more than 3 levels
    											if(!isArrayKeyAnEmptyString($datafield, $option3)){
    												if(!is_array($option3[$datafield])){
    													// debugMessage($option3);
    												} else {
    													foreach ($option3[$datafield] as $dkey => $option4){
    														// debugMessage($akey.' ~ '.$ckey.' ~ '.$dkey.' ~ '.$option4);
    														if(!is_array($option4) && !isEmptyString($option4)){
    															$key = $i;
    															$data[$key]['datasetid'] = $this->_getParam('datasetid');
    															$data[$key]['elementid'] = $attrid;
    															$data[$key]['type'] = $vtype;
    															$data[$key]['optionids'] = $akey.','.$ckey.','.$dkey;
    															$data[$key]['result'] = $option4;
    															
    															// if not input field
    															$i++;
    														}
    													}
    												}
    											} 
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    			} 
    			 
    			// loop through all formvalues (not arrays) looking for select options
    			foreach ($formvalues as $k => $value){
    				if(!is_array($value)){
    					if(stringContains('attribute_'.$attrid, $k)){
    						if(!isArrayKeyAnEmptyString($k, $formvalues)/*  && $vtype == C_LS */){
    							$keyarray = explode('_', $k); $keydata = array();
    							if(!isArrayKeyAnEmptyString('0', $keyarray)){
    								unset($keyarray[0]);
    							}
    							if(!isArrayKeyAnEmptyString('1', $keyarray)){
    								unset($keyarray[1]);
    							}
    							if(count($keyarray) > 0){
    								$lastkey = end(array_keys($keyarray)); // debugMessage(array_keys($keyarray));
    								if($keyarray[$lastkey] == 'value'){
    									unset($keyarray[$lastkey]);
    								}
    								if($vtype == C_LS){
    									$data[$i]['datasetid'] = $this->_getParam('datasetid');
    									$data[$i]['elementid'] = $attrid;
    									$data[$i]['type'] = $vtype;
    									$optionlist = count($keyarray) == 0 ? NULL : implode(',', $keyarray);
    									$data[$i]['optionids'] = $optionlist;
    									$data[$i]['result'] = $formvalues[$k];
    									$i++;
    								}
    								if($vtype == C_LM){
    									$data[$i]['datasetid'] = $this->_getParam('datasetid');
    									$data[$i]['elementid'] = $attrid;
    									$data[$i]['type'] = $vtype;
    									$optionlist = count($keyarray) == 0 ? NULL : implode(',', $keyarray);
    									$data[$i]['optionids'] = $optionlist;
    									$data[$i]['result'] = implode(',', $formvalues[$k]);
    									$i++;
    								}
    								// debugMessage($data[$i]);
    							}
    						}
    					}
    				}
    			} // end look for select options 
    		} // end single line attributes
    		
    		// check if batch entries were inserted for processing
    		// debugMessage($batchattributes);
    		if(count($batchattributes) > 0){
    			foreach ($batchattributes as $attrid => $lineattribute) {
    				$key = $i;
    				$idsarray = explode('*', $attrid);
    				$attrid = $idsarray[0]; $lineid = $idsarray[1];
    				
    				$datafield = 'value';
    				$vtype = $lineattribute['type'];
    				if($vtype == C_C){
    					$datafield = 'value_lat';
    					$datafield2 = 'value_lng';
    				}
    				if($vtype == C_DT){ // datetime
    					$datafield = 'value_date';
    					$datafield2 = 'value_time';
    				}
    				if($vtype == C_LS){
    					$datafield = 'value_old';
    				}
    				if($vtype == C_LM){ // datetime
    					$datafield = 'value_old';
    				}
    				// debugMessage($lineattribute);
    				if(!isArrayKeyAnEmptyString($datafield, $lineattribute)){
    					if(!is_array($lineattribute[$datafield])){ // debugMessage($attrid.' ~ '.$lineattribute[$datafield]);
    						$data[$key]['datasetid'] = $this->_getParam('datasetid');
    						$data[$key]['elementid'] = $attrid;
    						$data[$key]['type'] = $vtype;
    						$data[$key]['optionids'] = NULL;
    						$data[$key]['lineid'] = $lineid;
    						$data[$key]['isbatch'] = '1';
    						$data[$key]['result'] = $lineattribute[$datafield];
    					
    						if($vtype == C_C){ // cordinates
    							$data[$key]['result'] = $lineattribute[$datafield];
    							if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    								$data[$key]['result'] = $lineattribute[$datafield].','.$lineattribute[$datafield2];
    							}
    						}
    						if($vtype == C_D){ // date
    							$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield]));
    						}
    						if($vtype == C_TM){ // time
    							$data[$key]['result'] = date('H:i:s', strtotime($lineattribute[$datafield]));
    						}
    						if($vtype == C_DT){ // datetime
    							$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield]));
    							if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    								$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield])).' '.date('H:i:s', strtotime($lineattribute[$datafield2]));
    							}
    						}
    						if($vtype == C_Y || $vtype == C_YN){
    							if(isArrayKeyAnEmptyString($datafield, $lineattribute)){
    								$data[$key]['result'] = 0;
    							}
    						}
    						// debugMessage($key);
    						if($vtype != C_LS && $vtype != C_LM){
    							$i++;
    						}
    					} else {
    						// debugMessage($lineattribute[$datafield]);
    						foreach ($lineattribute[$datafield] as $akey => $option1){
    							// debugMessage($akey);
    							if(!is_array($option1)){
    								// debugMessage($vtype.' ~ '.$akey.' ~ '.$option1);
    								$key = $i;
    								$data[$key]['datasetid'] = $this->_getParam('datasetid');
    								$data[$key]['elementid'] = $attrid;
    								$data[$key]['type'] = $vtype;
    								$data[$key]['optionids'] = $akey;
    								$data[$key]['lineid'] = $lineid;
    								$data[$key]['isbatch'] = '1';
    								$data[$key]['result'] = $option1;
    						
    								if($vtype == C_C){ // cordinates
    									$data[$key]['result'] = $lineattribute[$datafield][$akey];
    									if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    										$data[$key]['result'] = $lineattribute[$datafield][$akey].','.$lineattribute[$datafield2][$akey];
    									}
    								}
    								if($vtype == C_D){ // date
    									$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    								}
    								if($vtype == C_TM){ // time
    									$data[$key]['result'] = date('H:i:s', strtotime($lineattribute[$datafield][$akey]));
    								}
    								if($vtype == C_DT){ // datetime
    									$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    									if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    										$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey])).' '.date('H:i:s', strtotime($lineattribute[$datafield2][$akey]));
    									}
    								}
    								if($vtype == C_Y || $vtype == C_YN){
    									if(isArrayKeyAnEmptyString($akey, $lineattribute[$datafield])){
    										$data[$key]['result'] = 0;
    									}
    								}
    								if($vtype == C_LS){ // select single
    									if(!isArrayKeyAnEmptyString('attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_value', $formvalues)){
    						
    										$data[$key]['result'] = $formvalues['attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_value'];
    									}
    								}
    								if($vtype == C_LM){ // multiple single
    									if(!isArrayKeyAnEmptyString('attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_value', $formvalues)){
    										$data[$key]['result'] = implode(',', $formvalues['attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_value']);
    									}
    								}
    								$i++;
    							} else {
    								foreach ($option1 as $bkey => $option2){
    									if($bkey == $datafield){
    										// debugMessage($akey); debugMessage($option2);
    										foreach ($option2 as $ckey => $option3){
    						
    											if(!is_array($option3)){
    												$key = $i;
    												$data[$key]['datasetid'] = $this->_getParam('datasetid');
    												$data[$key]['elementid'] = $attrid;
    												$data[$key]['type'] = $vtype;
    												$data[$key]['optionids'] = $akey.','.$ckey;
    												$data[$key]['lineid'] = $lineid;
    												$data[$key]['isbatch'] = '1';
    												$data[$key]['result'] = $option3;
    						
    												if($vtype == C_C){ // cordinates
	    												$data[$key]['result'] = $lineattribute[$datafield][$akey];
	    												if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
	    													$data[$key]['result'] = $lineattribute[$datafield][$akey].','.$lineattribute[$datafield2][$akey];
	    												}
    												}
    												
    												if($vtype == C_D){ // date
    													$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    												}
    												if($vtype == C_TM){ // time
    													$data[$key]['result'] = date('H:i:s', strtotime($lineattribute[$datafield][$akey]));
    												}
    												if($vtype == C_DT){ // datetime
    													$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey]));
    													if(!isArrayKeyAnEmptyString($datafield2, $lineattribute)){
    														$data[$key]['result'] = date('Y-m-d', strtotime($lineattribute[$datafield][$akey])).' '.date('H:i:s', strtotime($lineattribute[$datafield2][$akey]));
    													}
    												}
    												if($vtype == C_Y || $vtype == C_YN){
	    												if(isArrayKeyAnEmptyString($akey, $lineattribute[$datafield])){
	    													$data[$key]['result'] = 0;
	    												}
    												}
    												if($vtype == C_LS){ // select single
	    														if(!isArrayKeyAnEmptyString('attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_'.$ckey.'_value', $formvalues)){
	    													$data[$key]['result'] = $formvalues['attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_'.$ckey.'_value'];
	    												}
    												}
    												if($vtype == C_LM){ // multiple single
    														if(!isArrayKeyAnEmptyString('attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_'.$ckey.'_value', $formvalues)){
    														$data[$key]['result'] = implode(',', $formvalues['attribute_'.$attrid.'_'.$lineid.'_'.$akey.'_'.$ckey.'_value']);
    													}
    												}/* */
    												$i++;
    											} else {
    												// if more than 3 levels
    											}
    										}
    									}
    								}
    							}
    						}
    					}
    				}
    				
    				if($vtype == C_LS){ // select single
    					if(!isArrayKeyAnEmptyString('attribute_'.$attrid.'_'.$lineid.'_value', $formvalues)){
    						$data[$key]['datasetid'] = $this->_getParam('datasetid');
    						$data[$key]['elementid'] = $attrid;
    						$data[$key]['type'] = $vtype;
    						$data[$key]['optionids'] = NULL;
    						$data[$key]['lineid'] = $lineid;
    						$data[$key]['isbatch'] = '1';
    						$data[$key]['result'] = $formvalues['attribute_'.$attrid.'_'.$lineid.'_value'];
    						$i++;
    					}
    				}
    				if($vtype == C_LM){ // select single
    					if(!isArrayKeyAnEmptyString('attribute_'.$attrid.'_'.$lineid.'_value', $formvalues)){
    						$data[$key]['datasetid'] = $this->_getParam('datasetid');
    						$data[$key]['elementid'] = $attrid;
    						$data[$key]['type'] = $vtype;
    						$data[$key]['optionids'] = NULL;
    						$data[$key]['lineid'] = $lineid;
    						$data[$key]['isbatch'] = '1';
    						$data[$key]['result'] = implode(',', $formvalues['attribute_'.$attrid.'_'.$lineid.'_value']);
    						$i++;
    					}
    				}
    			}
    			// end loop through attributes
    		}
    	}
    	
    	// debugMessage($data);
    	if(count($data) > 0){
    		$formvalues['resultdetails'] = $data;
    		unset($formvalues['attribute']);
    	}
    	// exit;
    	
    	$entity->processPost($formvalues);
    	debugMessage($entity->toArray());
    	debugMessage('errors are '.$entity->getErrorStackAsString());
    	// exit;
    	 
    	if($entity->hasError()){
    		$session->setVar(ERROR_MESSAGE, $entity->getErrorStackAsString());
    		debugMessage('err '.$entity->getErrorStackAsString());
    		$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)));
    	} else {
    		try {
    			if($this->_getParam('status') == 1){
    				$entity->setdatesubmitted(NULL);
    				$entity->setdateapproved(NULL);
    				$entity->setapprovedbyid(NULL);
    			}
    			if($this->_getParam('status') == 2){
    				$entity->setdatesubmitted(NULL);
    				$entity->setdateapproved(NULL);
    				$entity->setapprovedbyid(NULL);
    				if($entity->getDataset()->isMultiStep()){
    					$entity->setapprovedbyid1(NULL);
    					$entity->setapprovedbyid2(NULL);
    					$entity->setapprovedbyid3(NULL);
    					$entity->setdateapproved1(NULL);
    					$entity->setdateapproved2(NULL);
    					$entity->setdateapproved3(NULL);
    					$entity->setreason1(NULL);
    					$entity->setreason2(NULL);
    					$entity->setreason3(NULL);
    					$entity->setstatus1(NULL);
    					$entity->setstatus2(NULL);
    					$entity->setstatus3(NULL);
    				}
    			}
    			if($this->_getParam('status') == 3 && $this->_getParam('status_old') != 3){
    				$entity->setdatesubmitted(DEFAULT_DATETIME);
    				$entity->setdateapproved(DEFAULT_DATETIME);
    				$entity->setapprovedbyid(isEmptyString($session->getVar('userid')) ? DEFAULT_ID : $session->getVar('userid'));
    			}
    			$entity->save(); // debugMessage($payment->toArray());
    			$entity->afterSave();
    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
    			$successurl .= '/'.encode($entity->getID());
    		} catch (Exception $e) {
    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); debugMessage('save error '.$e->getMessage());
    			$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)));
    		}
    	}
    	 
    	// debugMessage($successurl); exit();
    	$this->_helper->redirector->gotoUrl($successurl);
    }
}