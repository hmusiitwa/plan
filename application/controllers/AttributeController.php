<?php

class AttributeController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Attribute";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
		if($action == "view" || $action == 'listsearch' || $action == 'listsubmit'){
	 		return "list";
	 	}
	 	if($action == "index" || $action == "update" || $action == "create" || $action == "edit"){
	 		return "index";
	 	}
		return parent::getActionforACL();
    }
}
