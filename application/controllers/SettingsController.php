<?php

class SettingsController extends SecureController   {
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * Return the Application Settings since we need to make the url more friendly
	 *
	 * @return String
	 */
	function getResourceForACL() {
		$action = strtolower($this->getRequest()->getActionName());
		if($action == 'index' || $action == 'processconfig'){
			return 'Navigation';
		}
		return 'Settings';
	}
	/**
	 * Get the name of the resource being accessed 
	 *
	 * @return String 
	 */
	function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		$reportactions = getActionsResources('8');
		foreach($reportactions as $aid => $actiondetails){
			if($action == $actiondetails['slug']) {
				return $actiondetails['slug'];
			}
		}
		if($action == 'index' || $action == 'processconfig'){
			return 'adminmenu';
		}
		// parent::getActionforACL();
		return 'index';
	}
	
	function profileAction(){}
	function uploadsAction(){}
	function notificationAction(){}
	function smsAction(){}
	function backupAction(){}
	function prefferenceAction(){}
	function systemAction(){}
	function indexAction(){
		
	}
	
	function processconfigAction(){
		$session = SessionWrapper::getInstance(); 
     	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$formvalues = $this->_getAllParams();
		$successurl = decode($formvalues[URL_SUCCESS]);
		// debugMessage($formvalues);
		$postarray = array();
		for ($i = 1; $i <= $formvalues['t']; $i++) {
			$postarray[$i]['id'] = $formvalues['id_'.$i];
			$postarray[$i]['displayname'] = $formvalues['displayname_'.$i];
			$postarray[$i]['optionvalue'] = $formvalues['optionvalue_'.$i];
		}

		$config_collection = new Doctrine_Collection(Doctrine_Core::getTable("AppConfig"));
		foreach ($postarray as $line){
			$appconfig = new AppConfig();
			$appconfig->populate($line['id']);

			$appconfig->processPost($line);
			/*debugMessage('error is '.$appconfig->getErrorStackAsString());
			debugMessage($appconfig->toArray());*/
			if($appconfig->isValid()) {
				$config_collection->add($appconfig);
			}	
		}
		// check for atleast one option and save
		if($config_collection->count() > 0){
			try {
				// debugMessage($config_collection->toArray());
				$config_collection->save();
				$session->setVar(SUCCESS_MESSAGE, $formvalues[SUCCESS_MESSAGE]);
				
				# clear cache after updating options
				$temppath = APPLICATION_PATH.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR; // debugMessage($temppath);
				$files = glob($temppath.'zend_cache---config*');
				foreach($files as $file){
					debugMessage($file);
					if(is_file($file)){
						unlink($file);
				  	}
				}
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, "An error occured in updating the parameters. ".$e->getMessage());
			}
		}
		// debugMessage($successurl);
		$this->_helper->redirector->gotoUrl($successurl);	
		// exit();
	}
}

