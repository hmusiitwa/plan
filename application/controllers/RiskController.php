<?php

class RiskController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Risk Mgt";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
		if($action == "index" || $action == "update" || $action == "create" || $action == "edit" || $action == "update"){
	 		return "index";
	 	}
	 	if($action == "view" || $action == 'listsearch' || $action == 'listsubmit' || $action == "viewsuccess" || $action == "savesuccess"){
	 		return "list";
	 	}
	 	if($action == "review" || $action == "processreview" || $action == "comment" || $action == "updatestatus"){
	 		return "update";
	 	}
		return parent::getActionforACL();
    }
    public function commentAction() {
    	 
    }
    public function reviewAction() {
    	
    }
    public function processreviewAction() {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$conn = Doctrine_Manager::connection();
    	
    	$classname = $this->_getParam('entityname');
    	$entity = new $classname;
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	 
    	if(!isEmptyString($id)){
    		$entity->populate($id);
    		$formvalues['lastupdatedby'] = $session->getVar('userid');
    		$formvalues['lastupdatedate'] = DEFAULT_DATETIME;
    	} else {
    		$formvalues['createdby'] = $session->getVar('userid');
    	}
    	 
    	
    	$formvalues['id'] = $id;
    	$entity->processPost($formvalues); // exit;
    	/* debugMessage($entity->toArray());
    	debugMessage('errors are '.$entity->getErrorStackAsString()); exit(); */
    	 
    	if($entity->hasError()){
    		$session->setVar(ERROR_MESSAGE, $entity->getErrorStackAsString());
    	} else {
    		try {
    			$entity->save(); // debugMessage($payment->toArray());
    			if(isEmptyString($id)){
    				$entity->afterSave();
    			} else {
    				$entity->afterUpdate();
    			}
    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
    		} catch (Exception $e) {
    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
    		}
    	}
    }
    
    function updatestatusAction() {
    	$this->_setParam("action", ACTION_DELETE);
    
    	$session = SessionWrapper::getInstance();
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$successurl = decode($formvalues[URL_SUCCESS]);
    	$successmessage = '';
    	if(!isArrayKeyAnEmptyString(SUCCESS_MESSAGE, $formvalues)){
    		$successmessage = $this->_translate->translate($this->_getParam(SUCCESS_MESSAGE));
    	}
    
    	$entity = new ContentRisk();
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	$entity->populate($id);
    	// debugMessage($successmessage);
    	/* debugMessage($user->toArray());
    	 exit(); */
    	if($entity->updateStatus($formvalues['status'])) {
    		if(!isEmptyString($successmessage)){
    			$session->setVar(SUCCESS_MESSAGE, $successmessage);
    		}
    		$this->_helper->redirector->gotoUrl($successurl);
    	}
    
    	return false;
    }
    function savesuccessAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams();
    	//debugMessage($formvalues);
    
    	$successurl = decode($this->_getParam(URL_SUCCESS));
    	if(!isEmptyString($this->_getParam('id'))){
    		$successurl .= '/vid/'.$this->_getParam('id');
    	}
    	//debugMessage($successurl);
    	//
    	$this->_helper->redirector->gotoUrl($successurl);
    }
}