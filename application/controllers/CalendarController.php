<?php

class CalendarController extends IndexController  {
	/**
	 * Override unknown actions to enable ACL checking
	 *
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		return ACTION_VIEW;
	}
	
	function eventsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$config = Zend_Registry::get("config");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams();
		$conn = Doctrine_Manager::connection();
	
		$user = new UserAccount();
		// $user->populate($formvalues['id']);
		
		$custom_query = "";
		
		$startlimit = getFirstDayOfMonth(1, date('Y'));
		if(!isEmptyString($this->_getParam('startdate'))){
			$startlimit = $this->_getParam('startdate');
		}
		$endlimit = getLastDayOfMonth(12, date('Y'));
		if(!isEmptyString($this->_getParam('enddate'))){
			$endlimit = $this->_getParam('enddate');
		}
		$custom_query .= " AND TO_DAYS(a.startdate) BETWEEN TO_DAYS('".$startlimit."') AND TO_DAYS('".$endlimit."') ";
		
		if(!isEmptyString($this->_getParam('projectid'))){
			$custom_query .= " AND a.projectid = '".$this->_getParam('projectid')."' ";
		}
		$query = "SELECT a.id as id, a.refno, a.title, a.type, a.startdate, a.enddate from activity a WHERE a.id <> '' ".$custom_query." order by a.startdate "; // debugMessage($query);
		$events = $conn->fetchAll($query); 
	
		$jsondata = array();
		$i = 0;
		if(count($events) > 0){
			// $jsondata = $events;
			$leaveoptions = getHoursDaysDropdown();
			foreach ($events as $key => $value){
				$jsondata[$i]['id'] = $value['id'];
				$jsondata[$i]['title'] = '[Ref '.$value['refno'].'] '.$value['title'];
				$daysbtn = getDaysBetweenDates($value['startdate'], $value['enddate']);
				if($daysbtn > 1){
					$jsondata[$i]['title'] = '[Ref '.$value['refno'].'] '.$value['title'].' ['.changeMySQLDateToPageFormat($value['startdate']).' - '.changeMySQLDateToPageFormat($value['enddate']).']';
				}
				$jsondata[$i]['description'] = $value['title'];
				$jsondata[$i]['start'] = date('Y-m-d', strtotime($value['startdate']));
				$jsondata[$i]['end'] = date('Y-m-d', strtotime($value['enddate']));
				$jsondata[$i]['className'] = 'activity';
				$listurl = $this->view->baseUrl('calendar/index');
				$jsondata[$i]['ajaxurl'] = $this->view->baseUrl('activity/view/id/'.encode($value['id']).'/pgc/true');
				$jsondata[$i]['editurl'] = $this->view->baseUrl('programme/addcontent/id/'.encode($value['id']).'/pgc/true/type/4/level/4/successurl/'.encode($listurl));
				$i++;
			}
		}
	
		// debugMessage($jsondata);
		echo json_encode($jsondata);
	}
}
