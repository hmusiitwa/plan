<?php

class AccessController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	function getResourceForACL() {
		return "Document";
	}
	function getActionforACL() {
		return "list";
	}
	
	function indexAction(){
		
	}
	function processaccessAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); debugMessage($formvalues);
		
		$perms = array(); $delete_array = array();
		$delete_collection = new Doctrine_Collection(Doctrine_Core::getTable("Access"));
		if(!isArrayKeyAnEmptyString('permissions', $formvalues)){
			foreach ($formvalues['permissions'] as $key => $value){
				if(!in_array($value['folderid'], $formvalues['folders'])){
					// debugMessage($value['folderid']);
					if(!isArrayKeyAnEmptyString('id', $value)){
						$delete_array[$key] = $value;
					}
				} else {
					if(isArrayKeyAnEmptyString('userid', $formvalues)){
						$formvalues['permissions'][$key]['userid'] = $formvalues['userid'];
					}
					if(isArrayKeyAnEmptyString('id', $value)){
						$formvalues['permissions'][$key]['createdby'] = $session->getVar('userid');
					}
					if(isArrayKeyAnEmptyString('addfolder', $value)){
						$formvalues['permissions'][$key]['addfolder'] = 0;
					}
					if(isArrayKeyAnEmptyString('create', $value)){
						$formvalues['permissions'][$key]['create'] = 0;
					}
					if(isArrayKeyAnEmptyString('edit', $value)){
						$formvalues['permissions'][$key]['edit'] = 0;
					}
					if(isArrayKeyAnEmptyString('view', $value)){
						$formvalues['permissions'][$key]['view'] = 0;
					}
					if(isArrayKeyAnEmptyString('list', $value)){
						$formvalues['permissions'][$key]['list'] = 0;
					}
					if(isArrayKeyAnEmptyString('delete', $value)){
						$formvalues['permissions'][$key]['delete'] = 0;
					}
					if(isArrayKeyAnEmptyString('move', $value)){
						$formvalues['permissions'][$key]['move'] = 0;
					}
					if(isArrayKeyAnEmptyString('email', $value)){
						$formvalues['permissions'][$key]['email'] = 0;
					}
					if(isArrayKeyAnEmptyString('share', $value)){
						$formvalues['permissions'][$key]['share'] = 0;
					}
					if(isArrayKeyAnEmptyString('archive', $value)){
						$formvalues['permissions'][$key]['archive'] = 0;
					}
				}
			}
			debugMessage($formvalues['permissions']);
			$access_collection = new Doctrine_Collection(Doctrine_Core::getTable("Access"));
			foreach ($formvalues['permissions'] as $key => $value){
				$access = new Access();
				if(!isArrayKeyAnEmptyString('id', $value)){
					$access->populate($value['id']);
				}
				if(in_array($value['folderid'], $formvalues['folders'])){
					$access->processPost($value);
					if(!$access->hasError()){
						$access_collection->add($access);
					} else {
						debugMessage($access->getErrorStackAsString());
					}
				}
			}
			// debugMessage($access_collection->toArray()); exit;
			// save access collection
			if($access_collection->count() > 0){
				try {
					$access_collection->save();
					$session->setVar(SUCCESS_MESSAGE, $this->_getParam("successmessage"));
					// delete denied objects
					if(count($delete_array) > 0){
						foreach ($delete_array as $k => $value){
							$delete = new Access();
							$delete->populate($value['id']);
							$delete_collection->add($delete);
						}
					}
					if($delete_collection->count() > 0){
						try {
							$delete_collection->delete();
						} catch (Exception $e) {
							debugMessage('err deleting '.$e->getMessage());
						}
					}
				} catch (Exception $e) {
					$session->setVar(ERROR_MESSAGE, "Error updating settings. <br>".$e->getMessage());
					debugMessage($e->getMessage());
				}
			}
			
		}
		// exit;
		# redirect back
		$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	}
}