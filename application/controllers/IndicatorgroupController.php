<?php

class IndicatorgroupController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Indicator";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	if($action == "list" || $action == "view"){
	 		return "listgroups";
	 	}
	 	if($action == "index" || $action == "update" || $action == "create" || $action == "edit" || $action == "delete"){
	 		return "indexgroups";
	 	}
		return parent::getActionforACL();
    }
}
