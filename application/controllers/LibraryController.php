<?php

class LibraryController extends SecureController  {
	
	public function init()    {
		parent::init();
	
		$current_timestamp = strtotime('now'); $now_iso = date('Y-m-d H:i:s', $current_timestamp); $this->view->now_iso = $now_iso; //debugMessage('now '.$now_iso.'-'.$current_timestamp);
		$onehourago_timestamp = strtotime('-1 hour'); $onehourago_iso = date('Y-m-d H:i:s', $onehourago_timestamp );
		$this->view->onehourago_iso = $onehourago_iso; $this->view->onehourago_timestamp = $onehourago_timestamp;// debugMessage('now '.$onehourago_iso.'-'.$onehourago_timestamp);
		$sixhourago_timestamp = strtotime('-6 hour'); $sixhourago_iso = date('Y-m-d H:i:s', $sixhourago_timestamp);
		$this->view->sixhourago_iso = $sixhourago_iso; $this->view->sixhourago_timestamp = $sixhourago_timestamp;
		$twelvehourago_timestamp = strtotime('-12 hour'); $twelvehourago_iso = date('Y-m-d H:i:s', $twelvehourago_timestamp);
		$this->view->twelvehourago_timestamp = $twelvehourago_timestamp; $this->view->twelvehourago_iso = $twelvehourago_iso;
	
		// debugMessage($logged_today_sql);
		$today_iso = date('Y-m-d'); $today = changeMySQLDateToPageFormat($today_iso);  $this->view->today_iso = $today_iso; //debugMessage('today '.$today_iso);
		$today_iso_short = date('M j', $current_timestamp);
		
		$yestday_iso = date('Y-m-d', strtotime('1 day ago')); $yestday = changeMySQLDateToPageFormat($yestday_iso); $this->view->yestday_iso = $yestday_iso; //debugMessage('yesterday '.$yestday_iso);
		$yestday_iso_short = date('M j', strtotime($yestday_iso));
		$weekday = date("N");
	
		// monday of week
		$mondaythisweek_iso = date('Y-m-d', strtotime('monday this week')); $mondaythisweek = changeMySQLDateToPageFormat($mondaythisweek_iso);
		if($weekday == 1){
			$mondaythisweek_iso = $today_iso;
			$mondaythisweek = $today;
		}
		if($weekday == 7){
			$mondaythisweek_iso = date('Y-m-d', strtotime('monday last week'));
			$mondaythisweek = changeMySQLDateToPageFormat($mondaythisweek_iso);
		}
		$this->view->mondaythisweek_iso = $mondaythisweek_iso; //debugMessage('monday this week '.$mondaythisweek_iso);
	
		// sunday of week
		$sundaythisweek_iso = date('Y-m-d', strtotime('sunday this week')); $sundaythisweek = changeMySQLDateToPageFormat($sundaythisweek_iso);
		if($weekday == 1){
			$sundaythisweek_iso = date('Y-m-d', strtotime('today + 7 days')); $sundaythisweek = changeMySQLDateToPageFormat($sundaythisweek_iso);
		}
		if($weekday == 7){
			$sundaythisweek_iso = $today_iso; $sundaythisweek = $today;
		}
		$this->view->sundaythisweek_iso = $sundaythisweek_iso; // debugMessage('sunday this week '.$sundaythisweek_iso);
	
		// monday last week
		$mondaylastweek_iso = date('Y-m-d', strtotime('-7 days', strtotime($mondaythisweek_iso))); //debugMessage('monday last week '.$mondaylastweek_iso);
		$this->view->mondaylastweek_iso = $mondaylastweek_iso;
		// sunday last week
		$sundaylastweek_iso = date('Y-m-d', strtotime('-7 days', strtotime($sundaythisweek_iso))); // debugMessage('sunday last week '.$sundaylastweek_iso);
		$this->view->sundaylastweek_iso = $sundaylastweek_iso;
		
		// firstday this month
		$firstdayofthismonth_iso = getFirstDayOfCurrentMonth(); //debugMessage('1st day this month '.$firstdayofthismonth_iso);
		$this->view->firstdayofthismonth_iso = $firstdayofthismonth_iso;
		// lastday this month
		$lastdayofthismonth_iso = getLastDayOfCurrentMonth(); //debugMessage('last day this month '.$lastdayofthismonth_iso);
		$this->view->lastdayofthismonth_iso = $lastdayofthismonth_iso;
		
		// firstday last month
		$firstdayoflastmonth_iso = getFirstDayOfMonth(date('m')-1, date('Y')); //debugMessage('1st day last month '.$firstdayoflastmonth_iso);
		$this->view->firstdayoflastmonth_iso = $firstdayoflastmonth_iso;
		// lastday last month
		$lastdayoflastmonth_iso = getLastDayOfMonth(date('m')-1, date('Y')); //debugMessage('last day last month '.$lastdayoflastmonth_iso);
		$this->view->lastdayoflastmonth_iso = $lastdayoflastmonth_iso;
		
		// firstday 2 month ago
		$firstdayof2monthago_iso = getFirstDayOfMonth(date('m')-2, date('Y')); //debugMessage('1st day 2 month ago '.$firstdayof2monthago_iso);
		$this->view->firstdayof2monthago_iso = $firstdayof2monthago_iso;
		// lastday 2 month ago
		$lastdayof2monthago_iso = getLastDayOfMonth(date('m')-2, date('Y')); //debugMessage('last day last month '.$lastdayof2monthago_iso);
		$this->view->lastdayof2monthago_iso = $lastdayof2monthago_iso;
		
		// firstday 3 month ago
		$firstdayof3monthago_iso = getFirstDayOfMonth(date('m')-3, date('Y')); //debugMessage('1st day 3 month ago '.$firstdayof3monthago_iso);
		$this->view->firstdayof3monthago_iso = $firstdayof3monthago_iso;
		// lastday 3 month ago
		$lastdayof3monthago_iso = getLastDayOfMonth(date('m')-3, date('Y')); //debugMessage('last day last month '.$lastdayof3monthago_iso);
		$this->view->lastdayof3monthago_iso = $lastdayof3monthago_iso;
		
		// firstday this year
		$firstdayofyear_iso = getFirstDayOfMonth(1, date('Y')); //debugMessage('1st day this year '.$firstdayofyear_iso);
		$this->view->firstdayofyear_iso = $firstdayofyear_iso;
		// lastday this year
		$lastdayofyear_iso = getLastDayOfMonth(12, date('Y')); //debugMessage('last day this year '.$lastdayofyear_iso);
		$this->view->lastdayofyear_iso = $lastdayofyear_iso;
		// first day of month one year ago
		$startofmonth_oneyearago = getFirstDayOfMonth(date('m', strtotime('1 year ago')), date('Y', strtotime('1 year ago')));
		$this->view->startofmonth_oneyearago = $startofmonth_oneyearago;
	
		$firstsystemday_iso = '2013-01-01';
		$this->view->firstsystemday_iso = $firstsystemday_iso;
	}
	/**
	 * @see SecureController::getActionforACL()
	 * 
	 * The dashboard can only be viewed, however the default is create for the index.phtml file. 
	 *
	 * @return String
	 */
	function getResourceForACL() {
		return "Document";
	}
	function getActionforACL() {
		return 'list'; 
	}
	
	function indexAction(){
		$session = SessionWrapper::getInstance();
		# check if user is entering last inserted id
		if($this->_getParam('opendir') == '1'){
			$currenturl = stripUrl($this->view->viewurl);
			$gotoid = encode($session->getVar('gotofolder'));
			$parentid = $this->_getParam('parentid');
			$newurl = stripUrl(str_replace($parentid, $gotoid, $currenturl));
			$newurl = stripUrl(str_replace("opendir/1", "", stripUrl($newurl)));
			// debugMessage('going to fetch last insert id '.$newurl);
			$this->_helper->redirector->gotoUrl($newurl);
		}
	}
	
	function indexsearchAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		/* debugMessage($this->getRequest()->getQuery());
		debugMessage($this->_getAllParams()); exit(); */
		$this->_helper->redirector->gotoSimple("index", $this->getRequest()->getControllerName(),
				$this->getRequest()->getModuleName(),
				array_remove_empty(array_merge_maintain_keys($this->_getAllParams(), $this->getRequest()->getQuery())));
	}
	function viewAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	}
	function pdfviewAction(){
		$this->_helper->layout->disableLayout();
	}
	function infoAction(){
		$this->_helper->layout->disableLayout();
	}
	
	function uploadAction(){
		$this->_helper->layout->disableLayout();
	}
	
	function processuploadssuccessAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$data = array(
			'result' => 'success', 
			'filelist' => $this->_getParam('filelist')
		);
		echo json_encode($data);
	}
	function processuploadsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		$conn = Doctrine_Manager::connection();
		
		if(isArrayKeyAnEmptyString('uploader_count', $formvalues)){
			$session->setVar(ERROR_MESSAGE, "No files detected for upload"); // debugMessage('no files');
			exit;
		} else {
			$nooffiles = $formvalues['uploader_count'];
			if($nooffiles == '0' || !is_numeric($nooffiles)){
				$session->setVar(ERROR_MESSAGE, "No files detected for upload"); // debugMessage('no files');
				exit;
			}
			$tempdir = BASE_PATH.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.getLibDir().DIRECTORY_SEPARATOR."temp";
			
			$filesarray = array();
			for($i = 0; $i < $nooffiles; $i++){
				if(!isArrayKeyAnEmptyString('uploader_'.$i.'_name', $formvalues)){
					$filename = $formvalues['uploader_'.$i.'_name'];
					$filepath = $tempdir.DIRECTORY_SEPARATOR.$filename;
					if(file_exists($filepath)){
						$filesarray[$i]['companyid'] = $formvalues['companyid'];
						$filesarray[$i]['folderid'] = $formvalues['folderid'];
						$filesarray[$i]['createdby'] = $formvalues['createdby'];
						$filesarray[$i]['filename'] = $filename;
						$filesarray[$i]['filesize'] = filesize($filepath);
						$filesarray[$i]['extension'] = '.'.findExtension($filepath);
					}
				}
			}
			// debugMessage($filesarray);
			if(count($filesarray) == '0'){
				$session->setVar(ERROR_MESSAGE, "No files detected for upload"); // debugMessage('no files');
				exit;
			}
			
			# save collection of files to db
			$files_collection = new Doctrine_Collection(Doctrine_Core::getTable("File"));
			foreach ($filesarray as $afile){
				$file = new File();
				$q = Doctrine_Query::create()->from('file f')->where("filename = '".$afile['filename']."' AND folderid = '".$afile['folderid']."'");
				$result = $q->execute();
				if($result){
					$result->delete();
				}
				
				$file->processPost($afile);
				// debugMessage('err '.$file->getErrorStackAsString());
				// debugMessage($file->toArray());
				if(!$file->hasError()){
					$files_collection->add($file);
				}
			}
			// debugMessage($files_collection->toArray());
			if($files_collection->count() == 0){
				$session->setVar(ERROR_MESSAGE, "Error in uploading files"); // debugMessage('no files');
				exit;
			}
			
			try {
				$files_collection->save();
				foreach ($files_collection as $savedfile){
					$savedfile->afterSave();
				}
				$session->setVar(SUCCESS_MESSAGE, "Successfully uploaded"); // debugMessage($formvalues['successmessage']);
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, "Error in uploading files - ".$e->getMessage()); debugMessage('Error saving upload(s) '.$e->getMessage());
				// exit;
			}
		}
	}
	function pushuploadsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$conn = Doctrine_Manager::connection();
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		
		header("Expires: 0");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		// 5 minutes execution time
		@set_time_limit(5 * 60);
		
		// Settings
		$targetDir = BASE_PATH.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.getLibDir().DIRECTORY_SEPARATOR."temp";
		//$targetDir = 'uploads';
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds
		
		// Create target dir
		if (!file_exists($targetDir)) {
			@mkdir($targetDir, 0777, true);
		}
		
		// Get a file name
		if (isset($_REQUEST["name"])) {
			$fileName = $_REQUEST["name"];
		} elseif (!empty($_FILES)) {
			$fileName = $_FILES["file"]["name"];
		} else {
			$fileName = uniqid("file_");
		}
		
		$filePath = $targetDir.DIRECTORY_SEPARATOR.$fileName;
		
		// Chunking might be enabled
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		
		
		// Open temp file
		if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		if (!empty($_FILES)) {
			if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
		
			// Read binary input stream and append it to temp file
			if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		} else {
			if (!$in = @fopen("php://input", "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		}
		
		while ($buff = fread($in, 4096)) {
			fwrite($out, $buff);
		}
		
		@fclose($out);
		@fclose($in);
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off
			rename("{$filePath}.part", $filePath);
		}
		
		// Return Success JSON-RPC response
		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
	}
	
	function processinfoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams();
		
		if(isEmptyString($this->_getParam('id'))){
			$this->_setParam('action', ACTION_CREATE);
			parent::createAction();
		} else {
			$this->_setParam('action', ACTION_EDIT);
			parent::editAction();
		}
	}

	function deleteAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); // exit;
		$errcount = 0; $successcount = 0; $errmsg = ''; $filestr = '';
		
		// exit;
		if($this->_getParam('mode') == 'emptytrash'){
			$trash = new Folder();
			$trash->populate(decode($this->_getParam('parentid'))); debugMessage($trash->toArray()); 
			$folders = $trash->getFolders(); // debugMessage($folders->toArray());
			$files = $trash->getFiles(); 
			// remove folders from remote
			foreach ($folders as $folder) {
				if(is_dir($folder->getAbsolutePath())){
					// debugMessage('exists '.$folder->getAbsolutePath());
					rrmdir2($folder->getAbsolutePath());
				}
			}
			// remove files from remote
			foreach ($files as $file) {
				if(file_exists($file->getAbsoluteFilePath())){
					// debugMessage('exists '.$folder->getAbsolutePath());
					unlink($file->getAbsoluteFilePath());
				}
			}
			try {
				$folders->delete(); // delete folders
				$files->delete(); // delete files
				$msg = "Successfully emptied Trash.";
				$session->setVar(SUCCESS_MESSAGE, $msg); debugMessage($msg);
			} catch (Exception $e) {
				$errmsg = 'Error deleting item(s) from Trash - '.$e->getMessage();
				$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
			}
			// exit;
		}
		if($this->_getParam('mode') == 'batch'){
			// debugMessage($formvalues);
			$filelist = decode($formvalues['filelist']);
			$dirlist = decode($formvalues['dirlist']); // exit;
			$istrashing = false; $isdeleting = false;
			if($this->_getParam('empty') == '1'){
				$istrashing = false; $isdeleting = true;
			} else {
				$istrashing = true; $isdeleting = false;
			}
			
			if(!isEmptyString($filelist)){
				// delete files selected
				$filearray = explode(',', $filelist); // debugMessage($filearray); // exit;
				if(count($filearray) > 0){
					$file_collection = new Doctrine_Collection(Doctrine_Core::getTable("File"));
					foreach ($filearray as $id){
						$file = new File();
						$file->populate($id);
						if(!isEmptyString($file->getID())){
							$file_collection->add($file);
						}
					}
					// debugMessage($file_collection->toArray());
					if($file_collection->count() > 0){
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($file_collection as $record){
							$parentdir = $record->getFolder();
							$abspath = $record->getAbsoluteFilePath(); 
							$filestr .= $record->getRelativePathFromHome().',<br>';
							$homefolder = $record->getFolder()->getHomeFolder();
							$trashfolder = $record->getFolder()->getTrashFolder();
							$trashpath = $trashfolder->getTrashAbsoluteUrl(); 
							// debugMessage($abspath.' ~ '.stripURL($trashpath).DIRECTORY_SEPARATOR.$record->getFilename());
							
							# change parentid and save changes
							$record->setFolderID($trashfolder->getID());
							$record->setRestoreID($parentdir->getID());
							try { // save changes and move the files
								if($istrashing){
									$record->save();
									// check if each file exists on file system and move it to trash
									# move to trash but check if it exists and unlink
									if(file_exists($trashpath.DIRECTORY_SEPARATOR.$record->getFilename())){
										unlink($trashpath.DIRECTORY_SEPARATOR.$record->getFilename());
									}
									if(file_exists($abspath) && is_dir($trashpath)){
										rename($abspath, $trashpath.DIRECTORY_SEPARATOR.$record->getFilename()); // debugMessage('moved '.$record->getFilename());
									}
								}
								if($isdeleting){
									if(file_exists($abspath)){
										unlink($abspath); // delete permanent
									}
									$record->delete(); // delete from db
								}
								$successcount++;
							} catch (Exception $e) {
								$errcount++;
								$errmsg .= 'Error deleting File <b>'.$record->getRelativePathFromHome().'</b> - '.$e->getMessage();
							}
						}
						if($successcount > 0){
							$successlink = ""; debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully deleted. '.$successlink);
							if($isdeleting){
								$session->setVar(SUCCESS_MESSAGE, 'Successfully deleted. '.$successlink);
							}
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				}
				
			}
			if(!isEmptyString($dirlist)){
				// debugMessage($dirlist);
				$dirarray = explode(',', $dirlist); // debugMessage($dirarray); // exit;
				if(count($dirarray) > 0){
					$dir_collection = new Doctrine_Collection(Doctrine_Core::getTable("Folder"));
					foreach ($dirarray as $id){
						$folder = new Folder();
						$folder->populate($id);
						if(!isEmptyString($folder->getID())){
							$dir_collection->add($folder);
						}
					}
					// debugMessage($dir_collection->toArray());
					if($dir_collection->count() > 0){
						
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($dir_collection as $record){
							$homefolder = $record->getHomeFolder();
							$trashfolder = $record->getTrashFolder();
							$trashpath = $trashfolder->getTrashAbsoluteUrl();
							$filestr .= $record->getRelativePathFromHome().',<br>';
							$parentdir = $record->getParent();
							$abspath = $record->getAbsolutePath(); 
							// debugMessage($abspath.' ~ '.stripURL($trashpath).DIRECTORY_SEPARATOR.$record->getName()); 
							
							try {
								if($istrashing){
									$record->setParentID($trashfolder->getID());
									$record->setRestoreID($parentdir->getID());
									$record->setPath($homefolder->getID().'/'.$trashfolder->getID());
									$record->save(); // save changes
									chown($trashpath.DIRECTORY_SEPARATOR, 0777);
									if(is_dir($trashpath.DIRECTORY_SEPARATOR.$record->getName())){
										rmdirr($trashpath.DIRECTORY_SEPARATOR.$record->getName());
									}
									full_copy($abspath, $trashpath.DIRECTORY_SEPARATOR.$record->getName());
									
									if(file_exists($trashpath.DIRECTORY_SEPARATOR.$record->getName())){
										chown($abspath, 0777);
										rmdirr($abspath); debugMessage('removed after copy');
									}
								}
								if($isdeleting){
									if(file_exists($abspath)){
										rmdirr($abspath); // delete permanent
									}
									$record->delete(); // delete from db
								}
								$successcount++;
							} catch (Exception $e) {
								$errcount++;
								$errmsg .= 'Error deleting Folder <b>'.$record->getRelativePathFromHome().'</b> - '.$e->getMessage();
							}
							// exit;
						}
						if($successcount > 0){
							$successlink = ""; debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully deleted. '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				}
			}
		}
		
		# add log to audit trail
		$view = new Zend_View();
		// $controller = $this->getController();
		$url = decode($this->_getParam('id'));
		$module = '2';
		$usecase = '2.3';
		$type = PATH_DELETE;
		$actionst = "Trashed";
		if($this->_getParam('empty') == '1' || $isdeleting){
			$actionst = "Permanently Deleted";
		}
		if($this->_getParam('mode') == 'emptytrash'){
			$actionst = "Emptied Trash";
		}
		
		$audit_values = getAuditInstance();
		$audit_values['companyid'] = $session->getVar('companyid');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$transactiondate = date("Y-m-d H:i:s");
		$audit_values['transactiondate'] = $transactiondate;
		$audit_values['status'] = "Y";
		if($successcount == 0){
			$audit_values['status'] = "N";
		}
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['transactiondetails'] = $actionst.' Files: <i>'.$filestr.'</i>';
		$audit_values['url'] = $dirlist.':'.$filelist;
		//debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		// exit;
		# redirect back
		$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	}
	function restoreAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit;
		
		if($this->_getParam('mode') == 'batch'){
			// debugMessage($formvalues);
			$filelist = decode($formvalues['filelist']);
			$dirlist = decode($formvalues['dirlist']); // exit;
				
			if(!isEmptyString($filelist)){
				// delete files selected
				$filearray = explode(',', $filelist); // debugMessage($filearray); // exit;
				if(count($filearray) > 0){
					$file_collection = new Doctrine_Collection(Doctrine_Core::getTable("File"));
					foreach ($filearray as $id){
						$file = new File();
						$file->populate($id);
						if(!isEmptyString($file->getID())){
							$file_collection->add($file);
						}
					}
					// debugMessage($file_collection->toArray());
					if($file_collection->count() > 0){
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($file_collection as $record){
							$oldpath = $record->getAbsoluteFilePath(); 
							$newpath = $record->getRestore()->getAbsolutePath().$record->getName();
							$filestr .= stripUrl($record->getRestore()->getRelativePathFromHome()).'/'.$record->getName().',<br>';
							// debugMessage($oldpath.' ~ '.$newpath);
								
							# check if restore file exists  and move it physically
							if(file_exists($oldpath) && !file_exists($newpath)){
								full_copy($oldpath, $newpath);
								rrmdir2($oldpath);
							}
							if(file_exists($newpath) && file_exists($oldpath)){
								chmod($oldpath, 0777);
								rrmdir2($oldpath);
							}
							if(file_exists($newpath) && !file_exists($oldpath)){
								$record->setFolderID($record->getRestoreID());
								$record->setRestoreID(NULL);
								$record->clearRelated('restore');
								$record->clearRelated('folder');
								// debugMessage($file->toArray());
								// exit;
								try {
									$record->save();
									$session->setVar(SUCCESS_MESSAGE, 'Successfully restored');
									$successcount++;
								} catch (Exception $e) {
									$errcount++;
									$errmsg .= 'Error restoring File '.$record->getName().':  '.$e->getMessage();
								}
							}
						}
						if($successcount > 0){
							$successlink = ""; debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully restored. '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				}
		
			}
			if(!isEmptyString($dirlist)){
				// debugMessage($dirlist);
				$dirarray = explode(',', $dirlist); // debugMessage($dirarray); // exit;
				if(count($dirarray) > 0){
					$dir_collection = new Doctrine_Collection(Doctrine_Core::getTable("Folder"));
					foreach ($dirarray as $id){
						$folder = new Folder();
						$folder->populate($id);
						if(!isEmptyString($folder->getID())){
							$dir_collection->add($folder);
						}
					}
					// debugMessage($dir_collection->toArray());
					if($dir_collection->count() > 0){
		
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($dir_collection as $record){
							$oldpath = $record->getAbsolutePath(); 
							$newpath = $record->getRestore()->getAbsolutePath().$record->getName();
							$filestr .= stripUrl($record->getRestore()->getRelativePathFromHome()).'/'.$record->getName().',<br>';
							debugMessage($oldpath.' ~ '.$newpath);
							# check if restore dir exists and move it physically
							if(is_dir($oldpath) && !is_dir($newpath)){
								full_copy($oldpath, $newpath);
								rrmdir2($oldpath);
							}
							if(is_dir($newpath) && is_dir($oldpath)){
								chmod($oldpath, 0777);
								rrmdir2($oldpath);
							}
							if(is_dir($newpath) && !is_dir($oldpath)){
								debugMessage('going to update');
								$record->setParentID($record->getRestoreID());
								$newpath = $record->getParent()->getPath().$record->getRestoreID();
								if($record->getRestore()->isHome()){
									$newpath = '/'.$record->getHomeFolderID();
								}
								$record->setPath($newpath);
								$record->setRestoreID(NULL);
								$record->clearRelated('restore');
								$record->clearRelated('parent');
								// debugMessage($folder->toArray());
								// exit;
								try {
									$record->save();
									$session->setVar(SUCCESS_MESSAGE, 'Successfully restored');
									$successcount++;
								} catch (Exception $e) {
									$errcount++;
									$errmsg .= 'Error restoring Folder '.$record->getName().':  '.$e->getMessage();
									$session->setVar(ERROR_MESSAGE, $errmsg);
								}
							}
						}
						if($successcount > 0){
							$successlink = ""; debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully restored. '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				}
			}
		}
		
		# add log to audit trail
		$view = new Zend_View();
		// $controller = $this->getController();
		$url = decode($this->_getParam('id'));
		$usecase = '2.5';
		$module = '2';
		$type = PATH_MOVE;
		$actionst = "Restored from Trash";
		
		$audit_values = getAuditInstance();
		$audit_values['companyid'] = $session->getVar('companyid');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$transactiondate = date("Y-m-d H:i:s");
		$audit_values['transactiondate'] = $transactiondate;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['transactiondetails'] = $actionst.'<i>'.$filestr.'</i>';
		$audit_values['url'] = $dirlist.':'.$filelist;
		//debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		# redirect back
		$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	}
	function archiveAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		
		$filestr = '';
		# if archiving multiple files at a time
		if($this->_getParam('mode') == 'batch'){
			// moving muliple files
			$dfolder = new Folder();
			$dfolder->populate($this->_getParam('destfolderid'));
			
			$filelist = decode($formvalues['filelist']);
			$dirlist = decode($formvalues['dirlist']); // exit;
			// debugMessage($filelist); debugMessage($dirlist); exit;
			$dfolder = new Folder();
			$dfolder->populate($this->_getParam('destfolderid'));
			
			$folder = new Folder();
			$folder->populate($this->_getParam('sourceid')); // debugMessage($folder->toArray()); exit;
			$parentdir = $folder->getParent();
			$homefolder = $folder->getHomeFolder();
			//$trashfolder = $folder->getTrashFolder();
			//$trashpath = $trashfolder->getTrashAbsoluteUrl(); // debugMessage($trashpath);
			
			if(!isEmptyString($filelist)){
				// delete files selected
				$filearray = explode(',', $filelist); // debugMessage($filearray); // exit;
				if(count($filearray) > 0){
					$file_collection = new Doctrine_Collection(Doctrine_Core::getTable("File"));
					foreach ($filearray as $id){
						$file = new File();
						$file->populate($id);
						if(!isEmptyString($file->getID())){
							$file_collection->add($file);
						}
					}
					// debugMessage($file_collection->toArray());
					if($file_collection->count() > 0){
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($file_collection as $file){
							# check if Archive folder doesnt exist and create it
							if(!$file->hasArchiveOnFileSystem()){
								mkdir($file->getArchiveAbsoluteUrl(),0777); debugMessage('archive dir created');
							}
							
							# check if Archive folder is missing in the DB
							if($file->hasArchiveOnFileSystem() && !$file->hasArchiveOnDB()){
								$archivedata = array(
									'name' => 'Archive',
									'companyid' => $file->getCompanyID(),
									'parentid' => $file->getFolderID(),
									'path' => stripUrl($file->getFolder()->getPath()).'/'.$file->getFolderID(),
									'datecreated' => date('Y-m-d H:i:s'),
									'createdby' => $session->getVar('userid')
								); // debugMessage($archivedata);
								$archfolder = new Folder();
								$archfolder->processPost($archivedata);
								/* debugMessage($archfolder->toArray());
								debugMessage('err '.$archfolder->getErrorStackAsString()); */
								if(!$archfolder->hasError()){
									$archfolder->save();
									debugMessage('dir added to db');
								}
							}
							
							# move selected folder to archive
							if($file->hasArchiveOnFileSystem() && $file->hasArchiveOnDB()){
								$archive = $file->getArchiveFolder(); // debugMessage($archive->toArray());
								$filestr .= $file->getRelativePathFromHome().',<br>';
								$oldpath = $file->getAbsoluteFilePath(); debugMessage($oldpath);
								$newpath = stripUrl($file->getArchiveAbsoluteUrl()).DIRECTORY_SEPARATOR.$file->getName(); debugMessage($newpath);
								$file->setFolderID($archive->getID());
								try {
									$file->save();
									if(file_exists($newpath)){
										unlink($newpath);
									}
									rename($oldpath, $newpath);
									$successcount++;
								} catch (Exception $e) {
									$errcount++;
									$errmsg .= 'Error archiving Folder <b>'.$file->getName().'</b> - '.$e->getMessage();
								}
							}
						}
						if($successcount > 0){
							$successlink = ""; debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully archived item(s). '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				}
			}
			if(!isEmptyString($dirlist)){
				// debugMessage($dirlist);
				$dirarray = explode(',', $dirlist); // debugMessage($dirarray); // exit;
				if(count($dirarray) > 0){
					$dir_collection = new Doctrine_Collection(Doctrine_Core::getTable("Folder"));
					foreach ($dirarray as $id){
						$folder = new Folder();
						$folder->populate($id);
						if(!isEmptyString($folder->getID())){
							$dir_collection->add($folder);
						}
					}
					// debugMessage($dir_collection->toArray());
					if($dir_collection->count() > 0){
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($dir_collection as $record){
							# check if Archive folder doesnt exist and create it
							if(!$record->hasArchiveOnFileSystem()){
								mkdir($record->getArchiveAbsoluteUrl(),0777); debugMessage('archive dir created');
							}
							# check if Archive folder is missing in the DB
							if($record->hasArchiveOnFileSystem() && !$record->hasArchiveOnDB()){
								$archivedata = array(
									'name' => 'Archive',
									'companyid' => $record->getCompanyID(),
									'parentid' => $record->getParentID(),
									'path' => stripUrl($record->getPath()),
									'datecreated' => date('Y-m-d H:i:s'),
									'createdby' => $session->getVar('userid')
								);
								$archfolder = new Folder();
								$archfolder->processPost($archivedata);
								/* debugMessage($archfolder->toArray());
								debugMessage('err '.$archfolder->getErrorStackAsString()); */
								if(!$archfolder->hasError()){
									$archfolder->save();
									debugMessage('dir added to db');
								}	
							}
							# move selected folder to archive
							if($record->hasArchiveOnFileSystem() && $record->hasArchiveOnDB()){
								$archive = $record->getArchiveFolder(); // debugMessage($archive->toArray());
								$filestr .= $record->getRelativePathFromHome().',<br>';
								$oldpath = $record->getAbsolutePath(); debugMessage($oldpath);
								$newpath = stripUrl($archive->getAbsolutePath()).DIRECTORY_SEPARATOR.$record->getName();  debugMessage($newpath);
								$record->setParentID($archive->getID());
								$record->setPath(stripUrl($archive->getPath()).'/'.$archive->getID());
								try {
									$record->save();
									if(file_exists($newpath)){
										unlink($newpath);
									}
									rename($oldpath, $newpath);
									$successcount++;
								} catch (Exception $e) {
									$errcount++;
									$errmsg .= 'Error archiving Folder <b>'.$record->getName().'</b> - '.$e->getMessage();
								}
							}
						}
						if($successcount > 0){
							$successlink = ""; // debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully archived item(s). '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				}
			}
		}
		// exit;
		
		# add log to audit trail
		$view = new Zend_View();
		// $controller = $this->getController();
		$url = decode($this->_getParam('id'));
		$module = '2';
		$usecase = '2.6';
		$type = PATH_ARCHIVE;
		$actionst = "Archived";
		
		$audit_values = getAuditInstance();
		$audit_values['companyid'] = $session->getVar('companyid');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$transactiondate = date("Y-m-d H:i:s");
		$audit_values['transactiondate'] = $transactiondate;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['transactiondetails'] = $actionst.' Files: <i>'.$filestr.'</i>';
		$audit_values['url'] = $dirlist.':'.$filelist;
		//debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		# redirect back
		$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	}
	
	function moveAction(){
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams();
		// debugMessage($formvalues);
	}
	
	function processmoveAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$config = Zend_Registry::get("config");
		$this->_translate = Zend_Registry::get("translate");
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams();
		// debugMessage($formvalues); exit;
		
		if(isArrayKeyAnEmptyString('destfolderid', $formvalues)){
			$session->setVar(ERROR_MESSAGE, "No Destination specified"); // debugMessage('no files');
			exit;
		}
		
		$filestr = '';
		// moving 
		if($this->_getParam('actiontype') == "1"){
			if($this->_getParam('mode') == 'batch'){
				// moving muliple files
				$dfolder = new Folder();
				$dfolder->populate($this->_getParam('destfolderid'));
				
				$filelist = decode($formvalues['filelist']);
				$dirlist = decode($formvalues['dirlist']); // exit;
				// debugMessage($filelist); debugMessage($dirlist); exit;
				$dfolder = new Folder();
				$dfolder->populate($this->_getParam('destfolderid'));
				
				$folder = new Folder();
				$folder->populate($this->_getParam('sourceid')); // debugMessage($folder->toArray()); exit;
				$parentdir = $folder->getParent();
				$homefolder = $folder->getHomeFolder();
				//$trashfolder = $folder->getTrashFolder();
				//$trashpath = $trashfolder->getTrashAbsoluteUrl(); // debugMessage($trashpath);
				
				if(!isEmptyString($filelist)){
					// delete files selected
					$filearray = explode(',', $filelist); // debugMessage($filearray); // exit;
					if(count($filearray) > 0){
						$file_collection = new Doctrine_Collection(Doctrine_Core::getTable("File"));
						foreach ($filearray as $id){
							$file = new File();
							$file->populate($id);
							if(!isEmptyString($file->getID())){
								$file_collection->add($file);
							}
						}
						// debugMessage($file_collection->toArray());
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($file_collection as $record){
							$oldpath = $record->getAbsoluteFilePath();
							$filestr .= $record->getRelativePathFromHome().',<br>';
							$newpath = stripUrl($dfolder->getAbsolutePath()).DIRECTORY_SEPARATOR.$record->getName();
							// debugMessage($oldpath.' ~ '.$newpath);
								
							# change parentid and save changes
							$record->setFolderID($this->_getParam('destfolderid')); // debugMessage($file->toArray());
							$record->setRestoreID($folder->getID()); // debugMessage($file->toArray());
							try { // save changes and move the files
								$record->save();
								// check if each file exists on file system and move it to destination
								if(file_exists($oldpath) && !file_exists($newpath)){
									full_copy($oldpath, $newpath);
									if(file_exists($newpath)){
										try {
											unlink($oldpath);
											debugMessage('unlink old passed');
										} catch (Exception $e) {
											debugMessage('failed unlink after copy');
										}
									}
								}
								$successcount++;
							} catch (Exception $e) {
								$errcount++;
								$errmsg .= 'Error deleting File <b>'.$record->getName().'</b> - '.$e->getMessage();
							}
						}
						if($successcount > 0){
							$successlink = ""; // debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully moved items. '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
			
				}
				if(!isEmptyString($dirlist)){
					// debugMessage($dirlist);
					$dirarray = explode(',', $dirlist); // debugMessage($dirarray); // exit;
					if(count($dirarray) > 0){
						$dir_collection = new Doctrine_Collection(Doctrine_Core::getTable("Folder"));
						foreach ($dirarray as $id){
							$folder = new Folder();
							$folder->populate($id);
							if(!isEmptyString($folder->getID())){
								$dir_collection->add($folder);
							}
						}
						// debugMessage($file_collection->toArray());
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($dir_collection as $record){
							$oldpath = $record->getAbsolutePath();
							$filestr .= $record->getRelativePathFromHome().',<br>';
							$newpath = $dfolder->getAbsolutePath().$record->getName().DIRECTORY_SEPARATOR;
				
							$record->setParentID($this->_getParam('destfolderid'));
							$record->setPath(stripUrl($dfolder->getPath()).'/'.$dfolder->getID()); // debugMessage($folder->toArray());
							$record->setRestoreID($folder->getID()); // debugMessage($folder->toArray());
							// debugMessage($oldpath.' ~ '.$newpath); // exit;
							try {
								$record->save(); // save changes
								if(is_dir($oldpath) && !is_dir($newpath)){
									full_copy($oldpath, $newpath);
									if(is_dir($newpath)){
										try {
											rrmdir2($oldpath);
											debugMessage('moved passed');
										} catch (Exception $e) {
											debugMessage('failed unlink after copy');
										}
									}
								}
								$successcount++;
							} catch (Exception $e) {
								$errcount++;
								$errmsg .= 'Error deleting Folder <b>'.$record->getName().'</b> - '.$e->getMessage();
							}
						}
						if($successcount > 0){
							$successlink = ""; debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully moved items. '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					}
				} // end check for folders in collection
			} // end dirlist
		} // end moving
		
		// copying
		if($this->_getParam('actiontype') == "2"){ 
			// copying muliple files
			if($this->_getParam('mode') == 'batch'){
				// moving muliple files
				$dfolder = new Folder();
				$dfolder->populate($this->_getParam('destfolderid'));
				
				$filelist = decode($formvalues['filelist']);
				$dirlist = decode($formvalues['dirlist']); // exit;
				// debugMessage($filelist); debugMessage($dirlist); exit;
				$dfolder = new Folder();
				$dfolder->populate($this->_getParam('destfolderid'));
				
				$folder = new Folder();
				$folder->populate($this->_getParam('sourceid')); // debugMessage($folder->toArray()); exit;
				$parentdir = $folder->getParent();
				$homefolder = $folder->getHomeFolder();
				//$trashfolder = $folder->getTrashFolder();
				//$trashpath = $trashfolder->getTrashAbsoluteUrl(); // debugMessage($trashpath);
				
				if(!isEmptyString($filelist)){
					// delete files selected
					$filearray = explode(',', $filelist); // debugMessage($filearray); // exit;
					if(count($filearray) > 0){
						$file_collection = new Doctrine_Collection(Doctrine_Core::getTable("File"));
						foreach ($filearray as $id){
							$fileitem = new File();
							$fileitem->populate($id);
							if(!isEmptyString($fileitem->getID())){
								$file_collection->add($fileitem);
							}
						}
						// debugMessage($file_collection->toArray());
						$errcount = 0; $successcount = 0; $errmsg = '';
						foreach ($file_collection as $file){
							$newfile = new File();
							$data = array(
								'companyid' => $file->getCompanyID(), 
								'filename' => $file->getName(),
								'title' => $file->getTitle(),
								'description' => $file->getDescription(),
								'folderid' => $dfolder->getID(), 
								'restoreid' => '',
								'type' => $file->getType(),
								'extension' => $file->getExtension(),
								'filesize' => $file->getFileSize(),
								'datecreated' => date('Y-m-d H:i:s'),
								'createdby' => $session->getVar('userid')
							);
							
							$newfile->processPost($data);
							/* debugMessage($newfile->toArray());
							debugMessage('error '.$newfile->getErrorStackAsString()); */
							
							$oldpath = $file->getAbsoluteFilePath();
							$filestr .= $file->getRelativePathFromHome().',<br>';
							$newpath = stripUrl($dfolder->getAbsolutePath()).DIRECTORY_SEPARATOR.$file->getName();
							// debugMessage($oldpath.' ~ '.$newpath);
							
							if($newfile->hasError()){
								$errmsg = "Error copying item(s):<br/> ".$newfile->getErrorStackAsString();
								$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
								$errcount++;
							} else { 
								# change parentid and save changes
								try { // save changes and move the files
									$newfile->save();
									// check if each file exists on file system and copy it to destination
									if(file_exists($oldpath) && !file_exists($newpath)){
										chmod($newpath, 777);
										full_copy($oldpath, $newpath);
									}
									$successcount++;
								} catch (Exception $e) {
									$errcount++;
									$errmsg .= 'Error copying File <b>'.$file->getName().'</b> - '.$e->getMessage();
								}
							}
						}
						if($successcount > 0){
							$successlink = ""; // debugMessage($successlink);
							$session->setVar(SUCCESS_MESSAGE, 'Successfully copied items. '.$successlink);
						}
						if($errcount > 0){
							$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
						}
					} // end check for files in array
			
				} // end file copy
				if(!isEmptyString($dirlist)){
					// debugMessage($dirlist);
					$dirarray = explode(',', $dirlist); // debugMessage($dirarray); // exit;
					if(count($dirarray) > 0){
						$dir_collection = new Doctrine_Collection(Doctrine_Core::getTable("Folder"));
						foreach ($dirarray as $id){
							$folder = new Folder();
							$folder->populate($id);
							if(!isEmptyString($folder->getID())){
								$dir_collection->add($folder);
							}
						}
						// debugMessage($file_collection->toArray());
						if($dir_collection->count() > 0){
							$errcount = 0; $successcount = 0; $errmsg = '';
							foreach ($dir_collection as $record){
								$oldpath = $record->getAbsolutePath();
								$filestr .= $record->getRelativePathFromHome().',<br>';
								
								$newfolder = new Folder();
								$data = array(
										'id'=>NULL,
										'companyid' => $record->getCompanyID(),
										'name' => $record->getName(),
										'description' => $record->getDescription(),
										'parentid' => $dfolder->getID(),
										'restoreid' => '',
										'path' => stripUrl($dfolder->getPath()).'/'.$dfolder->getID(),
										'datecreated' => date('Y-m-d H:i:s'),
										'createdby' => $session->getVar('userid')
								); // debugMessage($data);
								
								$newfolder->processPost($data);
								/*debugMessage($newfolder->toArray());
								debugMessage('error '.$newfolder->getErrorStackAsString()); */
								
								$newpath = $dfolder->getAbsolutePath().$folder->getName().DIRECTORY_SEPARATOR;
								if($newfolder->hasError()){
									$errcount++;
									$errmsg = "Error copying item(s):<br/> ".$newfolder->getErrorStackAsString();
									$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
									// exit;
								} else {
									//debugMessage($oldpath.' ~ '.$newpath); // exit;
									try {
										$newfolder->save(); // save changes
										// move the folder on physical storage
										if(is_dir($oldpath) && !is_dir($newpath)){
											full_copy($oldpath, $newpath);
											if(is_dir($newpath)){
												debugMessage('folder '.$newpath.'created ');
											}
										}
										$successcount++;
									} catch (Exception $e) {
										$errcount++;
										$errmsg .= 'Error deleting Folder <b>'.$record->getName().'</b> - '.$e->getMessage();
									}
								}
							}
							if($successcount > 0){
								$successlink = ""; debugMessage($successlink);
								$session->setVar(SUCCESS_MESSAGE, 'Successfully copied items. '.$successlink);
							}
							if($errcount > 0){
								$session->setVar(ERROR_MESSAGE, $errmsg); debugMessage($errmsg);
							}
						} // end check for files in collection
					} // end check for files in array
				} // end dir copy	
					
			} // end batch copy
		} // end copy 
		
		# add log to audit trail
		$view = new Zend_View();
		// $controller = $this->getController();
		$url = decode($this->_getParam('id'));
		$usecase = '2.5';
		$module = '2';
		$type = PATH_MOVE;
		$actionst = "Moved";
		if($this->_getParam('actiontype') == "2"){
			$usecase = '2.4';
			$type = PATH_COPY;
			$actionst = "Copied";
		}
		
		$audit_values = getAuditInstance();
		$audit_values['companyid'] = $session->getVar('companyid');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$transactiondate = date("Y-m-d H:i:s");
		$audit_values['transactiondate'] = $transactiondate;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['transactiondetails'] = $actionst.' Files: 
		<i class="bolded">Source:</i> 
		<i>'.$filestr.'</i> 
		<i>Destination:</i> 
		<i>'.$dfolder->getRelativePathFromHome().'</i>';
		$audit_values['url'] = $dirlist.':'.$filelist;
		//debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		// exit;
		if(!isEmptyString($this->_getParam('successurl'))){
			$this->_helper->redirector->gotoUrl(decode($formvalues['successurl']));
		}
	}
	function filepathAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		
		if(!isArrayKeyAnEmptyString('id', $formvalues)){
			$folder = new Folder();
			$folder->populate($formvalues['id']);
			echo $folder->getRelativePathFromHome();
		}
		echo '';
	}
	
	function systemAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		$system = array('id'=>"");
		if(!isArrayKeyAnEmptyString('id', $formvalues)){
			$folder = new Folder();
			$folder->populate(1); // debugMessage($folder->toArray());
			$system = $folder->getTree($this->_getParam('parentid'));
			// debugMessage($system);
			die('['.json_encode($system).']');
		}
		die(json_encode(array()));
	}
	function accessAction(){
		
	}
	function processaccessAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
	}
	function notifyAction(){
	
	}
	function processnotifyAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
	}
}