<?php

class SearchController extends IndexController  {
	
	function indexAction() {
    	$session = SessionWrapper::getInstance(); 
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$conn = Doctrine_Manager::connection();
		$formvalues = $this->_getAllParams(); //debugMessage($formvalues); // exit;
		$userid = $session->getVar('userid');
		$acl = getACLInstance();
		$custom_query = "";
		
		$q = $formvalues['searchword'];
		$html = '';
		$hasdata = false;
		
		
		if($acl->checkPermission('Project', 'list')){
			$query = "SELECT q.id FROM project as q 
			left join project_outline p on p.projectid = q.id
			WHERE ".$custom_query."
			(q.title like '%".$q."%' or
			q.refno like '%".$q."%' or
			p.refno like '%".$q."%')
			GROUP BY q.id
			order by q.title asc LIMIT 5 ";
			
			// debugMessage($query); exit;
			$result = $conn->fetchAll($query); 
			$count_results = count($result);
			// debugMessage($result); 
			if($count_results > 0){
				$hasdata = true;
				$html .=
				'<div class="panel panel-default" style="max-width:300px;">
				<div class="panel-heading" style="padding:10px; padding-left:25px;">
				<div class="row">
				<div class="col-xs-7 bolded p-0"><h3 class="panel-title" style="padding:0; font-size:13px; ">Projects</h3></div>
				<div class="col-xs-5 p-0 floatright pull-right">
				<a href="'.$this->view->baseUrl('project/list/qterm/'.$q).'" class="blockanchor  floatright pull-right" style="absolute; color:#333; top:-3px; left:0; bottom:0;">.. view all</a>
				</div>
				</div>
				</div>
				<div class="panel-body" style="padding:5px;">
				<ul class="clearfix" style="margin-left:0; padding-left:0;">';
				foreach ($result as $row){
					$project = new Project();
					$project->populate($row['id']);
			
					$b_q='<b>'.$q.'</b>';
					$name= $project->getTitle(); $name = str_ireplace($q, $b_q, $name);
					$refno = $project->getRefNo(); $refno = str_ireplace($q, $b_q, $refno);
					$media = '/uploads/default/medium_group.jpg';
					$typelabel = '';
					$viewurl = $this->view->baseUrl('project/view/id/'.encode($row['id']));
					$html .= '
					<li style="height:auto; min-height:80px;" class="display_box" align="left" url="'.$viewurl.'" theid="'.$row['id'].'">
					<a href="'.$viewurl.'" style="color:#666; text-decoration:none; position:relative; left:0; right:0;">
					<div class="col-sm-2 m-t-0 p-t-0 centeralign">
					<i class="fa fa-briefcase" style="font-size:32px;"></i>
					</div>
					<div class="col-xs-10">
					<span class="name blocked">'.$name.'</span>
					<span class="blocked">'.$project->getTypeLabel().': '.$refno.'</span>
					</div>
					</a>
					</li>';
				}
			}
		}
		
		if($acl->checkPermission('Indicator', 'list')){
			$query = "SELECT q.id FROM indicator as q
			WHERE ".$custom_query."
			(q.title like '%".$q."%' or
			q.refno like '%".$q."%')
			GROUP BY q.id
			order by q.title asc LIMIT 5 ";
				
			// debugMessage($query); exit;
			$result = $conn->fetchAll($query);
			$count_results = count($result);
			// debugMessage($result);
			if($count_results > 0){
				$hasdata = true;
				$html .=
				'<div class="panel panel-default" style="max-width:300px;">
				<div class="panel-heading" style="padding:10px; padding-left:25px;">
				<div class="row">
				<div class="col-xs-7 bolded p-0"><h3 class="panel-title" style="padding:0; font-size:13px; ">Indicators</h3></div>
				<div class="col-xs-5 p-0 floatright pull-right">
				<a href="'.$this->view->baseUrl('indicator/list/qterm/'.$q).'" class="blockanchor floatright pull-right" style="absolute; color:#333; top:-3px; left:0; bottom:0;">.. view all</a>
				</div>
				</div>
				</div>
				<div class="panel-body" style="padding:5px;">
				<ul class="clearfix" style="margin-left:0; padding-left:0;">';
				foreach ($result as $row){
					$indicator = new Indicator();
					$indicator->populate($row['id']);
						
					$b_q='<b>'.$q.'</b>';
					$name= $indicator->getName(); $name = str_ireplace($q, $b_q, $name);
					$refno = $indicator->getRefNo(); $refno = str_ireplace($q, $b_q, $refno);
					$viewurl = $this->view->baseUrl('indicator/view/id/'.encode($row['id']));
					$html .= '
					<li style="height:auto; min-height:80px;" class="display_box clearfix" align="left" url="'.$viewurl.'" theid="'.$row['id'].'">
					<a href="'.$viewurl.'" style="color:#666; text-decoration:none; position:relative; left:0; right:0;">
					<div class="col-sm-2 p-0 m-t-10 centeralign">
					<i class="fa fa-flask" style="font-size:32px;"></i>
					</div>
					<div class="col-xs-10">
					<span class="name blocked">'.$name.'</span>
					<span class="blocked">'.$indicator->getProject()->getTypeLabel().': '.$indicator->getProject()->getRefNo().'</span>
					<span class="blocked">Indicator No#: '.$refno.'</span>
					</div>
					</a>
					</li>';
				}
			}
		}
		
		if($acl->checkPermission('Dataset', 'list')){
			$query = "SELECT q.id FROM data_set as q
			WHERE ".$custom_query."
			(q.name like '%".$q."%' or
			q.refno like '%".$q."%')
			GROUP BY q.id
			order by q.name asc LIMIT 5 ";
		
			// debugMessage($query); exit;
			$result = $conn->fetchAll($query);
			$count_results = count($result);
			// debugMessage($result);
			if($count_results > 0){
				$hasdata = true;
				$html .=
				'<div class="panel panel-default" style="max-width:300px;">
				<div class="panel-heading" style="padding:10px; padding-left:25px;">
				<div class="row">
				<div class="col-xs-7 bolded p-0"><h3 class="panel-title" style="padding:0; font-size:13px; ">Datasets</h3></div>
				<div class="col-xs-5 p-0 floatright pull-right">
				<a href="'.$this->view->baseUrl('dataset/list/qterm/'.$q).'" class="blockanchor  floatright pull-right" style="absolute; color:#333; top:-3px; left:0; bottom:0;">.. view all</a>
				</div>
				</div>
				</div>
				<div class="panel-body" style="padding:5px;">
				<ul class="clearfix" style="margin-left:0; padding-left:0;">';
				foreach ($result as $row){
					$entity = new Dataset();
					$entity->populate($row['id']);
		
					$b_q='<b>'.$q.'</b>';
					$name= $entity->getName(); $name = str_ireplace($q, $b_q, $name);
					$refno = $entity->getRefNo(); $refno = str_ireplace($q, $b_q, $refno);
					$viewurl = $this->view->baseUrl('dataset/view/id/'.encode($row['id']));
					$html .= '
					<li style="height:auto; min-height:80px;" class="display_box" align="left" url="'.$viewurl.'" theid="'.$row['id'].'">
					<a href="'.$viewurl.'" style="color:#666; text-decoration:none; position:relative; left:0; right:0;">
					<div class="col-sm-2 m-t-0 p-t-0 centeralign">
					<i class="fa fa-edit" style="font-size:32px;"></i>
					</div>
					<div class="col-xs-10">
					<span class="name blocked">'.$name.'</span>
					</div>
					</a>
					</li>';
				}
			}
		}
		
		if($acl->checkPermission('Issue Tracking', 'list')){
			$query = "SELECT q.id FROM content_issue as q
			WHERE ".$custom_query."
			(q.name like '%".$q."%' or
			q.refno like '%".$q."%')
			GROUP BY q.id
			order by q.name asc LIMIT 5 ";
		
			// debugMessage($query); exit;
			$result = $conn->fetchAll($query);
			$count_results = count($result);
			// debugMessage($result);
			if($count_results > 0){
				$hasdata = true;
				$html .=
				'<div class="panel panel-default" style="max-width:300px;">
				<div class="panel-heading" style="padding:10px; padding-left:25px;">
				<div class="row">
				<div class="col-xs-7 bolded p-0"><h3 class="panel-title" style="padding:0; font-size:13px; ">Issues Register</h3></div>
				<div class="col-xs-5 p-0 floatright pull-right">
				<a href="'.$this->view->baseUrl('issuetracking/list/qterm/'.$q).'" class="blockanchor  floatright pull-right" style="absolute; color:#333; top:-3px; left:0; bottom:0;">.. view all</a>
				</div>
				</div>
				</div>
				<div class="panel-body" style="padding:5px;">
				<ul class="clearfix" style="margin-left:0; padding-left:0;">';
				foreach ($result as $row){
					$entity = new ContentIssue();
					$entity->populate($row['id']);
		
					$b_q='<b>'.$q.'</b>';
					$name= $entity->getName(); $name = str_ireplace($q, $b_q, $name);
					$refno = $entity->getRefNo(); $refno = str_ireplace($q, $b_q, $refno);
					$viewurl = $this->view->baseUrl('issuetracking/view/id/'.encode($row['id']));
					$html .= '
					<li style="height:auto; min-height:80px;" class="display_box" align="left" url="'.$viewurl.'" theid="'.$row['id'].'">
					<a href="'.$viewurl.'" style="color:#666; text-decoration:none; position:relative; left:0; right:0;">
					<div class="col-sm-2 m-t-0 p-t-0 centeralign">
					<i class="fa fa-tasks" style="font-size:32px;"></i>
					</div>
					<div class="col-xs-10">
					<span class="name blocked">'.$name.'</span>
					</div>
					</a>
					</li>';
				}
			}
		}
		
		if($acl->checkPermission('Risk Mgt', 'list')){
			$query = "SELECT q.id FROM content_risk as q
			WHERE ".$custom_query."
			(q.name like '%".$q."%' or
			q.refno like '%".$q."%')
			GROUP BY q.id
			order by q.name asc LIMIT 5 ";
		
			// debugMessage($query); exit;
			$result = $conn->fetchAll($query);
			$count_results = count($result);
			// debugMessage($result);
			if($count_results > 0){
				$hasdata = true;
				$html .=
				'<div class="panel panel-default" style="max-width:300px;">
				<div class="panel-heading" style="padding:10px; padding-left:25px;">
				<div class="row">
				<div class="col-xs-7 bolded p-0"><h3 class="panel-title" style="padding:0; font-size:13px; ">Risk Register</h3></div>
				<div class="col-xs-5 p-0 floatright pull-right">
				<a href="'.$this->view->baseUrl('risk/list/qterm/'.$q).'" class="blockanchor  floatright pull-right" style="absolute; color:#333; top:-3px; left:0; bottom:0;">.. view all</a>
				</div>
				</div>
				</div>
				<div class="panel-body" style="padding:5px;">
				<ul class="clearfix" style="margin-left:0; padding-left:0;">';
				foreach ($result as $row){
					$entity = new ContentRisk();
					$entity->populate($row['id']);
		
					$b_q='<b>'.$q.'</b>';
					$name= $entity->getName(); $name = str_ireplace($q, $b_q, $name);
					$refno = $entity->getRefNo(); $refno = str_ireplace($q, $b_q, $refno);
					$viewurl = $this->view->baseUrl('risk/view/id/'.encode($row['id']));
					$html .= '
					<li style="height:auto; min-height:80px;" class="display_box" align="left" url="'.$viewurl.'" theid="'.$row['id'].'">
					<a href="'.$viewurl.'" style="color:#666; text-decoration:none; position:relative; left:0; right:0;">
					<div class="col-sm-2 m-t-0 p-t-0 centeralign">
					<i class="fa fa-arrow-circle-o-right" style="font-size:32px;"></i>
					</div>
					<div class="col-xs-10">
					<span class="name blocked">'.$name.'</span>
					</div>
					</a>
					</li>';
				}
			}
		}
		
		# search users if loggedin user has access
		if($acl->checkPermission('User', ACTION_LIST)){
			$query = "SELECT u.id FROM useraccount as u 
				WHERE ".$custom_query."
			   (u.firstname like '%".$q."%' or 
				u.lastname like '%".$q."%' or 
				u.email like '%".$q."%' or 
				u.phone like '%".$q."%' or 
				u.username like '%".$q."%') 
				GROUP BY u.id
				order by concat(u.firstname,' ', u.lastname) asc LIMIT 5 ";
			// debugMessage($query); exit;
			$result = $conn->fetchAll($query);
			$count_results = count($result);
			// debugMessage($result); exit;
			if($count_results > 0){
				$hasdata = true;
				$html .= 
					'<div class="panel panel-default" style="max-width:300px;">
                    	<div class="panel-heading" style="padding:10px; padding-left:25px;">
                    		<div class="row">
                        		<div class="col-xs-8 bolded p-0"><h3 class="panel-title" style="padding:0; font-size:13px; ">Users</h3></div>
								<div class="col-xs-4 p-0">
									<a href="'.$this->view->baseUrl('profile/list/searchterm/'.$q).'" class="blockanchor" style="absolute; color:#333; top:-3px; left:0; bottom:0;">.. view all</a>
								</div>
							</div>
                		</div>
                     	<div class="panel-body" style="padding:5px;">
							<ul class="clearfix" style="margin-left:0; padding-left:0;">';
								foreach ($result as $row){
									$user = new UserAccount();
									$user->populate($row['id']);
										
									$b_q='<b>'.$q.'</b>';
									$name= $user->getName(); $name = str_ireplace($q, $b_q, $name);
									$phone = $user->getPhone(); $phone = str_ireplace($q, $b_q, $phone);
									$email = $user->getEmail(); $email = str_ireplace($q, $b_q, $email);
									$media = $user->getMediumPicturePath();
									$viewurl = $this->view->baseUrl('profile/view/id/'.encode($row['id']));
									$html .= '
									<li style="height:auto; min-height:80px;" class="display_box" align="left" url="'.$viewurl.'" theid="'.$row['id'].'">
										<a href="'.$viewurl.'" style="color:#666; text-decoration:none; position:relative; left:0; right:0;">
											<div class="col-sm-2 p-0 m-t-10">
												<img class="img-circle img-responsive" src="'.$media.'" style="width:100%; height:auto;" />
											</div>
											<div class="col-xs-10">
												<span class="name blocked">'.$name.'</span>
												<span class="blocked" style="margin-top:5px;">Email: '.$email.'</span>
												<span class="blocked">Phone: '.$phone.'</span>
											</div>
										</a>
									</li>';
					
					
								}
						}
					}
		
		# add navigation to searchable parameters
		$result = array(
			'id' => 1,
			'users' => ''
		);
		
		# check no data is available for all areas and return no results message
		if(!$hasdata){
			$html .= '
				<li class="display_box" align="center" style="height:30px;">
					<span style="width:100%; display:block; text-align:center;">No results for <b>'.$q.'</b></span>
				</li>';
		}
		$html .= '</ul></div></div>';
		echo $html;
    }
}