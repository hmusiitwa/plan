<?php

class DirectoryController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Directory";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	if($action == "view" || $action == 'listsubmit' || $action == "listdata" || $controller == "directory" || $action == "list"  || $action == "search"){
	 		return "list";
	 	}
		return parent::getActionforACL();
    }
    
    public function searchAction() {
    	// disable rendering of the view and layout so that we can just echo the AJAX output
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$conn = Doctrine_Manager::connection();
    	$session = SessionWrapper::getInstance();
    	$config = Zend_Registry::get("config");
    	$this->_translate = Zend_Registry::get("translate");
    
    	$formvalues = $this->_getAllParams();
    	// debugMessage($formvalues);
    	//exit();
    	 
    	$q = $formvalues['term'];
    	$query = "SELECT
    	l.id as villageid,
    	l.id as id,
    	l.locationtype as locationtype,
    	l.name as name,
    	l.name as village,
    	l.districtid,
    	d.name as district,
    	l.countyid,
    	c.name as county,
    	l.subcountyid,
    	s.name as subcounty,
    	l.parishid,
    	ps.name as parish
    	FROM location as l
    	left join location d on (l.districtid = d.id and d.locationtype = 2)
    	
    	left join location c on (l.countyid = c.id and c.locationtype = 3)
    	left join location s on (l.subcountyid = s.id and s.locationtype = 4)
    	left join location ps on (l.parishid = ps.id and ps.locationtype = 5)
    	WHERE l.name like '".$q."%' AND (l.locationtype = 6 OR l.locationtype = 5)
    	GROUP BY l.id order by l.name asc, l.locationtype asc ";
    	// debugMessage($query);
    	$data = $conn->fetchAll($query);
    	$count_results = count($data);
    	// debugMessage($data); exit;
    	 
    	echo json_encode($data);
    }
}
