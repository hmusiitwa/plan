<?php

class FController extends IndexController  {
	
	function idAction(){
		$session = SessionWrapper::getInstance();
		# check if user is entering last inserted id
		if($this->_getParam('opendir') == '1'){
			$currenturl = stripUrl($this->view->viewurl);
			$gotoid = encode($session->getVar('gotofolder'));
			$parentid = $this->_getParam('parentid');
			$newurl = stripUrl(str_replace($parentid, $gotoid, $currenturl));
			$newurl = stripUrl(str_replace("opendir/1", "", stripUrl($newurl)));
			// debugMessage('going to fetch last insert id '.$newurl);
			$this->_helper->redirector->gotoUrl($newurl);
		}
	}
	
	function pdfviewAction(){
		$this->_helper->layout->disableLayout();
	}
	function infoAction(){
		$this->_helper->layout->disableLayout();
	}
	function uploadAction(){
		$this->_helper->layout->disableLayout();
	}
	function processinfoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		if(isEmptyString($this->_getParam('id'))){
			$this->_setParam('action', ACTION_CREATE);
			parent::createAction();
		} else {
			$this->_setParam('action', ACTION_EDIT);
			parent::editAction();
		}
	}
}