<?php

class ProjectController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Project";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
		if($action == "edit" || $action == "update"){
	 		return "index";
	 	}
	 	if($action == "view" || $action == 'listsearch' || $action == 'viewsearch' || $action == 'listsubmit' || $action == 'loginfo'){
	 		return "list";
	 	}
		return parent::getActionforACL();
    }
    function viewsearchAction() {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	/* debugMessage($this->getRequest()->getQuery());
    		debugMessage($this->_getAllParams()); exit(); */
    	$this->_helper->redirector->gotoSimple("view", $this->getRequest()->getControllerName(),
    			$this->getRequest()->getModuleName(),
    			array_remove_empty(array_merge_maintain_keys($this->_getAllParams(), $this->getRequest()->getQuery())));
    }
    function loginfoAction() {
    	$this->_helper->layout->disableLayout();
    	
    }
}
