<?php

class MessagingController extends IndexController  {
	
	/**
	 * Get the name of the resource being accessed 
	 *
	 * @return String 
	 */
	function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName()); 
		
		if ($action == "listsearch"){
			return "inbox";
		}
		/* if ($action == "markasread" || $action == "processmassmail" || $action == "processnotification" || 
			$action == "processreply" || $action == "subscriber" || $action == "invite"
		) {
			return ACTION_VIEW; 
		}
		if ($action == "sent" || $action == "sentsearch" ||  $action == "massmail" || $action == "subscriber" || $action == "invite") {
			return ACTION_LIST; 
		} */
		return parent::getActionforACL(); 
	}
	
	public function inboxAction(){
		
	}
	public function listsearchAction(){
		//debugMessage($this->getRequest()->getQuery());
		// debugMessage($this->_getAllParams()); exit();
		$this->_helper->redirector->gotoSimple("inbox", $this->getRequest()->getControllerName(),
				$this->getRequest()->getModuleName(),
				array_remove_empty(array_merge_maintain_keys($this->_getAllParams(), $this->getRequest()->getQuery())));
	}
    public function markasreadAction(){
		// disable rendering of the view and layout so that we can just echo the AJAX output 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$session = SessionWrapper::getInstance(); 
		// debugMessage($this->_getAllParams()); exit();
		// user is deleting more than one message
		$idsarray = array();
		
		$idsarray = $this->_getParam("messagesformarking");
		// remove all empty keys in the array of ids to be deleted
		foreach ($idsarray as $key => $value){
			if(isEmptyString($value)){
				unset($idsarray[$key]);
			}
		}
		// debugMessage($idsarray);
		$messagerecipient = new MessageRecipient();
		// mark selected message details using selected mark action
   		$messagerecipient->markAsRead($idsarray, $this->_getParam("markaction"));
		// debugMessage("Message(s) were successfully marked");
		// fetch number of remaining unread messages for the user 
		$remaining = $messagerecipient->countUnReadMessages($session->getVar('userid'));		
		$session->setVar('unread', $remaining);
		// if no unread messages hide unread label else show unread in brackets
		if($remaining == '0'){
			$newmsghtml = '<a title="Messages" href="'.$this->_helper->url('list', 'message').'"><img src="'.$this->_helper->url('email.png', 'images').'">Messages</a>';		
		} else {
			$newmsghtml = '<a title="Messages" href="'.$this->_helper->url('list', 'message').'"><img src="'.$this->_helper->url('email.png', 'images').'">Messages (<label class="unread">'.$session->getVar('unread').' Unread</label>)</a>';	
		}
		
		$session->setVar('newmsghtml', $newmsghtml);
		echo $session->getVar('newmsghtml');
		
		return false;
	}
	
	function deleteAction(){		
		// disable rendering of the view and layout so that we can just echo the AJAX output 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		if($this->_getParam("deletemultiple") == '1'){
			// debugMessage($this->_getAllParams());
			// user is deleting more than one message
			$idsarray = $this->_getParam("messagesfordelete");
			// remove all empty keys in the array of ids to be deleted
			foreach ($idsarray as $key => $value){
				if(isEmptyString($value)){
					unset($idsarray[$key]);
				}
			}
			// debugMessage($idsarray);
			$message = new MessageRecipient();
			if($message->deleteMultiple($idsarray)){
				debugMessage("Message(s) were successfully deleted");
			} else {
				debugMessage("An error occured in deleting your message(s)");
			}
			
		} else {
			// user is deleting only one message from reply page
			// the messageid being deleted
			$msgid = $this->_getParam("idfordelete");
			// debugMessage($this->_getAllParams());
			
			// populate message to be deleted
			$message = new Message();
			$message->populate($msgid);
			
			if($message->delete()){
				debugMessage("Message was successfully deleted");
			} else {
				debugMessage("An error occured in deleting your message");
			}
		}
		return false;
	}
	
	function processnotificationAction(){
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
		$session = SessionWrapper::getInstance(); 	
	    $config = Zend_Registry::get("config");
	    $message_collection = new Doctrine_Collection(Doctrine_Core::getTable("Message"));
	    
	    $formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit;
	    $recipients_array = array(); 
	    $messagedata = array(); 
	    $users = array(); $phones = array();
	    $execresult = array('result'=>'', 'msg'=>'');
	    
	    $type = $formvalues['type'];
	    if(isEmptyString($this->_getParam('type'))){
	    	$type = 1;
	    }
	    if($type == 1){
	    	$ismail = true; $issms = false;
	    	$msgtype = "mail";
	    }
	    if($type == 2){
	    	$issms = true; $ismail = false;
	    	$msgtype = "sms";
	    }
	    $custom_query = "";
	    if(isArrayKeyAnEmptyString('selecttype', $formvalues)){
	    	$formvalues['selecttype'] = 2;
	    }
	    
	    if($formvalues['selecttype'] == 1){
	    	
	    }
	    if($formvalues['selecttype'] == 2){
	    	if(!isArrayKeyAnEmptyString('userids', $formvalues)){
	    		$users = $formvalues['userids'];
	    	}
	    	if(!isArrayKeyAnEmptyString('usernames', $formvalues)){
	    		$userlist = str_replace(' ', '', $formvalues['usernames']);
	    		$username_array = explode(',', $userlist);
	    		$hasadmin = false;
	    		foreach ($username_array as $username){
	    			$uid = getUserIDFromUsername($username);
	    			if(!isEmptyString($uid)){
	    				$users[] = getUserIDFromUsername($username);
	    			}
	    			if($username == 'support' || $username == 'admin'){
	    				$hasadmin = true;
	    			}
	    		}
	    		if($hasadmin){
	    			$admins = getUsers(1);
	    			foreach ($admins as $k => $anadmin){
	    				if(!in_array($k, $users)){
	    					$users[] = $k;
	    				}
	    			}
	    		}
	    	}
	    }
	    if($formvalues['selecttype'] == 3){
	    	if($ismail){
	    		
	    	}
	    	if($issms){
	    		
	    	}
	    }
	    if($formvalues['selecttype'] == 4){
	    	if($ismail){
	    		$users = getUsersWithEmail();
	    	}
	    	if($issms){
	    		$users = getUsersWithPhone();
	    	}
	    }
	    
	// file upload
	    if($formvalues['selecttype'] == 5){
	    	//if ($_FILES['uploadedfile']['error'] == UPLOAD_ERR_OK & is_uploaded_file($_FILES['uploadedfile']['tmp_name'])) { //checks that file is uploaded
	    		//echo file_get_contents($_FILES['uploadedfile']['tmp_name']);
	    	//}
	    	if(isset($_FILES["filename"]) && $_FILES["filename"]["error"] == UPLOAD_ERR_OK) {
	    		if(!isset($_FILES['filename']['name'])){
	    			$session->setVar(ERROR_MESSAGE, "Error: No file detected for upload!");
	    			$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	    			$execresult = array('result'=>'fail', 'msg'=>"Error: No file detected for upload!");
	    		} else {
	    			$filecontent = file_get_contents($_FILES['filename']['tmp_name']);
	    			// debugMessage($filecontent);
	    			$phones = explode(",", str_replace(' ', '', $filecontent));
	    			if(!stringContains(',', $filecontent)){
	    				$phones = preg_replace('/\s+/', ' ', $phones)/*  preg_split('/\s+/', $phones) */;
	    				if(!isArrayKeyAnEmptyString('0', $phones)){
	    					$phones = explode(" ", $phones[0]);
	    					if(count($phones) > 0){
	    						$phones = array_remove_empty($phones);
	    					}
	    				}
	    			}
	    			if(count($phones) > 0){
	    				$users = $phones;
	    			}
	    		}
	    	}
	    }
	    // inline
	    if($formvalues['selecttype'] == 6){
	    	if(!isArrayKeyAnEmptyString('phone', $formvalues) || !isArrayKeyAnEmptyString('phonetype', $formvalues)){
	    		if($formvalues['phonetype'] == 1){
	    			$phones = array($formvalues['phone']);
	    		}
	    		if($formvalues['phonetype'] == 2){
	    			$phones = explode(",", str_replace(' ', '', $formvalues['multiphone']));
	    			if(!stringContains(',', $formvalues['multiphone'])){
	    				$phones = preg_replace('/\s+/', ' ', $phones)/*  preg_split('/\s+/', $phones) */;
	    				if(!isArrayKeyAnEmptyString('0', $phones)){
	    					$phones = explode(" ", $phones[0]);
	    					if(count($phones) > 0){
	    						$phones = array_remove_empty($phones);
	    					}
	    				}
	    				// debugMessage($phones);
	    			}
	    		}
	    		if(count($phones) > 0){
	    			$users = $phones;
	    		}
	    	}
	    }
	    
	    if(!isEmptyString($this->_getParam('sendername')) && !isEmptyString($this->_getParam('senderemail')) && isEmptyString(getUserID())){
	    	// debugMessage('test');
	    	$admins = getUsers(1);
	    	foreach ($admins as $k => $anadmin){
	    		if(!in_array($k, $users)){
	    			$users[] = $k;
	    		}
	    	}
	    	$formvalues['selecttype'] = 2;
	    }
	   	// debugMessage($users); exit;
	    # if no receipients specified
	    if(count($users) == 0){
	    	$session->setVar(ERROR_MESSAGE, "Error: No Receipients specified!");
	    	$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	    	$execresult = array('result'=>'fail', 'msg'=>"Error: No Receipients specified!");
	    	exit;
	    }
		
	    $messages = array(); $sent = array(); 
	    $messages['contents'] = $formvalues['contents'];
	    $messages['type'] = $formvalues['type'];
	    if(!isArrayKeyAnEmptyString('subjecttype', $formvalues)){
	    	$messages['subjecttype'] = $formvalues['subjecttype'];
	    }
	    if(!isArrayKeyAnEmptyString('subject', $formvalues)){
	    	$messages['subject'] = $formvalues['subject'];
	    } else {
	    	$messages['subject'] = '';
	    }
		$messages['senderid'] = NULL;
		if(!isArrayKeyAnEmptyString('senderid', $formvalues)){
			$messages['senderid'] = $formvalues['senderid'];
		}
		if(!isArrayKeyAnEmptyString('senderemail', $formvalues) && isEmptyString($session->getVar('userid'))){
			$messages['senderemail'] = $formvalues['senderemail'];
		}
		if(!isArrayKeyAnEmptyString('sendername', $formvalues) && isEmptyString($session->getVar('userid'))){
			$messages['sendername'] = $formvalues['sendername'];
		}
		# process receipients depending on select type
		foreach ($users as $key => $userid){
			if($formvalues['selecttype'] == 2 || $formvalues['selecttype'] == 4){	
				$user = new UserAccount();
				$id = '';
				if($formvalues['selecttype'] == 2){
					$id = $userid;
				}
				if($formvalues['selecttype'] == 4){
					$id = $key;
				}
				
				$user->populate($id); // debugMessage($user->toArray());
				$recipients_array[$id]['recipientid'] = $user->getID();
				$messagedata[$id]['id'] = $user->getID();
				$messagedata[$id]['name'] = $user->getName();
				$messagedata[$id]['email'] = $user->getEmail();
				$messagedata[$id]['phone'] = $user->getPhone();
				$messagedata[$id]['sendemail'] = 1;
				if($ismail){
					$sent[] = $user->getName().' ('.$user->getEmail().')';
				}
				if($issms){
					$sent[] = $user->getName().' ('.$user->getPhone().')';
					$phones[] = $user->getPhone();
				}
			}
			
			if($formvalues['selecttype'] == 1 || $formvalues['selecttype'] == 3){
				
			}
			// if upload or inline 
			if($formvalues['selecttype'] == 5 || $formvalues['selecttype'] == 6){
				$messagedata[]['phone'] = trim($userid);
				$sent[] = $userid;
			}
		}
		$messages['recipients'] = $recipients_array;
		$messages['membertotal'] = count($messagedata);
		$messages['usertotal'] = count($recipients_array);
		$messages['type'] = "notification";
		$messages['subtype'] = "new_".$msgtype;
		/* debugMessage($sent); 
		debugMessage($messagedata); exit;  */
		
		// if sending message to a user
		if($formvalues['selecttype'] == 2 || $formvalues['selecttype'] == 4){
			$msg = new Message();
			$msg->processPost($messages);
			/* debugMessage($msg->toArray());
			debugMessage('error is '.$msg->getErrorStackAsString()); exit(); */
			# save the messages to system inbox
			if($msg->hasError()){
				$session->setVar(ERROR_MESSAGE, "Error: ".$msg->getErrorStackAsString());
				$session->setVar(FORM_VALUES, $this->_getAllParams());
				$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
				$execresult = array('result'=>'fail', 'msg'=>"Error: ".$msg->getErrorStackAsString()); // debugMessage($execresult);
				exit;
			} else {
				try {
					$msg->save();
					// send message to emails
					if(count($messagedata) > 0){
						foreach($messagedata as $key => $receipient){
							$msgdetail = new MessageRecipient();
							if(!isArrayKeyAnEmptyString('email', $receipient) && $receipient['sendemail'] == 1){
								$msgdetail->sendInboxEmailNotification($formvalues['senderemail'], $formvalues['sendername'], $messages['subject'], $receipient['email'], $receipient['name'], $messages['contents']);
								// save to audit trail
								$view = new Zend_View();
								$url = '';
								$profiletype = 'Email';
								$usecase = '7.2';
								$module = '7';
								$type = ALERT_EMAIL;
								
								$browser = new Browser();
								$audit_values = $session->getVar('browseraudit');
								$audit_values['module'] = $module;
								$audit_values['usecase'] = $usecase;
								$audit_values['transactiontype'] = $type;
								$audit_values['status'] = "Y";
								$audit_values['userid'] = $session->getVar('userid');
								$audit_values['entityid'] = $key;
								$audit_values['notification'] = $messages['contents'];
								$audit_values['email'] = $receipient['email'];
								$successmessage = "Email sent to ".$receipient['email'];
								$audit_values['transactiondetails'] = $successmessage;
								$audit_values['url'] = $url; // debugMessage($audit_values);
								$this->notify(new sfEvent($this, $type, $audit_values));
							}
						}
					}
					
					// send message to phones
					if(count($phones) > 0){
						$messagechuncks = array_chunk($messagedata, 100, true);
						if(count($messagedata) <= 100){
							$phonelist = implode(',',$phones);
							$result = sendSMSMessage($phones, $messages['contents'], '', '');
							// debugMessage($result);
						} else {
							foreach ($messagechuncks as $key => $messagegrp){
								$phones_temp_array = array();
								foreach ($messagegrp as $keynest => $messageline) {
									$phones_temp_array[] = $messageline['phone'];
								}
								$phonelist = implode(',',$phones_temp_array);
								$result = sendSMSMessage($phones_temp_array, $messages['contents'], '', '');
								// debugMessage($result);
							}
						}
						
						// save to audit trail
						foreach($messagedata as $key => $receipient){
							// log message sending to audit trail
							$view = new Zend_View();
							$url = '';
							$profiletype = 'SMS';
							$usecase = '7.1';
							$module = '7';
							$type = ALERT_SMS;
							
							$browser = new Browser();
							$audit_values = $session->getVar('browseraudit');
							$audit_values['module'] = $module;
							$audit_values['usecase'] = $usecase;
							$audit_values['transactiontype'] = $type;
							$audit_values['status'] = "Y";
							$audit_values['userid'] = $session->getVar('userid');
							$audit_values['entityid'] = $key;
							$audit_values['notification'] = $messages['contents'];
							$audit_values['phone'] = $receipient['phone'];
							if(!isArrayKeyAnEmptyString(4, $result)){
								$audit_values['subject'] = $result[4];
							}
							if(!isArrayKeyAnEmptyString(5, $result)){
								$audit_values['subject'] = $result[5][$receipient['phone']]['id'];
							}
							$successmessage = "SMS alert sent to ".$receipient['phone'];
							$audit_values['transactiondetails'] = $successmessage;
							$audit_values['url'] = $url; // debugMessage($audit_values);
							$this->notify(new sfEvent($this, $type, $audit_values));
						}
					}
					
					// set success messages to the session
					if(count($messagedata) == 1){
						$key = current(array_keys($messagedata));
						if($ismail){
							$rcpt = $messagedata[$key]['name'].' ('.$messagedata[$key]['email'].')';
							$sentmessage = "Message sent to ".$rcpt;
							$session->setVar(SUCCESS_MESSAGE, $sentmessage);
						}
						if($issms){
							$rcpt = $messagedata[$key]['name'].' ('.$messagedata[$key]['phone'].')';
							$sentmessage = "Message sent to ".$rcpt;
							$session->setVar(SUCCESS_MESSAGE, $sentmessage);
						}
					} else {
						
						$sentmessage = "Message successfully sent to <b>".count($messagedata)."</b> recipients. <br />See full list of recipient(s) at the bottom of this page.";
						if(isEmptyString(getUserID())){
							$sentmessage = "Message successfully sent. We shall contact you soon";
						}
						$sentresult = createHTMLListFromArray($sent, 'successmsg alert alert-success');
						if(!isEmptyString(getUserID())){
						$session->setVar('sentlist', $sentresult);
						}
						$session->setVar(SUCCESS_MESSAGE, $sentmessage);
					}
					$execresult = array('result'=>'success', 'msg'=>$sentmessage);
				} catch (Exception $e) {
					$session->setVar(ERROR_MESSAGE, "An error occured in sending the message. ".$e->getMessage());
					$session->setVar(FORM_VALUES, $this->_getAllParams());
					$execresult = array('result'=>'success', 'msg'=>"An error occured in sending the message. ".$e->getMessage());
				}
			}
		}
	    // exit;
	   	$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	   	echo json_encode($execresult);
    }
    
	function processreplyAction(){
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
		$session = SessionWrapper::getInstance(); 	
	    $config = Zend_Registry::get("config");
		
	    $formvalues = $this->_getAllParams(); debugMessage($formvalues);
	    $messages = array();
		$messages['senderid'] = $formvalues['senderid'];
		$messages['parentid'] = $formvalues['parentid'];
		$messages['subject'] = $formvalues['subject'];
		$messages['contents'] = $formvalues['contents'];
		$recipients_array = array(); $users = array();
		$users = $formvalues['recipientids'];
		foreach ($users as $userid){
			$recipients_array[$userid]['recipientid'] = $userid;
		}
		
		$messages['recipients'] = $recipients_array;
		// debugMessage($messages); 
		
		$msg = new Message();
		$msg->processPost($messages);
		/*debugMessage($msg->toArray());
		debugMessage('error is '.$msg->getErrorStackAsString()); exit();*/
		// save the messages to system inbox
		if($msg->hasError()){
			$session->setVar(ERROR_MESSAGE, "An error occured in sending the message. ".$msg->getErrorStackAsString());
		} else {
			try {
				$msg->save();
				// copy message to recepient's email of specified  / required for admin contact
				$messagereceipients = $msg->getRecipients();
				if($this->_getParam('copytoemail') == 1){
					foreach ($messagereceipients as $messageuser){
						if(!isEmptyString($messageuser->getRecipient()->getEmail())){
							$messageuser->sendInboxEmailNotification();
						}
					}
				}
				if($this->_getParam('copytophone') == 1){
					foreach ($messagereceipients as $messageuser){
						if(!isEmptyString($messageuser->getRecipient()->getPhone())){
							# check if user has phone number on profile
							$messageuser->sendSmsNotification();
						}
					}
				}
				// copy message to user's phone if specified
				$session->setVar(SUCCESS_MESSAGE, "Message successfully replied. ");
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, "An error occured in sending the message. ".$e->getMessage());
			}
		}
		$this->_helper->redirector->gotoUrl(decode($formvalues[URL_SUCCESS]));
	    // exit();
    }
}