<?php

class TestController extends IndexController  {
	
	function smsAction(){
		// disable rendering of the view and layout so that we can just echo the AJAX output
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
			
		$session = SessionWrapper::getInstance();
		$formvalues = $this->_getAllParams();
		// debugMessage($formvalues);
			
		$phone = $this->_getParam('phone');
		$message = $this->_getParam('msg');
		$sender =  $this->_getParam('sender');
			
		if(isEmptyString($message)){
			$message = "Dear User, This is an automated test message from HRMagic Route-SMS route - ".time()." please ignore";
		}
		if(isEmptyString($phone)){
			$phone = getSmsTestNumber();
		}
		if(isEmptyString($sender)){
			$sender = 'INFOSMS';
		}
		 
		sendSMSMessage($phone, $message, $sender);
	}
	
	function emailAction(){
    	$this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(TRUE);
	    
	 	$subject = isEmptyString($this->_getParam('subject')) ? 'Test Email' : $this->_getParam('subject');
	    $msg = $this->_getParam('msg');
	    if(isEmptyString($msg)){
	    	$msg = '--------------------------<br> This is a test message from '.getAppName().'. <br>Timestamp: '.time().' <br> Please Ignore<br>--------------------------';
	    }
	    
	    $to = $this->_getParam('to');
	    if(isEmptyString($to)){
	    	echo "Please specify receipient ('to') "; 
	    	exit;
	    } 
	    
    	if($this->_getParam('usedefault') == 1){
    		$to = 'hmanmstw@gmail.com';
    		if(APPLICATION_ENV == 'development'){
    			$to = 'test@devmail.infomacorp.com';
    		}
    	}
    	
    	debugMessage('Testing mail. Please wait...<br>--------------------------'); // exit;
    	sendTestMessage($subject, $msg, $to);
	    
	    
    }
    function clearcacheAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$path = APPLICATION_PATH.DIRECTORY_SEPARATOR."temp"; // debugMessage($path);
    	$cachefiles = glob($path.DIRECTORY_SEPARATOR.'*zend*'); debugMessage($cachefiles);
    	foreach ($cachefiles as $afile){
    		if(is_file($afile)){
    			unlink($afile);
    		}
    	}
    	debugMessage('cache deleted');
    }
    
    function balanceAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	debugMessage('Fetching sms balance...');
    	
    	checkSMSBalance();
    	debugMessage($session->getVar('currency').''.formatMoneyOnly($session->getVar('balance'),2));
    }
    
    function reportAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$time = time(); debugMessage($this->_getAllParams());// debugMessage($_GET);
    }
}
