<?php

class ValidationController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Validation";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
		if($action == "update" || $action == "create" || $action == "edit"){
	 		return "index";
	 	}
	 	if($action == "view" || $action == 'listsearch' || $action == 'listsubmit'){
	 		return "list";
	 	}
		return parent::getActionforACL();
    }
}