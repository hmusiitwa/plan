<?php

class IndicatorController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	public function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		return "Indicator";
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); 
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	if($action == "view" || $action == 'listsearch' || $action == 'listsubmit' || $action == "populateframe" || $action == 'fyearinfo' || $action == 'projectsavesuccess' || $action == 'setelements' || $action == 'processelements' || $action == 'setexpression' || $action == 'cryptvalue' || $action == 'processexpression'){
	 		return "list";
	 	}
	 	if($action == "index" || $action == "update" || $action == "create" || $action == "edit" || $action == "update" || $action == 'addtarget' || $action == 'processtarget' || $action == 'addtargetsuccess' || $action == "selectindicator" || $action == "processcpi"){
	 		return "index";
	 	}
		return parent::getActionforACL();
    }
    function fyearinfoAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$fyear = new ContentYear();
    	$fyear->populate($this->_getParam('id'));
    	$data = $fyear->toArray();
    	$data['startdate'] = date('M j, Y', strtotime($data['startdate']));
    	$data['enddate'] = date('M j, Y', strtotime($data['enddate']));
    	
    	// debugMessage($data);
    	echo json_encode($data);
    }
    
    function projectsavesuccessAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams();
    	// debugMessage($formvalues);
    	
    	$successurl = decode($this->_getParam(URL_SUCCESS));
    	if(!isEmptyString($this->_getParam('id')) && stringContains('details', $successurl)){
    		$successurl .= '/vid/'.$this->_getParam('id');
    	}
    	// debugMessage($successurl);
    	$this->_helper->redirector->gotoUrl($successurl);
    }
    
    function populateframeAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	if($this->_getParam('datatype') == 'objective'){
	    	$program = new Program();
	    	$program->populate($this->_getParam('id')); // debugMessage($com->toArray());
	    	$level = 'objective';
	    	$countindicators = countIndicatorsForObjective($this->_getParam('id')); // debugMessage('count is '.$countindicators);
	    	$alphabet = range('A', 'Z');
	    	$nextindicator = strtolower($alphabet[$countindicators]);
	    	$data = array(
	    			'id' => $program->getID(),
	    			'title' => $program->getName(),
	    			'refno' => $program->getCode(),
	    			'level' => 1,
	    			'leveltext' => $level,
	    			'noofindicators' => $countindicators,
	    			'nextalp' => $nextindicator,
	    			'nextref' => $program->getCode().'.'.$nextindicator,
	    			'weighttaken' => sumWeightForObjectiveIndicators($this->_getParam('id'))
	    	);
    	} else {
    		$frame = new ContentFrame();
    		$frame->populate($this->_getParam('id')); // debugMessage($frame->toArray());
    		
    		switch($frame->getLevel()){
    			case 2:
    				$level = 'outcome';
    				break;
    			case 3:
    				$level = 'output';
    				break;
    			default:
    				$level = '';
    				break;
    		}
    		$countindicators = countIndicatorsForContent($this->_getParam('id'), $frame->getLevel()); // debugMessage('count is '.$countindicators);
    		$alphabet = range('A', 'Z');
    		$nextindicator = strtolower($alphabet[$countindicators]);
    		$data = array(
    				'id' => $frame->getID(),
    				'title' => stripslashes($frame->getTitle()),
    				'refno' => $frame->getRefno(),
    				'level' => $frame->getLevel(),
    				'leveltext' => $level,
    				'noofindicators' => $countindicators,
    				'nextalp' => $nextindicator,
    				'nextref' => $frame->getRefno().'.'.$nextindicator,
    				'weighttaken' => sumWeightIndicatorsForContent($this->_getParam('id'), $frame->getLevel())
    		);
    	}
    	// debugMessage($data);
    	echo json_encode($data);
    }
    
    function setelementsAction(){
    	 
    }
    function processelementsAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	
    }
    function addtargetAction(){
    	
    }
    function addtargetsuccessAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$session->setVar(SUCCESS_MESSAGE, "Successfully saved changes");
    }
    function processtargetAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	$this->_setParam('action', 'create');
    	
    	parent::createAction();
    }
    function setexpressionAction(){
    	 
    }
    function cryptvalueAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$key =  my_simple_crypt($this->_getParam('key'));
    	echo json_encode(array('key' => $key));
    	
    	/*$string = '10';
    	 $encrypted = my_simple_crypt($string, 'e');
    	$decrypted = my_simple_crypt($encrypted, 'd');
    	var_dump($encrypted);
    	var_dump($decrypted);*/
    }
    function processexpressionAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
    	
    	$indicator = new Indicator();
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	 
    	if(!isEmptyString($id)){
    		$indicator->populate($id);
    		$formvalues['lastupdatedby'] = $session->getVar('userid');
    	} else {
    		$formvalues['createdby'] = $session->getVar('userid');
    	}
    	 
    	$indicator->processPost($formvalues);
    	/* debugMessage($frame->toArray());
    	 debugMessage('errors are '.$frame->getErrorStackAsString()); exit(); */
    	 
    	if($indicator->hasError()){
    		$session->setVar(ERROR_MESSAGE, $indicator->getErrorStackAsString());
    	} else {
    		try {
    			$indicator->save(); // debugMessage($payment->toArray());
    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
    		} catch (Exception $e) {
    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
    		}
    	}
    }
    
    function selectindicatorAction(){
    
    }
    function processcpiAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); debugMessage($formvalues);
    	 
    	$classname = $this->_getParam('entityname');
    	$entity = new $classname;
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	 
    	$entity->populate($id);
    	if($entity->getType() == 4){
	    	$formvalues['lastupdatedby'] = getUserID();
	    	$formvalues['id'] = $id;
	    	$entity->processPost($formvalues); // exit;
	    	/* debugMessage($entity->toArray());
	    	debugMessage('errors are '.$entity->getErrorStackAsString()); exit(); */
	    	 
	    	if($entity->hasError()){
	    		$session->setVar(ERROR_MESSAGE, $entity->getErrorStackAsString());
	    	} else {
	    		try {
	    			$entity->save(); // debugMessage($payment->toArray());
	    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
	    		} catch (Exception $e) {
	    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
	    		}
	    	}
    	}
    	if($entity->getType() == 1){
    		$entity_collection = new Doctrine_Collection(Doctrine_Core::getTable("Indicator"));
    		if(!isArrayKeyAnEmptyString('indicatorids', $formvalues)){
    			foreach ($formvalues['indicatorids'] as $id){
    				$indicator = new Indicator();
    				$indicator->populate($id);
    				if(isEmptyString($indicator->getCSIndicators())){
    					$indicatorids = array($entity->getID());
    					$indicator->setCSIndicators(arrayToCommaString($indicatorids));
    					$entity_collection->add($indicator);
    				} else {
    					$indicatorids = commaStringToArray($indicator->getCSIndicators());
    					$indicatorids[] = $entity->getID(); // debugMessage($indicatorids);
    						$indicator->setCSIndicators(arrayToCommaString(array_unique($indicatorids)));
    					$entity_collection->add($indicator);
    				}
    			}
    		}
    		$currentindicatorids = array();
    		$conn = Doctrine_Manager::connection();
    		$query = "select q.id from indicator q where FIND_IN_SET('".$entity->getID()."', q.csindicators) > 0 "; 
    		$results = $conn->fetchAll($query);
    		$p = 0;
    		foreach($results as $entry){ //  debugMessage($entry['id']);
    			$currentindicatorids[$p] = $entry['id'];
    			$p++;
    		}
    		// debugMessage($currentindicatorids);
    		$newids = isArrayKeyAnEmptyString('indicatorids',  $formvalues) ? array() : $this->_getParam('indicatorids'); // debugMessage($newids); exit;
    		foreach ($currentindicatorids as $lineid){
    			if(!in_array($lineid, $newids) || count($newids) == 0){
    				$indicator = new Indicator();
    				$indicator->populate($lineid);
    				$indicator->setCSIndicators("");
    				$entity_collection->add($indicator);
    			}
    		}
    		/* debugMessage($entity_collection->toArray());
    		exit; */
    		if($entity_collection->count() > 0){
	    		try {
	    			$entity_collection->save(); // debugMessage($payment->toArray());
	    			$session->setVar(SUCCESS_MESSAGE, $this->_getParam('successmessage'));
	    		} catch (Exception $e) {
	    			$session->setVar(ERROR_MESSAGE, $e->getMessage()); // debugMessage('save error '.$e->getMessage());
	    		}
    		}
    	}
    }
}
