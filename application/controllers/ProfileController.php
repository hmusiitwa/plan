<?php

class ProfileController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	function getResourceForACL() {
		$controller = strtolower($this->getRequest()->getControllerName());
		$action = strtolower($this->getRequest()->getActionName());
		$tab = $this->_getParam('tab');
		
		if($action == "processprofile"){
			return "Profile";
		}
		
		if($action == "picture" || $action == "processpicture" || $action == "uploadpicture" || $action == "croppicture") {
			$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
			if($id == getUserID()){
				return "Profile";
			} else {
				return "User";
			}
		}
		
		if(($controller == "profile")){
			if($action == "create" || $action == "list" || $action == "listsubmit" || $action == "listsearch" || $action == "delete" || $action == "resetpassword" || $action == "activateacc" || $action == "processinvite" || $action == "updatestatus" || $tab == "permissions" || $tab == "editpermissions" || $action == "processpermissions" || $action == "editrole" || $action == "processeditrole" || $action == "resetpermissions" || $action == 'employeestats' || $action == 'employeedetails' || $action == "bulkupdate" || $action == "processbulkupdate" || $action == "bulkdetails"){
				return "User";
			}
			if($action == "index" || $action == "update" || $action == "view") {
				$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
				if(!isEmptyString($id)){
					if($this->_getParam('tab') == "tbd"){
						return "User TBD";
					}
					if($this->_getParam('tab') == "account" && $action == "view"){
						return "Profile";
					}
					if($this->_getParam('tab') == "account" && $action == "index"){
						return "User";
					}
					if($id != getUserID()){
						return "User";
					} 
				} else {
					return "User";
				}
			}
		}
		
		return parent::getResourceForACL();
	}
	
	/**
	 * Override unknown actions to enable ACL checking 
	 * 
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	function getActionforACL() {
	 	$action = strtolower($this->getRequest()->getActionName()); // debugMessage($action);
	 	$controller = strtolower($this->getRequest()->getControllerName());
	 	$tab = $this->_getParam('tab');
	 	
	 	if($action == "processprofile"){
	 		return "view";
	 	}
	 	
		if($action == "index" || $action == "update") {
			$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
			if(!isEmptyString($id) && (
					$this->_getParam('tab') == "tbd" 
				)
			){
				return "modify";
			}
			if(!isEmptyString($id) && isEmptyString($this->_getParam('tab'))){
				return "update";
			}
			if(!isEmptyString($id) && $this->_getParam('tab') == "account"){
				return "update";
			}
			if(isEmptyString($id)){
				return "index";
			}
		}
		
		if($action == "edit" || $action == "updatestatus") {
			return "update";
		}
		if($action == "picture" || $action == "processpicture" || $action == "uploadpicture" || $action == "croppicture") {
			return "uploadpicture";
		}
		if($action == "viewinvitation" || $action == "processprofile") {
			return "view";
		}
		if($action == "processchangepassword") {
			return "changepassword";
		}
		if($action == "activateacc" || $action == "processinvite") {
			return "invite";
		}
		if($action == "updatestatus") {
			return "deactivate";
		}
		if($action == "processuploadpicture" || $action == "croppicture" || $action == "uploadpicture") {
			return "uploadpicture";
		}
		if($tab == "permissions" || $tab == "editpermissions" || $action == "processpermissions" || $action == "editrole" || $action == "processeditrole" || $action == "resetpermissions") {
			return "permissions";
		}
		return parent::getActionforACL();
    }
    
    function viewAction() {
    	$session = SessionWrapper::getInstance();
    	$failurl = $this->view->baseUrl("index/accessdenied");
    	$acl = getACLInstance();
    	$id = is_numeric($this->_getParam('id')) ? $this->_getParam('id') : decode($this->_getParam('id'));
    		
    	if(!isEmptyString($id) && !$acl->checkPermission('User', 'list')){
    		if(getUserID() != $id){
    			$this->_helper->redirector->gotoUrl($failurl);
    		}
    	}
    	parent::viewAction();
    }
    
    function indexAction(){
    	
    }
    
    function updateAction(){
    	$session = SessionWrapper::getInstance();
    	$failurl = $this->view->baseUrl("index/accessdenied");
    	$acl = getACLInstance();
    	$id = decode($this->_getParam('id'));
    	$tab = $this->_getParam('tab');
    	    	
    	if(!isEmptyString($id) && !$acl->checkPermission('User', 'update')){
    		if(getUserID() != $id){
    			$this->_helper->redirector->gotoUrl($failurl);
    		}
    	}
    }
    
    function processupdateAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	// $formvalues = $this->_getAllParams();
    	// debugMessage($formvalues); // exit;
    	parent::editAction();
    }
    
    function processprofileAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	
    	$formvalues = $this->_getAllParams(); // debugMessage($this->_getAllParams()); 
    	$params = $_REQUEST;
    	$params['id'] = decode($params['id']); // debugMessage($params);
    	/* $b = array();
    	parse_str(file_get_contents("php://input"), $b);
    	debugMessage($b); */
    	// exit;
    	$new_object = new UserAccount();
    	
    	$response_params = array();
    	if (isEmptyString($this->_getParam('id'))) {
    		$this->_setParam('createdby', $session->getVar('userid'));
    		$new_object->merge(array_remove_empty($this->_getAllParams()), false);
    		$action = ACTION_CREATE;
    	} else {
    		$response_params['id'] = $this->_getParam('id'); 
    		$this->_setParam('id', decode($this->_getParam('id'))); 
    		$new_object->populate($this->_getParam('id')); 
    		$this->_setParam('lastupdatedby', $session->getVar('userid'));
    		$action = ACTION_EDIT;
    	}
    
    	$new_object->processPost($this->_getAllParams());
    	/* if($new_object->hasError()){
    		debugMessage($new_object->toArray());
    		debugMessage('errors are '.$new_object->getErrorStackAsString());
    	} else {
    		debugMessage('no errors found');
    	}
    	exit(); */
    
    	if ($new_object->hasError()) {
    		if(isEmptyString($this->_getParam('ispopup'))){
    			$session->setVar(FORM_VALUES, $this->_getAllParams());
    			$session->setVar(ERROR_MESSAGE, $new_object->getErrorStackAsString());
    			$response_params['id'] = encode($this->_getParam('id'));
    			if (isEmptyString($this->_getParam(URL_FAILURE))) {
    				$this->_helper->redirector->gotoSimple('index', # the action
    						$this->getRequest()->getControllerName(), # the current controller
    						$this->getRequest()->getModuleName(), # the current module,
    						$response_params
    				);
    				return false;
    			} else {
    				$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)), $response_params);
    				return false;
    			}
    		} else {
    			$resultarray = array(
    					'id' => '',
    					'name' => '',
    					'result' => 'error',
    					'message' => 'Error: '.$new_object->getErrorStackAsString()
    			);
    			die(json_encode($resultarray));
    		}
    	}
    
    	// save the object to the database
    	if (isEmptyString($this->_getParam('ispopup'))) {
    		try {
    			switch ($action) {
    				case ACTION_CREATE :
    					if(in_array($new_object->getTableName(), array('useraccount'))){
    						$new_object->transactionSave();
    					} else {
    						$new_object->beforeSave(); // exit;
    						$new_object->save();
    						$new_object->afterSave();
    					}
    					break;
    				case ACTION_EDIT:
    					$new_object->beforeUpdate($this->_getParam('location'));
    					$new_object->save();
    					$new_object->afterUpdate(true, $this->_getParam('location'));
    					break;
    				default :
    					break;
    			}
    
    			// add a success message, if any, to the session for display
    			if (!isEmptyString($this->_getParam(SUCCESS_MESSAGE))) {
    				$session->setVar(SUCCESS_MESSAGE, $this->_translate->translate($this->_getParam(SUCCESS_MESSAGE)));
    			}
    			if (isEmptyString($this->_getParam(URL_SUCCESS))) {
    				$response_params['id'] = encode($new_object->getID());
    				$this->_helper->redirector->gotoSimple('view', # the action
    						$this->getRequest()->getControllerName(), # the current controller
    						$this->getRequest()->getModuleName(), # the current module,
    						$response_params # the parameters for the response
    				);
    				return false;
    			} else {
    				if($this->_getParam(URL_SUCCESS) == $this->_getParam(URL_FAILURE) && $this->_getParam('controller') == 'indicator'){
    					$this->_setParam('nosuccessid', '1');
    				}
    				$url = decode($this->_getParam(URL_SUCCESS));
    				/* debugMessage($formvalues);
    				 debugMessage($url); */
    				if(!isArrayKeyAnEmptyString('nosuccessid', $formvalues)){
    					//debugMessage('go to full url');
    					$this->_helper->redirector->gotoUrl($url);
    				} else {
    					// check if the last character is a / then add it
    					if (substr($url, -1) != "/") {
    						// add the slash
    						$url.= "/";
    					}
    					// add the ID parameter
    					$url.= "id/".encode($new_object->getID()); // debugMessage('add id to url');
    					$this->_helper->redirector->gotoUrl($url, $response_params);
    				}
    				// exit;
    				return false;
    			}
    
    		} catch (Exception $e) {
    			$session->setVar(FORM_VALUES, $this->_getAllParams());
    			$session->setVar(ERROR_MESSAGE, $e->getMessage());
    			$this->_logger->err("Error: ".$e->getMessage());
    			// debugMessage($e->getMessage()); exit();
    
    			// return to the create page
    			if (isEmptyString($this->_getParam(URL_FAILURE))) {
    				$this->_helper->redirector->gotoSimple('index', # the action
    						$this->getRequest()->getControllerName(), # the current controller
    						$this->getRequest()->getModuleName(), # the current module,
    						$response_params
    				);
    				return false;
    			} else {
    				$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)), $response_params);
    				return false;
    			}
    		}
    	} else {
    		try {
    			switch ($action) {
    				case ACTION_CREATE :
    					$new_object->beforeSave();
    					$new_object->save();
    					$new_object->afterSave();
    					break;
    				case ACTION_EDIT:
    					$new_object->beforeUpdate($this->_getParam('location'));
    					$new_object->save();
    					$new_object->afterUpdate(true, $this->_getParam('location'));
    					break;
    				default :
    					break;
    			}
    			$resultarray = array(
    					'id' => $new_object->getID(),
    					'name' => '',
    					'result' => 'success',
    					'message' => 'Successfully saved'
    			);
    		} catch (Exception $e) {
    			$resultarray = array(
    					'id' => '',
    					'name' => '',
    					'result' => 'error',
    					'message' => 'Error: '.$e->getMessage()
    			);
    		}
    		die(json_encode($resultarray));
    	}
    }
    
    function processchangepasswordAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$session = SessionWrapper::getInstance();
    	$this->_translate = Zend_Registry::get("translate");
    	if(!isEmptyString($this->_getParam('password'))){
    		$user = new UserAccount();
    		$user->populate(decode($this->_getParam('id')));
    		// debugMessage($user->toArray()); // exit;
    		#validate that the passwords aint the same
    		if($user->getPassword() != sha1($this->_getParam('currentpassword'))){
    			$session->setVar(ERROR_MESSAGE, "Invalid current Password"); // debugMessage('Invalida current');
    			exit;
    		}
    		if($this->_getParam('currentpassword') == $this->_getParam('password')){
    			$session->setVar(ERROR_MESSAGE, "New Password cannot be the same as the current passowrd!");
    			// debugMessage('New same as old');
    			exit;
    		}
    		
    		# Change password
    		try {
    			$user->changePassword($this->_getParam('password'));
    			$this->clearSession();
    			$session->setVar(SUCCESS_MESSAGE, "Password successfully updated. Please login using your new password"); // debugMessage('passed');
    			// $this->_redirect($this->view->baseUrl('index/profileupdatesuccess'));
    			exit;
    		} catch (Exception $e) {
    			$err = "Error in changing Password. ".$e->getMessage();
    			$session->setVar(ERROR_MESSAGE, $err);
    			// debugMessage($err);
    			exit;
    		}
    	} else {
    		$session->setVar(ERROR_MESSAGE, "No Password provided. Please try again");
    		// debugMessage('No password deteected');
    		exit;
    	}
    }

    function resetpasswordAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$this->_translate = Zend_Registry::get("translate");
    	$id = decode($this->_getParam('id')); // debugMessage($id);
    
    	$user = new UserAccount();
    	$user->populate($id); debugMessage($user->toArray());
    	// $formvalues = array('email'=>$user->getEmail());
    	$user->setEmail($user->getEmail());
    	// debugMessage('error '.$user->getErrorStackAsString()); exit();
    	if ($user->recoverPassword()) {
    		$session->setVar(SUCCESS_MESSAGE, sprintf($this->_translate->translate('profile_change_password_admin_confirmation'), $user->getName()));
    		// send a link to enable the user to recover their password
    		// debugMessage('no error found ');
    
    		$view = new Zend_View();
    		$url = $this->view->serverUrl($this->view->baseUrl('profile/view/id/'.encode($user->getID())));
    		$usecase = '1.9';
    		$module = '1';
    		$type = USER_RESET_PASSWORD;
    		$details = "Reset password request. Reset link sent to <a href='".$url."' class='blockanchor'>".$user->getName()."</a>";
    
    		$browser = new Browser();
    		$audit_values = $session->getVar('browseraudit');
    		$audit_values['module'] = $module;
    		$audit_values['usecase'] = $usecase;
    		$audit_values['transactiontype'] = $type;
    		$audit_values['userid'] = $session->getVar('userid');
    		$audit_values['url'] = $url;
    		$audit_values['transactiondetails'] = $details;
    		$audit_values['status'] = "Y";
    		// debugMessage($audit_values);
    		$this->notify(new sfEvent($this, $type, $audit_values));
    		 
    	} else {
    		$session->setVar(ERROR_MESSAGE, $user->getErrorStackAsString());
    		$session->setVar(FORM_VALUES, $this->_getAllParams()); // debugMessage('no error found ');
    	}
    	// exit();
    	$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_SUCCESS)));
    }
    
    function updatestatusAction() {
    	$this->_setParam("action", ACTION_DELETE);
    
    	$session = SessionWrapper::getInstance();
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$formvalues = $this->_getAllParams(); debugMessage($formvalues);
    	$successurl = decode($formvalues[URL_SUCCESS]);
    	$successmessage = '';
    	if(!isArrayKeyAnEmptyString(SUCCESS_MESSAGE, $formvalues)){
    		$successmessage = $this->_translate->translate($this->_getParam(SUCCESS_MESSAGE));
    	}
    
    	$user = new UserAccount();
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']);
    	$user->populate($id);
    	// debugMessage($successmessage);
    	/* debugMessage($user->toArray());
    	exit(); */
    	if($user->deactivateAccount($formvalues['status'])) {
    		if(!isEmptyString($successmessage)){
    			$session->setVar(SUCCESS_MESSAGE, $successmessage);
    		}
    		$this->_helper->redirector->gotoUrl($successurl);
    	}
    
    	return false;
    }
    
	function inviteuserAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams(); // debugMessage($this->_getAllParams());
    	
    	$user = new UserAccount();
    	$user->populate($formvalues['id']);
    	// debugMessage($user->toArray()); exit();
    	
    	try {
    		$user->inviteOne();
    		$session->setVar('invitesuccess', "Email Invitation sent to ".$user->getEmail());
    	} catch (Exception $e) {
    		$session->setVar(ERROR_MESSAGE, $e->getMessage());
    	}
    	
    	$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_SUCCESS)));
    	// exit();
    }

	function processpictureAction() {
    	// disable rendering of the view and layout so that we can just echo the AJAX output
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$session = SessionWrapper::getInstance();
    	$config = Zend_Registry::get("config");
    	$this->_translate = Zend_Registry::get("translate");
    
    	$formvalues = $this->_getAllParams();
    	 
    	// debugMessage($this->_getAllParams());
    	$user = new UserAccount();
    	$user->populate(decode($this->_getParam('id')));
    
    	// only upload a file if the attachment field is specified
    	$upload = new Zend_File_Transfer();
    	// set the file size in bytes
    	$upload->setOptions(array('useByteString' => false));
    
    	// Limit the extensions to the specified file extensions
    	$upload->addValidator('Extension', false, $config->uploads->photoallowedformats);
    	$upload->addValidator('Size', false, $config->uploads->photomaximumfilesize);
    
    	// base path for profile pictures
    	$destination_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_";
    
    	// determine if user has destination avatar folder. Else user is editing there picture
    	if(!is_dir($destination_path.$user->getID())){
    		// no folder exits. Create the folder
    		mkdir($destination_path.$user->getID(), 0777, true);
    		chmod($destination_path.$user->getID(), 0777);
    	}
    
    	// set the destination path for the image
    	$profilefolder = $user->getID();
    	$destination_path = $destination_path.$profilefolder.DIRECTORY_SEPARATOR."avatar";
    
    	if(!is_dir($destination_path)){
    		mkdir($destination_path, 0777, true);
    		chmod($destination_path, 0777);
    	}
    	// create archive folder for each user
    	$archivefolder = $destination_path.DIRECTORY_SEPARATOR."archive";
    	if(!is_dir($archivefolder)){
    		mkdir($archivefolder, 0777, true);
    		chmod($archivefolder, 0777);
    	}
    
    	$oldfilename = $user->getProfilePhoto();
    
    	//debugMessage($destination_path);
    	$upload->setDestination($destination_path);
    
    	// the profile image info before upload
    	$file = $upload->getFileInfo('profileimage');
    	$uploadedext = findExtension($file['profileimage']['name']);
    	$currenttime = mktime();
    	$currenttime_file = $currenttime.'.'.$uploadedext;
    	// debugMessage($file);
    
    	$thefilename = $destination_path.DIRECTORY_SEPARATOR.'base_'.$currenttime_file;
    	$thelargefilename = $destination_path.DIRECTORY_SEPARATOR.'large_'.$currenttime_file;
    	$updateablefile = $destination_path.DIRECTORY_SEPARATOR.'base_'.$currenttime;
    	$updateablelarge = $destination_path.DIRECTORY_SEPARATOR.'large_'.$currenttime;
    	// exit();
    	// rename the base image file
    	$upload->addFilter('Rename',  array('target' => $thefilename, 'overwrite' => true));
    	// exit();
    	// process the file upload
    	if($upload->receive()){
    		// debugMessage('Completed');
    		$file = $upload->getFileInfo('profileimage');
    		// debugMessage($file);
    			
    		$basefile = $thefilename;
    		// convert png to jpg
    		if(in_array(strtolower($uploadedext), array('png','PNG','gif','GIF'))){
    			ak_img_convert_to_jpg($thefilename, $updateablefile.'.jpg', $uploadedext);
    			unlink($thefilename);
    		}
    		$basefile = $updateablefile.'.jpg';
    			
    		// new profilenames
    		$newlargefilename = "large_".$currenttime_file;
    		// generate and save thumbnails for sizes 250, 125 and 50 pixels
    		resizeImage($basefile, $destination_path.DIRECTORY_SEPARATOR.'large_'.$currenttime.'.jpg', 400);
    		resizeImage($basefile, $destination_path.DIRECTORY_SEPARATOR.'medium_'.$currenttime.'.jpg', 165);
    			
    		// unlink($thefilename);
    		unlink($destination_path.DIRECTORY_SEPARATOR.'base_'.$currenttime.'.jpg');
    		$controller = $this->_getParam('controller');
    		if(isEmptyString($controller)){
    			$controller = 'profile';
    		}
    		// exit();
    		// update the useraccount with the new profile images
    		try {
    			$user->setProfilePhoto($currenttime.'.jpg');
    			$user->save();
    
    			// check if user already has profile picture and archive it
    			$ftimestamp = current(explode('.', $user->getProfilePhoto()));
    
    			$allfiles = glob($destination_path.DIRECTORY_SEPARATOR.'*.*');
    			$currentfiles = glob($destination_path.DIRECTORY_SEPARATOR.'*'.$ftimestamp.'*.*');
    			// debugMessage($currentfiles);
    			$deletearray = array();
    			foreach ($allfiles as $value) {
    				if(!in_array($value, $currentfiles)){
    					$deletearray[] = $value;
    				}
    			}
    			// debugMessage($deletearray);
    			if(count($deletearray) > 0){
    				foreach ($deletearray as $afile){
    					$afile_filename = basename($afile);
    					rename($afile, $archivefolder.DIRECTORY_SEPARATOR.$afile_filename);
    				}
    			}
    
    			$session->setVar(SUCCESS_MESSAGE, $this->_translate->translate("global_update_success"));
    			if(isMobile()){
    					$this->_helper->redirector->gotoUrl($this->view->baseUrl($controller."/view/id/".encode($user->getID())));
    			} else {
    					$this->_helper->redirector->gotoUrl($this->view->baseUrl($controller."/index/id/".encode($user->getID()).'/crop/1/tab/picture'));
    			}
    		} catch (Exception $e) {
    			$session->setVar(ERROR_MESSAGE, $e->getMessage());
    			$session->setVar(FORM_VALUES, $this->_getAllParams());
    				$this->_helper->redirector->gotoUrl($this->view->baseUrl($controller.'/index/id/'.encode($user->getID()).'/tab/picture'));
    		}
    	} else {
    		// debugMessage($upload->getMessages());
    		$uploaderrors = $upload->getMessages();
    		$customerrors = array();
    		if(!isArrayKeyAnEmptyString('fileUploadErrorNoFile', $uploaderrors)){
    			$customerrors['fileUploadErrorNoFile'] = "Please browse for image on computer";
    		}
    		if(!isArrayKeyAnEmptyString('fileExtensionFalse', $uploaderrors)){
    			$custom_exterr = sprintf($this->_translate->translate('upload_invalid_ext_error'), $config->uploads->photoallowedformats);
    			$customerrors['fileExtensionFalse'] = $custom_exterr;
    		}
    		if(!isArrayKeyAnEmptyString('fileUploadErrorIniSize', $uploaderrors)){
    			$custom_exterr = sprintf($this->_translate->translate('upload_invalid_size_error'), formatBytes($config->uploads->photomaximumfilesize,0));
    			$customerrors['fileUploadErrorIniSize'] = $custom_exterr;
    		}
    		if(!isArrayKeyAnEmptyString('fileSizeTooBig', $uploaderrors)){
    			$custom_exterr = sprintf($this->_translate->translate('upload_invalid_size_error'), formatBytes($config->uploads->photomaximumfilesize,0));
    			$customerrors['fileSizeTooBig'] = $custom_exterr;
    		}
    		$session->setVar(ERROR_MESSAGE, 'The following errors occured <ul><li>'.implode('</li><li>', $customerrors).'</li></ul>');
    		$session->setVar(FORM_VALUES, $this->_getAllParams());
    			
    			$this->_helper->redirector->gotoUrl($this->view->baseUrl('profile/index/id/'.encode($user->getID()).'/tab/picture'));
    	}
    }
    
    function croppictureAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	$session = SessionWrapper::getInstance();
    	$formvalues = $this->_getAllParams();
    
    	$user = new UserAccount();
    	$user->populate(decode($formvalues['id']));
    	$userfolder = $user->getID();
    	// debugMessage($formvalues);
    	//debugMessage($user->toArray());
    
    	$oldfile = "large_".$user->getProfilePhoto();
    	$base = BASE_PATH.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR.'user_'.$userfolder.''.DIRECTORY_SEPARATOR.'avatar'.DIRECTORY_SEPARATOR;
    
    	// debugMessage($user->toArray());
    	$src = $base.$oldfile;
    
    	$currenttime = mktime();
    	$currenttime_file = $currenttime.'.jpg';
    	$newlargefilename = $base."large_".$currenttime_file;
    	$newmediumfilename = $base."medium_".$currenttime_file;
    
    	// exit();
    	$image = WideImage::load($src);
    	$cropped1 = $image->crop($formvalues['x1'], $formvalues['y1'], $formvalues['w'], $formvalues['h']);
    	$resized_1 = $cropped1->resize(300, 300, 'fill');
    	$resized_1->saveToFile($newlargefilename);
    
    	//$image2 = WideImage::load($src);
    	$cropped2 = $image->crop($formvalues['x1'], $formvalues['y1'], $formvalues['w'], $formvalues['h']);
    	$resized_2 = $cropped2->resize(165, 165, 'fill');
    	$resized_2->saveToFile($newmediumfilename);
    
    	$user->setProfilePhoto($currenttime_file);
    	$user->save();
    		
    	// check if user already has profile picture and archive it
    	$ftimestamp = current(explode('.', $user->getProfilePhoto()));
    
    	$allfiles = glob($base.DIRECTORY_SEPARATOR.'*.*');
    	$currentfiles = glob($base.DIRECTORY_SEPARATOR.'*'.$ftimestamp.'*.*');
    	// debugMessage($currentfiles);
    	$deletearray = array();
    	foreach ($allfiles as $value) {
    		if(!in_array($value, $currentfiles)){
    			$deletearray[] = $value;
    		}
    	}
    	// debugMessage($deletearray);
    	if(count($deletearray) > 0){
    		foreach ($deletearray as $afile){
    			$afile_filename = basename($afile);
    			rename($afile, $base.DIRECTORY_SEPARATOR.'archive'.DIRECTORY_SEPARATOR.$afile_filename);
    		}
    	}
    	$session->setVar(SUCCESS_MESSAGE, "Successfully updated profile picture");
    	// $url = $this->view->baseUrl('profile/view/id/'.encode($user->getID()));
    	$url = decode($this->_getParam('successurl'));
    	$this->_helper->redirector->gotoUrl($url);
    	// exit();
    }
	
    function uploadpictureAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	 
    	$session = SessionWrapper::getInstance();
    	$config = Zend_Registry::get("config");
    
    	$formvalues = $this->_getAllParams();
    	 
    	if(isset($_FILES["FileInput"]) && $_FILES["FileInput"]["error"] == UPLOAD_ERR_OK) {
    		if(!isset($_FILES['FileInput']['name'])){
    			die("<span class='alert alert-danger'>Error: Please select an Image to Upload.</span>");
    		}
    
    		$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_";
    		// determine if destination folder exists and create it if not
    		if(!is_dir($real_path.$formvalues['userid'])){
    			// no folder exits. Create the folder
    			mkdir($real_path.$formvalues['userid'], 0777, true);
    			chmod($real_path.$formvalues['userid'], 0777);
    		}
    		$real_path = $real_path.$formvalues['userid'].DIRECTORY_SEPARATOR."avatar";
    		if(!is_dir($real_path)){
    			mkdir($real_path, 0777, true);
    			chmod($real_path, 0777);
    		}
    		$archivefolder = $real_path.DIRECTORY_SEPARATOR."archive";
    		if(!is_dir($archivefolder)){
    			mkdir($archivefolder, 0777, true);
    			chmod($archivefolder, 0777);
    		}
    			
    		$UploadDirectory = $real_path.DIRECTORY_SEPARATOR;
    		$File_Name = strtolower($_FILES['FileInput']['name']);
    		$File_Ext = findExtension($File_Name); //get file extention
    		$ext = strtolower($_FILES['FileInput']['type']);
    		$allowedformatsarray = explode(',', $config->uploads->photoallowedformats);
    		$Random_Number = mktime(); //Random number to be added to name.
    		$NewFileName = "base_".$Random_Number.'.'.$File_Ext; //new file name
    			
    		// check if this is an ajax request
    		if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
    			die("<span class='alert alert-danger'>Error: No Request received.</span>");
    		}
    		// validate maximum allowed size
    		if ($_FILES["FileInput"]["size"] > $config->uploads->photomaximumfilesize) {
    			die("<span class='alert alert-danger'>Error: Maximum allowed size exceeded.</span>");
    		}
    		// validate allowed formats
    		if(!in_array($File_Ext, $allowedformatsarray)){
    			die("<span class='alert alert-danger'>Error: Format '".$File_Ext."' not supported. Only formats '".$config->uploads->photoallowedformats."' allowed</span>");
    		}
    			
    		# move the file
    		try {
	    		move_uploaded_file($_FILES['FileInput']['tmp_name'], $UploadDirectory.$NewFileName);
	    		resizeImage($UploadDirectory.$NewFileName, $UploadDirectory.'large_'.$Random_Number.'.jpg', 400);
	    		resizeImage($UploadDirectory.$NewFileName, $UploadDirectory.'medium_'.$Random_Number.'.jpg', 165);
	    				// unlink($thefilename);
	    		unlink($UploadDirectory.'base_'.$Random_Number.'.jpg');
	    		$session->setVar(SUCCESS_MESSAGE, "Successfully uploaded! Resize/crop image to Complete.");
    
    			$user = new UserAccount();
    			$user->populate($formvalues['userid']);
    			$user->setProfilePhoto($Random_Number.'.jpg');
    			$user->save();
    
	    		// check if user already has profile picture and archive it
	    		$ftimestamp = current(explode('.', $user->getProfilePhoto()));
	    
	    		$allfiles = glob($UploadDirectory.'*.*');
	    		$currentfiles = glob($UploadDirectory.'*'.$ftimestamp.'*.*');
	    		// debugMessage($currentfiles);
	    		$deletearray = array();
	    		foreach ($allfiles as $value) {
		    		if(!in_array($value, $currentfiles)){
		    			$deletearray[] = $value;
		    		}
	    		}
	    		// debugMessage($deletearray);
    			if(count($deletearray) > 0){
		    		foreach ($deletearray as $afile){
    					$afile_filename = basename($afile);
    					rename($afile, $archivefolder.DIRECTORY_SEPARATOR.$afile_filename);
    				}
    			}
    
    			// die('File '.$NewFileName.' Uploaded.');
    			$result = array('oldfilename' => $File_Name, 'newfilename'=>$NewFileName, 'msg'=>'File '.$File_Name.' successfully uploaded', 'result'=>1);
    			// json_decode($result);
    			die(json_encode($result));
    		} catch (Exception $e) {
    			die('Error in uploading File '.$File_Name.'. '.$e->getMessage());
    		}
    	} else {
    		// debugMessage($formvalues);
    		// $url = $this->view->baseUrl('profile/index/id/'.encode($user->getID()).'/tab/picture/crop/1');
    		$url = decode($this->_getParam('successurl'));
    		$this->_helper->redirector->gotoUrl($url);
    	}
    }

    function terminateAction(){
	
	}
	
	function processterminateAction(){
		$session = SessionWrapper::getInstance();
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		/* if(isEmptyString($this->_getParam('id'))){
			$this->_setParam('id', 81);
			$this->_setParam('startdate', 'Apr 04, 2017');
			$this->_setParam('enddate', 'Apr 20, 2017');
			$this->_setParam('contractendreason', 'whynot. hgo i i hi h');
			$this->_setParam('deactivate', '1');
		} */
		// debugMessage($this->_getAllParams());
		
		$user = new UserAccount();
		$user->populate($this->_getParam('id'));
		if(!isEmptyString($this->_getParam('enddate'))){
			$user->setenddate(date('Y-m-d', strtotime($this->_getParam('enddate'))));
		}
		if(!isEmptyString($this->_getParam('startdate'))){
			$user->setstartdate(date('Y-m-d', strtotime($this->_getParam('startdate'))));
		}
		$user->setEmploymentStatus(0);
		$user->setTerminatedByID(getUserID());
		$user->setcontractendreason($this->_getParam('contractendreason'));
		// debugMessage($user->toArray());
		// exit;
		try {
			$user->save();
			if($this->_getParam('deactivate') == 1){
				$user->deactivateAccount(2);
			}
			$session->setVar('successmessage', "Successfully updated");
			// debugMessage('success');
		} catch (Exception $e) {
			// debugMessage('err '.$e->getMessage());
			$session->setVar('errormessage', "Error: ".$e->getMessage());
		}
	}
	
	function permissionsAction(){
	
	}
	
	function processpermissionsAction(){
		$session = SessionWrapper::getInstance();
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		 
		$post_array = $this->_getAllParams(); // debugMessage($this->_getAllParams()); // exit;
		$postperms = $post_array['permissions'];
		 
		$q = Doctrine_Query::create()->from('AclPermission a')->where("a.userid = '".$post_array['userid']."' ");
		// debugMessage($q->getSqlQuery());
		$perm = $q->execute(); // debugMessage($perm->toArray());
		$beforesave = $perm->toArray();
		if($perm->count() > 0){
			$perm->delete();
		}
		 
		$perm_collection = new Doctrine_Collection(Doctrine_Core::getTable("AclPermission"));
		foreach ($postperms as $key => $permission){
			foreach ($permission as $akey => $action){
				$data = array(
					'resourceid' => $key,
					'userid' => $post_array['userid'],
					'actionid' => $akey,
					'value' => $action,
					'datecreated' => DEFAULT_DATETIME,
					'createdby' => getUserID()
				);
				$newperm = new AclPermission();
				$newperm->processPost($data);
				/* debugMessage('error ~ '.$newperm->getErrorStackAsString());
				 debugMessage($newperm->toArray()); */
				if(isEmptyString($newperm->getID())){
					$perm_collection->add($newperm);
				}
			}
		}
		 
		$successurl = decode($post_array[URL_SUCCESS]);
		$failureurl = decode($post_array[URL_FAILURE]);
		// debugMessage($perm_collection->toArray()); exit;
		if(count($perm_collection) > 0){
			try {
				$perm_collection->save();
				$session->setVar(SUCCESS_MESSAGE, $this->_getParam(SUCCESS_MESSAGE));
				
				$usecase = '0.5';
				$module = '0';
				$type = SYSTEM_UPDATEROLE;
				$details = "User permissions for ".getUserNamesFromID($post_array['userid']).") overriden";
				
				$prejson = json_encode($beforesave);
				$after = $perm_collection->toArray(); // debugMessage($after);
				$postjson = json_encode($post_array); // debugMessage($postjson);
				$diff = array_diff($beforesave, $after);  // debugMessage($diff);
				$jsondiff = ''; // debugMessage($jsondiff);
				
				$audit_values = getAuditInstance();
				$audit_values['module'] = $module;
				$audit_values['usecase'] = $usecase;
				$audit_values['transactiontype'] = $type;
				$audit_values['status'] = "Y";
				$audit_values['transactiondetails'] = $details;
				$audit_values['isupdate'] = 1;
				
				$audit_values['prejson'] = $prejson;
				$audit_values['postjson'] = $postjson;
				$audit_values['jsondiff'] = $jsondiff;
				$this->notify(new sfEvent($this, $type, $audit_values));
				
				$this->_helper->redirector->gotoUrl($successurl);
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, $e->getMessage());
				$this->_helper->redirector->gotoUrl($failureurl);
			}
		}
		 
		$session->setVar(ERROR_MESSAGE, "Error processing permissions: Contact Admin ");
		$this->_helper->redirector->gotoUrl($failureurl);
	}
	
	function editroleAction(){
		 
	}
	
	function processeditroleAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		 
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		 
		$formvalues = $this->_getAllParams();
		 
		$newtype = $formvalues['type'];
		$user = new UserAccount();
		$user->populate($formvalues['id']);
		$oldtype = $user->getType();
		
		$alltypes = getUserType();
		try {
			// arrange all actions as a single trxn
			$conn->beginTransaction();
			 
			$user->setTypeIDs($newtype);
			$user->setType($newtype);
			$user->save();
			 
			// delete any custom permissions
			$q = Doctrine_Query::create()->from('AclPermission a')->where("a.userid = '".$user->getID()."' ");
			$customperms = $q->execute();
			if($customperms->count() > 0){
				$customperms->delete();
			}
			 
			$conn->commit();
			$session->setVar(SUCCESS_MESSAGE, 'Role successfully updated');
			
			$usecase = '0.5';
			$module = '0';
			$type = SYSTEM_UPDATEROLE;
			$details = "User role for ".$user->getName()." changed from '".$alltypes[$oldtype]."' to '".$alltypes[$newtype]."' ";
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = '21';
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['status'] = "Y";
			$audit_values['transactiondetails'] = $details;
			$this->notify(new sfEvent($this, $type, $audit_values));
			
		} catch (Exception $e) {
			$err = 'Error Updating Permissions: '.$e->getMessage();
			$session->setVar(ERROR_MESSAGE, $err);
			$usecase = '0.5';
			$module = '0';
			$type = SYSTEM_UPDATEROLE;
			$details = "Changing role for ".$user->getName()." from '".$alltypes[$oldtype]."' to '".$alltypes[$newtype]."' failed! Error: ".$err;
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = '21';
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['status'] = "N";
			$audit_values['transactiondetails'] = $details;
			$this->notify(new sfEvent($this, $type, $audit_values));
		}
	}
	
	function resetpermissionsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
	
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
	
		$formvalues = $this->_getAllParams();
		$user = new UserAccount();
		$user->populate($formvalues['id']);
		 
		try {
			// delete any custom permissions
			$q = Doctrine_Query::create()->from('AclPermission a')->where("a.userid = '".$user->getID()."' ");
			$customperms = $q->execute();
			if($customperms->count() > 0){
				$customperms->delete();
			}
			$session->setVar(SUCCESS_MESSAGE, $this->_getParam(SUCCESS_MESSAGE));
		} catch (Exception $e) {
			$session->setVar(ERROR_MESSAGE, 'Error Updating Permissions: '.$e->getMessage());
	
		}
		$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_SUCCESS)));
	}
	
	function activateaccAction(){
	
	}
	
	function processinviteAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	 
    	$session = SessionWrapper::getInstance();
    	$conn = Doctrine_Manager::connection();
    	 
    	$formvalues = $this->_getAllParams();
    	$user = new UserAccount();
    	
    	$user->populate($formvalues['id']);
    	$user->processPost($formvalues); // debugMessage($user->toArray());
    	// exit;
    	if($user->hasError()){
    		$session->setVar(ERROR_MESSAGE, $user->getErrorStackAsString());
    		exit;
    	}
    	
    	try {
    		$user->save();
    		$user->afterUpdate(false);
    		$session->setVar(SUCCESS_MESSAGE, "Successfully updated");
    	} catch (Exception $e) {
    		$session->setVar(ERROR_MESSAGE, $e->getMessage());
    		exit;
    	}
    }
}
