<?php

require_once 'eventdispatcher/sfEventDispatcher.php';
require_once 'eventdispatcher/sfEvent.php';

# event hander functionality
require_once APPLICATION_PATH.'/includes/eventhandlerfunctions.php';

class IndexController extends Zend_Controller_Action  {

	/**
	 * Logger instance
	 * 
	 * @var Zend_Log
	 */
	protected $_logger; 
	/**
	 * Translation instance
	 * 
	 * @var Zend_Translate 
	 */
	protected $_translate; 
	/**
	 * Dispatcher to handle events
	 *
	 * @var sfEventDispatcher
	 */
	protected $_eventdispatcher; 
	
	public function init(){
        // Initialize logger and translate actions
		$this->_logger = Zend_Registry::get("logger"); 
		$this->_translate = Zend_Registry::get("translate");
		// set the redirector to ignore the baseurl for redirections
		$this->_helper->redirector->setPrependBase(false); 
		$this->_eventdispatcher = initializeSFEventDispatcher();
		$session = SessionWrapper::getInstance();
		
		// load the application configuration
		loadConfig(); 
		
		$this->view->referer = $this->getRequest()->getHeader('referer');
		$this->view->viewurl = $_SERVER['REQUEST_URI'];
		
		$profileid = getUserID();
		$profile = new UserAccount();
		$profile->populate($profileid); $this->view->profile = $profile;
		
		$config = Zend_Registry::get("config");
		$this->view->layoutx = !isEmptyString($session->getVar('layout')) ? $session->getVar('layout') : $config->system->layout;
		$this->view->sidebarx = !isEmptyString($session->getVar('sidebar')) ? $session->getVar('sidebar') : $config->system->sidebar;
    
	}
    
    /**
     * Application landing page 
     */
    public function indexAction()  {
    	$session = SessionWrapper::getInstance(); 
    	if($this->getRequest()->getControllerName() == 'index' && !isEmptyString($session->getVar('userid'))){
    		$this->_helper->redirector->gotoUrl($this->view->baseUrl("dashboard"));
    	}
    	if($this->getRequest()->getControllerName() == 'index' && isEmptyString($session->getVar('userid'))){
    		$this->_helper->redirector->gotoUrl($this->view->baseUrl("user/login"));
    	}
    }
    
	/**
     * Action to display the access denied page when a user cannot execute the specified action on a resource    
     */
    function accessdeniedAction()  {
        // do nothing 
    }
    
   function createAction() {
   		// debugMessage(decode($this->_getParam(URL_SUCCESS)));
   		$formvalues = $this->_getAllParams(); // debugMessage($formvalues); // exit;
   		// exit();
   		// $this->_setParam('id', NULL); // exit();
   		$session = SessionWrapper::getInstance(); 
    	// the name of the class to be instantiated
    	if(isEmptyString($this->_getParam("entityname"))){
    		$session->setVar(ERROR_MESSAGE, "Session Error. Please try again");
    		$this->_helper->redirector->gotoUrl($this->view->baseUrl('dashboard'));
    		exit;
    	}
    	$classname = $this->_getParam("entityname");
    	$new_object = new $classname();
    	// exit;
    	// parameters for the response - default do not prepend the baseurl 
    	$response_params = array(); 
    	// add the createdby using the id of the user in session
    	if (isEmptyString($this->_getParam('id'))) {
    		// new entity
    		if (isEmptyString($this->_getParam('createdby'))) {
    			$this->_setParam('createdby', $session->getVar('userid'));
    		}
    		
    		// merge the post data to enable loading of any relationships in process post
    		//  TODO: Verify if this breaks any other functionality
			$new_object->merge(array_remove_empty($this->_getAllParams()), false); 
    	} else {
    		// id is already encoded during update so no need to encode it again 
    		$response_params['id'] = $this->_getParam('id'); 
    		// decode the id field and add it back to the array otherwise it will cause a type error during processPost
    		$this->_setParam('id', decode($this->_getParam('id'))); 
    		// load the details for the current entity from the database 
    		$new_object->populate($this->_getParam('id'));
    		$this->_setParam('lastupdatedby', $session->getVar('userid'));
    	}
    	
    	// populate the object with data from the post and validate the object
    	// to ensure that its wellformed 
    	
    	$new_object->processPost($this->_getAllParams()); 
		/* debugMessage($new_object->toArray());
		debugMessage('errors are '.$new_object->getErrorStackAsString());
		exit(); */
    	if ($new_object->hasError()) {
    		// return to the create page
    		if(isEmptyString($this->_getParam('ispopup'))){
    			// there were errors - add them to the session
    			$session->setVar(FORM_VALUES, $this->_getAllParams());
    			$session->setVar(ERROR_MESSAGE, $new_object->getErrorStackAsString());
    			
    			$response_params['id'] = encode($this->_getParam('id'));
	    		if (isEmptyString($this->_getParam(URL_FAILURE))) {
	    			$this->_helper->redirector->gotoSimple('index', # the action 
		    							    $this->getRequest()->getControllerName(), # the current controller
		    								$this->getRequest()->getModuleName(), # the current module,
		    								$response_params
	    	                             );
	    	        return false; 
	    		} else {
	    			$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)), $response_params); 
	    			return false; 
	    		}
    		} else {
    			$resultarray = array(
    					'id' => '',
    					'name' => '',
    					'result' => 'error',
    					'message' => 'Error: '.$new_object->getErrorStackAsString()
    			);
    			die(json_encode($resultarray));
    		}
    	}
    	
    	// save the object to the database
    	if (isEmptyString($this->_getParam('ispopup'))) {
    		try {
    			switch ($this->_getParam('action')) {
    				case ACTION_CREATE :
    					if(in_array($new_object->getTableName(), array('useraccount'))){
    						$new_object->transactionSave();
    					} else {
    						$new_object->beforeSave(); // exit;
    						$new_object->save();
    						// there are no errors so call the afterSave() hook
    						$new_object->afterSave();
    					}
    					/* debugMessage($new_object->toArray());
    					debugMessage('errors are '.$new_object->getErrorStackAsString()); exit(); */
    					break;
    				case ACTION_EDIT:
    					// update the entity
    					$new_object->beforeUpdate();
    					$new_object->save();
    					// there are no errors so call the afterSave() hook
    					$new_object->afterUpdate();
    					/* debugMessage($new_object->toArray());
    					 debugMessage('errors are '.$new_object->getErrorStackAsString()); exit(); */
    					break;
    				case ACTION_DELETE:
    					// update the entity
    					$new_object->delete();
    					// there are no errors so call the afterSave() hook
    					$new_object->afterDelete();
    					break;
    				case ACTION_APPROVE:
    					// update the entity
    					$new_object->approve();
    					// there are no errors so call the afterSave() hook
    					$new_object->afterApprove();
    					break;
    				default :
    					break;
    			}
    			
    			// add a success message, if any, to the session for display
    			if (!isEmptyString($this->_getParam(SUCCESS_MESSAGE))) {
    				$session->setVar(SUCCESS_MESSAGE, $this->_translate->translate($this->_getParam(SUCCESS_MESSAGE)));
    			}
    			if (isEmptyString($this->_getParam(URL_SUCCESS))) {
    				// add the id of the new object created which is encoded
    				$response_params['id'] = encode($new_object->getID());
    				$this->_helper->redirector->gotoSimple('view', # the action
    						$this->getRequest()->getControllerName(), # the current controller
    						$this->getRequest()->getModuleName(), # the current module,
    						$response_params # the parameters for the response
    						);
    				return false;
    			} else {
    				if($this->_getParam(URL_SUCCESS) == $this->_getParam(URL_FAILURE) && $this->_getParam('controller') == 'indicator'){
    					$this->_setParam('nosuccessid', '1');
    				}
    				$url = decode($this->_getParam(URL_SUCCESS));
	    			/* debugMessage($formvalues);
	    			 debugMessage($url); */
    				if(!isArrayKeyAnEmptyString('nosuccessid', $formvalues)){
    					//debugMessage('go to full url');
    					$this->_helper->redirector->gotoUrl($url);
    				} else {
    					// check if the last character is a / then add it
	    				if (substr($url, -1) != "/") {
		    				// add the slash
		    				$url.= "/";
		    			}
	    				// add the ID parameter
	    				$url.= "id/".encode($new_object->getID()); // debugMessage('add id to url');
	    				$this->_helper->redirector->gotoUrl($url, $response_params);
    				}
    				// exit;
    				return false;
    			}
    			
    		} catch (Exception $e) {
    			$session->setVar(FORM_VALUES, $this->_getAllParams());
    			$session->setVar(ERROR_MESSAGE, $e->getMessage());
    			$this->_logger->err("Error: ".$e->getMessage());
    			// debugMessage($e->getMessage()); exit();
    		
    			// return to the create page
    			if (isEmptyString($this->_getParam(URL_FAILURE))) {
    				$this->_helper->redirector->gotoSimple('index', # the action
    						$this->getRequest()->getControllerName(), # the current controller
    						$this->getRequest()->getModuleName(), # the current module,
    						$response_params
    				);
    				return false;
    			} else {
    				$this->_helper->redirector->gotoUrl(decode($this->_getParam(URL_FAILURE)), $response_params);
    				return false;
    			}
    		}
    	} else {
    		try {
    			switch ($this->_getParam('action')) {
    				case ACTION_CREATE :
    					$new_object->beforeSave();
    					$new_object->save();
    					$new_object->afterSave();
    					break;
    				case ACTION_EDIT:
    					$new_object->beforeUpdate();
    					$new_object->save();
    					$new_object->afterUpdate();
    					break;
    				default :
    					break;
    			}
    			$resultarray = array(
    					'id' => $new_object->getID(),
    					'name' => $new_object->getName(),
    					'result' => 'success',
    					'message' => 'Successfully saved'
    			);
    		} catch (Exception $e) {
    			$resultarray = array(
    					'id' => '',
    					'name' => '',
    					'result' => 'error',
    					'message' => 'Error: '.$e->getMessage()
    			);
    		}
    		die(json_encode($resultarray));
    	}
    }

    public function editAction() {
    	$this->_setParam("action", ACTION_EDIT); 
    	$this->createAction();
    }
    
    function batchdeleteAction(){
    	$this->_setParam("action", ACTION_DELETE);
    	
    	$session = SessionWrapper::getInstance();
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues); // exit;
    	$successurl = decode($formvalues[URL_SUCCESS]); // debugMessage('url '.$successurl); exit();
    	
    	$successmessage = $this->_getParam(SUCCESS_MESSAGE);
    	if(isEmptyString($this->_getParam(SUCCESS_MESSAGE))){
    		$successmessage = "Successfully deleted";
    	}
    	
    	$classname = $formvalues['classname'];
    	$obj_collection = new Doctrine_Collection(Doctrine_Core::getTable($classname));
    	$idsarray = explode(',', $this->_getParam("ids"));
    	// remove all empty keys in the array of ids to be deleted
    	foreach ($idsarray as $id){
    		$obj = new $classname;
    		$obj->populate($id); // debugMessage($obj->toArray());
    		if(!isEmptyString($obj->getID())){
    			$obj_collection->add($obj);
    		}
    	}
    	
    	if($obj_collection->count() > 0){
    		try {
    			$module = '';
    			$usecase = '';
    			$type = $classname;
    			$entityid = $this->_getParam("ids");
    			$deletedetails = 'Entries with id '.$obj->getID().' successfully deleted';
    			
    			$browser = new Browser();
    			$audit_values = $session->getVar('browseraudit');
    			$audit_values['module'] = $module;
    			$audit_values['usecase'] = $usecase;
    			$audit_values['transactiontype'] = $type;
    			if(!isEmptyString($entityid)){
    				$audit_values['entityid'] = $entityid;
    			}
    			$audit_values['status'] = "Y";
    			$audit_values['userid'] = $session->getVar('userid');
    			$audit_values['transactiondetails'] = $deletedetails;
    			$audit_values['prejson'] = $obj_collection->toArray();
    			
    			$obj_collection->delete(); // delete the entries now
    			
    			$successmessage = $this->_getParam(SUCCESS_MESSAGE);
    			if(isEmptyString($this->_getParam(SUCCESS_MESSAGE))){
    				$successmessage = "Successfully deleted";
    			}
    			if(isEmptyString($this->_getParam('src'))){
    				$session->setVar(SUCCESS_MESSAGE, $successmessage);
    			}
    			$this->notify(new sfEvent($this, $type, $audit_values));
    			
    			if(isEmptyString($this->_getParam('src'))){
    				$this->_helper->redirector->gotoUrl($successurl);
    			} else {
    				echo json_encode(array('result' => 'success', 'message' => $successmessage));
    			}
    			
    		} catch (Exception $e) {
    			if(isEmptyString($this->_getParam('src'))){
    				$this->_helper->redirector->gotoUrl($successurl);
    			} else {
    				echo json_encode(array('result' => 'error', 'message' => $e->getMessage()));
    			}
    		}
    	}
    	$this->_helper->redirector->gotoUrl($successurl);
    }
    
	function deleteAction() {
    	$this->_setParam("action", ACTION_DELETE);
    
    	$session = SessionWrapper::getInstance();
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);
    
    	$formvalues = $this->_getAllParams(); // debugMessage($formvalues); exit;
    	$successurl = decode($this->_getParam(URL_SUCCESS));
    	if(!isArrayKeyAnEmptyString(SUCCESS_MESSAGE, $formvalues)){
    		$successmessage = decode($formvalues[SUCCESS_MESSAGE]);
    	}
    	$classname = $formvalues['entityname'];
    	$altclassname = '';
    	if(!isArrayKeyAnEmptyString('altdeleteentity', $formvalues)){
    		$altclassname = $formvalues['altdeleteentity'];
    	}
    	// debugMessage($successurl);
    
    	$obj = new $classname;
    	$id = is_numeric($formvalues['id']) ? $formvalues['id'] : decode($formvalues['id']); // debugMessage($id);
    	$obj->populate($id); // debugMessage($obj->toArray());
    	$beforedelete = $obj->toArray(true); // debugMessage($beforedelete);
    	$prejson = json_encode($beforedelete); // debugMessage($postjson);
    	/* debugMessage($obj->toArray());
    	 exit(); */
    	$entityid = "";
    	
    	# prepare to notify depending on the action
    	switch ($classname) {
    		case 'UserAccount':
				$module = '1';
				$usecase = '1.5';
				$type = USER_DELETE;
				$entityid = $obj->getID();
				$deletedetails = 'User '.$obj->getName().' (ID '.$obj->getID().')  deleted';
    			break;
    		case 'LookupTypeValue':
    			$module = '0';
    			$usecase = '0.3';
    			$type = SYSTEM_DELETEVARIABLE;
    			$var_type = 'Variable';
    			$entityid = $obj->getID();
    			if($classname == 'LookupTypeValue'){
    				$var_type = 'Variable ';
    				$deletedetails = 'Variable - <b>'.$obj->getlookupvaluedescription().' </b>('.$obj->getLookupType()->getdisplayname().') successfully deleted';
    			}
    			break;
    		case 'AclGroup':
    			$module = '0';
    			$usecase = '0.6';
    			$type = SYSTEM_DELETEROLE;
    			$entityid = $obj->getID();
    			$deletedetails = 'Role '.$obj->getName().' (ID '.$obj->getID().')deleted';
    			break;
    		default:
    			$module = '';
    			$usecase = '';
    			$type = $classname;
    			$entityid = $obj->getID();
    			$deletedetails = 'Entry ID '.$obj->getID().' successfully deleted';
    			break;
    	}
    	
    	$audit_values = getAuditInstance();
    	$audit_values['module'] = $module;
    	$audit_values['usecase'] = $usecase;
    	$audit_values['transactiontype'] = $type;
    	if(!isEmptyString($entityid)){
    		$audit_values['entityid'] = $entityid;
    	}
    	$audit_values['status'] = "Y";
    	$audit_values['userid'] = $session->getVar('userid');
    	$audit_values['transactiondetails'] = $deletedetails;
    	$audit_values['prejson'] = $prejson;
    	// debugMessage($audit_values); 
    	// exit(); 
    	
    	try {
    		$obj->delete();
    		if(!isArrayKeyAnEmptyString('altdeleteid', $formvalues)){
    			$altobj = new $altclassname;
    			$altobj->populate($formvalues['altdeleteid']);
    			if(!isEmptyString($altobj->getID())){
    				$altobj->delete();
    			}
    		}
    		if(!isArrayKeyAnEmptyString('altdeleteids', $formvalues)){
    			$obj_collection = new Doctrine_Collection(Doctrine_Core::getTable($classname));
    			$altdeleteids = decode($formvalues['altdeleteids']);
    			$idsarray = commaStringToArray($altdeleteids);
    			foreach ($idsarray as $id){
    				$obj = new $altclassname;
    				$obj->populate($id);
    				if(!isEmptyString($obj->getID())){
    					$obj_collection->add($obj);
    				}
    			}
    			 
    			if($obj_collection->count() > 0){
    				$obj_collection->delete(); 
    			}
    		}
    		$successmessage = $this->_getParam(SUCCESS_MESSAGE);
    		if(isEmptyString($this->_getParam(SUCCESS_MESSAGE))){
    			$successmessage = "Successfully deleted";
    		}
    		if(isEmptyString($this->_getParam('src'))){
    			$session->setVar(SUCCESS_MESSAGE, $successmessage);
    		}
    		$this->notify(new sfEvent($this, $type, $audit_values));
    		if(isEmptyString($this->_getParam('src'))){
    			$this->_helper->redirector->gotoUrl($successurl);
    		} else {
    			$session->setVar(SUCCESS_MESSAGE, $successmessage);
    			echo json_encode(array('result' => 'success', 'message' => $successmessage));
    		}
    	} catch (Exception $e) {
    		if(isEmptyString($this->_getParam('src'))){
    			$this->_helper->redirector->gotoUrl($successurl);
    		} else {
    			echo json_encode(array('result' => 'error', 'message' => $e->getMessage()));
    		}
    	}
    }
    
    function listAction() {
    	$listcount = new LookupType();
    	$listcount->setName("LIST_ITEM_COUNT_OPTIONS");
    	$values = $listcount->getOptionValues(); 
    	asort($values, SORT_NUMERIC); 
    	$session = SessionWrapper::getInstance();
    	
    	$dropdown = new Zend_Form_Element_Select('itemcountperpage',
							array(
								'multiOptions' => $values, 
								'view' => new Zend_View(),
								'decorators' => array('ViewHelper'),
							    'class' => array('form-control','width75','inline','input-sm','perpageswitcher')
							)
						);
		if (isEmptyString($this->_getParam('itemcountperpage'))) {
			if(!isEmptyString($session->getVar('itemcountperpage'))){			
				$dropdown->setValue($session->getVar('itemcountperpage'));
				if($session->getVar('itemcountperpage') == 'ALL'){
					$session->setVar('itemcountperpage', '');
					$dropdown->setValue('50');
				}
			} else {
				$dropdown->setValue('50');
			}
		} else {
			$session->setVar('itemcountperpage', $this->_getParam('itemcountperpage'));
			$dropdown->setValue($session->getVar('itemcountperpage'));
		}
		
	    $this->view->listcountdropdown = '<span>Per page: '.$dropdown->render().'</span>'; 
    }
    /**
     * Redirect list searches to maintain the urls as per zend format 
     */
    function listsearchAction() {
    	//debugMessage($this->getRequest()->getQuery());
    	// debugMessage($this->_getAllParams()); exit();
    	$this->_helper->redirector->gotoSimple(ACTION_LIST, $this->getRequest()->getControllerName(), 
    											$this->getRequest()->getModuleName(),
    											array_remove_empty(array_merge_maintain_keys($this->_getAllParams(), $this->getRequest()->getQuery())));
    	
    }
    function listsubmitAction() {
    	$this->_setParam("action", ACTION_LIST);
    	//debugMessage($this->getRequest()->getQuery());
    	// debugMessage($this->_getAllParams()); exit();
    	$this->_helper->redirector->gotoSimple($this->_getParam('listaction'), $this->getRequest()->getControllerName(),
    			$this->getRequest()->getModuleName(),
    			array_remove_empty(array_merge_maintain_keys($this->_getAllParams(), $this->getRequest()->getQuery())));
    	 
    }
    function viewAction() {
    	
    }
    
    function updateAction(){
    
    }
	/**
     * Notify all listeners of the event, through the event dispatcher instance for the class. This is just a convenience method to
     * avoid accessing the event dispatcher directly
     *
     * @param sfEvent $event The event that has occured
     */
    function notify($event) {
    	$this->_eventdispatcher->notify($event); 
    }
    
    function selectchainAction() {
	    $select_type = $this->_getParam(SELECT_CHAIN_TYPE); 
		
    	switch ($select_type) { 		
    		default:
				echo '';
				break;
		}
		
		// disable rendering of the view and layout so that we can just echo the AJAX output 
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(TRUE);
	}
	function selectchaincustomAction() {
		// disable rendering of the view and layout so that we can just echo the AJAX output 
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(TRUE);    
		$select_type = $this->_getParam(SELECT_CHAIN_TYPE); 
		
		switch ($select_type) {
			case 'category_group_users':
				# get all the villages in a parish
				$result = getDetailsInCategory($this->_getParam('groupid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'category_options':
				# get all the villages in a parish
				$result = getDataCategoryOptions(true, $this->_getParam('idlist'), $this->_getParam('inlist'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'counties':
				# get all the counties in a district
				$result = getCounties();
				$result = json_encode($result);
				echo ($result);
				break;
			case 'county_subcounties':
				# get all the subcounties in a county
				$result = getSubcountiesInCounty($this->_getParam('countyid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_attributes':
				# get all the villages in a parish
				$result = getDataAttributes(true, $this->_getParam('idlist'), $this->_getParam('inlist'), false, $this->_getParam('addref'), $this->_getParam('datasetid'));
				if($this->_getParam('checkone') == 1 && isEmptyString($this->_getParam('idlist'))){
					$result = getDataAttributes(true, $this->_getParam('idlist'), $this->_getParam('inlist'), true, $this->_getParam('addref'), $this->_getParam('datasetid'));
				}
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_attributes_withcategories':
				# get all the villages in a parish
				$attributes = getDataAttributesAndSubAttributes($this->_getParam('datasetid'));
				// debugMessage($attributes);
				$result = json_encode($attributes);
				echo ($result);
				break;
			case 'data_categories':
				# get all the villages in a parish
				$result = getDataCategories(true, $this->_getParam('idlist'), $this->_getParam('inlist'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_category_groups':
				# get all the villages in a parish
				$result = getDataCategoryGroups(true, $this->_getParam('idlist'), $this->_getParam('inlist'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_indicators':
				# get all the villages in a parish
				$result = getDataIndicators(true, $this->_getParam('idlist'), $this->_getParam('inlist'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_users':
				# get all the villages in a parish
				$result = getDataUsers(true, $this->_getParam('idlist'), $this->_getParam('inlist'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_sets':
				# get all the villages in a parish
				$result = getDatasets($this->_getParam('projectid'), false, $this->_getParam('addref'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_sets_attributes':
				$idlist = '!!'; 
				if(!isEmptyString($this->_getParam('ids'))){
					$ids_array = array();
					$ids = commaStringToArray($this->_getParam('ids'));
					foreach ($ids as $id){
						$dataset = new Dataset();
						$dataset->populate($id); // debugMessage($dataset->toArray());
						if($dataset->getFormType() == 1){
							$ids_array[] = $dataset->getElementIDs();
						} else {
							$ids_array[] = $dataset->getElementIDsFromSections();
						}
					}
					$idlist = implode(',', array_unique($ids_array));
					// debugMessage($idlist);
					
					$result = getDataAttributes(false, $idlist, '', false, 1);
					$result = sortArrayByArray($result, commaStringToArray($idlist)); // debugMessage($result);
					
				} else {
					$result = array();
				}
				
				$result = json_encode($result);
				echo ($result);
				break;
			case 'districts':
				# get all the counties in a district
				$result = getDistricts();
				$result = json_encode($result);
				echo ($result);
				break;
			case 'data_variables':
				# get all the counties in a district
				$result = getDatavariables($this->_getParam('type'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'district_counties':
				# get all the counties in a district
				$result = getCountiesInDistrict($this->_getParam('districtid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'milestones':
				# get all the counties in a district
				$result = getMilestonePeriods($this->_getParam('type'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'objectives':
				# get all the villages in a parish
				$result = getObjectives();
				$result = json_encode($result);
				echo ($result);
				break;
			case 'outcome_outputs':
				# get all the villages in a parish
				$result = getOutputs($this->_getParam('type'), $this->_getParam('outcomeid'), $this->_getParam('projectid'), $this->_getParam('addref'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'programs':
				# get all the villages in a parish
				$result = getPrograms($this->_getParam('type'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'project_activities':
				# get all the villages in a parish
				$result = getProjectActivities($this->_getParam('projectid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'projects':
				# get all the villages in a parish
				$result = getProjects();
				$result = json_encode($result);
				echo ($result);
				break;
			case 'project_outcomes':
				# get all the villages in a parish
				$result = getOutcomes($this->_getParam('type'), $this->_getParam('programid'), $this->_getParam('projectid'), $this->_getParam('addref'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'parish_villages':
				# get all the villages in a parish
				$result = getVillagesInParishes($this->_getParam('parishid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'program_outcomes':
				# get all the villages in a parish
				$result = getOutcomes($this->_getParam('type'), $this->_getParam('programid'), $this->_getParam('projectid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'program_units':
				$result = getProgramUnits();
				$result = json_encode($result);
				echo ($result);
				break;
			case 'result_groups':
				# get all the villages in a parish
				$result = getResultGroups(1);
				$result = json_encode($result);
				echo ($result);
				break;
			case 'region_districts':
				# get all the counties in a district
				$result = getDistrictsInRegion($this->_getParam('regionid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'section_data_attributes':
				# get all the villages in a parish
				$result = getDataAttributes(true, $this->_getParam('idlist'), $this->_getParam('inlist'), false);
				if($this->_getParam('checkone') == 1){
					$result = getDataAttributes(true, $this->_getParam('idlist'), $this->_getParam('inlist'), true);
				}
				$result = json_encode($result);
				echo ($result);
				break;
			case 'section_data_indicators':
				# get all the villages in a parish
				$result = getDataIndicators(true, $this->_getParam('idlist'), $this->_getParam('inlist'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'subcounty_parishes':
				# get all the parishes in a subcounty
				$result = getParishesInSubCounty($this->_getParam('subcountyid'));
				$result = json_encode($result);
				echo ($result);
				break;
			case 'subcounties':
				# get all the counties in a district
				$result = getSubCounties();
				$result = json_encode($result);
				echo ($result);
				break;
			default:
				# get all the villages in a parishes
				echo '';
				break;
		}
	}
	/**
     * Action to download details into MS Excel
    */
    function exceldownloadAction()  {
    	// disable rendering of the view and layout so that we can just echo the AJAX output
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

        // required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression')) {
			ini_set('zlib.output_compression', 'Off');
		}
		
		$response = $this->getResponse();
		
		# This line will stream the file to the user rather than spray it across the screen
		$response->setHeader("Content-type", "application/vnd.ms-excel");
		
		# replace excelfile.xls with whatever you want the filename to default to
		$response->setHeader("Content-Disposition", "attachment;filename=".time().rand(1, 10).".xls");
		$response->setHeader("Expires", 0);
		$response->setHeader("Cache-Control", "private");
		session_cache_limiter("public");
		
		$session = SessionWrapper::getInstance();
		
		# the coluumns that have numbers, these have to be formatted differently from the rest of the
		# columns
		$number_column_array = explode(",", $this->_getParam(EXPORT_NUMBER_COLUMN_LIST));
		
		$xml = new ExcelXML();
		// the number of columns to ignore in the query, these are usually ids
		$xml->setStartingColumn(trim($this->_getParam(EXPORT_IGNORE_COLUMN_COUNT)));
		echo $xml->generateXMLFromQuery($session->getVar(CURRENT_RESULTS_QUERY));
    }
	/**
     * Action to download details into MS Excel
    */
    function printerfriendlyAction()  {
    	
    }
    /**
     * Clear user specific cache items on expiry of the session or logout of the user
     *
     */
    function clearUserCache() {
    	$session = SessionWrapper::getInstance(); 
    	
    	// clear the acl instance for the user
        $aclkey = "acl".$session->getVar('userid'); 
        $cache = Zend_Registry::get('cache');
        $cache->remove($aclkey); 
    }
    /**
     * Clear the user session and any cache files 
     *
     */
    function clearSession() {
    	// clear user specific cache
    	$this->clearUserCache();
    	
        // clear the session
        $session = SessionWrapper::getInstance(); 
        $session->clearSession();
    }
    
    /**
     * Pre-processing for all actions
     *
     * - Disable the layout when displaying printer friendly pages 
     *
     */
    function preDispatch(){
    	// disable rendering of the layout so that we can just echo the AJAX output
    	if(!isEmptyString($this->_getParam(PAGE_CONTENTS_ONLY))) { 
    		$this->_helper->layout->disableLayout();
    	}
    } 
    
    function addsuccessAction(){
		$session = SessionWrapper::getInstance(); 
     	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		$this->_setParam("action", ACTION_VIEW);
		
		$session->setVar(SUCCESS_MESSAGE, "Successfully saved");
   		if(!isArrayKeyAnEmptyString('successmessage', $formvalues)){
			$session->setVar(SUCCESS_MESSAGE, decode($formvalues['successmessage']));
		}
		
    } 

	function adderrorAction(){
		$session = SessionWrapper::getInstance(); 
     	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		
		$currenterror = $session->getVar(ERROR_MESSAGE);
		if(isEmptyString($currenterror)){
			$session->setVar(ERROR_MESSAGE, "An error occured in updating database");
		}  
	}
	
	function profileupdatesuccessAction(){
		$session = SessionWrapper::getInstance(); 
     	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		
		$session->setVar(SUCCESS_MESSAGE, "Profile successfully updated");
		if(!isArrayKeyAnEmptyString('successmessage', $formvalues)){
			$session->setVar(SUCCESS_MESSAGE, decode($formvalues['successmessage']));
		}
	}
	
	function updatesuccessAction(){
		$session = SessionWrapper::getInstance(); 
     	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
    	
		$session->setVar(SUCCESS_MESSAGE, "Successfully updated");
		if(!isArrayKeyAnEmptyString('successmessage', $formvalues)){
			$session->setVar(SUCCESS_MESSAGE, decode($formvalues['successmessage']));
		}
    }
	
	public function themechangeAction(){
		$session = SessionWrapper::getInstance();
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		$conn = Doctrine_Manager::connection();
		$session->setVar($this->_getParam('type'), $this->_getParam('value'));
		$layout = $this->_getParam('type');
		
		if($this->_getParam('type') == 'layout'){
			$userid = getUserID();
			$user = new UserAccount();
			$user->populate($userid);
			$user->setLayout($this->_getParam('value'));
			$user->save();
		} else {
			if($this->_getParam('type') == 'sidebar'){
				$userid = getUserID();
				$user = new UserAccount();
				$user->populate($userid);
				$user->setSidebar($this->_getParam('value'));
				$user->save();
			} else {
				$query = "UPDATE appconfig set optionvalue = '".$this->_getParam('value')."' where optionname = '".$this->_getParam('type')."' "; // debugMessage($query);
				$result = $conn->execute($query);
				$path = APPLICATION_PATH.DIRECTORY_SEPARATOR."temp"; // debugMessage($path);
				$cachefiles = glob($path.DIRECTORY_SEPARATOR.'*config*'); // debugMessage($cachefiles);
				foreach ($cachefiles as $afile){
					if(is_file($afile)){
						unlink($afile);
					}
				}
			}
		}
		if(!isEmptyString($this->_getParam('successurl'))){
			$this->_redirect(decode($this->_getParam('successurl')));
		}
	}
	
	function errorAction(){
		$session = SessionWrapper::getInstance();
	}
	
	public function searchsessionAction(){
		$session = SessionWrapper::getInstance();
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams();
		if(!isEmptyString($this->_getParam('value'))){
			$session->setVar('issearch', $this->_getParam('value'));
		}
		echo 'search now is '.$session->getVar('issearch');
	}
	
}
