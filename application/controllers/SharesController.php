<?php

class SharesController extends SecureController  {
	
	/**
	 * @see SecureController::getResourceForACL()
	 *
	 * @return String
	 */
	function getResourceForACL() {
		return "Shares";
	}
	/**
	 * Override unknown actions to enable ACL checking
	 *
	 * @see SecureController::getActionforACL()
	 *
	 * @return String
	 */
	public function getActionforACL() {
		$action = strtolower($this->getRequest()->getActionName());
		if($action == "addshare") {
			return ACTION_CREATE;
		}
		return parent::getActionforACL();
	}
	function indexAction(){
		$this->_helper->layout->disableLayout();
	}
	function addshareAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$formvalues = $this->_getAllParams(); // debugMessage($formvalues);
		$session = SessionWrapper::getInstance();
		
		$filelist = decode($this->_getParam('filelist'));
		$dirlist = decode($this->_getParam('dirlist')); // exit;
		// debugMessage($filelist); debugMessage($dirlist); 
		
		$share = new Share();
		$folder = new Folder();
		$folder->populate($this->_getParam('sourceid'));
		$result = array(); $filestr = '';
		$data = array(
			'companyid' => $folder->getCompanyID(),
			'userid' => $session->getVar('userid'),
			'createdby' => $session->getVar('userid'),
			'name' => 'share_'.time(),
			'type' => '1',
			'folderid' => $folder->getID(),
			'fileid' => '',
			'filelist' => '',
			'dirlist' => '',
			'status' => '1',
			'startdate' => date('Y-m-d'),
			'issecured' => '0',
			'browsesubfoler' => '1',
			'accesstype' => '1',
			'canread' => '1',
			'canlist' => '1'
		);
		
		if(!isEmptyString($filelist)){ // debugMessage($filelist);
			$filearray = explode(',', $filelist); //debugMessage($filearray); // exit;
			$idlistarray = array();
			if(is_array($filearray) && count($filearray) == 1 && isEmptyString($dirlist)){
				$file = new File();
				$file->populate($filearray[0]);
				if(!isEmptyString($file->getID())){
					$data['name'] = $file->getName();
					$data['fileid'] = $file->getID();
					$data['type'] = 2;
				}
				$filestr .= $file->getRelativePathFromHome().',<br/>';
			} 
			if(is_array($filearray) && count($filearray) > 1){
				$data['fileid'] = '';
				$data['filelist'] = implode(',', $filearray);
				if(isEmptyString($dirlist)){
					$data['type'] = '2';
				}
			}
			$data['filelist'] = $filelist;
		}
		
		if(!isEmptyString($dirlist)){ // debugMessage($dirlist);
			$dirarray = explode(',', $dirlist); //debugMessage($dirarray); // exit;
			$idlistarray = array();
			if(is_array($dirarray) && count($dirarray) > 1){
				$data['dirlist'] = implode(',', $dirarray);
			}
			if(is_array($dirarray) && count($dirarray) == 1 && isEmptyString($filelist)){
				$dir = new Folder();
				$dir->populate($dirarray[0]);
				if(!isEmptyString($dir->getID())){
					$data['name'] = $dir->getName();
					$data['folderid'] = $dir->getName();
				}
				$filestr .= $dir->getRelativePathFromHome().',<br/>';
			}
			$data['dirlist'] = $dirlist;
		}
		// debugMessage($data); exit;
		
		$share->processPost($data);
		/* debugMessage($share->toArray());
		debugMessage('errors are '.$share->getErrorStackAsString()); exit; */
		if($share->hasError()){
			$result['id'] = '';
			$result['type'] = '0';
			$result['msg'] = "Error creating share. ".$share->getErrorStackAsString();
		} else {
			try {
				$share->save();
				$result['id'] = $share->getID();
				$result['type'] = '1';
				$result['msg'] = "Share successfully created";
				
				# add log to audit trail
				$view = new Zend_View();
				// $controller = $this->getController();
				$usecase = '2.7';
				$module = '2';
				$type = PATH_SHARE;
				$actionst = "Shared Files";
				
				$audit_values = getAuditInstance();
				$audit_values['companyid'] = $session->getVar('companyid');
				$audit_values['module'] = $module;
				$audit_values['usecase'] = $usecase;
				$audit_values['transactiontype'] = $type;
				$audit_values['transactiondate'] = date("Y-m-d H:i:s");
				$audit_values['status'] = "Y";
				$audit_values['userid'] = $session->getVar('userid');
				$audit_values['transactiondetails'] = $actionst.'<i>'.$filestr.'</i>';
				$audit_values['url'] = $dirlist.':'.$filelist;
				//debugMessage($audit_values);
				$this->notify(new sfEvent($this, $type, $audit_values));
			} catch (Exception $e) {
				$result['id'] = '';
				$result['type'] = '0';
				$result['msg'] = "Error creating share. ".$e->getMessage();
			}
		}
		echo json_encode($result);
		// exit;
	}
}