<?php 
# whether or not the session has errors
$session = SessionWrapper::getInstance(); 
$sessionhaserror = !isEmptyString($session->getVar(ERROR_MESSAGE));

$userid = $session->getVar("userid");
$appname = getAppName();
$session->setVar('appname', $appname);
$loggedinuser = $this->profile;

$isloggedin = false;
if(!isEmptyString($userid)){
	$isloggedin = true;
}
$companyid = $session->getVar("companyid");
$libdir = $session->getVar("libdir");

$logotype = 1;
$haslogo = true;
$logourl = $this->baseUrl('images/logo2@2x.png');
$applogo = $this->baseUrl('images/logo.png');

$layout = $this->layoutx;
$sidebar = $this->sidebar;

# the request object instance
$request = Zend_Controller_Front::getInstance()->getRequest();
$hide_on_print_class = $request->getParam(PAGE_CONTENTS_ONLY) == "true" ? "hideonprint" : ""; 
$isprint = false;
if($request->print == 1){
	$isprint = true;
}
$ispgc = false;
if(!isEmptyString($request->pgc)){
	$ispgc = true;
}

# application config
$config = Zend_Registry::get('config');
$layout = $config->system->layout;


# pagination defaults
Zend_Paginator::setDefaultScrollingStyle('Sliding');
Zend_View_Helper_PaginationControl::setDefaultViewPartial("index/pagination_control.phtml");

// initialize the ACL for all views
$acl = getACLInstance(); 

$controller = $request->getControllerName();
$action = $request->getActionName();

$browserappend = " | ".$config->system->companyname;
$showsearch = true;
$homedir = 'System / ';
$blockcontent = '<h4><img src="/assets/images/loader.gif" /> Please wait...</h4>';

$has_no_data = true;
date_default_timezone_set(getTimeZine());