<?php
/**
 * Class that uses a Zend_View to creat an email template 
 */

class EmailTemplate extends Zend_View {
	
	public function __construct($config = array()) {
		parent::__construct($config = array());
		$config = Zend_Registry::get("config");
		
		// add the path to the scripts
		$this->setScriptPath(APPLICATION_PATH."/views/scripts/email/"); 
		$this->appname = getAppName();
		$this->companyname = getCompanyName();
		$this->appfullname = $config->system->appfullname;
		
		// default sign off name and email
		$mail = Zend_Registry::get('mail'); 
		$default_sender = $mail->getDefaultFrom(); 
		
		$this->signoffname = 'Customer Support';
		$this->signoffemail = getSupportEmailAddress(); 
		$this->contactusurl = "http://";
		$this->contactusurl = $this->serverUrl($this->baseUrl('contactus'));
		$this->logourl = $this->serverUrl($this->baseUrl('images/logo.jpg'));
		$this->loginurl = $this->serverUrl($this->baseUrl('user/login'));
		$this->baseurl = $this->serverUrl($this->baseUrl());
		$this->settingsurl = $this->serverUrl($this->baseUrl('profile/view/tab/account'));
		
		$this->themecolor = "#2c3e50";
	}
}

?>