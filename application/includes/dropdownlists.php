<?php

	# This class require_onces functions to access and use the different drop down lists within
	# this application

	/**
	 * function to return the results of an options query as array. This function assumes that
	 * the query returns two columns optionvalue and optiontext which correspond to the corresponding key
	 * and values respectively. 
	 * 
	 * The selection of the names is to avoid name collisions with database reserved words
	 * 
	 * @param String $query The database query
	 * 
	 * @return Array of values for the query 
	 */
	function getOptionValuesFromDatabaseQuery($query) {
		$conn = Doctrine_Manager::connection(); 
		$result = $conn->fetchAll($query);
		$valuesarray = array();
		foreach ($result as $value) {
			$title = preg_replace( '/[^[:print:]]/', '', ($value['optiontext']));
			$valuesarray[$value['optionvalue']]	= $title;
		}
		return decodeHtmlEntitiesInArray($valuesarray);
	}
	# function to generate months
	function getAllMonths() {
		$months = array(
		"January" => "January",		
		"February" => "February",
		"March" => "March",
		"April" => "April",
		"May" => "May",		
		"June" => "June",
		"July" => "July",
		"August" => "August",
		"September" => "September",		
		"October" => "October",
		"November" => "November",
		"December" => "December"	
		);
		return $months;
	}
	
	# function to generate months
	function getAllMonthsAsNumbers() {
		$months = array(
		"01" => "January",		
		"02" => "February",
		"03" => "March",
		"04" => "April",
		"05" => "May",		
		"06" => "June",
		"07" => "July",
		"08" => "August",
		"09" => "September",		
		"10" => "October",
		"11" => "November",
		"12" => "December"	
		);
		return $months;
	}
	# function to generate months
	function getShortMonthsAsNumbers($current='') {
		$months = array(
		"01" => "Jan",		
		"02" => "Feb",
		"03" => "Mar",
		"04" => "Apr",
		"05" => "May",		
		"06" => "Jun",
		"07" => "Jul",
		"08" => "Aug",
		"09" => "Sep",		
		"10" => "Oct",
		"11" => "Nov",
		"12" => "Dec"	
		);
		if(!isEmptyString($current)){
			return $months[$current];
		}
		return $months;
	}
	# split a date into day month and year
	function splitDate($date) {
		if(isEmptyString($date)){
			return array();
		}
		$date = date('Y-n-j',strtotime($date));
		$date_parts = explode('-', $date);
		// debugMessage($date_parts);
		return $date_parts;	
	}
	# function to generate months
	function getMonthsAsNumbers() {
		$months = array(
		"01" => "01",		
		"02" => "02",
		"03" => "03",
		"04" => "04",
		"05" => "05",		
		"06" => "06",
		"07" => "07",
		"08" => "08",
		"09" => "09",		
		"10" => "10",
		"11" => "11",
		"12" => "12"	
		);
		return $months;
	}
	# function to generate months short names
	function getAllMonthsAsShortNames($current ='') {
		$months = array(
		"1" => "Jan",		
		"2" => "Feb",
		"3" => "Mar",
		"4" => "Apr",
		"5" => "May",		
		"6" => "Jun",
		"7" => "Jul",
		"8" => "Aug",
		"9" => "Sept",		
		"10" => "Oct",
		"11" => "Nov",
		"12" => "Dec"	
		);
		if(!isEmptyString($current)){
			return $months[$current];
		}
		return $months;
	}
	
	function getMonthName($number) {
		$months = getAllMonthsAsNumbers();
		return $months[$number];
	}
	# determine days of week
	function getDaysOfWeek($current ='') {
		$days = array(
				"1" => "Monday",
				"2" => "Tuesday",
				"3" => "Wednesday",
				"4" => "Thursday",
				"5" => "Friday",
				"6" => "Saturday",
				"7" => "Sunday",
		);
		if(!isEmptyString($current)){
			return $days[$current];
		}
		return $days;
	}
	# determine days of week
	function getShortDaysOfWeek($current ='') {
		$days = array(
				"1" => "Mon",
				"2" => "Tue",
				"3" => "Wed",
				"4" => "Thur",
				"5" => "Fri",
				"6" => "Sat",
				"7" => "Sun",
		);
		if(!isEmptyString($current)){
			return $days[$current];
		}
		return $days;
	}
	
	# function to generate years
	function getAllYears($yearsback="", $yearsahead="") {				
		$aconfig = Zend_Registry::get("config"); 
		$years = array(); 
		$start_year = date("Y") - $aconfig->dateandtime->mindate;
		if(!isEmptyString($yearsback)){
			$start_year = date("Y") - $yearsback;
		}
		$end_year = date("Y") + $aconfig->dateandtime->maxdate;
		if(!isEmptyString($yearsahead)){
			$end_year = date("Y") + $yearsahead;
		}
		for($i = $start_year; $i <= $end_year; $i++) {
			$years[$i] = $i; 
		}		
		//return the years in descending order from the last year to the first and add true to maintain the array keys
		return array_reverse($years, true);
	}
	function getYearsInRage($start, $ahead) {
		$aconfig = Zend_Registry::get("config");
		$years = array();
		$start_year = date("Y") - $aconfig->dateandtime->mindate;
		if(!isEmptyString($start)){
			$start_year = $start;
		}
		$end_year = date("Y") + $aconfig->dateandtime->maxdate;
		if(!isEmptyString($ahead)){
			$end_year = $ahead;
		}
		for($i = $start_year; $i <= $end_year; $i++) {
			$years[$i] = $i;
		}
		return array_reverse($years, true);
	}
	
	# function to generate future years
	function getFutureYears() {				
		$aconfig = Zend_Registry::get("config"); 
		$years = array(); 
		$start_year = date("Y");
		
		$end_year = date("Y") + $aconfig->dateandtime->mindate;
		for($i = $start_year; $i <= $end_year; $i++) {
			$years[$i] = $i; 
		}		
		//return the years in descending order from the last year to the first and add true to maintain the array keys
		return $years;
	}
        # function to generate years
	function getMultipleYears() {				
		$aconfig = Zend_Registry::get("config"); 
		$years = array(); 
		$start_year = date("Y") - $aconfig->dateandtime->mindateofbirth;
		
		$end_year = date("Y");
		for($i = $start_year; $i <= $end_year; $i++) {
			$years[$i] = $i; 
		}		
		//return the years in descending order from the last year to the first and add true to maintain the array keys
		return array_reverse($years, true);
	}
	 # function to generate years
	function getSubscribeBirthYears() {				
		$aconfig = Zend_Registry::get("config"); 
		$years = array(); 
		// $start_year = (date("Y")) - 100;
		$start_year = 1900;
		
		$end_year = (date("Y"));
		for($i = $start_year; $i <= $end_year; $i++) {
			$years[$i] = $i; 
		}		
		//return the years in descending order from the last year to the first and add true to maintain the array keys
		return array_reverse($years, true);
	}
	# function to generate years
	function getMonthDays() {
		$days = array(); 
		$start_day = 1;
	
		$end_day = 31;
		for($i = $start_day; $i <= $end_day; $i++) {
			$days[$i] = $i; 
		}		
		//return the years in descending order from 2009 down to the start year and true to maintain the array keys
		return $days;
	}
	function getMonthsInPeriod($startdate, $enddate){
		$months = array();
		$keyone = date('M', strtotime($startdate));
		$months[$keyone] = $keyone;
		return $months;
	}
	function getEndDatesofPeriod($type, $year='', $caltype = 1){
		if($type == 1){
			$dates = array('1' => $year.'-09-30', '2' => $year.'-12-31', '3' => ($year+1).'-03-31', '4' => ($year+1).'-06-30');
			if($caltype == 2){
				$dates = array('1' => $year.'-03-31', '2' => $year.'-06-30', '3' => $year.'-09-30', '4' => $year.'-12-31');
			}
			return $dates;
		}
		if($type == 2){
			$dates = array();
			for($i = 1; $i<=12; $i++){
				if($i > 6){
					$dates[] = getLastDayOfMonth($i-6, $year);
				} else {
					$dates[] = getLastDayOfMonth($i, ($year+1));	
				}
			}
			return $dates;
		}
		return array();
	}
	function getStartDatesofPeriod($type, $year='', $caltype = 1){
		if($type == 1){
			$dates = array('1' => $year.'-07-01', '2' => $year.'-10-01', '3' => ($year+1).'-01-01', '4' => ($year+1).'-04-01');
			if($caltype == 2){
				$dates = array('1' => $year.'-01-01', '2' => $year.'-04-01', '3' => $year.'-07-01', '4' => $year.'-10-01');
			}
			return $dates;
		}
		if($type == 2){
			$dates = array();
			for($i = 1; $i<=12; $i++){
				if($i <= 6){
					$dates[$i] = getFirstDayOfMonth($i+6, $year);
				} else {
					$dates[$i] = getFirstDayOfMonth($i-6, ($year+1));	
				}
			}
			return $dates;
		}
		return array();
	}
	# get the first day of a month
	function getFirstDayOfMonth($month,$year) {
		return date("Y-m-d", mktime(0,0,0, $month,1,$year));
	}
	# get the last day of a month
	function getLastDayOfMonth($month,$year) {
		return date("Y-m-d", mktime(0,0,0, $month+1,0,$year));
	}
	# get the first day of current month
	function getFirstDayOfNextMonth($month,$year) {
		return date("Y-m-d", mktime(0,0,0, $month,2,$year));
	}
	# get the last day of the next month
	function getLastDayOfNextMonth($month,$year) {
		return date("Y-m-d", mktime(0,0,0, $month+2,0,$year));
	}
	# get the first day of last month
	function getFirstDayOfLastMonth($month,$year) {
		return date("Y-m-d", mktime(0,0,0, $month,-1,$year));
	}
	# get the last day of the last month
	function getLastDayOfLastMonth($month,$year) {
		return date("Y-m-d", mktime(0,0,0, $month-1,0,$year));
	}
	# get the first day of the current year and month
	function getFirstDayOfCurrentMonth(){
		return date("Y-m-d", mktime (0,0,0, date("n"),1, date("Y")));
	}
	# get the last day of the last month
	function getLastDayOfCurrentMonth() {
		return date("Y-m-d", mktime(0,0,0, date("n")+1,0, date("Y")));
	}
	function getFirstDateOfMonth($date){
		return date("Y-m-d", mktime (0,0,0, date("n", strtotime($date)),1, date("Y", strtotime($date))));
	}
	function getLastDateOfMonth($date) {
		return date("Y-m-d", mktime(0,0,0, date("n", strtotime($date))+1,0, date("Y", strtotime($date))));
	}
	# return array of months between two dates
	function get_months($date1, $date2) {
		$time1  = strtotime($date1);
		$time2  = strtotime($date2);
		$my     = date('mY', $time2);
		$shortmonths = getShortMonthsAsNumbers();
		
		$months[date('Y',$time1).date('m',$time1)] = array('yearmonth'=>date('Y',$time1).date('m',$time1), 'year'=>date('Y', $time1), 'monthnumber'=>date('m', $time1), 'monthfull'=>date('F', $time1), 'monthshort'=>$shortmonths[date('m', $time1)], 'monthdisplay'=>$shortmonths[date('m', $time1)].' '.date('Y', $time1));
	
		while($time1 < $time2) {
			$time1 = strtotime(date('Y-m-d', $time1).' +1 month');
			if(date('mY', $time1) != $my && ($time1 < $time2)){
				$months[date('Y',$time1).date('m',$time1)] = array('yearmonth'=>date('Y',$time1).date('m',$time1), 'year'=>date('Y', $time1), 'monthnumber'=>date('m', $time1), 'monthfull'=>date('F', $time1), 'monthshort'=>$shortmonths[date('m', $time1)], 'monthdisplay'=>$shortmonths[date('m', $time1)].' '.date('Y', $time1));
			}
		}
	
		$months[date('Y',$time2).date('m',$time2)] = array('yearmonth'=>date('Y',$time2).date('m',$time2), 'year'=>date('Y', $time2), 'monthnumber'=>date('m', $time2), 'monthfull'=>date('F', $time2), 'monthshort'=>$shortmonths[date('m', $time2)], 'monthdisplay'=>$shortmonths[date('m', $time2)].' '.date('Y', $time2));
		return $months;
	}
	# determine the weeks between a period
	function get_weeks($date1, $date2) {
		$time1  = strtotime($date1);
		$time2  = strtotime($date2);
		$my     = date('Y', $time2).(date('W', $time2)-1);
		$shortmonths = getShortMonthsAsNumbers();
		
		$weeks[date('Y', $time1).(date('W', $time1)-1)] = array('yearmonth'=>date('Y',$time1).date('m',$time1), 'year'=>date('Y', $time1), 'monthnumber'=>date('m', $time1), 'monthfull'=>date('F', $time1), 'monthshort'=>$shortmonths[date('m', $time1)], 'monthdisplay'=>$shortmonths[date('m', $time1)].' '.date('Y', $time1), 'yearweek'=> date('Y', $time1).(date('W', $time1)-1), 'week'=>(date('W', $time1)-1));
		
		while($time1 < $time2) {
			$time1 = strtotime(date('Y-m-d', $time1).' +7 day');
			if(date('Y', $time1).(date('W', $time1)-1) != $my && ($time1 < $time2)){
				$weeks[date('Y',$time1).(date('W', $time1)-1)] = array('yearmonth'=>date('Y',$time1).date('m',$time1), 'year'=>date('Y', $time1), 'monthnumber'=>date('m', $time1), 'monthfull'=>date('F', $time1), 'monthshort'=>$shortmonths[date('m', $time1)], 'monthdisplay'=>$shortmonths[date('m', $time1)].' '.date('Y', $time1), 'yearweek'=> date('Y', $time1).(date('W', $time1)-1), 'week'=>(date('W', $time1)-1));
			}
		}
		$weeks[date('Y', $time2).(date('W', $time2)-1)] = array('yearmonth'=>date('Y',$time2).date('m',$time2), 'year'=>date('Y', $time2), 'monthnumber'=>date('m', $time2), 'monthfull'=>date('F', $time2), 'monthshort'=>$shortmonths[date('m', $time2)], 'monthdisplay'=>$shortmonths[date('m', $time2)].' '.date('Y', $time2), 'yearweek'=> date('Y', $time2).(date('W', $time2)-1), 'week'=>(date('W', $time2)-1));
		
		return $weeks;
	}
	# determine no of days between two dates
	function getDaysBetweenDates($date1, $date2) {
		$time1  = strtotime($date1);
		$time2  = strtotime($date2);
		return ($time2-$time1)/(24*60*60);
	}
    /**
	 * Return an array containing the countries in the world
	 *
	 * @return Array Containing 2 digit country codes as the key, and the name of a couuntry as the value
	 */
	function getCountries($value = ''){
		$country_list = array(
			"GB" => "United Kingdom",
			"US" => "United States",
			"AF" => "Afghanistan",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AQ" => "Antarctica",
			"AG" => "Antigua And Barbuda",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => "Belgium",
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia",
			"BA" => "Bosnia And Herzegowina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"IO" => "British Indian Ocean Territory",
			"BN" => "Brunei Darussalam",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos (Keeling) Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo",
			"CD" => "Congo, The Democratic Republic Of The",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"CI" => "Cote D'Ivoire",
			"HR" => "Croatia (Local Name: Hrvatska)",
			"CU" => "Cuba",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"TP" => "East Timor",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands (Malvinas)",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"FX" => "France, Metropolitan",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"GA" => "Gabon",
			"GM" => "Gambia",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GN" => "Guinea",
			"GW" => "Guinea-Bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard And Mc Donald Islands",
			"VA" => "Holy See (Vatican City State)",
			"HN" => "Honduras",
			"HK" => "Hong Kong",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran (Islamic Republic Of)",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IL" => "Israel",
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KP" => "Korea, Democratic People's Republic Of",
			"KR" => "Korea, Republic Of",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Lao People's Democratic Republic",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libyan Arab Jamahiriya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macau",
			"MK" => "Macedonia, Former Yugoslav Republic Of",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"MX" => "Mexico",
			"FM" => "Micronesia, Federated States Of",
			"MD" => "Moldova, Republic Of",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => "Netherlands",
			"AN" => "Netherlands Antilles",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PA" => "Panama",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RE" => "Reunion",
			"RO" => "Romania",
			"RU" => "Russian Federation",
			"RW" => "Rwanda",
			"KN" => "Saint Kitts And Nevis",
			"LC" => "Saint Lucia",
			"VC" => "Saint Vincent And The Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"ST" => "Sao Tome And Principe",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SK" => "Slovakia (Slovak Republic)",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia, South Sandwich Islands",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SH" => "St. Helena",
			"PM" => "St. Pierre And Miquelon",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard And Jan Mayen Islands",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syrian Arab Republic",
			"TW" => "Taiwan",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania, United Republic Of",
			"TH" => "Thailand",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad And Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks And Caicos Islands",
			"TV" => "Tuvalu",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"AE" => "United Arab Emirates",
			"UM" => "United States Minor Outlying Islands",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VE" => "Venezuela",
			"VN" => "Viet Nam",
			"VG" => "Virgin Islands (British)",
			"VI" => "Virgin Islands (U.S.)",
			"WF" => "Wallis And Futuna Islands",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"YU" => "Yugoslavia",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe"
		);
		return $country_list;
	}
	/**
	 * Return full name of a country
	 *
	 * @return String Name of country
	 */
	function getFullCountryName($countrycode) {
		$countriesarray = getCountries();
		return isArrayKeyAnEmptyString($countrycode, $countriesarray) ? '' : $countriesarray[$countrycode];
	}
	/**
	 * Return an array containing the 2 digit US state codes and names of the states
	 *
	 * @return Array Containing 2 digit state codes as the key, and the name of a US state as the value
	 */
	function getStates() {
		$state_list = array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming");
		
		return $state_list; 
	}
	/**
	 * Return full name of a US state
	 *
	 * @return String Name of state
	 */
	function getFullStateName($state) {
		$statesarray = getStates();
		return $statesarray[$state];
	}
   
	# determine signup contact categories
	function getContactUsCategories(){
		$query = "SELECT l.lookuptypevalue as optionvalue, l.lookupvaluedescription as optiontext FROM lookuptypevalue AS l INNER JOIN lookuptype AS v ON l.lookuptypeid = v.id WHERE v.name =  'CONTACTUS_CATEGORIES' ";
		return getOptionValuesFromDatabaseQuery($query);
	}
	# all listable variable groupings
	function getAllists($section = ''){
		$conn = Doctrine_Manager::connection();
		$query = '';
		if(!isEmptyString($section)){
			$query = " AND l.section = '".$section."' ";			
		}
		$all_lists = $conn->fetchAll("SELECT l.id as id, l.displayname as name, l.section, l.aclkey FROM lookuptype AS l WHERE l.listable = 1 ".$query." order by l.displayname ASC ");
		return $all_lists;
	}
	# all listable variable groupings as lookuptype
	function getListableLookupTypes($current = ''){
		$query = "SELECT l.id as optionvalue, l.displayname as optiontext FROM lookuptype AS l WHERE l.listable = 1 order by l.displayname ASC ";
		$array = getOptionValuesFromDatabaseQuery($query);
		return $array;
	}	
	# user types
	function getUserType($value = '', $ignorelist =''){
		$custom_query = "";
		if(!isEmptyString($ignorelist)){
			$custom_query .= " AND a.id NOT IN(".$ignorelist.") ";
		}
		
		$query = "SELECT a.id as optionvalue, a.name as optiontext FROM aclgroup a where a.id <> '' ".$custom_query." order by optiontext ";
		$array = getOptionValuesFromDatabaseQuery($query);
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		return $array;
	}
	function getSystemUsers($value = ''){
		$query = "SELECT u.id as optionvalue, concat_ws(' ', u.firstname, u.lastname) as optiontext FROM useraccount as u WHERE u.id <> '' AND u.status = 1  ORDER BY optiontext ";
		$array = getOptionValuesFromDatabaseQuery($query);
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		return $array;
	}
	# user status
	function getUserStatus($value = ''){
		$array = array(0 =>'Inactive', 1 => 'Active', 2=>'Deactivated');
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		return $array;
	}
	function getDirectoryStatuses($value = ''){
		$array = array(0 =>'Inactive', 1 => 'Active');
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		return $array;
	}
	# product status values
	function getActiveStatus($value = ''){
		$array = array('1' => 'Enabled', '0' =>'Disabled');
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		return $array;
	}
	# check for level one categories
	function getLevelOneCategories($value = ''){
		$query = "SELECT c.id as optionvalue, c.name as optiontext FROM category c where c.level = 1 order by optiontext ";
		$array = getOptionValuesFromDatabaseQuery($query);
		if(!isEmptyString($value)){
			return $array[$value];
		}
		return $array;
	}
	
	# determine the application config variables
	function getConfigLookups() {
		$conn = Doctrine_Manager::connection();
		$all_users = $conn->fetchAll("SELECT * FROM appconfig where id <> '' group by section order by sectiondisplay ");
		return $all_users;
	}
	
	# categories in category table
	function getCategories($sectorid = '', $parentid = ''){
		$custom_query = '';
		if(!isEmptyString($sectorid)){
			$custom_query .= " AND c.sectorid = '".$sectorid."' ";
		}
		if(!isEmptyString($parentid)){
			$custom_query .= " AND c.parentid = '".$parentid."' ";
		}
		$query = "SELECT c.id as optionvalue, c.name as optiontext FROM category AS c WHERE c.status = 1 ".$custom_query." order by c.id ASC ";
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	# latest system user 
	function getLatestUsers($limit){
		$conn = Doctrine_Manager::connection();
		$all_users = $conn->fetchAll("SELECT u.*, concat_ws(' ', u.firstname, u.lastname) as name FROM useraccount as u WHERE u.id <> '' order by u.datecreated DESC, u.id DESC limit ".$limit);
		return $all_users;
	}
	function getUsers($type = '', $limit = '', $ignoretype = '', $ignorelist = '', $wherequery = '', $hasemail = false, $hasphone = false){
		$custom_query = '';
		if(!isEmptyString($type)){
			$custom_query .= " AND u.type = '".$type."' ";
		}
		if(!isEmptyString($ignoretype)){
			$custom_query .= " AND u.type != '".$ignoretype."' ";
		}
		if(!isEmptyString($ignorelist)){
			$custom_query .= " AND u.id NOT IN(".$ignorelist.") ";
		}
		if(!isEmptyString($wherequery)){
			$custom_query .= $wherequery;
		}
		if($hasemail){
			$custom_query .= " AND u.email <> '' ";
		}
		if($hasphone){
			$custom_query .= " AND u.phone <> '' ";
		}
		
		$limit_query = '';
		if(!isEmptyString($limit)){
			$limit_query= ' LIMIT '.$limit;
		}
		$valuesquery = "SELECT u.id AS optionvalue, concat(u.firstname,' ',u.lastname) as optiontext FROM useraccount u WHERE u.id <> '' ".$custom_query." group BY u.id ORDER BY optiontext ASC ".$limit_query;
		// debugMessage($valuesquery);
		return getOptionValuesFromDatabaseQuery($valuesquery);
	}
	function getDataUsers($direction = true, $idslist = '', $inlist = '', $checknone = false){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($inlist)){
				$custom_query .= " AND q.id IN(".$inlist.") ";
			}
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
			if($checknone && isEmptyString($inlist)){
				$custom_query .= " AND q.id = '!!!' ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		$query = "SELECT q.id as optionvalue, concat(q.firstname,' ',q.lastname) as optiontext from useraccount q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); // exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	# fetch all members with an email
	function getUsersWithEmail(){
		$custom_query = " ";
		$valuesquery = "SELECT u.id AS optionvalue, concat(u.firstname,' ',u.lastname,' [',u.email,']') as optiontext FROM useraccount u WHERE u.email <> '' ".$custom_query." ORDER BY optiontext ASC ";
		return getOptionValuesFromDatabaseQuery($valuesquery);
	}
	# fetch all members with a phone
	function getUsersWithPhone(){
		$custom_query = "";
		$valuesquery = "SELECT u.id AS optionvalue, concat(u.firstname,' ',u.lastname,' [',u.phone,']') as optiontext FROM useraccount u WHERE u.phone <> '' ".$custom_query." ORDER BY optiontext ASC ";
		return getOptionValuesFromDatabaseQuery($valuesquery);
	}
	# count users in a group 
	function countActiveUsersInGroup($roleid, $status = ''){
		$custom_query = "";
		if(!isEmptyString($status)){
			$custom_query .= " AND u.status = 1 ";
		}
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT count(u.id) FROM useraccount u WHERE FIND_IN_SET('".$roleid."', u.typeids) > 0 ".$custom_query." ";
		$result = $conn->fetchOne($valuesquery);
		return $result;
	}
	
	function getUserDetails($type = '', $limit = '', $status = 1, $user ='', $start = '', $end ='', $emails=''){
		$custom_query = '';
		if(!isEmptyString($type)){
			$custom_query .= " AND u.type = '".$type."' ";
		}
		if(!isEmptyString($emails)){
			$custom_query .= " AND u.email in(".$emails.")";
		}
		$limit_query = '';
		if(!isEmptyString($limit)){
			$limit_query= ' LIMIT '.$limit;
		}
		$status_query = ' u.status = 1 ';
		if(!isEmptyString($status) && $status == 0){
			$status_query = " u.status = 0 ";
		}
		if(!isEmptyString($status) && $status == 2){
			$status_query = " (u.status = 1 || u.status = 0) ";
		}
		if(!isEmptyString($status) && $status == 'x'){
			$status_query = " (u.status <> '') ";
		}
		if(!isEmptyString($user)){
			$custom_query = " AND u.id = '".$user."' ";
		}
		if(!isEmptyString($start) && !isEmptyString($end)){
			$limit_query = ' LIMIT '.$start.','.$end;
		}
		
		$conn = Doctrine_Manager::connection();
		$query = "SELECT u.id AS id, u.firstname as firstname, concat(u.firstname, ' ', u.lastname) as name, u.email as email FROM useraccount as u WHERE u.id <> '' AND (".$status_query.") ".$custom_query." GROUP BY u.id ORDER BY name ASC ".$limit_query;
		// debugMessage($query);
		$all_lists = $conn->fetchAll($query);
		return $all_lists;
	}
	
	function getUserIDFromUsername($username){
		$valuesquery = "SELECT u.id FROM useraccount as u WHERE u.id <> '' AND u.username = '".$username."' ";
		// debugMessage($valuesquery);
		$conn = Doctrine_Manager::connection();
		$result = $conn->fetchOne($valuesquery);
		return $result;
		// return true;
	}
	function getUserIDFromEmail($email){
		$valuesquery = "SELECT u.id FROM useraccount as u WHERE u.id <> '' AND u.email = '".$email."' ";
		// debugMessage($valuesquery);
		$conn = Doctrine_Manager::connection();
		$result = $conn->fetchOne($valuesquery);
		return $result;
		// return true;
	}
	function getUserNameFromID($id){
		$valuesquery = "SELECT concat(u.firstname, ' ', u.lastname) as name FROM useraccount as u WHERE u.id = '".$id."' ";
		// debugMessage($valuesquery);
		$conn = Doctrine_Manager::connection();
		$result = $conn->fetchOne($valuesquery);
		return $result;
		// return true;
	}
	function getUserNamesFromID($id){
		$valuesquery = "SELECT concat_ws(' ', u.firstname, u.lastname) FROM useraccount as u WHERE u.id = '".$id."' ";
		$conn = Doctrine_Manager::connection();
		$result = $conn->fetchOne($valuesquery);
		return $result;
	}
	# determine the resources
	function getResources(){
		$query = "SELECT r.name AS optiontext, r.id AS optionvalue FROM aclresource AS r where r.status = 1 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getAllResources(){
		$conn = Doctrine_Manager::connection();
		$query = "SELECT r.* FROM aclresource AS r where r.status = 1 ORDER BY r.orderkey";
		$result = $conn->fetchAll($query);
		return $result;
	}
	function getAclResourceName($key){
		$conn = Doctrine_Manager::connection();
		$query = "SELECT r.description FROM aclresource AS r where r.name = '".$key."'";
		$result = $conn->fetchOne($query);
		return $result;
	}
	function getAclResourceID($key){
		$conn = Doctrine_Manager::connection();
		$query = "SELECT r.id FROM aclresource AS r where r.name = '".$key."'";
		$result = $conn->fetchOne($query);
		return $result;
	}
	function getActionNameforACL($key, $resourceid){
		$conn = Doctrine_Manager::connection();
		$query = "SELECT a.name FROM aclaction AS a where a.slug = '".$key."' AND a.resourceid = '".$resourceid."' "; // debugMessage($query);
		$result = $conn->fetchOne($query);
		return $result;
	}	
	function getActionsResources($resourceid){
		$conn = Doctrine_Manager::connection();
		$query = "SELECT a.* FROM aclaction AS a where a.resourceid = '".$resourceid."' order by a.sortorder asc ";
		$result = $conn->fetchAll($query);
		return $result;
	}
	function getPermissionsForGroup($groupid, $userid =''){
		$conn = Doctrine_Manager::connection();
		$perm_query = "SELECT
		p.resourceid,
		re.controller AS controller,
		LOWER(`re`.`name`) AS `resource`,
		p.groupid AS groupid,
		p.actionid,
		a.slug as action,
		p.value
		FROM aclpermission as p
		INNER JOIN `aclresource` `re` ON (`p`.`resourceid` = `re`.`id` AND re.status = 1)
		INNER JOIN `aclaction` `a` ON (`p`.`actionid` = `a`.`id`)
		WHERE p.groupid = '".$groupid."' AND re.status = 1 ";
		
		$result = $conn->fetchAll($perm_query); // debugMessage($result); exit;
		return $result;
	}
	# determine the dropdown value for a lookuptype
	function getDatavariables($lookuptypetext = '', $value = '', $checkempty = false, $query = ""){
		$custom_query = "";
		if(!isEmptyString($query)){
			$custom_query = $query;
		}
		$query = "SELECT l.lookuptypevalue as optionvalue, trim(l.lookupvaluedescription) as optiontext FROM lookuptypevalue AS l INNER JOIN lookuptype AS v ON l.lookuptypeid = v.id WHERE v.name = '".$lookuptypetext."' ".$custom_query." ";
		// debugMessage($query); // exit();
		$array = getOptionValuesFromDatabaseQuery($query);
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		if($checkempty && isEmptyString($value)){
			return '';
		}
		return $array;
	}
	# dropdown options with ordering option
	function getDatavariablesWithOdering($lookuptypetext = '', $value = '', $checkempty = false, $query = ""){
		$custom_query = "";
		if(!isEmptyString($query)){
			$custom_query = $query;
		}
		$query = "SELECT l.lookuptypevalue as optionvalue, trim(l.lookupvaluedescription) as optiontext FROM lookuptypevalue AS l INNER JOIN lookuptype AS v ON l.lookuptypeid = v.id WHERE v.name = '".$lookuptypetext."' ".$custom_query." order by optiontext ";
		// debugMessage($query); // exit();
		$array = getOptionValuesFromDatabaseQuery($query);
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		if($checkempty && isEmptyString($value)){
			return '';
		}
		return $array;
	}
	 
	function getWeekDays(){
		return array(1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday');
	}
	/**
	 * Determine the first day of a week
	 * @param String $date the date of the week
	 * @return String the first date of a week
	 */
	function getWeekStartDate($date) {
	    $ts = strtotime($date);
	    $start = (date('w', $ts) == 1) ? $ts : strtotime('last monday', $ts);
	    return date('Y-m-d', $start);
	}
	/**
	 * Determine the first day of a week
	 * @param String $date the date of the week
	 * @return String the first date of a week
	 */
	function getWeekEndDate($date) {
		$ts = strtotime($date);
		$start = (date('w', $ts) == 7) ? $ts : strtotime('this sunday', $ts);
		return date('Y-m-d', $start);
	}
	# determine the date after adding x days
	function getDateAfterDays($date, $days) {
		return date('Y-m-d', strtotime('+'.$days.' days', strtotime($date)));
	}
	# determine the date after adding x days
	function getDateBeforeDays($date, $days) {
		return date('Y-m-d', strtotime('-'.$days.' days', strtotime($date)));
	}
	function getShortWeekDays(){
		return array(1=>'Mon', 2=>'Tue', 3=>'Wed', 4=>'Thur', 5=>'Fri', 6=>'Sat', 7=>'Sun');
	}
	function getMondayOfWeek($week, $year) {
		$timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ((7+1-(date( 'N', mktime( 0, 0, 0, 1, 1,  $year ) )))*86400) + ($week-2)*7*86400 + 1 ;
		return date('Y-m-d', $timestamp);
	}
	function getSundayOfWeek($week, $year) {
		$monday = getMondayOfWeek($week, $year); 
		return date('Y-m-d', strtotime('+6 days', strtotime($monday)));
	}
	# determine year month dropdown
	function getYearMonthDropdown($value ='', $checkempty = false){
		$array =  array('1'=>'Per Year', '2'=>'Per Month');
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		if($checkempty && isEmptyString($value)){
			return '';
		}
	
		return $array;
	}
	# determine day hours dropdown
	function getHoursDaysDropdown($value ='', $checkempty = false){
		$array =  array('1'=>'Hours', '2'=>'Days');
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		if($checkempty && isEmptyString($value)){
			return '';
		}
	
		return $array;
	}
	function getDefaultThemeOptions(){
		$conn = Doctrine_Manager::connection();
		$config = Zend_Registry::get("config"); 
		$result['layout'] = getDefaultLayout();
		$result['topbar'] = getDefaultTopBar();
		$result['sidebar'] = getDefaultSideBar();
		$result['colortheme'] = getDefaultTheme();
		$result['showsidebar'] = getDefaultShowSideBar();
		return $result;
	}
	# get the theme colors 
	function getAllThemeColors(){
		$colors = array(
			"red" => "#e51400",
			"orange" => "#f8a31f",
			"green" => "#339933",
			"brown" => "#a05000",
			"blue" => "#368ee0",
			"lime" => "#8cbf26",
			"teal" => "#00aba9",
			"purple" => "#ff0097",
			"pink" => "#6e2049",
			"magenta" => "#a200ff;",
			"grey" => "#333333",
			"darkblue" => "#204e81",
			"lightred" => "#e63a3a",
			"lightgrey" => "#e63a3a",
			"satblue" => "#2c5e7b",
			"satgreen" => "#56af45"
		);
		return $colors;
	}
	# determine country timezones
	function getAllTimezones(){
		$timezones = array (
			'America/Atka' => '(GMT-10:00) America/Atka (Hawaii-Aleutian Standard Time)',
			'America/Anchorage' => '(GMT-9:00) America/Anchorage (Alaska Standard Time)',
			'America/Juneau' => '(GMT-9:00) America/Juneau (Alaska Standard Time)',
			'America/Nome' => '(GMT-9:00) America/Nome (Alaska Standard Time)',
			'America/Yakutat' => '(GMT-9:00) America/Yakutat (Alaska Standard Time)',
			'America/Dawson' => '(GMT-8:00) America/Dawson (Pacific Standard Time)',
			'America/Ensenada' => '(GMT-8:00) America/Ensenada (Pacific Standard Time)',
			'America/Los_Angeles' => '(GMT-8:00) America/Los_Angeles (Pacific Standard Time)',
			'America/Tijuana' => '(GMT-8:00) America/Tijuana (Pacific Standard Time)',
			'America/Vancouver' => '(GMT-8:00) America/Vancouver (Pacific Standard Time)',
			'America/Whitehorse' => '(GMT-8:00) America/Whitehorse (Pacific Standard Time)',
			'Canada/Pacific' => '(GMT-8:00) Canada/Pacific (Pacific Standard Time)',
			'Canada/Yukon' => '(GMT-8:00) Canada/Yukon (Pacific Standard Time)',
			'Mexico/BajaNorte' => '(GMT-8:00) Mexico/BajaNorte (Pacific Standard Time)',
			'America/Boise' => '(GMT-7:00) America/Boise (Mountain Standard Time)',
			'America/Cambridge_Bay' => '(GMT-7:00) America/Cambridge_Bay (Mountain Standard Time)',
			'America/Chihuahua' => '(GMT-7:00) America/Chihuahua (Mountain Standard Time)',
			'America/Dawson_Creek' => '(GMT-7:00) America/Dawson_Creek (Mountain Standard Time)',
			'America/Denver' => '(GMT-7:00) America/Denver (Mountain Standard Time)',
			'America/Edmonton' => '(GMT-7:00) America/Edmonton (Mountain Standard Time)',
			'America/Hermosillo' => '(GMT-7:00) America/Hermosillo (Mountain Standard Time)',
			'America/Inuvik' => '(GMT-7:00) America/Inuvik (Mountain Standard Time)',
			'America/Mazatlan' => '(GMT-7:00) America/Mazatlan (Mountain Standard Time)',
			'America/Phoenix' => '(GMT-7:00) America/Phoenix (Mountain Standard Time)',
			'America/Shiprock' => '(GMT-7:00) America/Shiprock (Mountain Standard Time)',
			'America/Yellowknife' => '(GMT-7:00) America/Yellowknife (Mountain Standard Time)',
			'Canada/Mountain' => '(GMT-7:00) Canada/Mountain (Mountain Standard Time)',
			'Mexico/BajaSur' => '(GMT-7:00) Mexico/BajaSur (Mountain Standard Time)',
			'America/Belize' => '(GMT-6:00) America/Belize (Central Standard Time)',
			'America/Cancun' => '(GMT-6:00) America/Cancun (Central Standard Time)',
			'America/Chicago' => '(GMT-6:00) America/Chicago (Central Standard Time)',
			'America/Costa_Rica' => '(GMT-6:00) America/Costa_Rica (Central Standard Time)',
			'America/El_Salvador' => '(GMT-6:00) America/El_Salvador (Central Standard Time)',
			'America/Guatemala' => '(GMT-6:00) America/Guatemala (Central Standard Time)',
			'America/Knox_IN' => '(GMT-6:00) America/Knox_IN (Central Standard Time)',
			'America/Managua' => '(GMT-6:00) America/Managua (Central Standard Time)',
			'America/Menominee' => '(GMT-6:00) America/Menominee (Central Standard Time)',
			'America/Merida' => '(GMT-6:00) America/Merida (Central Standard Time)',
			'America/Mexico_City' => '(GMT-6:00) America/Mexico_City (Central Standard Time)',
			'America/Monterrey' => '(GMT-6:00) America/Monterrey (Central Standard Time)',
			'America/Rainy_River' => '(GMT-6:00) America/Rainy_River (Central Standard Time)',
			'America/Rankin_Inlet' => '(GMT-6:00) America/Rankin_Inlet (Central Standard Time)',
			'America/Regina' => '(GMT-6:00) America/Regina (Central Standard Time)',
			'America/Swift_Current' => '(GMT-6:00) America/Swift_Current (Central Standard Time)',
			'America/Tegucigalpa' => '(GMT-6:00) America/Tegucigalpa (Central Standard Time)',
			'America/Winnipeg' => '(GMT-6:00) America/Winnipeg (Central Standard Time)',
			'Canada/Central' => '(GMT-6:00) Canada/Central (Central Standard Time)',
			'Canada/East-Saskatchewan' => '(GMT-6:00) Canada/East-Saskatchewan (Central Standard Time)',
			'Canada/Saskatchewan' => '(GMT-6:00) Canada/Saskatchewan (Central Standard Time)',
			'Chile/EasterIsland' => '(GMT-6:00) Chile/EasterIsland (Easter Is. Time)',
			'Mexico/General' => '(GMT-6:00) Mexico/General (Central Standard Time)',
			'America/Atikokan' => '(GMT-5:00) America/Atikokan (Eastern Standard Time)',
			'America/Bogota' => '(GMT-5:00) America/Bogota (Colombia Time)',
			'America/Cayman' => '(GMT-5:00) America/Cayman (Eastern Standard Time)',
			'America/Coral_Harbour' => '(GMT-5:00) America/Coral_Harbour (Eastern Standard Time)',
			'America/Detroit' => '(GMT-5:00) America/Detroit (Eastern Standard Time)',
			'America/Fort_Wayne' => '(GMT-5:00) America/Fort_Wayne (Eastern Standard Time)',
			'America/Grand_Turk' => '(GMT-5:00) America/Grand_Turk (Eastern Standard Time)',
			'America/Guayaquil' => '(GMT-5:00) America/Guayaquil (Ecuador Time)',
			'America/Havana' => '(GMT-5:00) America/Havana (Cuba Standard Time)',
			'America/Indianapolis' => '(GMT-5:00) America/Indianapolis (Eastern Standard Time)',
			'America/Iqaluit' => '(GMT-5:00) America/Iqaluit (Eastern Standard Time)',
			'America/Jamaica' => '(GMT-5:00) America/Jamaica (Eastern Standard Time)',
			'America/Lima' => '(GMT-5:00) America/Lima (Peru Time)',
			'America/Louisville' => '(GMT-5:00) America/Louisville (Eastern Standard Time)',
			'America/Montreal' => '(GMT-5:00) America/Montreal (Eastern Standard Time)',
			'America/Nassau' => '(GMT-5:00) America/Nassau (Eastern Standard Time)',
			'America/New_York' => '(GMT-5:00) America/New_York (Eastern Standard Time)',
			'America/Nipigon' => '(GMT-5:00) America/Nipigon (Eastern Standard Time)',
			'America/Panama' => '(GMT-5:00) America/Panama (Eastern Standard Time)',
			'America/Pangnirtung' => '(GMT-5:00) America/Pangnirtung (Eastern Standard Time)',
			'America/Port-au-Prince' => '(GMT-5:00) America/Port-au-Prince (Eastern Standard Time)',
			'America/Resolute' => '(GMT-5:00) America/Resolute (Eastern Standard Time)',
			'America/Thunder_Bay' => '(GMT-5:00) America/Thunder_Bay (Eastern Standard Time)',
			'America/Toronto' => '(GMT-5:00) America/Toronto (Eastern Standard Time)',
			'Canada/Eastern' => '(GMT-5:00) Canada/Eastern (Eastern Standard Time)',
			'America/Caracas' => '(GMT-4:-30) America/Caracas (Venezuela Time)',
			'America/Anguilla' => '(GMT-4:00) America/Anguilla (Atlantic Standard Time)',
			'America/Antigua' => '(GMT-4:00) America/Antigua (Atlantic Standard Time)',
			'America/Aruba' => '(GMT-4:00) America/Aruba (Atlantic Standard Time)',
			'America/Asuncion' => '(GMT-4:00) America/Asuncion (Paraguay Time)',
			'America/Barbados' => '(GMT-4:00) America/Barbados (Atlantic Standard Time)',
			'America/Blanc-Sablon' => '(GMT-4:00) America/Blanc-Sablon (Atlantic Standard Time)',
			'America/Boa_Vista' => '(GMT-4:00) America/Boa_Vista (Amazon Time)',
			'America/Campo_Grande' => '(GMT-4:00) America/Campo_Grande (Amazon Time)',
			'America/Cuiaba' => '(GMT-4:00) America/Cuiaba (Amazon Time)',
			'America/Curacao' => '(GMT-4:00) America/Curacao (Atlantic Standard Time)',
			'America/Dominica' => '(GMT-4:00) America/Dominica (Atlantic Standard Time)',
			'America/Eirunepe' => '(GMT-4:00) America/Eirunepe (Amazon Time)',
			'America/Glace_Bay' => '(GMT-4:00) America/Glace_Bay (Atlantic Standard Time)',
			'America/Goose_Bay' => '(GMT-4:00) America/Goose_Bay (Atlantic Standard Time)',
			'America/Grenada' => '(GMT-4:00) America/Grenada (Atlantic Standard Time)',
			'America/Guadeloupe' => '(GMT-4:00) America/Guadeloupe (Atlantic Standard Time)',
			'America/Guyana' => '(GMT-4:00) America/Guyana (Guyana Time)',
			'America/Halifax' => '(GMT-4:00) America/Halifax (Atlantic Standard Time)',
			'America/La_Paz' => '(GMT-4:00) America/La_Paz (Bolivia Time)',
			'America/Manaus' => '(GMT-4:00) America/Manaus (Amazon Time)',
			'America/Marigot' => '(GMT-4:00) America/Marigot (Atlantic Standard Time)',
			
			'America/Martinique' => '(GMT-4:00) America/Martinique (Atlantic Standard Time)',
			'America/Moncton' => '(GMT-4:00) America/Moncton (Atlantic Standard Time)',
			'America/Montserrat' => '(GMT-4:00) America/Montserrat (Atlantic Standard Time)',
			'America/Port_of_Spain' => '(GMT-4:00) America/Port_of_Spain (Atlantic Standard Time)',
			'America/Porto_Acre' => '(GMT-4:00) America/Porto_Acre (Amazon Time)',
			'America/Porto_Velho' => '(GMT-4:00) America/Porto_Velho (Amazon Time)',
			'America/Puerto_Rico' => '(GMT-4:00) America/Puerto_Rico (Atlantic Standard Time)',
			'America/Rio_Branco' => '(GMT-4:00) America/Rio_Branco (Amazon Time)',
			'America/Santiago' => '(GMT-4:00) America/Santiago (Chile Time)',
			'America/Santo_Domingo' => '(GMT-4:00) America/Santo_Domingo (Atlantic Standard Time)',
			'America/St_Barthelemy' => '(GMT-4:00) America/St_Barthelemy (Atlantic Standard Time)',
			'America/St_Kitts' => '(GMT-4:00) America/St_Kitts (Atlantic Standard Time)',
			'America/St_Lucia' => '(GMT-4:00) America/St_Lucia (Atlantic Standard Time)',
			'America/St_Thomas' => '(GMT-4:00) America/St_Thomas (Atlantic Standard Time)',
			'America/St_Vincent' => '(GMT-4:00) America/St_Vincent (Atlantic Standard Time)',
			'America/Thule' => '(GMT-4:00) America/Thule (Atlantic Standard Time)',
			'America/Tortola' => '(GMT-4:00) America/Tortola (Atlantic Standard Time)',
			'America/Virgin' => '(GMT-4:00) America/Virgin (Atlantic Standard Time)',
			'Antarctica/Palmer' => '(GMT-4:00) Antarctica/Palmer (Chile Time)',
			'Atlantic/Bermuda' => '(GMT-4:00) Atlantic/Bermuda (Atlantic Standard Time)',
			'Atlantic/Stanley' => '(GMT-4:00) Atlantic/Stanley (Falkland Is. Time)',
			'Brazil/Acre' => '(GMT-4:00) Brazil/Acre (Amazon Time)',
			'Brazil/West' => '(GMT-4:00) Brazil/West (Amazon Time)',
			'Canada/Atlantic' => '(GMT-4:00) Canada/Atlantic (Atlantic Standard Time)',
			'Chile/Continental' => '(GMT-4:00) Chile/Continental (Chile Time)',
			'America/St_Johns' => '(GMT-3:-30) America/St_Johns (Newfoundland Standard Time)',
			
			'America/St_Johns' => '(GMT-3:-30) America/St_Johns (Newfoundland Standard Time)',
			'Canada/Newfoundland' => '(GMT-3:-30) Canada/Newfoundland (Newfoundland Standard Time)',
			'America/Araguaina' => '(GMT-3:00) America/Araguaina (Brasilia Time)',
			'America/Bahia' => '(GMT-3:00) America/Bahia (Brasilia Time)',
			'America/Belem' => '(GMT-3:00) America/Belem (Brasilia Time)',
			'America/Buenos_Aires' => '(GMT-3:00) America/Buenos_Aires (Argentine Time)',
			'America/Catamarca' => '(GMT-3:00) America/Catamarca (Argentine Time)',
			'America/Cayenne' => '(GMT-3:00) America/Cayenne (French Guiana Time)',
			'America/Cordoba' => '(GMT-3:00) America/Cordoba (Argentine Time)',
			'America/Fortaleza' => '(GMT-3:00) America/Fortaleza (Brasilia Time)',
			'America/Godthab' => '(GMT-3:00) America/Godthab (Western Greenland Time)',
			'America/Jujuy' => '(GMT-3:00) America/Jujuy (Argentine Time)',
			'America/Maceio' => '(GMT-3:00) America/Maceio (Brasilia Time)',
			'America/Mendoza' => '(GMT-3:00) America/Mendoza (Argentine Time)',
			'America/Miquelon' => '(GMT-3:00) America/Miquelon (Pierre & Miquelon Standard Time)',
			'America/Montevideo' => '(GMT-3:00) America/Montevideo (Uruguay Time)',
			'America/Paramaribo' => '(GMT-3:00) America/Paramaribo (Suriname Time)',
			'America/Recife' => '(GMT-3:00) America/Recife (Brasilia Time)',
			'America/Rosario' => '(GMT-3:00) America/Rosario (Argentine Time)',
			'America/Santarem' => '(GMT-3:00) America/Santarem (Brasilia Time)',
			'America/Sao_Paulo' => '(GMT-3:00) America/Sao_Paulo (Brasilia Time)',
			'Antarctica/Rothera' => '(GMT-3:00) Antarctica/Rothera (Rothera Time)',
			'Brazil/East' => '(GMT-3:00) Brazil/East (Brasilia Time)',
			'America/Noronha' => '(GMT-2:00) America/Noronha (Fernando de Noronha Time)',
			'Atlantic/South_Georgia' => '(GMT-2:00) Atlantic/South_Georgia (South Georgia Standard Time)',
			
			'Brazil/DeNoronha' => '(GMT-2:00) Brazil/DeNoronha (Fernando de Noronha Time)',
			'America/Scoresbysund' => '(GMT-1:00) America/Scoresbysund (Eastern Greenland Time)',
			'Atlantic/Azores' => '(GMT-1:00) Atlantic/Azores (Azores Time)',
			'Atlantic/Cape_Verde' => '(GMT-1:00) Atlantic/Cape_Verde (Cape Verde Time)',
			'Africa/Abidjan' => '(GMT+0:00) Africa/Abidjan (Greenwich Mean Time)',
			'Africa/Accra' => '(GMT+0:00) Africa/Accra (Ghana Mean Time)',
			'Africa/Bamako' => '(GMT+0:00) Africa/Bamako (Greenwich Mean Time)',
			'Africa/Banjul' => '(GMT+0:00) Africa/Banjul (Greenwich Mean Time)',
			'Africa/Bissau' => '(GMT+0:00) Africa/Bissau (Greenwich Mean Time)',
			'Africa/Casablanca' => '(GMT+0:00) Africa/Casablanca (Western European Time)',
			'Africa/Conakry' => '(GMT+0:00) Africa/Conakry (Greenwich Mean Time)',
			'Africa/Dakar' => '(GMT+0:00) Africa/Dakar (Greenwich Mean Time)',
			'Africa/El_Aaiun' => '(GMT+0:00) Africa/El_Aaiun (Western European Time)',
			'Africa/Freetown' => '(GMT+0:00) Africa/Freetown (Greenwich Mean Time)',
			'Africa/Lome' => '(GMT+0:00) Africa/Lome (Greenwich Mean Time)',
			'Africa/Monrovia' => '(GMT+0:00) Africa/Monrovia (Greenwich Mean Time)',
			'Africa/Nouakchott' => '(GMT+0:00) Africa/Nouakchott (Greenwich Mean Time)',
			'Africa/Ouagadougou' => '(GMT+0:00) Africa/Ouagadougou (Greenwich Mean Time)',
			'Africa/Sao_Tome' => '(GMT+0:00) Africa/Sao_Tome (Greenwich Mean Time)',
			'Africa/Timbuktu' => '(GMT+0:00) Africa/Timbuktu (Greenwich Mean Time)',
			'America/Danmarkshavn' => '(GMT+0:00) America/Danmarkshavn (Greenwich Mean Time)',
			'Atlantic/Canary' => '(GMT+0:00) Atlantic/Canary (Western European Time)',
			'Atlantic/Faeroe' => '(GMT+0:00) Atlantic/Faeroe (Western European Time)',
			'Atlantic/Faroe' => '(GMT+0:00) Atlantic/Faroe (Western European Time)',
			'Atlantic/Madeira' => '(GMT+0:00) Atlantic/Madeira (Western European Time)',
			'Atlantic/Reykjavik' => '(GMT+0:00) Atlantic/Reykjavik (Greenwich Mean Time)',
			'Atlantic/St_Helena' => '(GMT+0:00) Atlantic/St_Helena (Greenwich Mean Time)',
			'Europe/Belfast' => '(GMT+0:00) Europe/Belfast (Greenwich Mean Time)',
			'Europe/Dublin' => '(GMT+0:00) Europe/Dublin (Greenwich Mean Time)',
			'Europe/Guernsey' => '(GMT+0:00) Europe/Guernsey (Greenwich Mean Time)',
			'Europe/Isle_of_Man' => '(GMT+0:00) Europe/Isle_of_Man (Greenwich Mean Time)',
			'Europe/Jersey' => '(GMT+0:00) Europe/Jersey (Greenwich Mean Time)',
			'Europe/Lisbon' => '(GMT+0:00) Europe/Lisbon (Western European Time)',
			'Europe/London' => '(GMT+0:00) Europe/London (Greenwich Mean Time)',
			'Africa/Algiers' => '(GMT+1:00) Africa/Algiers (Central European Time)',
			'Africa/Bangui' => '(GMT+1:00) Africa/Bangui (Western African Time)',
			'Africa/Brazzaville' => '(GMT+1:00) Africa/Brazzaville (Western African Time)',
			'Africa/Ceuta' => '(GMT+1:00) Africa/Ceuta (Central European Time)',
			'Africa/Douala' => '(GMT+1:00) Africa/Douala (Western African Time)',
			'Africa/Kinshasa' => '(GMT+1:00) Africa/Kinshasa (Western African Time)',
			'Africa/Lagos' => '(GMT+1:00) Africa/Lagos (Western African Time)',
			'Africa/Libreville' => '(GMT+1:00) Africa/Libreville (Western African Time)',
			'Africa/Luanda' => '(GMT+1:00) Africa/Luanda (Western African Time)',
			'Africa/Malabo' => '(GMT+1:00) Africa/Malabo (Western African Time)',
			'Africa/Ndjamena' => '(GMT+1:00) Africa/Ndjamena (Western African Time)',
			'Africa/Niamey' => '(GMT+1:00) Africa/Niamey (Western African Time)',
			'Africa/Porto-Novo' => '(GMT+1:00) Africa/Porto-Novo (Western African Time)',
			'Africa/Tunis' => '(GMT+1:00) Africa/Tunis (Central European Time)',
			'Africa/Windhoek' => '(GMT+1:00) Africa/Windhoek (Western African Time)',
			'Arctic/Longyearbyen' => '(GMT+1:00) Arctic/Longyearbyen (Central European Time)',
			
			'Atlantic/Jan_Mayen' => '(GMT+1:00) Atlantic/Jan_Mayen (Central European Time)',
			'Europe/Amsterdam' => '(GMT+1:00) Europe/Amsterdam (Central European Time)',
			'Europe/Andorra' => '(GMT+1:00) Europe/Andorra (Central European Time)',
			'Europe/Belgrade' => '(GMT+1:00) Europe/Belgrade (Central European Time)',
			'Europe/Berlin' => '(GMT+1:00) Europe/Berlin (Central European Time)',
			'Europe/Bratislava' => '(GMT+1:00) Europe/Bratislava (Central European Time)',
			'Europe/Brussels' => '(GMT+1:00) Europe/Brussels (Central European Time)',
			'Europe/Budapest' => '(GMT+1:00) Europe/Budapest (Central European Time)',
			'Europe/Copenhagen' => '(GMT+1:00) Europe/Copenhagen (Central European Time)',
			'Europe/Gibraltar' => '(GMT+1:00) Europe/Gibraltar (Central European Time)',
			'Europe/Ljubljana' => '(GMT+1:00) Europe/Ljubljana (Central European Time)',
			'Europe/Luxembourg' => '(GMT+1:00) Europe/Luxembourg (Central European Time)',
			'Europe/Madrid' => '(GMT+1:00) Europe/Madrid (Central European Time)',
			'Europe/Malta' => '(GMT+1:00) Europe/Malta (Central European Time)',
			'Europe/Monaco' => '(GMT+1:00) Europe/Monaco (Central European Time)',
			'Europe/Oslo' => '(GMT+1:00) Europe/Oslo (Central European Time)',
			'Europe/Paris' => '(GMT+1:00) Europe/Paris (Central European Time)',
			'Europe/Podgorica' => '(GMT+1:00) Europe/Podgorica (Central European Time)',
			'Europe/Prague' => '(GMT+1:00) Europe/Prague (Central European Time)',
			'Europe/Rome' => '(GMT+1:00) Europe/Rome (Central European Time)',
			'Europe/San_Marino' => '(GMT+1:00) Europe/San_Marino (Central European Time)',
			'Europe/Sarajevo' => '(GMT+1:00) Europe/Sarajevo (Central European Time)',
			'Europe/Skopje' => '(GMT+1:00) Europe/Skopje (Central European Time)',
			'Europe/Stockholm' => '(GMT+1:00) Europe/Stockholm (Central European Time)',
			'Europe/Tirane' => '(GMT+1:00) Europe/Tirane (Central European Time)',
			'Europe/Vaduz' => '(GMT+1:00) Europe/Vaduz (Central European Time)',
			'Europe/Vatican' => '(GMT+1:00) Europe/Vatican (Central European Time)',
			'Europe/Vienna' => '(GMT+1:00) Europe/Vienna (Central European Time)',
			'Europe/Warsaw' => '(GMT+1:00) Europe/Warsaw (Central European Time)',
			'Europe/Zagreb' => '(GMT+1:00) Europe/Zagreb (Central European Time)',
			'Europe/Zurich' => '(GMT+1:00) Europe/Zurich (Central European Time)',
			'Africa/Blantyre' => '(GMT+2:00) Africa/Blantyre (Central African Time)',
			'Africa/Bujumbura' => '(GMT+2:00) Africa/Bujumbura (Central African Time)',
			'Africa/Cairo' => '(GMT+2:00) Africa/Cairo (Eastern European Time)',
			'Africa/Gaborone' => '(GMT+2:00) Africa/Gaborone (Central African Time)',
			'Africa/Harare' => '(GMT+2:00) Africa/Harare (Central African Time)',
			'Africa/Johannesburg' => '(GMT+2:00) Africa/Johannesburg (South Africa Standard Time)',
			'Africa/Kigali' => '(GMT+2:00) Africa/Kigali (Central African Time)',
			'Africa/Lubumbashi' => '(GMT+2:00) Africa/Lubumbashi (Central African Time)',
			'Africa/Lusaka' => '(GMT+2:00) Africa/Lusaka (Central African Time)',
			'Africa/Maputo' => '(GMT+2:00) Africa/Maputo (Central African Time)',
			'Africa/Maseru' => '(GMT+2:00) Africa/Maseru (South Africa Standard Time)',
			'Africa/Mbabane' => '(GMT+2:00) Africa/Mbabane (South Africa Standard Time)',
			'Africa/Tripoli' => '(GMT+2:00) Africa/Tripoli (Eastern European Time)',
			'Asia/Amman' => '(GMT+2:00) Asia/Amman (Eastern European Time)',
			'Asia/Beirut' => '(GMT+2:00) Asia/Beirut (Eastern European Time)',
			'Asia/Damascus' => '(GMT+2:00) Asia/Damascus (Eastern European Time)',
			'Asia/Gaza' => '(GMT+2:00) Asia/Gaza (Eastern European Time)',
			'Asia/Istanbul' => '(GMT+2:00) Asia/Istanbul (Eastern European Time)',
			'Asia/Jerusalem' => '(GMT+2:00) Asia/Jerusalem (Israel Standard Time)',
			
			'Asia/Nicosia' => '(GMT+2:00) Asia/Nicosia (Eastern European Time)',
			'Asia/Tel_Aviv' => '(GMT+2:00) Asia/Tel_Aviv (Israel Standard Time)',
			'Europe/Athens' => '(GMT+2:00) Europe/Athens (Eastern European Time)',
			'Europe/Bucharest' => '(GMT+2:00) Europe/Bucharest (Eastern European Time)',
			'Europe/Chisinau' => '(GMT+2:00) Europe/Chisinau (Eastern European Time)',
			'Europe/Helsinki' => '(GMT+2:00) Europe/Helsinki (Eastern European Time)',
			'Europe/Istanbul' => '(GMT+2:00) Europe/Istanbul (Eastern European Time)',
			'Europe/Kaliningrad' => '(GMT+2:00) Europe/Kaliningrad (Eastern European Time)',
			'Europe/Kiev' => '(GMT+2:00) Europe/Kiev (Eastern European Time)',
			'Europe/Mariehamn' => '(GMT+2:00) Europe/Mariehamn (Eastern European Time)',
			'Europe/Minsk' => '(GMT+2:00) Europe/Minsk (Eastern European Time)',
			'Europe/Nicosia' => '(GMT+2:00) Europe/Nicosia (Eastern European Time)',
			'Europe/Riga' => '(GMT+2:00) Europe/Riga (Eastern European Time)',
			'Europe/Simferopol' => '(GMT+2:00) Europe/Simferopol (Eastern European Time)',
			'Europe/Sofia' => '(GMT+2:00) Europe/Sofia (Eastern European Time)',
			'Europe/Tallinn' => '(GMT+2:00) Europe/Tallinn (Eastern European Time)',
			'Europe/Tiraspol' => '(GMT+2:00) Europe/Tiraspol (Eastern European Time)',
			'Europe/Uzhgorod' => '(GMT+2:00) Europe/Uzhgorod (Eastern European Time)',
			'Europe/Vilnius' => '(GMT+2:00) Europe/Vilnius (Eastern European Time)',
			'Europe/Zaporozhye' => '(GMT+2:00) Europe/Zaporozhye (Eastern European Time)',
			'Africa/Addis_Ababa' => '(GMT+3:00) Africa/Addis_Ababa (Eastern African Time)',
			'Africa/Asmara' => '(GMT+3:00) Africa/Asmara (Eastern African Time)',
			'Africa/Asmera' => '(GMT+3:00) Africa/Asmera (Eastern African Time)',
			'Africa/Dar_es_Salaam' => '(GMT+3:00) Africa/Dar_es_Salaam (Eastern African Time)',
			'Africa/Djibouti' => '(GMT+3:00) Africa/Djibouti (Eastern African Time)',
			'Africa/Kampala' => '(GMT+3:00) Africa/Kampala (Eastern African Time)',
			'Africa/Khartoum' => '(GMT+3:00) Africa/Khartoum (Eastern African Time)',
			'Africa/Mogadishu' => '(GMT+3:00) Africa/Mogadishu (Eastern African Time)',
			'Africa/Nairobi' => '(GMT+3:00) Africa/Nairobi (Eastern African Time)',
			'Antarctica/Syowa' => '(GMT+3:00) Antarctica/Syowa (Syowa Time)',
			'Asia/Aden' => '(GMT+3:00) Asia/Aden (Arabia Standard Time)',
			'Asia/Baghdad' => '(GMT+3:00) Asia/Baghdad (Arabia Standard Time)',
			'Asia/Bahrain' => '(GMT+3:00) Asia/Bahrain (Arabia Standard Time)',
			'Asia/Kuwait' => '(GMT+3:00) Asia/Kuwait (Arabia Standard Time)',
			'Asia/Qatar' => '(GMT+3:00) Asia/Qatar (Arabia Standard Time)',
			'Europe/Moscow' => '(GMT+3:00) Europe/Moscow (Moscow Standard Time)',
			'Europe/Volgograd' => '(GMT+3:00) Europe/Volgograd (Volgograd Time)',
			'Indian/Antananarivo' => '(GMT+3:00) Indian/Antananarivo (Eastern African Time)',
			'Indian/Comoro' => '(GMT+3:00) Indian/Comoro (Eastern African Time)',
			'Indian/Mayotte' => '(GMT+3:00) Indian/Mayotte (Eastern African Time)',
			'Asia/Tehran' => '(GMT+3:30) Asia/Tehran (Iran Standard Time)',
			'Asia/Baku' => '(GMT+4:00) Asia/Baku (Azerbaijan Time)',
			'Asia/Dubai' => '(GMT+4:00) Asia/Dubai (Gulf Standard Time)',
			'Asia/Muscat' => '(GMT+4:00) Asia/Muscat (Gulf Standard Time)',
			'Asia/Tbilisi' => '(GMT+4:00) Asia/Tbilisi (Georgia Time)',
			'Asia/Yerevan' => '(GMT+4:00) Asia/Yerevan (Armenia Time)',
			'Europe/Samara' => '(GMT+4:00) Europe/Samara (Samara Time)',
			'Indian/Mahe' => '(GMT+4:00) Indian/Mahe (Seychelles Time)',
			'Indian/Mauritius' => '(GMT+4:00) Indian/Mauritius (Mauritius Time)',
			'Indian/Reunion' => '(GMT+4:00) Indian/Reunion (Reunion Time)',
			
			'Asia/Kabul' => '(GMT+4:30) Asia/Kabul (Afghanistan Time)',
			'Asia/Aqtau' => '(GMT+5:00) Asia/Aqtau (Aqtau Time)',
			'Asia/Aqtobe' => '(GMT+5:00) Asia/Aqtobe (Aqtobe Time)',
			'Asia/Ashgabat' => '(GMT+5:00) Asia/Ashgabat (Turkmenistan Time)',
			'Asia/Ashkhabad' => '(GMT+5:00) Asia/Ashkhabad (Turkmenistan Time)',
			'Asia/Dushanbe' => '(GMT+5:00) Asia/Dushanbe (Tajikistan Time)',
			'Asia/Karachi' => '(GMT+5:00) Asia/Karachi (Pakistan Time)',
			'Asia/Oral' => '(GMT+5:00) Asia/Oral (Oral Time)',
			'Asia/Samarkand' => '(GMT+5:00) Asia/Samarkand (Uzbekistan Time)',
			'Asia/Tashkent' => '(GMT+5:00) Asia/Tashkent (Uzbekistan Time)',
			'Asia/Yekaterinburg' => '(GMT+5:00) Asia/Yekaterinburg (Yekaterinburg Time)',
			'Indian/Kerguelen' => '(GMT+5:00) Indian/Kerguelen (French Southern & Antarctic Lands Time)',
			'Indian/Maldives' => '(GMT+5:00) Indian/Maldives (Maldives Time)',
			'Asia/Calcutta' => '(GMT+5:30) Asia/Calcutta (India Standard Time)',
			'Asia/Colombo' => '(GMT+5:30) Asia/Colombo (India Standard Time)',
			'Asia/Kolkata' => '(GMT+5:30) Asia/Kolkata (India Standard Time)',
			'Asia/Katmandu' => '(GMT+5:45) Asia/Katmandu (Nepal Time)',
			'Antarctica/Mawson' => '(GMT+6:00) Antarctica/Mawson (Mawson Time)',
			'Antarctica/Vostok' => '(GMT+6:00) Antarctica/Vostok (Vostok Time)',
			'Asia/Almaty' => '(GMT+6:00) Asia/Almaty (Alma-Ata Time)',
			'Asia/Bishkek' => '(GMT+6:00) Asia/Bishkek (Kirgizstan Time)',
			'Asia/Dacca' => '(GMT+6:00) Asia/Dacca (Bangladesh Time)',
			'Asia/Dhaka' => '(GMT+6:00) Asia/Dhaka (Bangladesh Time)',
			'Asia/Novosibirsk' => '(GMT+6:00) Asia/Novosibirsk (Novosibirsk Time)',
			'Asia/Omsk' => '(GMT+6:00) Asia/Omsk (Omsk Time)',
			'Asia/Qyzylorda' => '(GMT+6:00) Asia/Qyzylorda (Qyzylorda Time)',
			'Asia/Thimbu' => '(GMT+6:00) Asia/Thimbu (Bhutan Time)',
			'Asia/Thimphu' => '(GMT+6:00) Asia/Thimphu (Bhutan Time)',
			'Indian/Chagos' => '(GMT+6:00) Indian/Chagos (Indian Ocean Territory Time)',
			'Asia/Rangoon' => '(GMT+6:30) Asia/Rangoon (Myanmar Time)',
			'Indian/Cocos' => '(GMT+6:30) Indian/Cocos (Cocos Islands Time)',
			'Antarctica/Davis' => '(GMT+7:00) Antarctica/Davis (Davis Time)',
			'Asia/Bangkok' => '(GMT+7:00) Asia/Bangkok (Indochina Time)',
			'Asia/Ho_Chi_Minh' => '(GMT+7:00) Asia/Ho_Chi_Minh (Indochina Time)',
			'Asia/Hovd' => '(GMT+7:00) Asia/Hovd (Hovd Time)',
			'Asia/Jakarta' => '(GMT+7:00) Asia/Jakarta (West Indonesia Time)',
			'Asia/Krasnoyarsk' => '(GMT+7:00) Asia/Krasnoyarsk (Krasnoyarsk Time)',
			'Asia/Phnom_Penh' => '(GMT+7:00) Asia/Phnom_Penh (Indochina Time)',
			'Asia/Pontianak' => '(GMT+7:00) Asia/Pontianak (West Indonesia Time)',
			'Asia/Saigon' => '(GMT+7:00) Asia/Saigon (Indochina Time)',
			'Asia/Vientiane' => '(GMT+7:00) Asia/Vientiane (Indochina Time)',
			'Indian/Christmas' => '(GMT+7:00) Indian/Christmas (Christmas Island Time)',
			'Antarctica/Casey' => '(GMT+8:00) Antarctica/Casey (Western Standard Time (Australia))',
			'Asia/Brunei' => '(GMT+8:00) Asia/Brunei (Brunei Time)',
			'Asia/Choibalsan' => '(GMT+8:00) Asia/Choibalsan (Choibalsan Time)',
			'Asia/Chongqing' => '(GMT+8:00) Asia/Chongqing (China Standard Time)',
			'Asia/Chungking' => '(GMT+8:00) Asia/Chungking (China Standard Time)',
			'Asia/Harbin' => '(GMT+8:00) Asia/Harbin (China Standard Time)',
			'Asia/Hong_Kong' => '(GMT+8:00) Asia/Hong_Kong (Hong Kong Time)',
			'Asia/Irkutsk' => '(GMT+8:00) Asia/Irkutsk (Irkutsk Time)',
			
			'Asia/Kashgar' => '(GMT+8:00) Asia/Kashgar (China Standard Time)',
			'Asia/Kuala_Lumpur' => '(GMT+8:00) Asia/Kuala_Lumpur (Malaysia Time)',
			'Asia/Kuching' => '(GMT+8:00) Asia/Kuching (Malaysia Time)',
			'Asia/Macao' => '(GMT+8:00) Asia/Macao (China Standard Time)',
			'Asia/Macau' => '(GMT+8:00) Asia/Macau (China Standard Time)',
			'Asia/Makassar' => '(GMT+8:00) Asia/Makassar (Central Indonesia Time)',
			'Asia/Manila' => '(GMT+8:00) Asia/Manila (Philippines Time)',
			'Asia/Shanghai' => '(GMT+8:00) Asia/Shanghai (China Standard Time)',
			'Asia/Singapore' => '(GMT+8:00) Asia/Singapore (Singapore Time)',
			'Asia/Taipei' => '(GMT+8:00) Asia/Taipei (China Standard Time)',
			'Asia/Ujung_Pandang' => '(GMT+8:00) Asia/Ujung_Pandang (Central Indonesia Time)',
			'Asia/Ulaanbaatar' => '(GMT+8:00) Asia/Ulaanbaatar (Ulaanbaatar Time)',
			'Asia/Ulan_Bator' => '(GMT+8:00) Asia/Ulan_Bator (Ulaanbaatar Time)',
			'Asia/Urumqi' => '(GMT+8:00) Asia/Urumqi (China Standard Time)',
			'Australia/Perth' => '(GMT+8:00) Australia/Perth (Western Standard Time (Australia))',
			'Australia/West' => '(GMT+8:00) Australia/West (Western Standard Time (Australia))',
			'Australia/Eucla' => '(GMT+8:45) Australia/Eucla (Central Western Standard Time (Australia))',
			'Asia/Dili' => '(GMT+9:00) Asia/Dili (Timor-Leste Time)',
			'Asia/Jayapura' => '(GMT+9:00) Asia/Jayapura (East Indonesia Time)',
			'Asia/Pyongyang' => '(GMT+9:00) Asia/Pyongyang (Korea Standard Time)',
			'Asia/Seoul' => '(GMT+9:00) Asia/Seoul (Korea Standard Time)',
			'Asia/Tokyo' => '(GMT+9:00) Asia/Tokyo (Japan Standard Time)',
			'Asia/Yakutsk' => '(GMT+9:00) Asia/Yakutsk (Yakutsk Time)',
			'Australia/Adelaide' => '(GMT+9:30) Australia/Adelaide (Central Standard Time (South Australia))',
			'Australia/Broken_Hill' => '(GMT+9:30) Australia/Broken_Hill (Central Standard Time (South Australia/New South Wales))',
			'Australia/Darwin' => '(GMT+9:30) Australia/Darwin (Central Standard Time (Northern Territory))',
			'Australia/North' => '(GMT+9:30) Australia/North (Central Standard Time (Northern Territory))',
			'Australia/South' => '(GMT+9:30) Australia/South (Central Standard Time (South Australia))',
			'Australia/Yancowinna' => '(GMT+9:30) Australia/Yancowinna (Central Standard Time (South Australia/New South Wales))',
			'Antarctica/DumontDUrville' => '(GMT+10:00) Antarctica/DumontDUrville (Dumont-d\'Urville Time)',
			'Asia/Sakhalin' => '(GMT+10:00) Asia/Sakhalin (Sakhalin Time)',
			'Asia/Vladivostok' => '(GMT+10:00) Asia/Vladivostok (Vladivostok Time)',
			'Australia/ACT' => '(GMT+10:00) Australia/ACT (Eastern Standard Time (New South Wales))',
			'Australia/Brisbane' => '(GMT+10:00) Australia/Brisbane (Eastern Standard Time (Queensland))',
			'Australia/Canberra' => '(GMT+10:00) Australia/Canberra (Eastern Standard Time (New South Wales))',
			'Australia/Currie' => '(GMT+10:00) Australia/Currie (Eastern Standard Time (New South Wales))',
			'Australia/Hobart' => '(GMT+10:00) Australia/Hobart (Eastern Standard Time (Tasmania))',
			'Australia/Lindeman' => '(GMT+10:00) Australia/Lindeman (Eastern Standard Time (Queensland))',
			'Australia/Melbourne' => '(GMT+10:00) Australia/Melbourne (Eastern Standard Time (Victoria))',
			'Australia/NSW' => '(GMT+10:00) Australia/NSW (Eastern Standard Time (New South Wales))',
			'Australia/Queensland' => '(GMT+10:00) Australia/Queensland (Eastern Standard Time (Queensland))',
			'Australia/Sydney' => '(GMT+10:00) Australia/Sydney (Eastern Standard Time (New South Wales))',
			'Australia/Tasmania' => '(GMT+10:00) Australia/Tasmania (Eastern Standard Time (Tasmania))',
			'Australia/Victoria' => '(GMT+10:00) Australia/Victoria (Eastern Standard Time (Victoria))',
			'Australia/LHI' => '(GMT+10:30) Australia/LHI (Lord Howe Standard Time)',
			'Australia/Lord_Howe' => '(GMT+10:30) Australia/Lord_Howe (Lord Howe Standard Time)',
			'Asia/Magadan' => '(GMT+11:00) Asia/Magadan (Magadan Time)',
			'Antarctica/McMurdo' => '(GMT+12:00) Antarctica/McMurdo (New Zealand Standard Time)',
			'Antarctica/South_Pole' => '(GMT+12:00) Antarctica/South_Pole (New Zealand Standard Time)',
			'Asia/Anadyr' => '(GMT+12:00) Asia/Anadyr (Anadyr Time)',
			'Asia/Kamchatka' => '(GMT+12:00) Asia/Kamchatka (Petropavlovsk-Kamchatski Time)',
			) ;
		return $timezones;
	}
	# determine timezone from value
	function getFullTimezone($zone) {
		$zonesarray = getAllTimezones();
		return isArrayKeyAnEmptyString($zone, $zonesarray) ? '' : $zonesarray[$zone];
	}
	# dropdown of doc types 
	function getAllDocumentTypes(){
		$query = "SELECT l.lookuptypevalue as optionvalue, l.lookupvaluedescription as optiontext FROM lookuptypevalue AS l WHERE l.lookuptypeid = 7 order by optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	# document types 
	function getDocumentTypes($type = '', $current = ''){
		$type_query = "";
		if(!isEmptyString($type)){
			$type_query .= " AND d.type = '".$type."' ";
		}
		if(!isEmptyString($current)){
			$type_query .= " AND d.id = '".$current."' ";
		}
		$conn = Doctrine_Manager::connection();
		$all_lists = $conn->fetchAll("SELECT d.* FROM lookuptypevalue AS d WHERE d.lookuptypeid = 7 ".$type_query." order by d.id ASC ");
		return $all_lists;
	}
	# document types 
	function getDocTypes($type = '', $current = ''){
		$type_query = "";
		if(!isEmptyString($type)){
			$type_query .= " AND d.type = '".$type."' ";
		}
		if(!isEmptyString($current)){
			$type_query .= " AND d.id = '".$current."' ";
		}
		$query = "SELECT d.lookuptypevalue as optionvalue, d.lookupvaluedescription as optiontext from lookuptypevalue AS d WHERE d.lookuptypeid = 7 ".$type_query." order by d.id ASC ";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getReportingPeriods(){
		$periods = array('1'=>'Last Month','2'=>'Last Week','3'=>'Yesterday', '4'=> 'Today', '5'=> 'Tomorrow', '6'=>'This Week', '7'=>'Next Week', '8'=> 'This Month', '9'=>'Next Month');
		return $periods;
	}
	function getPastReportingPeriods(){
		$periods = array('4'=> 'Today', '3'=>'Yesterday', '6'=>'This Week', '2'=>'Last Week', '8' => 'This Month', '1'=>'Last Month', '10' => 'Last 3 Months', '11' => 'Last 6 Months', '12' => 'Year Todate', '0' => 'Custom Period');
		return $periods;
	}
	# return districts/counties in country
	function getDistricts($country = 'UG', $region = '') {
		$custom_query = '';
		if(!isEmptyString($region)){
			$custom_query = " AND l.regionid = '".$region."' ";
		}
		if(isEmptyString($country)){
			$country = 'UG';
		}
		$query = "SELECT l.name AS optiontext, l.id AS optionvalue FROM location AS l WHERE l.locationtype = '2' AND l.country = UPPER('".$country."') ".$custom_query." ORDER BY optiontext";
		// debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getZones($country = 'UG') {
		$custom_query = '';
		if(isEmptyString($country)){
			$country = 'UG';
		}
		$query = "SELECT l.name AS optiontext, l.id AS optionvalue FROM location AS l WHERE l.locationtype = '7' ".$custom_query." ORDER BY optiontext";
		// debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getRegions($country = 'UG') {
		$custom_query = '';
		if(isEmptyString($country)){
			$country = 'UG';
		}
		$query = "SELECT l.name AS optiontext, l.id AS optionvalue FROM location AS l WHERE l.locationtype = '1' ".$custom_query." ORDER BY optiontext";
		// debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getDistrictsByCode($country = 'UG', $region = '') {
		$custom_query = '';
		if(!isEmptyString($region)){
			$custom_query = " AND l.regionid = '".$region."' ";
		}
		if(isEmptyString($country)){
			$country = 'UG';
		}
		$query = "SELECT l.code AS optiontext, l.id AS optionvalue FROM location AS l WHERE l.locationtype = '2' AND l.country = UPPER('".$country."') ".$custom_query." ORDER BY optiontext";
		// debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	/**
	 * Get the districts in the specified region
	 *
	 * @param Integer $regionid The id of the region
	 *
	 * @return Array
	 */
	function getDistrictsInRegion($regionid) {
		if (isEmptyString($regionid)) {
			return array();
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE regionid = '".$regionid."' AND locationtype = 2 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getRegionForDistrict($districtid) {
		$conn = Doctrine_Manager::connection();
		$query = "select l.regionid from location l where l.id = '".$districtid."' ";
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		return $result;
	}
	/**
	* Get the Counties in the specified region
	*
	* @param Integer $districtid The id of the district
	*
	* @return Array
	*/
	function getCountiesInDistrict($districtid) {
		if (isEmptyString($districtid)) {
			return array();
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE districtid = '".$districtid."' AND locationtype = 3 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getCounties($districtid = '') {
		$where_query = " WHERE locationtype = 3 ";
		if (!isEmptyString($districtid)) {
			$where_query .= " AND districtid = '".$districtid."' ";
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location ".$where_query." ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getSubCounties($countyid = '') {
		$where_query = " WHERE locationtype = 4 ";
		if (!isEmptyString($countyid)) {
			$where_query .= " AND countyid = '".$countyid."' ";
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location ".$where_query." ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getParishes($subcountyid = '') {
		$where_query = " WHERE locationtype = 5 ";
		if (!isEmptyString($subcountyid)) {
			$where_query .= " AND subcountyid = '".$subcountyid."' ";
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location ".$where_query." ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	/**
	 * Get the Sub-Counties in the specified County
	 *
	 * @param Integer $countyid The id of the county
	*
	* @return Array
	*/
	function getSubcountiesInCounty($countyid) {
		if (isEmptyString($countyid)) {
			return array();
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE countyid = '".$countyid."' AND locationtype = 4 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
			/**
		 * Get the Parishes in the specified Sub-County
		 *
		 * @param Integer $subcountyid The id of the sub-county
	*
	* @return Array
	*/
	function getParishesInSubCounty($subcountyid) {
		if (isEmptyString($subcountyid)) {
			return array();
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE subcountyid = '".$subcountyid."' AND locationtype = 5 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	/**
	* Get the Villages in the specified Parish
	*
	* @param Integer $parishid The id of the parish
	*
	* @return Array
	*/
	function getVillagesInParishes($parishid) {
		if (isEmptyString($parishid)) {
			return array();
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE parishid = '".$parishid."' AND locationtype = 6 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	/**
	* Get the parishes in the specified district
	*
	* @param Integer $districtid - the id of the district
	*
	* @return Array
	*/
	function getParishesInDistrict($districtid) {
		if (isEmptyString($districtid)) {
			return array();
		}
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE districtid = '".$districtid."' AND locationtype = 5 ORDER BY optiontext";
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getCommaListDataAsText($list = '', $values = array(), $separator = ", "){
		$text = '--';
		if(!isEmptyString($list)){
			$allvalues = $values;
			$values_array = explode(',', $list);
	
			$list_array = array();
			foreach ($values_array as $key => $imp){
				$list_array[$imp] = $allvalues[$imp];
			}
			$text = implode($separator, $list_array);
		}
		return $text;
	}
	function getListDataAsArray($values = array()){
		$current_array = array();
		if(!isEmptyString($values)){
			$current_array = explode(',', $values);
		}
		return $current_array;
	}
	/**
	 * Get the subcounties in the specified district
	 *
	 * @param Integer $districtid The id of the district
	 *
	 * @return Array
	 */
	function getSubCountiesInDistrict($districtid) {
		if (isEmptyString($districtid)) {
			return array();
		}
		$mun_query = "";
		$district = new Location();
		$district->populate($districtid);
		if($district->isMunicipality()){
			$mun_query = " OR districtid = '".$district->getDistrictID()."' ";
		}
		
		$query = "SELECT id as optionvalue, name as optiontext FROM location WHERE (districtid = '".$districtid."' ".$mun_query.") AND locationtype = 4 ORDER BY optiontext"; // debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getReportStatuses(){
		return array('1' => 'Approved', '2' => 'Submitted', '3' => 'Saved', '4' => 'Rejected');
	}
	function getResultStatuses(){
		return array('1' => 'Draft', '2' => 'Submitted for Approval', '3' => 'Approved', '4' => 'Rejected');
	}
	function getSubjectTypes(){
		$array = array(
			'2' => 'Ask Question', 
			'3' => 'Feedback', 
			'4' => 'Login Problem/Access Denied', 
			'5' => 'System Alert',
			'1' => 'Report a Bug'
		);
		return $array;
	}
	function getModelLevels($isproject = false){
		if($isproject){
			$array = array(
				'0' => 'Goal', 
				'1' => 'Objective', 
				'2' => 'Outcome', 
				'3' => 'Output',
				'4' => 'Activity'
			);
		} else {
			$array = array(
				/*'0' => 'Goal', */
				'1' => 'Objective', 
				'2' => 'Outcome', 
				'3' => 'Output'
			);
		}
		
		return $array;
	}
	function getResultAreas(){
		return getModelLevels(true);
	}
	function getStrategyLevels($value = ''){
		$array = array(
			'1' => 'Programme Framework', 
			'2' => 'Organization Framework', 
			'3' => 'Global Framework',
			'5' => 'NPD2 Framework',
			'6' => 'SDG Framework'
		);
		if(!isEmptyString($value)){
			if(!isArrayKeyAnEmptyString($value, $array)){
				return $array[$value];
			} else {
				return '';
			}
		}
		return $array;
	}
	function getEvaluationMethods(){
		$array = array(
			'1' => 'Cummulative Sum', 
			'2' => 'Average Score', 
			'3' => 'Maximum Score', 
			'4' => 'Minimum Score',
		);
		return $array;
	}
	function getMilestoneTypes(){
		$array = array(
			'1' => 'Quarterly', 
			'2' => 'Monthly'
		);
		return $array;
	}
	function getMilestoneStatuses(){
		$array = array(
			'0' => 'Not Started',
			'1' => 'Active', 			
			'2' => 'Completed'
		);
		return $array;
	}
	
	function getTimeUnits(){
		$array = array(
			/*'1' => 'seconds',
			'2' => 'hours',
			'3' => 'minutes',*/
			'4' => 'days',
			'5' => 'weeks',
			'6' => 'months',
			'7' => 'years'
		);
		return $array;
	}
	function getPeriodFrequencies(){
		$array = array(
			'1' => 'Daily',
			'2' => 'Weekly',
			'3' => 'Fortnightly',
			'4' => 'Monthly',
			'5' => 'Every 2 Months',
			'6' => 'Every 3 Months',
			'7' => 'Every 4 Months',
			'8' => 'Every 6 Months',
			'9' => 'Annually',
			'10' => 'At start of project/program',
			'11' => 'At end of project/program',
			'12' => 'Mid Term',
		);
		return $array;
	}
	function getObjectives($type = 1, $projectid = '', $addref = ''){
		$custom_query = "";
		if($type == 4){
			$custom_query = " AND p.projectid = '".$projectid."' ";
		}
		$query = "SELECT p.id as optionvalue, concat(p.code, '. ', p.name) as optiontext from program p WHERE p.type = '".$type."' ".$custom_query." order by CAST(p.code AS SIGNED) "; //debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getPrograms($type = 1, $projectid = '', $ids=''){
		$custom_query = "";
		if(!isEmptyString($ids)){
			$custom_query .= " AND p.id IN(".$ids.") ";
		}
		$query = "SELECT p.id as optionvalue, p.name as optiontext from program p WHERE p.type = '".$type."' ".$custom_query." order by CAST(p.code AS SIGNED) "; // debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getGoals($type = 1, $projectid = ""){
		$custom_query = "";
		if(!isEmptyString($projectid)){
			$custom_query .= " AND o.projectid = '".$projectid."'";
		}
		
		$query = "SELECT o.id as optionvalue, concat(o.refno, '. ', o.title) as optiontext from content_logframe o WHERE o.type = '".$type."' ".$custom_query." AND o.level = 0 AND o.cspid = 1 order by CAST(o.refno AS SIGNED) "; // debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getOutcomes($type = 1, $programid = "", $projectid = "", $addref = ''){
		$custom_query = "";
		if(!isEmptyString($type)){
			$custom_query .= " AND o.type = '".$type."' ";
		}
		if(!isEmptyString($programid)){
			$custom_query .= " AND o.programid = '".$programid."'";
		}
		if(!isEmptyString($projectid)){
			$custom_query .= " AND o.projectid = '".$projectid."'";
		}
		$display_label = "concat(o.refno, '. ', o.title)";
		if($addref == 1){
			$display_label = "concat(IFNULL(p.refno, ''), ': #', IFNULL(o.refno, ''), '. ', IFNULL(o.title, ''))";
		}
		
		$query = "SELECT o.id as optionvalue, ".$display_label." as optiontext from content_logframe o 
		left join project p on o.projectid = p.id
		WHERE o.id <> '' ".$custom_query." AND o.level = '2' AND o.cspid = '1' group by o.id order by CAST(o.refno AS SIGNED) "; // debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getOutputs($type = "1", $outcomeid = "", $projectid = "", $addref = ''){
		$custom_query = "";
		if(!isEmptyString($type)){
			$custom_query .= " AND o.type = '".$type."'";
		}
		if(!isEmptyString($outcomeid)){
			$custom_query .= " AND o.parentid = '".$outcomeid."'";
		}
		if(!isEmptyString($projectid)){
			$custom_query .= " AND o.projectid = '".$projectid."'";
		}
		$display_label = "concat(o.refno, '. ', o.title)";
		if($addref == 1){
			$display_label = "concat(IFNULL(p.refno, ''), ': #', IFNULL(o.refno, ''), '. ', IFNULL(o.title, ''))";
		}
		
		$query = "SELECT o.id as optionvalue, concat(o.refno, '. ', o.title) as optiontext 
		from content_logframe o 
		left join project p on o.projectid = p.id
		WHERE o.level = '3' ".$custom_query." AND o.cspid = 1 group by o.id order by CAST(o.refno AS SIGNED) "; // debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getProgramUnits($ids = ''){
		$custom_query = "";
		if(!isEmptyString($ids)){
			$custom_query .= " AND p.id IN(".$ids.") ";
		}
		$query = "SELECT p.id as optionvalue, p.name as optiontext from program_unit p WHERE p.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getProgramUnitList() {
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$query =  "select p.id as id, p.name as name, p.gpslat as gpslat, p.gpslng as gpslng from program_unit p group by p.id order by p.name";
		//debugMessage($query);
		$all_lists = $conn->fetchAll($query);
		return $all_lists;
	}	
	function getProjectActivities($id = ''){
		$custom_query = "";
		if(!isEmptyString($id)){
			$custom_query .= " AND a.projectid = '".$id."' ";
		}
		$query = "SELECT a.id as optionvalue, concat(a.refno, ' - ', a.title) as optiontext from activity a WHERE a.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getDatasets($projectid ="", $period = false, $addref =''){
		$custom_query = "";
		if(!isEmptyString($projectid)){
			$custom_query .= " AND d.projectid = '".$projectid."' ";
		}
		if($period){
			$custom_query .= " AND NOW() BETWEEN d.startdate AND d.enddate ";
		}
		$column = 'd.name';
		if($addref){
			$column = " concat(d.refno, ': ', d.name)";
		}
		$query = "SELECT d.id as optionvalue, ".$column." as optiontext from data_set d WHERE d.id <> '' ".$custom_query." order by optiontext "; //debugMessage($query); // exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	# number of indicators for 
	function countIndicatorsForObjective($id){
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT count(i.id) FROM indicator i WHERE i.level = 1 AND i.programid = '".$id."' ".$custom_query." ";
		$result = $conn->fetchOne($valuesquery);
		return $result;
	}
	function countIndicatorsForContent($id, $level){
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT count(i.id) FROM indicator i WHERE i.level = '".$level."' AND i.contentid = '".$id."' ".$custom_query." ";
		$result = $conn->fetchOne($valuesquery);
		return $result;
	}
	function countUnReadMessages($userid){
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT count(q.id) FROM messagerecipient q WHERE q.isread = '0' AND q.recipientid = '".$userid."' ".$custom_query." "; // debugMessage($valuesquery);
		$result = $conn->fetchOne($valuesquery);
		return $result;
	}
	# number of indicators for 
	function sumWeightForObjectiveIndicators($id){
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT sum(i.weight) FROM indicator i WHERE i.level = 1 AND i.programid = '".$id."' ".$custom_query." ";
		$result = $conn->fetchOne($valuesquery);
		return $result;
	}
	function sumWeightIndicatorsForContent($id, $level){
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT sum(i.weight) FROM indicator i WHERE i.level = '".$level."' AND i.contentid = '".$id."' ".$custom_query." ";
		$result = $conn->fetchOne($valuesquery);
		return isEmptyString($result) ? 0 : $result;
	}
	function getDefaultCurrency(){
		$custom_query = "";
		$conn = Doctrine_Manager::connection();
		$valuesquery = "SELECT l.lookuptypevaluedescription FROM lookuptypevalue l WHERE i.lookuptypeid = '24' AND i.isdefault = '1' ".$custom_query." ";
		$result = $conn->fetchOne($valuesquery);
		return isEmptyString($result) ? 0 : $result;
	}
	function getFYears(){
		$query = "SELECT p.id as optionvalue, p.name as optiontext from content_fyear p WHERE p.cspid = 1 AND p.level = 1 order by p.startdate "; //debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getMilestonePeriods($level = '1', $parent = ''){
		$custom_query = "";
		if(!isEmptyString($level)){
			$custom_query = " AND m.category = '".$level."' ";
		}
		if(!isEmptyString($parent)){
			$custom_query = " AND m.parentid = '".$parent."' ";
		}
		$query = "SELECT m.id as optionvalue, m.name as optiontext from content_milestone m WHERE m.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getActiveMilestone($level = '') {
		$custom_query = "";
		if(!isEmptyString($level)){
			$custom_query = " AND m.category = '".$level."' ";
		}
		$query = "SELECT m.id FROM content_milestone m WHERE m.status = 1 ".$custom_query." "; // debugMessage($query);
		$conn = Doctrine_Manager::connection();
		$result = $conn->fetchOne($query);
		return $result;
	}
	function getMilestoneFromPeriod($date, $type = 1){
		$query = "SELECT m.id FROM content_milestone m WHERE m.category = '".$type."' AND TO_DAYS('".$date."') BETWEEN TO_DAYS(m.startdate) AND TO_DAYS(m.enddate) ".$custom_query." "; // debugMessage($query);
		$conn = Doctrine_Manager::connection();
		$result = $conn->fetchOne($query); // debugMessage('id '.$result);
		return $result;
	}
	function getFPYears($cspid = 1) {
		$query = "SELECT f.id as optionvalue, f.name as optiontext FROM content_fyear f WHERE f.level = 1 ORDER BY CAST(f.refno AS SIGNED)  "; // debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	function getFQuarters(){
		$query = "SELECT p.id as optionvalue, p.name as optiontext from content_fyear p WHERE p.level = 2 order by p.startdate "; //debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getAllPeriods(){
		$query = "SELECT p.id as optionvalue, p.name as optiontext from content_fyear p WHERE p.cspid = 1 order by p.startdate "; //debugMessage($query);
		return getOptionValuesFromDatabaseQuery($query);
	}
	/**
	 * Encrypt and decrypt
	 * 
	 * @author Nazmul Ahsan <n.mukto@gmail.com>
	 * @link http://nazmulahsan.me/simple-two-way-function-encrypt-decrypt-string/
	 *
	 * @param string $string string to be encrypted/decrypted
	 * @param string $action what to do with this? e for encrypt, d for decrypt
	 */
	function my_simple_crypt( $string, $action = 'e' ) {
		// you may change these values to your own
		$secret_key = 'my_simple_secret_key';
		$secret_iv = 'my_simple_secret_iv';
	
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
	
		if( $action == 'e' ) {
			$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
		}
		else if( $action == 'd' ){
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}
	
		return $output;
	}
	function getDataAggregationTypes(){
		$array = array(
			'1' => 'Sum',
			'2' => 'Average',
			'3' => 'Count',
			'4' => 'Variance',
			'5' => 'Min',
			'6' => 'Max',
			'7' => 'Text',
			'8' => 'N/A'
		);
		return $array;
	}
	
	function getIndicatorTypes(){
		$query = "SELECT q.id as optionvalue, q.name as optiontext from indicator_type q WHERE q.id <> '' order by q.id "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	function getDataCategoryOptions($direction = true, $idslist = ''){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		$query = "SELECT q.id as optionvalue, q.name as optiontext from data_category_option q WHERE q.id <> '' ".$custom_query." order by optiontext "; //debugMessage($query); // exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getOptionsForID($id){
		if(isEmptyString($id)){
			return '';
		}
		$conn = Doctrine_Manager::connection();
		$query = "SELECT q.optionids from data_category q WHERE q.id = '".$id."' "; // debugMessage($query); exit;
		return $conn->fetchOne($query);
	}
	function getCategoriesForGroup($id){
		if(isEmptyString($id)){
			return '';
		}
		$conn = Doctrine_Manager::connection();
		$query = "SELECT q.categoryids from data_category_group q WHERE q.id = '".$id."' "; // debugMessage($query); exit;
		return $conn->fetchOne($query);
	}
	function getDataCategories($direction = true, $idslist = ''){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		$query = "SELECT q.id as optionvalue, q.name as optiontext from data_category q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getDataCategoryGroups($direction = true, $idslist = ''){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		$query = "SELECT q.id as optionvalue, q.name as optiontext from data_category_group q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getDataAttributes($direction = true, $idslist = '', $inlist = '', $checknone = false, $addref = '1', $datasetid = ""){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
			if(!isEmptyString($inlist)){
				$custom_query .= " AND q.id IN(".$inlist.") ";
			}
			if($checknone && isEmptyString($inlist)){
				$custom_query .= " AND q.id = '!!!' ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		$column = 'q.name';
		if($addref != ''){
			
		}
		$column = " concat(q.refno, ': ', q.name)";
		if(!isEmptyString($datasetid)){
			$dataset = new Dataset();
			$dataset->populate($datasetid); // debugMessage($dataset->toArray());
			if(!isEmptyString($dataset->getelementids())){
				$custom_query .= " AND q.id IN(".$dataset->getelementids().") ";
			} else {
				$custom_query .= " AND q.id ='!!!' ";
			}
		}
		
		$query = "SELECT q.id as optionvalue, ".$column." as optiontext from data_attribute q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); // exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	function getAllAttributes($addref = ''){
		$custom_query = "";
		$column = 'q.name';
		if($addref != ''){
			
		}
		$column = " concat(q.refno, ': ', q.name)";
		$query = "SELECT q.id as optionvalue, ".$column." as optiontext from data_attribute q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); // exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	function getAllAttributesWithTheirCategories($addref = ''){
		$custom_query = "";
		$column = 'q.name';
		if($addref != ''){
			
		}
		$column = " concat(q.refno, ': ', q.name)";
		$query = "SELECT q.id as optionvalue, ".$column." as optiontext from data_attribute q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); // exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getDataAttributesAndSubAttributes($datasetid = ''){
		$conn = Doctrine_Manager::connection();
		$custom_query = "";
		if(!isEmptyString($datasetid)){
			$dataset = new Dataset();
			$dataset->populate($datasetid);
			if(!isEmptyString($dataset->getelementids())){
				$custom_query .= " AND q.id IN(".$dataset->getelementids().") ";
			}
		}		
		$query = "SELECT q.* from data_attribute q WHERE q.id <> '' ".$custom_query." order by q.name "; // debugMessage($query); // exit;
		$result = $conn->fetchAll($query);
		
		$attributes = array();
		foreach ($result as $data){
			if(($data['categorytype'] == 1 && !isEmptyString($data['category'])) || ($data['categorytype'] == 2 && !isEmptyString($data['categorygroup']))){
				$attb = new Attribute();
				$attb->populate($data['id']); // debugMessage($attb->toArray());
				
				$categorydata = array();
				if($data['categorytype'] == 1 && !isEmptyString($data['category'])){
					$categorydata = commaStringToArray($attb->getCategory());
					$options = getDataCategoryOptions(false, getOptionsForID($attb->getcategory()));
					if(count($options) > 1){
						$key = $attb->getID().'#Total';
						$value = $attb->getrefno().': '.$attb->getName();
						$attributes[$key]['name'] = $value;
						$attributes[$key]['dataref'] = my_simple_crypt($key);
						$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$value.' [Total]" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute '.my_simple_crypt($key).'" datalabel="'.$value.' [Total]"><span>'.snippet($value, 75, '...').' [Total]</span></option>';
						
					}
					foreach($options as $k => $value){
						$key = $attb->getID().'#'.$k;
						$value = $attb->getrefno().': ['.$value.']';
						$title = $attb->getrefno().': '.$attb->getName().' ['.$value.']';
						$attributes[$key]['name'] = $value;
						$attributes[$key]['dataref'] = my_simple_crypt($key);
						$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$title.'" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute nested '.my_simple_crypt($key).'" datalabel="'.$title.'"><span>'.snippet($value, 75, '...').'</span></option>';
					}
				}
				if($data['categorytype'] == 2 && !isEmptyString($data['categorygroup'])){
					$listname = array();
					$categorydata = commaStringToArray(getCategoriesForGroup($attb->getCategoryGroup()));
					
					$row1 = isArrayKeyAnEmptyString(0, $categorydata) ? '' : $categorydata[0];
					$cat1options_array = commaStringToArray(getOptionsForID($row1));
					$options1 = getDataCategoryOptions(false, getOptionsForID($row1));
						
					$row2 = isArrayKeyAnEmptyString(1, $categorydata) ? '' : $categorydata[1]; 
					$cat2options_array = commaStringToArray(getOptionsForID($row2));
					$options2 = getDataCategoryOptions(false, getOptionsForID($row2));
					
					$row3 = isArrayKeyAnEmptyString(2, $categorydata) ? '' : $categorydata[2];
					$cat3options_array = commaStringToArray(getOptionsForID($row3));
					$options3 = getDataCategoryOptions(false, getOptionsForID($row3));
					
					if(!isEmptyString($row3) && count($categorydata) == 3){
						if(count($options1) > 1){
							$key = $attb->getID().'#Total';
							$value = $attb->getrefno().': '.$attb->getName();
							$attributes[$key]['name'] = $value;
							$attributes[$key]['dataref'] = my_simple_crypt($key);
							$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$value.' [Total]" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute '.my_simple_crypt($key).'" datalabel="'.$value.' [Total]"><span>'.snippet($value, 75, '...').' [Total]</span></option>';
						}
						foreach($options1 as $key1 => $value1){
							foreach($options2 as $key2 => $value2){
								foreach($options3 as $key3 => $value3){
									$key = $attb->getID().'#'.$key1.','.$key2.','.$key3;
									$value = $attb->getrefno().': ['.$value1.' -> '.$value2.' -> '.$value3.']';
									$title = $attb->getrefno().': '.$attb->getName().' ['.$value1.' -> '.$value2.' -> '.$value3.']';
									$attributes[$key]['name'] = $value;
									$attributes[$key]['dataref'] = my_simple_crypt($key);
									$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$title.'" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute nested '.my_simple_crypt($key).'" datalabel="'.$title.'"><span>'.snippet($value, 75, '...').'</span></option>';
								}
							}
						}
					}
					
					if(!isEmptyString($row2) && count($categorydata) == 2){
						foreach($options1 as $key1 => $value1){
							if(count($options2) > 1){
								$key = $attb->getID().'#Total';
								$value = $attb->getrefno().': '.$attb->getName();
								$attributes[$key]['name'] = $value;
								$attributes[$key]['dataref'] = my_simple_crypt($key);
								$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$value.' [Total]" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute '.my_simple_crypt($key).'" datalabel="'.$value.' [Total]"><span>'.snippet($value, 75, '...').' [Total]</span></option>';
							}
							foreach($options2 as $key2 => $value2){
								$key = $attb->getID().'#'.$key1.','.$key2;
								$value = $attb->getrefno().': ['.$value1.' -> '.$value2.']';
								$title = $attb->getrefno().': '.$attb->getName().' ['.$value1.' -> '.$value2.']';
								
								$attributes[$key]['name'] = $value;
								$attributes[$key]['dataref'] = my_simple_crypt($key);
								$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$title.'" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute nested '.my_simple_crypt($key).'" datalabel="'.$title.'"><span>'.snippet($value, 75, '...').'</span></option>';
							}
						}
					}
					
					if(!isEmptyString($row1) && count($categorydata) == 1){
						if(count($options1) > 1){
							$key = $attb->getID().'#'.$key1;
							$value = $attb->getrefno().': '.$attb->getName();
							$attributes[$key]['name'] = $value;
							$attributes[$key]['dataref'] = my_simple_crypt($key);
							$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$value.' [Total]" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute '.my_simple_crypt($key).'" datalabel="'.$value.' [Total]"><span>'.snippet($value, 75, '...').' [Total]</span></option>';
						}
						foreach($options1 as $key1 => $value1){
							$key = $attb->getID().'#'.$key1;
							$value = $attb->getrefno().': ['.$value1.']';
							$title = $attb->getrefno().': '.$attb->getName().' ['.$value1.']';
							
							$attributes[$key]['name'] = $value;
							$attributes[$key]['dataref'] = my_simple_crypt($key);
							$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$title.'" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute nested '.my_simple_crypt($key).'" datalabel="'.$title.'"><span>'.snippet($value, 75, '...').'</span></option>';
						}
					}
				}
			} else {
				$key = $data['id'];
				$value = $data['refno'].': '.$data['name'];
				$attributes[$key]['name'] = $value;
				$attributes[$key]['dataref'] = my_simple_crypt($key);
				$attributes[$key]['optioncode'] = '<option value="'.$key.'" title="'.$value.'" dataid="'.$key.'" dataref="'.my_simple_crypt($key).'" class="inlinetip attribute '.my_simple_crypt($key).'" datalabel="'.$value.'"><span>'.snippet($value, 75, '...').'</span></option>';
			}
		}
		
		return $attributes;
	}
	
	function getAttributesInDataset($datasetid){
		$conn = Doctrine_Manager::connection();
		$custom_query = " AND q.id = '".$datasetid."' ";
		$query = "SELECT q.elementids from data_set q WHERE q.id <> '' ".$custom_query." "; // debugMessage($query); // exit;
		$result = $conn->fetchOne($query);
		return $result;
	}
	function getAssignedDataAttributesInSection($datasetid, $id = ''){
		$conn = Doctrine_Manager::connection();
		$custom_query = " AND q.datasetid = '".$datasetid."' ";
		if(!isEmptyString($id)){
			$custom_query .= " AND q.id <> '".$id."' ";
		}
		$query = "SELECT group_concat(q.elementids separator ',') as ids from data_set_section q WHERE q.id <> '' ".$custom_query." "; // debugMessage($query); // exit;
		$result = $conn->fetchOne($query);
		return $result;
	}
	function getDataIndicators($direction = true, $idslist = '', $inlist = '', $checknone = false, $addref = false, $query = ""){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($inlist)){
				$custom_query .= " AND q.id IN(".$inlist.") ";
			}
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
			if($checknone && isEmptyString($inlist)){
				$custom_query .= " AND q.id = '!!!' ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		if(!isEmptyString($query)){
			$custom_query .= $query;
		}
		$column = 'q.title';
		if($addref){
			
		}
		$column = " concat(q.refno, ': ', q.title)";
		$query = "SELECT q.id as optionvalue, ".$column." as optiontext from indicator q WHERE q.id <> '' ".$custom_query." order by optiontext "; //debugMessage($query); //exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	function getIndicators(){
		return getDataIndicators(true, '', '', false, true);
	}
	function getIndicatorsWithProjectNo($direction = true, $idslist = '', $inlist = '', $checknone = false){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($inlist)){
				$custom_query .= " AND q.id IN(".$inlist.") ";
			}
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
			if($checknone && isEmptyString($inlist)){
				$custom_query .= " AND q.id = '!!!' ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		
		$column = " concat(p.refno, ': ', q.refno, ' -', q.title)";
		$query = "SELECT q.id as optionvalue, ".$column." as optiontext from indicator q left join project p on q.projectid = p.id WHERE q.id <> '' AND q.type = 4 ".$custom_query." order by optiontext "; //debugMessage($query); //exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getCSIndicators($direction = true, $idslist = '', $inlist = '', $checknone = false, $query = ""){
		$custom_query = "";
		if($direction){
			if(!isEmptyString($inlist)){
				$custom_query .= " AND q.id IN(".$inlist.") ";
			}
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id NOT IN(".$idslist.") ";
			}
			if($checknone && isEmptyString($inlist)){
				$custom_query .= " AND q.id = '!!!' ";
			}
		} else {
			if(!isEmptyString($idslist)){
				$custom_query .= " AND q.id IN(".$idslist.") ";
			} else {
				$custom_query .= " AND q.id = '!!!' ";
			}
		}
		if(!isEmptyString($query)){
			$custom_query .= $query;
		}
		
		$column = " concat(q.refno, ' - ', q.title)";
		$query = "SELECT q.id as optionvalue, ".$column." as optiontext from indicator q WHERE q.type = 1 ".$custom_query." order by optiontext "; //debugMessage($query); //exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	
	function getAssignedIndicatorsInSection($datasetid, $id = ''){
		$conn = Doctrine_Manager::connection();
		$custom_query = " AND q.datasetid = '".$datasetid."' ";
		if(!isEmptyString($id)){
			$custom_query = " AND q.id <> '".$id."' ";
		}
		$query = "SELECT group_concat(q.indicatorids separator ',') as ids from data_set_section q WHERE q.id <> '' ".$custom_query." "; // debugMessage($query); exit;
		$result = $conn->fetchOne($query);
		return $result;
	}
	function getDataTypes(){
		return getDataTypesAndDescription();
	}
	function getDataTypesAndDescription(){
		$array = array(
			'1' => 'Number', // C_N
			'2' => 'Percentage', // 5 C_P
			'3' => 'Text', // 7 C_TX
			'4' => 'GPS Coordinate', // 6 C_C
			'5' => 'File', // 9 C_F
			'6' => 'Date', // 10 C_D
			'7' => 'Time', // 11 C_TM
			'8' => 'Date Time', // 14 C_DT
			'9' => 'Single Choice List', // 15 C_LS
			'10' => 'Multiple Choice List', // 16 C_LM
			'11' => 'Yes or No Option', // 17 C_YN
			'12' => 'Yes only Option' // 13 C_Y
			
/*			'1' => 'Integer [Any whole number positive or negative]',
'2' => 'Positive Integer [Any whole number greater than or equal to zero]',
'3' => 'Negative Integer [Any whole number less than or equal to zero',
'4' => 'Unit Interval [Any real number greater than or equal to 0 and less than or equal to 1]',
'5' => 'Percentage [Whole numbers between 0 and 75 inclusive]',
'6' => 'Coordinate [A point coordinate specified as longitude/latitude in decimal degrees. Format "-19.23, 56.42" and comma separated]',
'7' => 'Text [Render as input text in data entry forms]',
'8' => 'Long Text [Renders as text area in data entry forms]',
'9' => 'File [A file resource which can be used to store external files, such as documents and photos]',
'10' => 'Date [Render as date calendar widget for data entry forms]',
'11' => 'Time: [Render as time widget for data entry forms. Times is stored in the format HH:mm, where HH is a number between 0-23 and mm is a number between 00 and 59]',
'12' => 'Yes/No [Boolean values, will render as drop-down lists in data entry forms]',
'13' => 'Yes only [True values, will render as check-boxes in data entry]'	*/											
		);
		return $array;
	}
	function getDataAggTypes(){
		$array = array(
			'1' => 'Aggregate',
			'2' => 'Tracker',
		);
		return $array;
	}
	function getDataAggTypesWithDescription(){
		return getDataAggTypes();
	}
	function getIndicatorLevels(){
		$array = array(
			'1' => 'Objective Indicator',
			'2' => 'Outcome Indicator',
			'3' => 'Output Indicator',
		);
		return $array;
	}
	function getRiskTypes(){
		$array = array(
			'1' => 'Organization Risk',
			'2' => 'Programme Risk',
			'3' => 'Project Risk'
		);
		return $array;
	}
	function getReviewTypes(){
		$array = array(
			'1' => 'Annual',
			'2' => 'Monthly',
			'3' => 'Quarterly'			
		);
		return $array;
	}
	function getFormTypes(){
		$array = array(
			'1' => 'Default',
			'2' => 'Section',
		);
		return $array;
	}
	function getPeriodTypes(){
		$array = array(
			'1' => 'Daily',
			'2' => 'Weekly',
			'3' => 'Every 2 Weeks',
			'4' => 'Monthly',
			'5' => 'Every 2 Months',
			'6' => 'Every 3 Months (Quarter)',
			'7' => 'Every 4 Months',
			'8' => 'Every 6 Months',
			'9' => 'Yearly (Annual)',
			'13' => 'Once (Single result)',
			'14' => 'Infinite results'
		);
		return $array;
	}
	function getProjects($focusid = '', $puid = ''){
		$custom_query = "";
		if(!isEmptyString($focusid)){
			$custom_query .= " AND q.ownerid IN(".$focusid.") ";
		}
		if(!isEmptyString($puid)){
			$custom_query .= " AND '".$puid."', q.programid) > 0 ";
		}
		$query = "SELECT q.id as optionvalue, concat(q.refno, ' - ', q.title) as optiontext from project q WHERE q.id <> '' ".$custom_query." order by q.title "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getNumbersInRange($start="1", $end="3") {
		$values = array();
		for($i = $start; $i <= $end; $i++) {
			$values[$i] = $i;
		}
		return $values;
	}
	function getAudienceTypes(){
		$array = array(
			'1' => 'Users',
			'2' => 'Results Group',
			'3' => 'Public'
		);
		return $array;
	}
	function getFileFormats(){
		$array = array('doc', 'xls', 'ppt', 'pdf', 'txt', 'jpg', 'png', 'xls', 'zip');
		return $array;
	}
	function getResultGroups($type = ''){
		$custom_query = '';
		if(!isEmptyString($type)){
			$custom_query = " AND q.type = '".$type."'";
		}
		$query = "SELECT q.id as optionvalue, q.name as optiontext from user_category_group q WHERE q.id <> '' ".$custom_query." order by q.id "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getDetailsInCategory($groupid){
		$group = new UserCategory();
		$group->populate($groupid);
		return $group->toArray();
	}
	function getSystemAttributes($id = ''){
		$custom_query = '';
		if(!isEmptyString($id)){
			$custom_query = " AND q.id = '".$id."'";
		}
		$query = "SELECT q.id as optionvalue, q.name as optiontext from data_category_system q WHERE q.id <> '' ".$custom_query." order by optiontext "; // debugMessage($query); exit;
		return getOptionValuesFromDatabaseQuery($query);
	}
	function getOptionValuesForSystemAttribute($id, $params = array()){
		$systemcat = new SystemAttribute();
		$systemcat->populate($id);
		// debugMessage($systemcat->toArray()); exit;
		if(isEmptyString($systemcat->getQuery())){
			if(!isEmptyString($systemcat->getprocedure())){
				if(count($params) > 0){
					return call_user_func_array($systemcat->getprocedure(), $params);				
				} else {
					return call_user_func($systemcat->getprocedure());
				}
			} else { 
				return array();
			}
		} else {
			$query = $systemcat->getQuery(); 
			if(stringContains('".$custom_query."', $query)){
				$query = str_replace('".$custom_query."', '', $query);
			}
			return getOptionValuesFromDatabaseQuery($query);
		}
	}
	function getAccessSettings($userid='', $folderid= '', $folderpath = ''){ // debugMessage('inside '.$folderpath);
		$session = SessionWrapper::getInstance();
		if(isEmptyString($userid)){
			$userid = $session->getVar('userid');
		}
		$customquery = " AND a.userid = '".$userid."' AND a.folderid = '".$folderid."'";
		if(!isEmptyString($folderpath) && $folderpath != '/'){
			$path = explode('/', $folderpath); // debugMessage($path);
			$customquery = " AND a.userid = '".$userid."' AND a.folderid IN(".implode(',', array_remove_empty($path)).") ";
		}
		
		$conn = Doctrine_Manager::connection();
		$query = "select a.* from access a where (a.id <> '' ".$customquery.") order by a.folderid "; 
		// debugMessage($query); exit;
		$result = $conn->fetchAll($query); 
		if(count($result) == 0){
			$data = array();
			/* $a = new Access();
			$a->setUserID($userid);
			$a->setFolderID($folderid); */
			// return $a->toArray();
			return $data;
		} else {
			if(count($result) == 1){
				return $result[0];
			} else {
				$data = array();
				foreach ($result as $key=> $value){
					$data[$value['folderid']] = $value;
				}
				return $data;
			}
		}
		// return true;
	}
	
	function formatLabel($str){
		/*if($str == 'Female'){
			return str_replace('Female', 'M', $str);
		} elseif($str == 'Male'){			
			return str_replace('Male', 'M', $str);
		} else {
			
		}*/
		return $str;
	}
	function getCategoryOptionList($categorytype ='', $category = '', $categorygroup =''){
		$list = array();
		$alloptions = array(); $currentoptions = array();
		if($categorytype == 1 && !isEmptyString($category)){
			$alloptions = $currentoptions = getDataCategoryOptions(false, getOptionsForID($category));
		}
		if($categorytype == 2 && !isEmptyString($categorygroup)){
			// $alloptions = $currentoptions = getDataCategoryOptions(false, getOptionsForID($this->getcategory()));
		}
		
		return $currentoptions;
	}
	function getConfigOptionList($optiontype = '', $optionid ='', $systemattr='', $systemattrvalues=''){
		$list = array();
		$alloptions = array(); $currentoptions = array();
		if($optiontype == 1 && !isEmptyString($optionid)){
			$alloptions = $currentoptions = getDataCategoryOptions(false, getOptionsForID($optionid));
		}
		if($optiontype == 2){
			if($systemattr == '9'){
				$alloptions = $currentoptions = getDistricts();
			}
			if($systemattr == '1'){ // projects
				$alloptions = $currentoptions = getProjects();
			}
			if($systemattr == '2'){ // activities
				$alloptions = $currentoptions = getProjectActivities();
			}
			if($systemattr == '3'){ // objectives
				$alloptions = $currentoptions = getObjectives();
			}
			if($systemattr == '4'){ // indicators
				$alloptions = $currentoptions = getDataIndicators();
			}
			if($systemattr == '5'){ // project outcomes
				$alloptions = $currentoptions = getOutcomes(4);
			}
			if($systemattr == '6'){ // project outputs
				$alloptions = $currentoptions = getOutputs(4, '', '', 1);
			}
			if($systemattr == '8'){ // programme units
				$alloptions = $currentoptions = getProgramUnits();
			}
			if($systemattr == '9'){ // district locations
				$alloptions = $currentoptions = getDistricts();
			}
			if($systemattr == '10'){ // system users
				$alloptions = $currentoptions = getDataUsers();
			}
			if($systemattr == '11'){ // country outcomes
				$alloptions = $currentoptions = getOutcomes(1);
			}
			if($systemattr == '12'){ // Locations: Constituency
				$alloptions = $currentoptions = getCounties();
			}
			if($systemattr == '13'){ // Locations: Subcounties
				$alloptions = $currentoptions = getSubCounties();
			}
			if($systemattr == '14'){ // Datasets
				$alloptions = $currentoptions = getDatasets('', 1);
			}
			if($systemattr == '15'){ // Attributes
				$alloptions = $currentoptions = getDataAttributes();
			}
			if($systemattr == '16'){ // Frameworks: SDG Objectives
				$alloptions = $currentoptions = getPrograms(6);
			}
			if($systemattr == '18'){ // Frameworks: NDP Objectives
				$alloptions = $currentoptions = getPrograms(5);
			}
			if($systemattr == '19'){ // Frameworks: Organization  Objectives
				$alloptions = $currentoptions = getPrograms(2);
			}
			if($systemattr == '20'){ // Frameworks: Organization  Outcomes
				$alloptions = $currentoptions = getOutcomes(2);
			}
			if($systemattr == '17'){ // Frameworks: Global Objectives
				$alloptions = $currentoptions = getPrograms(3);
			}
			if($systemattr == '21'){ // Frameworks: Global Outcomes
				$alloptions = $currentoptions = getOutcomes(3);
			}
			if($systemattr == '22'){ // Indicator units
				$alloptions = $currentoptions = getDatavariables('INDICATOR_UNITS');
			}
			if($systemattr == '23'){ // activity types
				$alloptions = $currentoptions = getDatavariables('ACTIVITY_TYPES');
			}
			if($systemattr == '24'){ // Sub Impact Areas
				$alloptions = $currentoptions = getDatavariables('PROGRAM_SUBIMPACT_AREAS');
			}
			if($systemattr == '25'){ // Milestones: Level 1
				$alloptions = $currentoptions = getMilestonePeriods(1);
			}
			if($systemattr == '26'){ // Milestones: Level 2
				$alloptions = $currentoptions = getMilestonePeriods(2);
			}
			if(!isEmptyString($systemattrvalues)){
				$currentoptions = getArrayOfListValues($systemattrvalues, $alloptions);
			}
		}
		return $currentoptions;
	}
	
	function getIndicatorProgressDetails($id, $milestoneid = ''){
		$conn = Doctrine_Manager::connection();
		
		$indicator = new Indicator();
		$indicator->populate($id);
		$indicator_info = array();
		if(isEmptyString($milestoneid)){
			$milestoneid = getActiveMilestone(1);
		}
		$selectedmilestones = commaStringToArray($milestoneid);
		
		$indicatorgroups = getDatavariablesWithOdering("INDICATOR_UNITS"); 
		$indicatortypes = getIndicatorTypes();
		
		$indicator_info['ref'] = $indicator->getProject()->getRefNo().': '.$indicator->getRefNo();
		if($indicator->getType() == 1){
			$indicator_info['ref'] = $indicator->getRefNo();
		}
		$indicator_info['title'] = $indicator->getTitle();
		$indicator_info['type'] = $indicator->getIndicatorType();
		$indicator_info['typelabel'] = isArrayKeyAnEmptyString($indicator->getIndicatorType(), $indicatortypes) ? '' : $indicatortypes[$indicator->getIndicatorType()];
		$indicator_info['uom'] = isArrayKeyAnEmptyString($indicator->getUom(), $indicatorgroups) ? '' : $indicatorgroups[$indicator->getUom()];
		$indicator_info['level'] = $indicator->getIndicatorLevel();
		$indicator_info['baseline'] = $indicator->getBaseline();
		
		$expstr_num = ''; $expstr_den = '';
		if(!isEmptyString($indicator->getNumerator())){
			$expstr_num = str_replace('[', '(', $indicator->getNumerator());
			$expstr_num = str_replace(']', ')', $expstr_num);
			$exp_num_query = '';
			
			// preg_match_all("/\[[^\]]*\]/", $indicator->getNumerator(), $matches_withbrackets); // with brackets
			preg_match_all("/\[([^\]]*)\]/", $indicator->getNumerator(), $matches_nobrackets); // no brackets
			//debugMessage($matches_withbrackets[0]);
			$brackets = $matches_nobrackets[1];
			
			// debugMessage($expstr);
			foreach ($brackets as $key => $expressions){
				$line_queries = array();
				$decrptval = my_simple_crypt($expressions, 'd'); // debugMessage($decrptval);
				$exp_split = explode('#', $decrptval); //debugMessage($exp_split);
				$line_queries['attributeid'] = $exp_split[0];
				if(!isArrayKeyAnEmptyString(1, $exp_split)){
					if($exp_split['1'] == 'Total'){
						$attr = new Attribute();
						$attr->populate($exp_split[0]);
						
						if($attr->getcategorytype() == 1 && !isEmptyString($attr->getcategorytype())){
							$categorydata = commaStringToArray($attr->getCategory());
							$options = getDataCategoryOptions(false, getOptionsForID($attr->getcategory())); // debugMessage($options);
							foreach($options as $key => $value){
								$line_queries['optionids'][] = $key;
							}
						}
						if($attr->getcategorytype() == 2 && !isEmptyString($attr->getcategorygroup())){
							$categorydata = commaStringToArray(getCategoriesForGroup($attr->getCategoryGroup()));
							
							$row1 = isArrayKeyAnEmptyString(0, $categorydata) ? '' : $categorydata[0];
							$options1 = getDataCategoryOptions(false, getOptionsForID($row1));
							
							$row2 = isArrayKeyAnEmptyString(1, $categorydata) ? '' : $categorydata[1];
							$options2 = getDataCategoryOptions(false, getOptionsForID($row2));
								
							$row3 = isArrayKeyAnEmptyString(2, $categorydata) ? '' : $categorydata[2];
							$options3 = getDataCategoryOptions(false, getOptionsForID($row3));
							
							if(!isEmptyString($row3) && count($categorydata) == 3){
								foreach($options1 as $key1 => $value1){
									foreach($options2 as $key2 => $value2){
										foreach($options3 as $key3 => $value3){
											$keystr = $key1.",".$key2.",".$key3;
											$line_queries['optionids'][] = $keystr;
										}
									}
								}
							}
								
							if(!isEmptyString($row2) && count($categorydata) == 2){
								foreach($options1 as $key1 => $value1){
									foreach($options2 as $key2 => $value2){
										$keystr = $key1.",".$key2;
										$line_queries['optionids'][] = $keystr;
									}
								}
							}
								
							if(!isEmptyString($row1) && count($categorydata) == 1){
								foreach($options1 as $key1 => $value1){
									$line_queries['optionids'][] = $key1;
								}
							}
						}
						// debugMessage($options); 
					} else {
						$line_queries['optionids'][] = $exp_split[1];
					}
				}
				$indicator_info['replaceable_options'][] = $line_queries;
			}
			
			
			// select the data now
			foreach ($indicator_info['replaceable_options'] as $key => $expdata){
				$custom_query = " d.elementid = '".$expdata['attributeid']."' ";
				if(!isArrayKeyAnEmptyString('optionids', $expdata)){
					$custom_query .= " AND d.optionids IN('".implode("','", $expdata['optionids'])."') ";
				}
				$results_query = " SUM(IF(".$custom_query.", d.result, 0)) ";
				
				$indicator_info['replaceable_options'][$key]['subquery'] = $results_query;
				$expstr_num = str_replace($brackets[$key], $results_query, $expstr_num);
			}
		}
		$indicator_info['numerator_exp'] = $indicator->getNumerator();
		$indicator_info['numerator_exp_query'] = $expstr_num;
		
		foreach($selectedmilestones as $amilestone){
			$actualresult = ''; $results_query = '';
			if(!isEmptyString($expstr_num)){
				$results_query = "SELECT ".$expstr_num." as Result from data_set_result_detail d 
				INNER JOIN data_set_result_summary s on d.resultid = s.id where s.milestoneid = '".$amilestone."' ";
				// debugMessage($results_query); 
				$indicator_info['query'][$amilestone] = $results_query;
				$actualresult = $conn->fetchOne($results_query); 
			}
			$indicator_info['query'][$amilestone] = $results_query;
		
		
			$indicator_info['actual'][$amilestone] = '';
			if($indicator->isQuantitative()) {
				$indicator_info['actual'][$amilestone] = '0';
				if($actualresult){
					$indicator_info['actual'][$amilestone] = $actualresult;
				}
			}
			
			$target_custom = '';
			if(!isEmptyString($amilestone)){
				$target_custom .= " AND t.milestoneid = '".$amilestone."' ";
			}
			$target_query = "SELECT * from content_target t where t.indicatorid = '".$id."' ".$target_custom; // debugMessage($target_query);
			$targetinfo = $conn->fetchRow($target_query); 
			if(!$targetinfo){
				$targetinfo = array();
			}
			$indicator_info['target'][$amilestone] = $targetinfo;
		}
						
		// debugMessage($indicator_info); 
		return $indicator_info;
	}
?>
