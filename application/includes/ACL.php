<?php
require_once 'Zend/Acl.php';
require_once 'Zend/Exception.php';

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);

class ACL extends Zend_Acl   {
	
	public $user_id;
	public $permissions;
	public $permissions_user;
	private $availableGroups;
	
	public function __construct($auserid = "") {
		// do not proceed if no user is defined
		if (isEmptyString($auserid)) {
			return false;
		}
		
		$conn = Doctrine_Manager::connection();
		
		// initialize the array of available groups
		$this->availableGroups = array();
		$this->permissions = array();
		$this->permissions_user = array();
		$this->user_id = $auserid;
		
		// get the groups from the database for the specified user
		$grpstr = $conn->fetchOne("SELECT typeids FROM useraccount WHERE id = '".$auserid."'");
		$groups = explode(',', $grpstr); // debugMessage($groups); // exit;
		
		// get the resources from the database
		$resources = $conn->fetchAll("SELECT id FROM aclresource"); // debugMessage($resources);
		
		// get the permissions for the specified user's groups
		$perm_query = "SELECT 
				p.resourceid,
				re.controller AS controller,
				LOWER(`re`.`name`) AS `resource`,
				p.groupid AS groupid,
				p.actionid,
				a.slug as action,
				p.`value`,
				GROUP_CONCAT(p.`value` separator ',') as valuelist,
				IF(FIND_IN_SET('1', GROUP_CONCAT(p.`value` separator ',')) > 0, 1, 0) as perm
				FROM aclpermission as p 
				INNER JOIN `aclresource` `re` ON (`p`.`resourceid` = `re`.`id`)
				INNER JOIN `aclaction` `a` ON (`p`.`actionid` = `a`.`id`)
				WHERE FIND_IN_SET('".$grpstr."', p.groupid) GROUP BY p.resourceid, p.actionid
				
		";
		// WHERE u.id = '".$auserid."' GROUP BY p.resourceid, p.actionid
		
		$permissions = $conn->fetchAll($perm_query); // debugMessage($perm_query); debugMessage($permissions); exit;
		$this->permissions = $permissions; // exit;
		
		// get the custom permissions for the specified user
		$perm_user_query = "SELECT
				p.resourceid,
				re.controller AS controller,
				LOWER(`re`.`name`) AS `resource`,
				p.groupid AS groupid,
				p.actionid,
				a.slug as action,
				p.value as value,
				p.value as perm
				FROM aclpermission as p
				INNER JOIN `aclresource` `re` ON (`p`.`resourceid` = `re`.`id`)
				INNER JOIN `aclaction` `a` ON (`p`.`actionid` = `a`.`id`)
				WHERE p.userid = '".$auserid."' GROUP BY p.resourceid, p.actionid
		";
		$permissions_user = $conn->fetchAll($perm_user_query);  // debugMessage($perm_user_query); debugMessage($permissions_user); // exit;
		$customuserperms = array();
		if(count($permissions_user) > 0){
			foreach($permissions_user as $perms){
				$customuserperms[$perms['resourceid'].'~'.$perms['actionid']] = $perms;
			}
			$this->permissions_user = $customuserperms;
		}
		// debugMessage($customuserperms); exit;
		
		// add the groups to the ACL
		foreach ($groups as $groupid) {
			$group = new AclGroup();
			// load the details of the user group
			$group->populate($groupid);
			$this->addRole($group); 
			
			// add the group to the array of available groups
			$this->availableGroups[] = $group;
		}
		
		// add the resources to the ACL, the name of the resource and its parent are what are used as identifiers for the resource in the ACL
		foreach ($resources as $value) {
			$ares = new AclResource();
			$ares->populate($value['id']);
			$this->add($ares);
		}
		// add the permissions to the ACL
		if(count($customuserperms) > 0){
			foreach($permissions_user as $value){
				$value['value'] = $value['perm'];
				if(!isArrayKeyAnEmptyString($value['resourceid'].'~'.$value['actionid'], $customuserperms)){
					if($customuserperms[$value['resourceid'].'~'.$value['actionid']]['value'] == '1') {
						$this->allow($value['groupid'], $value['resource'], $value['action']);
						//debugMessage('allowed '.$value['groupid'].' ~ '.$value['resource'].' ~ '. $value['action']);
					} else {
						$this->deny($value['groupid'], $value['resource'], $value['action']);
						//debugMessage('denied '.$value['groupid'].' ~ '.$value['resource'].' ~ '. $value['action']);
					}
				}
			}
		} else {
			foreach ($permissions as $value) {
				$value['value'] = $value['perm'];
				if($value['value'] == '1') {
					$this->allow($value['groupid'], $value['resource'], $value['action']);
					//debugMessage('ballowed '.$value['groupid'].' ~ '.$value['resource'].' ~ '. $value['action']);
				} else {
					$this->deny($value['groupid'], $value['resource'], $value['action']);
					//debugMessage('bdenied '.$value['groupid'].' ~ '.$value['resource'].' ~ '. $value['action']);
				}
			}
		}
		// exit;
	}
	
	/**
	 * Checl whether the user can execute the action on the named resource
	 *
	 * @param String $resourceName The name of the resource
	 * @param String $action The action to be executred
	 */
	function checkPermission($resourceName, $action) {
		// make the resourcenmae lower case to match the ones which were loaded
		$resourceName = strtolower($resourceName); 
		if (!$this->availableGroups) {
			// there are no groups loaded
			return false; 
		}
		foreach ($this->availableGroups as $group) {
			try {	
				// check if the action can be executed on the resource for the specific role
				// use the parent method since we are dealing with one role at a time here
				if (parent::isAllowed($group, $resourceName, $action)) {
					// action is allowed on the resource 
					return true;
				} else {
					
				}
			} catch (Zend_Exception $ze){
				// either a resource or a group is not defined in the ACL. Deny access to it
				error_log($ze->__toString());
				return false;
			}
		}
		// by default the action is denied on the resource
		return false; 
	}
	/**
	 * Return an array containing the defined actions
	 *
	 * @return Array Array containing the defined actions
	 */
	/* static function getActions() {
		return array(ACTION_YESNO, ACTION_CREATE, ACTION_EDIT, ACTION_VIEW, ACTION_LIST, ACTION_DELETE, ACTION_APPROVE, ACTION_EXPORT);
	} */
	/**
	 * Overidden method from Zend_ACL to enable processing of multiple roles
	 *
	 * @param Zend_Acl_Role_Interface|string|array $group This parameter is ignored
	 * @param Zend_Acl_Resource_Interface|string|array $resource
	 * @param string $action
	 * @return Whether or not the action can be executed on the resource 
	 */
	public function isAllowed($group, $resource, $action) {
		return $this->checkPermission($group, $resource, $action); 
	}

}
?>
