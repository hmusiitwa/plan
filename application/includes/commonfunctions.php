<?php

date_default_timezone_set('Africa/Kampala');

# functions to create and manage drop down lists
require_once 'dropdownlists.php';
require_once 'BrowserDetection.php';

define("ACTION_CREATE", "create");
define("ACTION_INDEX", "index"); // maps to the default controller for Zend Framework, same as the create action in the ACL 
define("ACTION_EDIT", "edit");
define("ACTION_UPDATE", "update");
define("ACTION_APPROVE", "approve");
define("ACTION_DELETE", "delete");
define("ACTION_EXPORT", "export");
define("ACTION_VIEW", "view");
define("ACTION_LIST", "list");
define("ACTION_YESNO", "flag");

# redirect, success and error urls during the processing of an action 
define("URL_REDIRECT", "redirecturl"); // url forwarded to when a user has to login 
define("URL_SUCCESS", "successurl"); // override the url when an action suceeds
define("URL_FAILURE", "failureurl"); // override the url when an action fails

# the separator between a table name and a column name for filtering since the . cannot be used as
# a separator for HTML field names
define("HTML_TABLE_COLUMN_SEPARATOR", "__");

# the session variable holding the values from the REQUEST when an error occurs
define("FORM_VALUES", "formvalues");
# the session variable holding the error message when processing a form 
define("ERROR_MESSAGE", "errors"); 
# the session variable holding the success message when processing a form 
define("SUCCESS_MESSAGE", "successmessage"); 
# the session variable holding the error message for the error page which is not cleared from page to page 
define("APPLICATION_ERROR_PAGE_MESSAGE", "error_page_erros"); 
# the session variable for the access control lists 
define("SESSION_ACL", "acl"); 

# calendar view options
define("CALENDAR_VIEW_MONTH", "month_view"); 
define("CALENDAR_VIEW_WEEK", "week_view"); 

# constant for showing views in a popup
define("PAGE_CONTENTS_ONLY", "pgc"); 
define("EXPORT_TO_EXCEL", "excel"); 

# constant for the select chain value
define("SELECT_CHAIN_TYPE", "select_chain_type"); 

# excel generation constants
# a comma delimited list of column indexes with numbers
define("EXPORT_NUMBER_COLUMN_LIST", "numbercolumnlist");
# the number of columns to ignore at the beginning of the query 
define("EXPORT_IGNORE_COLUMN_COUNT", "columncheck");  
# the query string with all the results
define("ALL_RESULTS_QUERY", "arq");
# the query string with the searches and filters applied
define("CURRENT_RESULTS_QUERY", "crq");
# the page title for current list
define("PAGE_TITLE", "ttl");

define('DEFAULT_USER_GROUP', '2');
define('DEFAULT_ID', '1');
define('VALIDATE_PHONE_ACTIVATED', false);
define('DEFAULT_DATETIME', date("Y-m-d H:i:s", time()));
define('DEFAULT_PROGRAM_LINES', 20);
define('DEFAULT_CUSTOMERNO_PREFIX', 'C');
define('DEFAULT_PROD_PREFIX', 'P');
define('DEFAULT_STOCKADJ_PREFIX', 'SA');
define('DEFAULT_STOCKTRST_PREFIX', 'ST');
define('DEFAULT_STOCKPROD_PREFIX', 'SP');
define('DEFAULT_SALESORDER_PREFIX', 'S');
define('DEFAULT_PRODUCTN_PREFIX', 'PD');
define('DEFAULT_PRICEADJ_PREFIX', 'PA');
define('DEFAULT_BOM_PREFIX', 'E');
define('DEFAULT_LOCATION', '1');
define('HOME', 'Home');
define('PROJECT_DIR', '3');
define('DATASET_DIR', '4');
define('BASEDOC_DIR', '3');
define('DAYS_TOEDIT', '7');
define('NUMBER_TYPES', serialize(array(1,2)));
define('C_N', 1);
define('C_P', 2);
define('C_TX', 3);
define('C_C', 4);
define('C_F', 5);
define('C_D', 6);
define('C_TM', 7);
define('C_DT', 8);
define('C_LS', 9);
define('C_LM', 10);
define('C_YN', 11);
define('C_Y', 12);
define('FLEXLICENSE', 'Z71L-XAFG2O-0B720Q-6C4T6S');
// Z70L-XABE3Z-2K6J6P-0B5N1P // Z7N7-XCJH5K-6D2436-256265
define('PASSWORD_EXPIRY_DURATION', 15);

function getDateTime(){
	return date("Y-m-d H:i:s", time());
}

function getDefaultLayout($id = '1'){
	$config = Zend_Registry::get("config"); 
	return $config->system->layout;
}
function getDefaultTopBar($id = '1'){
	$config = Zend_Registry::get("config"); 
	return $config->system->topbar;
}
function getDefaultSideBar($id = '1'){
	$config = Zend_Registry::get("config"); 
	return $config->system->sidebar;
}
function getDefaultTheme($id = '1'){
	$config = Zend_Registry::get("config"); 
	return $config->system->colortheme;
}
function getDefaultShowSideBar($id = '1'){
	$config = Zend_Registry::get("config"); 
	return $config->system->showsidebar;
}
function getDefaultAppName(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->system->appname;
}
function getAppName($id = ''){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->system->appname;
}
function getAppFullName(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config");
	return $config->system->appfullname;
}
function getAppLogo($id = ''){
	$session = SessionWrapper::getInstance();
	return $this->baseUrl('images/logo.png');
}
function getCompanyName(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->system->companyname;
}
function getCompanySignoff(){
	$config = Zend_Registry::get("config"); 
	return $config->system->companysignoff;
}
function getCopyrightInfo(){
	$config = Zend_Registry::get("config"); 
	return $config->system->copyrightinfo;
}
function getDefaultAdminEmail($default = false){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->notification->defaultadminemail;
}
function getDefaultAdminName($default = false){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->notification->defaultadminname;
}
function getEmailMessageSender(){
	$config = Zend_Registry::get("config"); 
	return getDefaultAdminEmail();
}
function getSupportEmailAddress(){
	$config = Zend_Registry::get("config"); 
	return $config->notification->supportemailaddress;
}
function getAltSupportEmailAddress(){
	$config = Zend_Registry::get("config"); 
	return $config->notification->supportemailaddress2;
}
function getSupportPhone(){
	$config = Zend_Registry::get("config"); 
	return $config->notification->supportphone;
}
function getFacebookPage(){
	$config = Zend_Registry::get("config");
	return $config->notification->facebookpg;
}
function getTwitterPage(){
	$config = Zend_Registry::get("config");
	return $config->notification->twitterpg;
}
function getNotificationSenderName(){
	$config = Zend_Registry::get("config"); 
	return getDefaultAdminName();
}
function getSmsServer(){
	$config = Zend_Registry::get("config");
	return $config->sms->serverurl;
}
function getSmsUsername(){
	$config = Zend_Registry::get("config");
	return $config->sms->serverusername;
}
function getSmsPassword(){
	$config = Zend_Registry::get("config");
	return $config->sms->serverpassword;
}
function getSmsPort(){
	$config = Zend_Registry::get("config");
	return $config->sms->serverport;
}
function getSmsSenderName(){
	$config = Zend_Registry::get("config");
	return $config->sms->sendername;
}
function getSmsTestNumber(){
	$config = Zend_Registry::get("config");
	return $config->sms->testnumber;
}
function getSmsStatus(){
	$config = Zend_Registry::get("config");
	$smsflag = $config->sms->smsdelivery;
	if(APPLICATION_ENV == "development"){
		// $smsflag = "off";
	}
	return $smsflag == 1 || $smsflag == 'on' || $smsflag == 'yes' ? true : false;
}
function getWebsiteConnection(){
	$manager = Doctrine_Manager::getInstance();
	return $manager->connection(WEBSITE_CONNECT_STRING);
}
function getMinPhoneLength(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->phoneminlength;
}
function getMaxPhoneLength(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->phonemaxlength;
}
function getMaxPostalCodeLength(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->postalcodemaxlength;
}
function getDefaultPhoneCode(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->country->countryphonecode;
}
function getCountryCode(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->country->countryisocode;
}
function getCountryCurrencySymbol(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->country->currencysymbol;
}
function getCountryCurrencyCode(){
	$session = SessionWrapper::getInstance();
	$config = Zend_Registry::get("config"); 
	return $config->country->currencycode;
}
function getCurrencyDecimalPlaces(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->currencydecimalplaces;
}
function getNumberDecimalPlaces(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->numberdecimalplaces;
}
function getNationalIDMinLength(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->nationalidminlength;
}
function getNationalIDMaxLength(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->nationalidmaxlength;
}
function getTimeZine(){
	$config = Zend_Registry::get("config");
	$session = SessionWrapper::getInstance();
	return $config->country->timezone;
}
function getThemeColor(){
	$config = Zend_Registry::get("config"); 
	return $config->system->colortheme;
}
function getExpiryDuration(){
	return PASSWORD_EXPIRY_DURATION;
}

function changeMySQLDateToPageFormat($mysqldate, $format = '') {
	$aconfig = Zend_Registry::get("config"); 
	$session = SessionWrapper::getInstance();
	if (isEmptyString($mysqldate)) {
		return $mysqldate;
	} else {
		$formatby = '';
		if(isEmptyString($formatby)) {
			$formatby = $aconfig->dateandtime->mediumformat;
		}
		// debugMessage($formatby); // exit;
		return date($formatby, strtotime($mysqldate));
	}
}
/**
 * Transform a date from the format displayed on pages(mm/dd/yyyy) to the MySQL database date format (yyyy-mm-dd). 
 * If the date from the database is an empty string or the string NULL, it is transformed to a NULL value.
 *
 * @param String $pagedate The string representing the date
 * @return String The MYSQL datetime format or NULL if the provided date is an empty string or the string NULL 
 */
function changeDateFromPageToMySQLFormat($pagedate, $ignoretime = true) {
	if ($pagedate == "NULL") {
		return NULL;
	}
	if (isEmptyString($pagedate)) {
		return NULL;
	} else {
		if($ignoretime){
			return date("Y-m-d", strtotime($pagedate));
		} else {
			return date("Y-m-d H:i:s", strtotime($pagedate));
		}
	}
}

function formatDateAndTime($mysqldate, $ignoretime = true){
	if(isEmptyString($mysqldate)){
		return NULL;
	}
	
	$session = SessionWrapper::getInstance();
	$timestr = '';
	if(!$ignoretime){
		$timestr = formatTimeCustom($mysqldate);
	}
	return changeMySQLDateToPageFormat($mysqldate).' '.$timestr;
}
function formatTimeCustom($timestring, $format = ''){
	if(isEmptyString($timestring)){
		return NULL;
	}
	$formatby = '';
	$session = SessionWrapper::getInstance();
	$otime = new DateTime($timestring);
	if(isEmptyString($formatby)){
		$formatby = $format;
	}
	if(isEmptyString($formatby)){
		$formatby = "h:i A";
	}
	
	$str_24hr = '';
	if($formatby == 'H:i'){
		$str_24hr = ' Hrs';		
	}
	return $otime->format($formatby).$str_24hr;
}
function getCurrentMysqlTimestamp(){
	return date('Y-m-d H:i:s', strtotime('NOW'));
}
function decimalToTime($decimal){
	return gmdate('H:i', floor($decimal * 3600));
}
/**
 * Check whether or not the string is empty. The string is emptied
 *
 * @param String $str The string to be checked
 * 
 * @return Boolean Whether or not the string is empty
 */
function isEmptyString($str) {
	if ($str == "") {
		return true; 
	}
	if (trim($str) == "") {
		return true;
	} else {
		return false;
	}
}

/**
 * Check whether or not the value of the key in the specified array is empty
 *
 * @param String $key The key whose value is to be checked
 * @param Array $arr The array to check  
 * 
 * @return Boolean Whether or not the array key is empty
 */
function isArrayKeyAnEmptyString($key, $arr) {
	if (!array_key_exists($key, $arr)) {
		return true; 
	}
	if (is_string($arr[$key])) {
		return isEmptyString($arr[$key]);
	}
	return false; 
}
/**
 * Check whether or not the string is empty. The string is emptied
 *
 * @param String $str The string to be checked
 * 
 * @return boolean Whether or not the string is empty
 */
function isNotAnEmptyString($str) {
	return ! isEmptyString($str);
}

/**
 * Return a debug message with a break tag before and two break tags after
 *
 * @param Object $obj The object to be printed
 */
function debugMessage($obj) {
	echo "<br />";
	print_r($obj);
	echo "<br /><br />";
}

/**
 * Return the value of the checked HTML attribute for a checkbox or radio button
 *
 * @param Boolean $bool whether or not the HTML control is checked
 * @return String the checked attribute
 */
function getCheckedAttribute($bool) {
	if ($bool) {
		return ' checked="checked"';
	}
	return "";
}
/**
 *  Merge the arrays passed to the function and keep the keys intact.
 *  If two keys overlap then it is the last added key that takes precedence.
 * 
 * @return Array the merged array
 */
function array_merge_maintain_keys() {
	$args = func_get_args();
	$result = array();
	foreach ( $args as &$array ) {
		foreach ( $array as $key => &$value ) {
			$result[$key] = $value;
		}
	}
	return $result;
}

# function that trims every value of an array
function trim_value(&$value) {
	$value = trim($value);
}

/**
 * Recursively Remove empty values from an array. If any of the keys contains an 
 * array, the values are also removed.
 *
 * @param Array $input The array
 * @return Array with the specified values removed or the filtered values
 */
function array_remove_empty($arr) {
	$narr = array();
	while ( list ($key, $val) = each($arr) ) {
		if (is_array($val)) {
			$val = array_remove_empty($val);
			// does the result array contain anything?
			if (count($val) != 0) {
				// yes :-)
				$narr[$key] = $val;
			}
		} else {
			if (! isEmptyString($val)) {
				$narr[$key] = $val;
			}
		}
	}
	unset($arr);
	return $narr;

}
/**
 * Create a Zend_Mail instance from the registry, clear all recipients and the existing subject
 *
 * @return Zend_Mail
 */
function getMailInstance() {
	$server = $_SERVER['SERVER_NAME'];
	$mail = Zend_Registry::get("mail");
	$mail->clearSubject();
	$mail->clearRecipients();
	$mail->setBodyHtml('');

	// exit;
	return $mail;
}
/**
 * Send test email
 *
 * @param String $subject The subject of the email 
 * @param String $message The contents of the email 
 */
function sendTestMessage($subject = "", $message = "", $email ="hmanmstw@gmail.com") {
	$mailer = getMailInstance(); 
	$mailer->clearFrom();
	$mailer->clearReplyTo();
	
	# get an instance of the PHP Mailer
	$from_email = getDefaultAdminEmail(); 
	$mailer->setReplyTo($from_email);
	$mailer->setFrom($from_email, getDefaultAdminName());
	
	$mailer->AddTo($email);
	debugMessage("Sending test message below from " . $mailer->getFrom().' to '.$email);
	$mailer->setSubject($subject);
	$mailer->setBodyHTML($message);
	
	debugMessage($message);
	debugMessage("Result >>");
	try {
		$result = $mailer->send();
		debugMessage("Message successfully sent to ".$email);
	} catch ( Exception $e ) {
		debugMessage("Error sending message: <br> ".$e->getMessage());
	}
}
# send sms message to phone number
function sendSMSMessage($to, $txt, $source = '', $msgid = '', $country="UG", $contentid = "", $ids = "", $servicedetailid = "", $savetooutbox = true) {
	$session = SessionWrapper::getInstance();
	$time = time();
	$phone = $to;
	$message = $txt;
	$message = trim(preg_replace('/\s+/', ' ', trim($message))); // strip line breaks
	$message = str_replace('<br>', ' ', $message);
	
	$allowsend = getSmsStatus();
	if(APPLICATION_ENV == 'development'){
		$allowsend = 'off';
	}
	
	if(isEmptyString($source)){
		$source = getSmsSenderName();
	}
	$server = getSmsServer(); 
	$username = getSmsUsername();
	$password = getSmsPassword();
	$credential = encode($username.':'.$password); // debugMessage('~ '.$source);
	$headers = array(
		'Content-Type: application/json',
		'accept' => 'application/json',
		'Authorization: Basic '.$credential
	);
	
	if(is_array($to)){
		$numbers = $to; 
	} else {
		$numbers = explode(',', $phone);
	}
	// debugMessage($numbers);
	
	$destarray = array();
	foreach ($numbers as $aphone){
		$destarray[] = array('to'=>trim($aphone), 'messageId'=> 'smsid_'.$time.'_'.trim($aphone));
	}
		
	$jsonDataEncoded = '{
		"bulkId":"bulkid_'.$time.'",
		"messages":[
		  {
			 "from":"'.$source.'",
			 "destinations":'.json_encode($destarray).',
			 "text":"'.$message.'"
		  }
		]
	}';
	// debugMessage($jsonDataEncoded); // exit;
	
	$smsresult = array(1=>'', 2=>'', 3=>'', 4=>'');
	if(($allowsend == 'on' || $allowsend == 'yes' || $allowsend == '1') && count($numbers) > 0){
		//debugMessage('sending sms');
		//$result = curlContents($server, 'POST', $jsonDataEncoded, $headers, false); debugMessage($result);
		if(!isEmptyString($result)){
			$resultarray = objectToArray(json_decode(($result))); // debugMessage($resultarray); 
			// exit;
			// check if parameter to outbox has been set
			if($savetooutbox){ // debugMessage('saving to outbox');
				$phonearray = $numbers;
				$countphones = count($phonearray);
				
				if(!isArrayKeyAnEmptyString('messages', $resultarray)){
					if(!isArrayKeyAnEmptyString('0', $resultarray['messages'])){
						$data = $resultarray['messages']['0']; // debugMessage($data); exit;
					}
					if(count($resultarray['messages']) <= 1){
						$resultcode = $data['status']['name'];
						$description = $data['status']['description'];
						$smsid = $data['messageId'];
						
						$smsresult['1'] = $resultcode;
						$smsresult['2'] = $smsid;
						$smsresult['3'] = $description;
							
						// save to outbox too
						$outbox_array = array(
							"msg" => $message,
							"phone" => $data['to'],
							"source" => $source,
							"resultcode" => $resultcode,
							"deliverdetails" => $description,
							"smsid" => $smsid,
							"datecreated" => getCurrentMysqlTimestamp(),
							"createdby" => $session->getVar('userid'),
							"msgcount" => $countphones,
							"country" => $country
						);
						
						if(!isEmptyString($msgid)){
							$outbox_array['messageid'] = $msgid;
						}
						if(!isEmptyString($servicedetailid)){
							$outbox_array['servicedetailid'] = $servicedetailid;
						}
						if(!isEmptyString($contentid)){
							$outbox_array['contentid'] = $contentid;
						}
						
						$outbox = new Outbox();
						$outbox->processPost($outbox_array);
						//debugMessage($smsresult);
						//debugMessage($outbox->toArray());
						// debugMessage('outbox error '.$outbox->getErrorStackAsString());
						if(!$outbox->hasError()){
							$outbox->save();
							$smsresult['4'] = $outbox->getID();
						}
					} else {
						$outbox_collection = new Doctrine_Collection(Doctrine_Core::getTable("Outbox"));
						$outbox_result = array();
						foreach ($resultarray['messages'] as $resultline){
							$data = $resultline; // debugMessage($data);
							$resultcode = $data['status']['name'];
							$description = $data['status']['description'];
							$smsid = $data['messageId'];
								
							$smsresult['1'] = $resultcode;
							$smsresult['2'] = $smsid;
							$smsresult['3'] = $description;
							
							// save to outbox too
							$outbox_array = array(
									"msg" => $message,
									"phone" => $data['to'],
									"source" => $source,
									"resultcode" => $resultcode,
									"deliverdetails" => $description,
									"smsid" => $smsid,
									"datecreated" => getCurrentMysqlTimestamp(),
									"createdby" => $session->getVar('userid'),
									"msgcount" => $countphones,
									"country" => $country
							);
								
							if(!isEmptyString($msgid)){
								$outbox_array['messageid'] = $msgid;
							}
							if(!isEmptyString($servicedetailid)){
								$outbox_array['servicedetailid'] = $servicedetailid;
							}
							if(!isEmptyString($contentid)){
								$outbox_array['contentid'] = $contentid;
							}
							$outbox_array['msgcount'] = 1;
							
							$outbox = new Outbox();
							$outbox->processPost($outbox_array);
							if(!$outbox->hasError()){
								$outbox_collection->add($outbox);
							} else {
								debugMessage('outbox error '.$outbox->getErrorStackAsString());
							}
						}
						// debugMessage($outbox_collection->toArray());
						if($outbox_collection->count() > 0){
							if($outbox_collection->save()){ // save collection to outbox
								foreach ($outbox_collection as $savedobj){
									$outbox_result[$savedobj->getPhone()] = array('id'=>$savedobj->getID(),'phone'=>$savedobj->getPhone(),'smsid'=>$savedobj->getsmsid(),'status'=>$savedobj->getresultcode());
								}
								$smsresult['5'] = $outbox_result;
							}
						}
					}
				} else {
					$smsresult['1'] = 'ERROR';
					$smsresult['3'] = $result;
				}
				
			}
		} else {
			$smsresult['1'] = 'ERROR';
			$smsresult['3'] = $result;
		}
		debugMessage($smsresult);
		checkSMSBalance();
		return $smsresult;
	}
	debugMessage($smsresult);
	checkSMSBalance();
	return $smsresult;
}
function checkSMSBalance(){
	$session = SessionWrapper::getInstance();
	
	$server = 'http://api.infobip.com/account/1/balance';
	$username = getSmsUsername();
	$password = getSmsPassword();
	$credential = encode($username.':'.$password);
	$headers = array(
			'Content-Type: application/json',
			'accept' => 'application/json',
			'Authorization: Basic '.$credential
	);
	$jsonDataEncoded = '{}';
		
	$result = curlContents($server, 'GET', $jsonDataEncoded, $headers, false); // debugMessage($result);
	// $session->setVar("currency", '');
	// $session->setVar("balance", '');
	if(!isEmptyString($result)){
		$result_array = objectToArray(json_decode($result)); // debugMessage($result_array);
		if(is_array($result_array)){
			$session->setVar("currency", $result_array['currency']);
			$session->setVar("balance", $result_array['balance']);
		}
	}
	return true;
}
/**
 * Wrapper function for the encoding of the urls using base64_encode 
 *
 * @param String $str The string to be encoded
 * @return String The encoded string 
 */
function encode($str) {
	// return str_replace('/', '*', base64_encode($str)); 
	return base64_encode($str);
}
/**
 * Wrapper function for the decoding of the urls using base64_decode 
 *
 * @param String $str The string to be decoded
 * @return String The encoded string 
 */
function decode($str) {
	// return base64_decode(str_replace('*', '/', $str));
	return base64_decode($str);
}

/**
 * Function to generate a JSON string from an array of data, using the keys and values
 *
 * @param $data The data to be converted into a string
 * @param $default_option_value Value of the default option
 * @param $default_option_text Test for the default 
 * 
 * @return the JSON string containing the select options
 */
function generateJSONStringForSelectChain($data, $default_option_value = "", $default_option_text = "<Select One>") {
	$values = array(); 
	//debugMessage($data);
	if (!isEmptyString($default_option_value)) {
		# use the text and option from the data
		if(!isArrayKeyAnEmptyString($default_option_value, $data)){
			$values[] = '{"id":"' . $default_option_value . '", "label":"' . $data[$default_option_value] . '"}';
			// remove the default option from the available options
			unset($data[$default_option_value]);
		}
	}
	# add a default option
	$values[] = '{"id":"", "label":"' . $default_option_text . '"}';
	foreach ( $data as $key => $value ) {
		$values[] = '{"id":"'.$key.'", "label":"' . $value . '"}';
		//debugMessage($strstring);
	}
	# remove the first comma at the end
	return '[' . implode("," , $values). "]";
}
/**
 * Format a number to two decimal places and a comma separator between thousands. Empty Strings are considered to be numeric
 *
 * @param Number $number The number to be formatted
 * @return Number The formatted version of the number
 */
function formatNumber($number, $pts = '', $removezero = true) {
	if($number == 0) {
		return '0';
	}
	if(isEmptyString($number)) {
		return '';
	}
	if(!is_numeric($number)) {
		return '';
	}
	$aconfig = Zend_Registry::get("config"); 
	$decimals = $aconfig->country->numberdecimalplaces;
	if(!isEmptyString($pts)){
		$decimals = $pts;
	}
	if($removezero){
		return rtrim(rtrim(number_format($number, $decimals), '0'), '.');
	}
	return number_format($number, $decimals);
}
function formatSeparator($number){
	if(isEmptyString($number)) {
		return '';
	}
	if(!is_numeric($number)) {
		return '';
	}
	return rtrim(rtrim($number, '0'), '.');
}
function formatNum($number, $pts = '', $removezero = true) {
	if(isEmptyString($number)) {
		return '';
	}
	if(!is_numeric($number)) {
		return '';
	}
	$aconfig = Zend_Registry::get("config"); 
	$decimals = $aconfig->country->numberdecimalplaces;
	if(!isEmptyString($pts)){
		$decimals = $pts;
	}
	if($removezero){
		return rtrim(rtrim(number_format($number, $decimals), '0'), '.');
	}
	return number_format($number, $decimals);
}
function formatDigit($number) {
	if(isEmptyString($number)) {
		return '';
	}
	if(!is_numeric($number)) {
		return '';
	}
	return number_format($number);
}
function formatMoney($amount, $pts = '') {
	if(isEmptyString($amount)) {
		return '0.00';
	}
	if(!is_numeric($amount)) {
		return '';
	}
	$aconfig = Zend_Registry::get("config"); 
	$decimals = $aconfig->country->numberdecimalplaces;
	if(!isEmptyString($pts)){
		$decimals = $pts;
	}
	return rtrim(rtrim(number_format($amount, $decimals, '.', ''), '0'), '.');
}
function formatMoneyOnly($amount, $pts = '') {
	if(isEmptyString($amount)) {
		return '0.00';
	}
	$aconfig = Zend_Registry::get("config");
	$decimals = $aconfig->country->currencydecimalplaces;
	if(!isEmptyString($pts)){
		$decimals = $pts;
	}
	return rtrim(rtrim(number_format($amount, $decimals), '0'), '.');
}
/**
 * Generate an HTML list from an array of values
 *
 * @param Array $array
 * @return String 
 */
function createHTMLListFromArray($array, $classname="") {
	$str = ""; 
	// return empty string if no array is passed
	if (!is_array($array)) {
		return $str; 
	}
	// return an empty string if the array is empty
	if (!$array) {
		return $str; 
	}
	$class = "";
	if(!isEmptyString($classname)){
		$class = " class='".$classname."'";
	}
	// opening list tag and the first li element
	$str  = "<ul ".$class."><li>";
	// explode the array and generate the inner list items
	$str .= implode($array, "</li><li>");
	// close the last list item, and the ul
	$str .= "</li></ul>"; 
	
	return $str; 
}
function createHTMLCommaListFromArray($array, $separator = "', '") {
	$str = ""; 
	// return empty string if no array is passed
	if (!is_array($array)) {
		return $str; 
	}
	// return an empty string if the array is empty
	if (!$array) {
		return $str; 
	}
	
	// explode the array and generate the inner list items
	$str .= implode($array, $separator);
	
	return $str; 
}
/**
  * Load the application configuration from the database
  * 
  */
function loadConfig() {
	$cache = Zend_Registry::get('cache');
	// load the configuration from the cache
	$config = $cache->load('config'); 
	if (!$config) {
		// do nothing 
	} else {
		// add the config object to the registry 
		Zend_Registry::set('config', $config);
		return; 
	}
	
	// load the active application configuration from the database
	$sql = "SELECT section, optionname, optionvalue FROM appconfig WHERE active = 'Y'";

	$conn = Doctrine_Manager::connection(); 
	$result = $conn->fetchAll($sql); 
	
	// generate a config array from the data
	if (!$result) {
		// do nothing no data returned
	} else {
		$config_array = array(); 
		foreach ($result as $line) {
			if (isEmptyString($line['section'])) {
				// no section name provided so add the option to the root
				$config_array[$line['optionname']] = $line['optionvalue']; 
			} else {
				// add the option to the section 
				$config_array[$line['section']][$line['optionname']]= $line['optionvalue'];
			}  
		}
		# Add the config object to the registry
		$config = new Zend_Config($config_array); 
		Zend_Registry::set('config', $config);
		# cache the config object
		$cache->save($config, 'config');
	}

}
/**
 * Return an instance of the access control list 
 *
 * @return ACL 
 */
function getACLInstance() {
	$cache = Zend_Registry::get('cache'); 
	$session = SessionWrapper::getInstance(); 
	// check if the acl is cached
	$aclkey = "acl".$session->getVar('userid'); 
	$acl = $cache->load($aclkey); 
	if (!$acl) {
		$acl = new ACL($session->getVar('userid')); 
	}
	
	return $acl; 
}
function getAuditInstance(){
	$server = $_SERVER['SERVER_NAME']; // debugMessage($server);
	$browser = new Browser();
	$audit_values = array(
		"browserdetails" => $browser->getBrowserDetailsForAudit(),
		"browser"=>$browser->getBrowser(),
		"version"=>$browser->getVersion(),
		"useragent"=>$browser->getUserAgent(),
		"os"=>$browser->getPlatform(),
		"ismobile"=>$browser->isMobile() ? '1' : 0,
		"companyid"=>getCompanyID(),
		"userid" => getUserID(),
		"ipaddress"=>$browser->getIPAddress()
	);
	
	// debugMessage($audit_values); exit;
	
	return $audit_values;
}
/**
 * Return the file extension from a file name
 * 
 * @param string $filename
 * @return The file extension 
 */
function findExtension($filename){  
	return substr(strrchr($filename,'.'),1);
}
/**
 * Decode all html entities of an array  
 * @param Array $elem the array to be decoded
 */
function decodeHtmlEntitiesInArray(&$elem){ 
	if (!is_array($elem)) { 
    	$elem=html_entity_decode($elem); 
	}  else  { 
		foreach ($elem as $key=>$value){
			$elem[$key]=decodeHtmlEntitiesInArray($value);
		} 
  	} 
	return $elem; 
}
 /**
 * Trims a given string with a length more than a specified length with a more link to view the details 
 *
 * @param string $text
 * @param int $length
 * @param string $tail
 * @return string the substring with a more link as the tail
 */
function snippet($text, $length, $tail) {
	$text = trim($text);
	$txtl = strlen($text);
	if ($txtl > $length) {
		for($i = 1; $text[$length - $i] != " "; $i ++) {
			if ($i == $length) {
				return substr($text, 0, $length) . $tail;
			}
		}
		for(; $text[$length - $i] == "," || $text[$length - $i] == "." || $text[$length - $i] == " "; $i ++) {
			;
		}
		$text = substr($text, 0, $length - $i + 1) . $tail;
	}
	return $text;
} 
function formatBytes($size, $precision = 2) { 
    $base = log($size) / log(1000);
    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

    return round(pow(1000, $base - floor($base)), $precision) . $suffixes[floor($base)];
}
 /*
 * Generate a thumbnail from a source and a new width
 */
function resizeImage($in_filename, $out_filename, $width){
	$src_img = ImageCreateFromJpeg($in_filename);

    $old_x = ImageSX($src_img);
    $old_y = ImageSY($src_img);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
  	$desired_height = floor($old_y *($width/$old_x));
  
    $dst_img = ImageCreateTrueColor($width, $desired_height);
    ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $width, $desired_height, $old_x, $old_y);

    ImageJpeg($dst_img, $out_filename, 100);

    ImageDestroy($dst_img);
    ImageDestroy($src_img);
}
# determine key for an array in a multimensional array
function array_search_key($all, $checkarray) {
   foreach ($all as $key => $value) {
		// debugMessage($subkey);
		if($checkarray['id'] == $value['id'] && count(array_diff($value, $checkarray)) == 0){
			return $key;
		}
   }
}
# determine key for id in multidimensional array
function array_search_key_by_id($themultiarray, $theid) {
   foreach ($themultiarray as $key => $value) {
		// debugMessage($subkey);
		if($theid == $value['id']){
			return $key;
		}
   }
}
# synchronise to multimensional arrays
function multidimensional_array_merge($oldarray, $newarray){
    $result = array();
    foreach($oldarray as $key => $value){
    	$result[$key] = $value;
        foreach ($newarray as $n_key => $n_value) {
        	if(strval($n_key) == strval($key)){
        		// debugMessage($value); debugMessage($newarray[$key]);
        		$merged = array_merge($oldarray[$key], $newarray[$n_key]);
        		unset($result[$key]);
        		$result[$key] = $merged;
        	} else {
        		$result[$n_key] = $n_value;
        	}
        }
        // debugMessage($oldarray[$n_key]);
    }
    return $result;
}
# convert text to url
function textToUrl($txtstring){
	if(isEmptyString($txtstring)){
		return '---';
	}
	return "<a href='".$txtstring."' target='_blank' title='Visit address'>".$txtstring."</a>";
}
# determine if person has profile image
function hasProfileImage($id, $photoname){
	$real_path = APPLICATION_PATH."/../public/uploads/user_";
 	if (APPLICATION_ENV == "production") {
 		$real_path = str_replace("public/", "", $real_path); 
 	}
	$real_path = $real_path.$id.DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."base_".$photoname;
	if(file_exists($real_path) && !isEmptyString($photoname)){
		return true;
	}
	return false;
}
# determine path to thumbnail profile picture
function getThumbnailPicturePath($id, $gender, $photoname) {
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$path= "";
	if(isMale($gender)){
		$path = $baseUrl.'/uploads/user_0/avatar/default_thumbnail_male.jpg';
	}  
	if(isFemale($gender)){
		$path = $baseUrl.'/uploads/user_0/avatar/default_thumbnail_female.jpg'; 
	}
	if(hasProfileImage($id, $photoname)){
		$path = $baseUrl.'/uploads/user_'.$id.'/avatar/thumbnail_'.$photoname;
	}
	return $path;
}
# determine path to medium profile picture
function getMediumPicturePath($id, $gender, $photoname) {
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$path= "";
	if(isMale($gender)){
		$path = $baseUrl.'/uploads/user_0/avatar/default_medium_male.jpg';
	}  
	if(isFemale($gender)){
		$path = $baseUrl.'/uploads/user_0/avatar/default_medium_female.jpg'; 
	}
	if(hasProfileImage($id, $photoname)){
		$path = $baseUrl.'/uploads/user_'.$id.'/avatar/medium_'.$photoname;
	}
	return $path;
}
# determine path to large profile picture
function getLargePicturePath($id, $gender, $photoname) {
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$path= "";
	if(isMale($gender)){
		$path = $baseUrl.'/uploads/user_0/avatar/default_large_male.jpg';
	}  
	if(isFemale($gender)){
		$path = $baseUrl.'/uploads/user_0/avatar/default_large_female.jpg'; 
	}
	if(hasProfileImage($id, $photoname)){
		$path = $baseUrl.'/uploads/user_'.$id.'/avatar/large_'.$photoname;
	}
	// debugMessage($path);
	return $path;
}
# determine if male
function isMale($gender){
	return $gender == '1' ? true : false; 
}
# determine if female
function isFemale($gender){
	return $gender == '2' ? true : false; 
}
function getGenderText($gender){
	$str = 'Male';
	if(isFemale($gender)){
		$str = 'Female';
	}
	return $str;
}
function getUserPicture($id, $gender, $photoname) {
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$photo_path = $baseUrl.'/uploads/default/default_medium_male.jpg';
	if($gender == 2){
		$photo_path = $baseUrl.'/uploads/default/default_thumbnail_female.jpg'; 
	}
	$hasprofileimage = false;
	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_";
	$real_path = $real_path.$id.DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$photoname;
	if(!isEmptyString($photoname) && file_exists($real_path)){
		$hasprofileimage = true;
	}
	
	if($hasprofileimage){
		$photo_path = $baseUrl.'/uploads/users/user_'.$id.'/avatar/medium_'.$photoname;
	}
	return $photo_path;
}
function getDirectoryPicture($id, $gender, $photoname) {
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$photo_path = $baseUrl.'/uploads/default/default_medium_male.jpg';
	if($gender == 2){
		$photo_path = $baseUrl.'/uploads/default/default_thumbnail_female.jpg'; 
	}
	if($gender == 'organization'){
		$photo_path = $baseUrl."/uploads/default/medium_group.jpg";
	}
	$hasprofileimage = false;
	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."directory".DIRECTORY_SEPARATOR;
	$real_path = $real_path.$id.DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$photoname;
	if(!isEmptyString($photoname) && file_exists($real_path)){
		$hasprofileimage = true;
	}
	
	if($hasprofileimage){
		$photo_path = $baseUrl.'/uploads/directory/'.$id.'/avatar/medium_'.$photoname;
	}
	return $photo_path;
}
function getImagePath($id, $filename, $gender){
	$hasprofileimage = false;
	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_".$id.DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$filename;
	if(file_exists($real_path) && !isEmptyString($filename)){
		$hasprofileimage = true;
	}
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$photo_path = $baseUrl.'/uploads/default/default_medium_male.jpg';
	if(isFemale($gender)){
		$photo_path = $baseUrl.'/uploads/default/default_thumbnail_female.jpg';
	}
	if($gender == 'organization'){
		$photo_path = $baseUrl."/uploads/default/medium_group.jpg";
	}
	if($hasprofileimage){
		$photo_path = $baseUrl.'/uploads/users/user_'.$id.'/avatar/medium_'.$filename;
	}
	
	return $photo_path;
}
function getItemImagePath($filename, $relpath, $abspath){
	$hasprofileimage = false;
	$real_path = BASE_PATH.DIRECTORY_SEPARATOR.$abspath; //debugMessage($real_path);
	if(file_exists($real_path) && !isEmptyString($filename)){
		$hasprofileimage = true;
	}
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl(); // debugMessage($baseUrl.$relpath);
	$photo_path = $baseUrl.'/uploads/default/default_medium_product.jpg';
	if($hasprofileimage){
		$photo_path = $baseUrl.$relpath;
	}
	
	return $photo_path;
}
function hasLogo($id, $filename){
	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."company".DIRECTORY_SEPARATOR."comp_".$id.DIRECTORY_SEPARATOR."logo".DIRECTORY_SEPARATOR.$filename;
	if(file_exists($real_path) && !isEmptyString($filename)){
		return true;
	}
	
	return false;
}
function getCompanyLogoPath($id, $filename){
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$photo_path = $baseUrl.'/uploads/default/default_logo.png';
	if(hasLogo($id, $filename)){
		$photo_path = $baseUrl.'/uploads/company/comp_'.$id.'/logo/'.$filename;
	}
	
	return $photo_path;
}
# determine if loggedin user is admin
function isAdmin() {
	$session = SessionWrapper::getInstance(); 
	$return = false;
	if($session->getVar('type') == '1'){
		$return = true;
	}
	$type_array = is_array($session->getVar('typeids_array')) ? $session->getVar('typeids_array') : array();
	if(in_array_any(array(1), $type_array)){
		return true;
	}
	return $return;
}
function isDistrictClerk() {
	$session = SessionWrapper::getInstance(); 
	$return = false;
	$type_array = is_array($session->getVar('typeids_array')) ? $session->getVar('typeids_array') : array();
	if(in_array_any(array(2), $type_array)){
		return true;
	}
	return $return;
}
function isContractManager() {
	$session = SessionWrapper::getInstance(); 
	$return = false;
	$type_array = is_array($session->getVar('typeids_array')) ? $session->getVar('typeids_array') : array();
	if(in_array_any(array(6), $type_array)){
		return true;
	}
	return $return;
}
function isDataClerk() {
	$session = SessionWrapper::getInstance(); 
	$return = false;
	$type_array = is_array($session->getVar('typeids_array')) ? $session->getVar('typeids_array') : array();
	if(in_array_any(array(3), $type_array)){
		return true;
	}
	return $return;
}
function isContentAdmin() {
	$session = SessionWrapper::getInstance(); 
	$return = false;
	$type_array = is_array($session->getVar('typeids_array')) ? $session->getVar('typeids_array') : array();
	if(in_array_any(array(5), $type_array)){
		return true;
	}
	return $return;
}
function getCompanyID() {
	// $session = SessionWrapper::getInstance();
	return 1;
}
function getLibDir(){
	//$session = SessionWrapper::getInstance();
	return 'Library';
}
# determine if user is logged in
function isLoggedIn(){
	$session = SessionWrapper::getInstance();
	return isEmptyString($session->getVar('userid')) ? false : true;
}
function getUserID(){
	$session = SessionWrapper::getInstance(); 
	return $session->getVar('userid');
}
function isPublicUser(){
	$session = SessionWrapper::getInstance();
	return isEmptyString($session->getVar('userid')) ? true : false;
}
function png2jpg($originalFile, $outputFile, $quality) {
    $image = imagecreatefrompng($originalFile);
    imagejpeg($image, $outputFile, $quality);
    imagedestroy($image);
}
function ak_img_convert_to_jpg($target, $newcopy, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $ext = strtolower($ext);
    $img = "";
    if ($ext == "gif"){ 
        $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
        $img = imagecreatefrompng($target);
    }
    $tci = imagecreatetruecolor($w_orig, $h_orig);
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w_orig, $h_orig, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 84);
}
/**
 * Determine the Unix timestamp of a given MySql date
 * @param string the MySql date whose timestamp is being determined
 * @return string the timestamp of the MySql date specified
 */
function convertMySqlDateToUnixTimestamp($mysqldate) {
	if ($mysqldate) {
		$parts = explode(' ', $mysqldate);
	}
	$datebits = explode('-', $parts[0]);
	if (3 != count($datebits)) {
		return -1;
	}
	if (isset($parts[1])) {
		$timebits = explode(':', $parts[1]);
		if (3 != count($timebits)) return -1;
		return mktime($timebits[0], $timebits[1], $timebits[2], $datebits[1], $datebits[2], $datebits[0]);
	}
	return mktime (0, 0, 0, $datebits[1], $datebits[2], $datebits[0]);
}
function sort_multi_array ($array, $key, $order="DESC"){
  $keys = array();
  for ($i=1;$i<func_num_args();$i++) {
    $keys[$i-1] = func_get_arg($i);
  }

  // create a custom search function to pass to usort
  $func = function ($a, $b) use ($keys) {
    for ($i=0;$i<count($keys);$i++) {
      if ($a[$keys[$i]] != $b[$keys[$i]]) {
        return ($a[$keys[$i]] < $b[$keys[$i]]) ? -1 : 1;
      }
    }
    return 0;
  };
  usort($array, $func);
  if($order != "DESC"){
  	$result = $array;
  } else {
  	$result = array_reverse($array, true);
  }
  return $result;
}
function num_to_letter($num, $uppercase = FALSE){
	$num -= 1;
	
	$letter = 	chr(($num % 26) + 97);
	$letter .= 	(floor($num/26) > 0) ? str_repeat($letter, floor($num/26)) : '';
	return 		($uppercase ? strtoupper($letter) : $letter); 
}
# return the formatted phone number
function getShortPhone($phone){
	if(isEmptyString($phone)){
		return '';
	}
	return str_pad(ltrim($phone, getDefaultPhoneCode()), 10, '0', STR_PAD_LEFT); 
} 
function getFullPhone($phone){
	if(isEmptyString($phone)){
		return '';
	}
	return str_pad(ltrim($phone, '0'), 12, getDefaultPhoneCode(), STR_PAD_LEFT);
}
function _is_curl_installed() {
    if  (in_array  ('curl', get_loaded_extensions())) {
        return true;
    }
    else{
        return false;
    }
}
function clean_num($num){
	if(isEmptyString($num)){
		return '';		
	}
	return rtrim(rtrim(number_format($num, 2, ".", ""), '0'), '.');
}
// function to covert simple xml to an array
// Convert an xml object in to an array
/**
 * convert xml string to php array - useful to get a serializable value
 *
 * @param string $xmlstr 
 * @return array
 * @author Adrien aka Gaarf
 */
function xmlstr_to_array($xmlstr) {
  $doc = new DOMDocument();
  $doc->loadXML($xmlstr);
  return domnode_to_array($doc->documentElement);
}
function domnode_to_array($node) {
  $output = array();
  switch ($node->nodeType) {
   case XML_CDATA_SECTION_NODE:
   case XML_TEXT_NODE:
    $output = trim($node->textContent);
   break;
   case XML_ELEMENT_NODE:
    for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) { 
     $child = $node->childNodes->item($i);
     $v = domnode_to_array($child);
     if(isset($child->tagName)) {
       $t = $child->tagName;
       if(!isset($output[$t])){
       	// debugMessage($output[$t]);
        $output[$t] = array();
       }
       $output[$t][] = $v;
     }
     elseif($v) {
      $output = (string) $v;
     }
    }
    if(is_array($output)) {
     if($node->attributes->length) {
      $a = array();
      foreach($node->attributes as $attrName => $attrNode) {
       $a[$attrName] = (string) $attrNode->value;
      }
      $output['@attributes'] = $a;
     }
     foreach ($output as $t => $v) {
      if(is_array($v) && count($v)==1 && $t!='@attributes') {
       $output[$t] = $v[0];
      }
     }
    }
   break;
  }
  return $output;
}
# left pad number with zeros
function number_pad($number,$n) {
	return str_pad((int) $number,$n,"0",STR_PAD_LEFT);
}
# determine the number of days between two dates
function dateDiff($start, $end) {
	$start_ts = strtotime($start);
	$end_ts = strtotime($end);
	$diff = $end_ts - $start_ts;
	return round($diff / 86400);

}

function format($str){
	return isEmptyString($str) ? '--' : $str;
}
function getStyleIncludesHor(){
	$files = array(
		# major css to run application
		"assets/css/bootstrap.min.css", 
		"assets/css/hor/core.css", 
		"assets/css/hor/components.css", 
		"assets/css/icons.css",
		"assets/css/hor/pages.css", 
		"assets/css/hor/menu.css", 
		"assets/css/hor/responsive.css",
		"assets/js/plugins/switchery/switchery.min.css"
);
	return $files;
}
function getStyleIncludesVer(){
	$files = array(
		# major css to run application
		"assets/css/bootstrap.min.css", 
		"assets/css/ver/core.css", 
		"assets/css/ver/components.css", 
		"assets/css/icons.css",
		"assets/css/ver/pages.css", 
		"assets/css/ver/menu.css", 
		"assets/css/ver/responsive.css",
		"assets/js/plugins/switchery/switchery.min.css"
	);
	return $files;
}
function getOtherStyleIncludes(){
	$files = array(
		# major css to run application
		"assets/js/plugins/datatables/jquery.dataTables.min.css",
		"assets/js/plugins/datatables/buttons.bootstrap.min.css",
		"assets/js/plugins/datatables/fixedHeader.bootstrap.min.css",
		"assets/js/plugins/datatables/responsive.bootstrap.min.css",
		"assets/js/plugins/datatables/scroller.bootstrap.min.css",
		"assets/js/plugins/datatables/dataTables.colVis.css",
		"assets/js/plugins/datatables/dataTables.bootstrap.min.css",
		"assets/js/plugins/datatables/fixedColumns.dataTables.min.css",
		"assets/js/plugins/jquery.steps/css/jquery.steps.css",
		"assets/js/plugins/select2/css/select2.min.css",
		"assets/js/plugins/custombox/css/custombox.min.css",
		"assets/js/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css",
		"assets/js/plugins/custombox/css/custombox.min.css",
		"assets/js/plugins/custom/chosen/chosen.css",
		"assets/js/plugins/tooltipster/tooltipster.bundle.min.css",
		"assets/js/plugins/tooltipster/tooltipster-sideTip-light.min.css",
		"assets/js/plugins/timepicker/bootstrap-timepicker.min.css",
		"assets/js/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css",
		"assets/js/plugins/custom/multi-select/multiple-select.css",
		"assets/js/plugins/custom/gridstack/gridstack.min.css",
		"assets/js/plugins/custom/gridstack/gridstack-extra.css",
		"assets/js/plugins/custom/pivotjs/pivot.min.css",
		"assets/js/plugins/custom/pivotjs/c3.min.css",
		"assets/js/plugins/custom/pivotjs/handsontable.full.min.css",
		"assets/js/plugins/custom/pivotjs/nrecopivottableext.css",
			
		"assets/js/plugins/jquery-ui/jquery-ui.min.css",
		"assets/css/custom/custom.css",
		"assets/css/custom/app.css"
	);
	return $files;
}
function getJsIncludes(){
	$files = array(
		# major javascript to run application
		"assets/js/jquery.min.js", 
		"assets/js/bootstrap.min.js",
		"assets/js/detect.js", 
		"assets/js/fastclick.js", 
		"assets/js/jquery.blockUI.js", 
		"assets/js/waves.js",
		"assets/js/jquery.slimscroll.js", 
		"assets/js/jquery.scrollTo.min.js",
		"assets/js/plugins/switchery/switchery.min.js"/*,
		"assets/js/jquery.core.js"*/
	);
	return $files;
}
function getJsIncludes2(){
	$files = array(
		"assets/js/plugins/datatables/jquery.dataTables.min.js",
		"assets/js/plugins/datatables/dataTables.bootstrap.js",
		"assets/js/plugins/datatables/dataTables.buttons.min.js",
		"assets/js/plugins/datatables/buttons.bootstrap.min.js",
		"assets/js/plugins/datatables/jszip.min.js",
		"assets/js/plugins/datatables/pdfmake.min.js",
		"assets/js/plugins/datatables/vfs_fonts.js",
		"assets/js/plugins/datatables/buttons.html5.min.js",
		"assets/js/plugins/datatables/buttons.print.min.js",
		"assets/js/plugins/datatables/dataTables.fixedHeader.min.js",
		"assets/js/plugins/datatables/dataTables.keyTable.min.js",
		"assets/js/plugins/datatables/dataTables.responsive.min.js",
		"assets/js/plugins/datatables/responsive.bootstrap.min.js",
		"assets/js/plugins/datatables/dataTables.scroller.min.js",
		"assets/js/plugins/datatables/dataTables.colVis.js",
		"assets/js/plugins/datatables/dataTables.fixedColumns.min.js",
		"assets/pages/jquery.datatables.init.js"
	);
	return $files;
}
function getOtherJsPlugins(){
	$files = array(
		"assets/js/plugins/custom/validation/jquery.validate.min.js", 
		"assets/js/plugins/custom/validation/additional-methods.min.js",
		"assets/js/plugins/custom/bootbox/jquery.bootbox.js",
		"assets/js/plugins/custombox/js/custombox.min.js",
		"assets/js/plugins/custombox/js/legacy.min.js",
		"assets/js/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js",
        "assets/js/plugins/autoNumeric/autoNumeric.js",
		"assets/js/plugins/custom/pGenerator.jquery.js",
		"assets/js/plugins/select2/js/select2.min.js",
		"assets/js/plugins/custom/jquery.elastic.source.js",
		"assets/js/plugins/custom/chosen/chosen.jquery.js",
		"assets/js/plugins/jquery-ui/jquery-ui.min.js",
		"assets/js/plugins/custom/table2CSV.js",
		"assets/js/plugins/custom/highcharts/highcharts.js",
		"assets/js/plugins/custom/highcharts/exporting.js",
		"assets/js/plugins/custom/multiselect.min.js",
		"assets/js/plugins/tooltipster/tooltipster.bundle.min.js",
		"assets/js/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js",
		"assets/js/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js",
		"assets/js/plugins/moment/moment.js",
		"assets/js/plugins/timepicker/bootstrap-timepicker.js",
		"assets/js/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js",
		"assets/js/plugins/custom/multi-select/multiple-select.js",
			
		"assets/js/plugins/custom/gridstack/lodash.min.js",
		"assets/js/plugins/custom/gridstack/gridstack.all.js",
		"assets/js/plugins/custom/gridList/gridList.js",
		"assets/js/plugins/custom/gridList/jquery.gridList.js",
		"assets/js/plugins/custom/gridList/spec/fixtures.js",
		"assets/js/plugins/custom/pivotjs/pivot.min.js",
		"assets/js/plugins/custom/pivotjs/jquery.ui.touch-punch.min.js",
		"assets/js/plugins/custom/pivotjs/handsontable.full.min.js",
		
		"assets/js/plugins/custom/pivotjs/export_renderers.js",
		"assets/js/plugins/custom/pivotjs/d3.min.js",
		"assets/js/plugins/custom/pivotjs/c3.min.js",
		"assets/js/plugins/custom/pivotjs/d3_renderers.min.js",
		"assets/js/plugins/custom/pivotjs/c3_renderers.js",
		"assets/js/plugins/custom/pivotjs/nrecopivottableext.js",
		"assets/js/plugins/custom/pivotjs/subtotal.js",
		"assets/js/plugins/custom/clipboard.min.js",
		
		"assets/js/app.js"
	);
	return $files;
}

function greatUser($name){
	$b = time(); 
	
	$hour = date("g",$b);
	$m = date ("A", $b);
	
	$txt = '';
	if ($m == "AM"){
		if ($hour == 12){
			$txt = "Good Evening";
		} else if ($hour < 4){
	 		$txt = "Good Evening";
	 	} elseif ($hour > 3){
			$txt = "Good Morning";
	 	}
	 } elseif ($m == "PM") {
	 	if ($hour == 12){
	 		$txt = "Good Afternoon";
	 	} elseif ($hour < 7){
	 		$txt = "Good Afternoon";
	 	} elseif ($hour > 6) {
	 		$txt = "Good Evening";
	 	}
	 }
	 return $txt.', '.$name; 
}

function getAgent(){
	$tablet_browser = 0;
	$mobile_browser = 0;
	 
	if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
	    $tablet_browser++;
	}
	 
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
	    $mobile_browser++;
	}
	 
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
	    $mobile_browser++;
	}
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
	    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
	    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
	    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
	    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
	    'newt','noki','palm','pana','pant','phil','play','port','prox',
	    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
	    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
	    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
	    'wapr','webc','winw','winw','xda ','xda-');
	 
	if (in_array($mobile_ua,$mobile_agents)) {
	    $mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
	    $mobile_browser++;
	    //Check for tablets on opera mini alternative headers
	    $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
	    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
	      $tablet_browser++;
	    }
	}
	 
	if ($tablet_browser > 0) {
	   // do something for tablet devices
	   return 'tablet';
	}
	else if ($mobile_browser > 0) {
	   // do something for mobile devices
	   return 'mobile';
	}
	else {
	   // do something for everything else
	   return 'desktop';
	}
}
function retrieve_remote_file_size($url){
     $ch = curl_init($url);

     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
     curl_setopt($ch, CURLOPT_HEADER, TRUE);
     curl_setopt($ch, CURLOPT_NOBODY, TRUE);

     $data = curl_exec($ch);
     $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

     curl_close($ch);
     return $size;
}
/** 
* Counts the number occurrences of a certain day of the week between a start and end date
* The $start and $end variables must be in UTC format or you will get the wrong number 
* of days  when crossing daylight savings time
* @param - $day - the day of the week such as "Monday", "Tuesday"...
* @param - $start - a UTC timestamp representing the start date
* @param - $end - a UTC timestamp representing the end date
* @return Number of occurences of $day between $start and $end
*/
function countDays($day, $start, $end)
{        
    //get the day of the week for start and end dates (0-6)
    $w = array(date('w', $start), date('w', $end));

    //get partial week day count
    if ($w[0] < $w[1])
    {            
        $partialWeekCount = ($day >= $w[0] && $day <= $w[1]);
    }else if ($w[0] == $w[1])
    {
        $partialWeekCount = $w[0] == $day;
    }else
    {
        $partialWeekCount = ($day >= $w[0] || $day <= $w[1]);
    }

    //first count the number of complete weeks, then add 1 if $day falls in a partial week.
    return floor( ( $end-$start )/60/60/24/7) + $partialWeekCount;
}
function stringContains($substr, $str){
	if(strpos($str, $substr) !== false) {
		return true;
	}
	return false;
}
function curlContents($url, $method = 'GET', $data = false, $headers = false, $returnInfo = false) {    
    $ch = curl_init();
    
    if($method == 'POST') {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        if($data !== false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
    } else {
        if($data !== false) {
            if(is_array($data)) {
                $dataTokens = array();
                foreach($data as $key => $value) {
                    array_push($dataTokens, urlencode($key).'='.urlencode($value));
                }
                $data = implode('&', $dataTokens);
            }
            curl_setopt($ch, CURLOPT_URL, $url.'?'.$data);
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }
    }
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    if($headers !== false) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    $contents = curl_exec($ch);
    
    if($returnInfo) {
        $info = curl_getinfo($ch);
    }
	// debugMessage(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL));
    curl_close($ch);

    if($returnInfo) {
        return array('contents' => $contents, 'info' => $info);
    } else {
    	// debugMessage($contents);
        return $contents;
    }
}
function getClientUrl (Zend_Http_Client $client){ 
	$string = '';
    try {
        $c = clone $client;
        /*
         * Assume there is nothing on 80 port.
         */
        $c->setUri ('http://127.0.0.1');

        $c->getAdapter ()
            ->setConfig (array (
            'timeout' => 0
        ));

        $c->request ();
    } catch (Exception $e){
        $string = $c->getLastRequest ();
        $string = substr ($string, 4, strpos ($string, "HTTP/1.1\r\n") - 5);
    }
    return $client->getUri (true) . $string;
}
/**
 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
 * array containing the HTTP server response header fields and content.
 */
function getPageHtml( $url )
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
function get_web_page($url) {
	$options = array (CURLOPT_RETURNTRANSFER => true, // return web page
			CURLOPT_HEADER => false, // don't return headers
			CURLOPT_FOLLOWLOCATION => false, // follow redirects
			//CURLOPT_ENCODING => "", // handle compressed
			//CURLOPT_USERAGENT => "test", // who am i
			//CURLOPT_AUTOREFERER => true, // set referer on redirect
			//CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			//CURLOPT_TIMEOUT => 120, // timeout on response
			//CURLOPT_MAXREDIRS => 10 
			); // stop after 10 redirects

	$ch = curl_init ( $url );
	curl_setopt_array ( $ch, $options );
	$content = curl_exec ( $ch );
	$err = curl_errno ( $ch );
	$errmsg = curl_error ( $ch );
	$header = curl_getinfo ( $ch );
	$httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

	curl_close ( $ch );

	$header ['errno'] = $err;
	$header ['errmsg'] = $errmsg;
	$header ['content'] = $content;
	return $header;
}
function fileUploaded() {
	if(empty($_FILES)) {
		return false;
	}
	return true;
}
function getGoogleMapsUrl(){
	$config = Zend_Registry::get("config"); 
	// $value = $config->api->google_disablemaps; 
	// $value = $config->api->google_disablemaps;
	return "https://maps.google.com/maps/api/js?sensor=true&key=".getGoogleMapsKey();
}
function getGoogleMapsKey(){
	$config = Zend_Registry::get("config"); 
	// $value = $config->api->google_mapsapikey; 
	$value = "AIzaSyAjkTHnLzEkyF1ntVoUkZthaZWV4VN5DJE";
	return $value;
}
function loadMaps(){
	$config = Zend_Registry::get("config"); 
	// $value = $config->api->google_disablemaps; 
	$value = 1;
	return $value == 1 || $value == 'on' || $value == 'yes' || $value == 'true' ? true : false;
}
# format url to strip leading forward slash
function stripURL($url){
	return rtrim(rtrim($url,"/"), "\\");
}
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
			* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}
function array_deep_diff($d1, $d2) {
	if (is_array($d1) && is_array($d2))  {
		$diff = array();
		foreach (array_unique(array_merge(array_keys($d1), array_keys($d2))) as $key) {
			if (!array_key_exists($key, $d1)) {
				$diff['added'][$key] = $d2[$key];
			} else if (!array_key_exists($key, $d2)) {
				$diff['removed'][$key] = $d1[$key];
			} else {
				$tmp = array_deep_diff($d1[$key], $d2[$key]);
				if (!empty($tmp)) $diff[$key] = $tmp;
			}
		}
		return $diff;

	} else if (!is_array($d1) && !is_array($d2))  {
		if ($d1 == $d2) return array();
		$ret = $d1.'->'.$d2;
		// just a little touch for numerics, might not be needed
		/* if (is_numeric($d1) && is_numeric($d2)) {
			if ($d1 > $d2) $ret .= ' [- ' . ($d1 - $d2) . ']';
			if ($d1 < $d2) $ret .= ' [+ ' . ($d2 - $d1) . ']';
		} */
		return $ret;
	} else {
		return array('Array compared with NonArray');
	}
}
function getTranslations(){
	$translate = Zend_Registry::get("translate");
	
	$labels = array(
		// globals
		'name' => $translate->translate('global_name_label'), 
		// system labels
		'lookupvaluedescription' => 'Value', 
		// member labels
		'type' => $translate->translate('profile_type_label'),
		'firstname' => $translate->translate('profile_firstname_label'),
		'lastname' => $translate->translate('profile_lastname_label'),
		'othername' => $translate->translate('profile_othernames_label'),
		'displayname' => $translate->translate('profile_displayname_label'),
		'salutation' => $translate->translate('profile_suffix_label'),
		'country' => $translate->translate('global_country_label'),
		'nationality' => $translate->translate('profile_nationality_label'),
		'address1' => $translate->translate('global_address_label'),
		'address2' => '',
		'postalcode' => '',
		'gpslat' => $translate->translate('global_gpslat_label'),
		'gpslng' => $translate->translate('global_gpslng_label'),
		'email' => $translate->translate('profile_emailonly_label'),
		'email2' => $translate->translate('profile_altemail_label'),
		'phone' => $translate->translate('profile_phone_label'),
		'phone2' => $translate->translate('profile_altphone_label'),
		'phone_isactivated' => '',
		'phone_actkey' => '',
		'username' => $translate->translate('profile_username_label'),
		'password' => $translate->translate('profile_password_label'),
		'trx' => '',
		'status' => $translate->translate('profile_status_label'),
		'activationkey' => $translate->translate('profile_actkey_label'),
		'activationdate' => $translate->translate('profile_dateactivated_label'),
		'agreedtoterms' => '',
		'securityquestion' => '',
		'securityanswer' => '',
		'isinvited' => '',
		'invitedbyid' => $translate->translate('Invited By'),
		'hasacceptedinvite' => '',
		'dateinvited' => $translate->translate('Date Invited'),
		'bio' => $translate->translate('profile_bio_label'),
		'gender' => $translate->translate('profile_gender_label'),
		'dateofbirth' => $translate->translate('profile_dateofbirth_label'),
		'profilephoto' => $translate->translate('Profile Photo'),
		'maritalstatus' => $translate->translate('profile_maritalstatus_label'),
		'contactname' => $translate->translate('profile_nextkin_name_label'),
		'contactphone' => $translate->translate('profile_nextkin_phone_label'),
		'contactrshp' => $translate->translate('profile_nextkin_rship_label'),
		'profession' => $translate->translate('profile_profession_label'),
		'createdby' => $translate->translate('global_addedby_label'),
		'datecreated' => $translate->translate('global_dateadded_label'),
		'lastupdatedby' => $translate->translate('global_updatedby_label'),
		'lastupdatedate' => $translate->translate('global_updatedate_label'),
		
	);
	
	return $labels;
}
function getStartAndEndDate($week, $year) {
	$dto = new DateTime();
	$dto->setISODate($year, $week);
	$ret['week_start'] = $dto->format('Y-m-d');
	$dto->modify('+6 days');
	$ret['week_end'] = $dto->format('Y-m-d');
	return $ret;
}
# relative path to file
function hasAttachment($filename, $userid){
	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_".$userid.DIRECTORY_SEPARATOR."benefits".DIRECTORY_SEPARATOR.$filename;
	if(file_exists($real_path) && !isEmptyString($filename)){
		return true;
	}
	return false;
}
function hasAttachment2($filename){
	$alt_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR.$filename;
	if(file_exists($alt_path) && !isEmptyString($filename)){
		return true;
	}
	return false;
}
# determine path to attachment
function getFilePath($filename, $userid) {
	$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	$path = '';
	if(hasAttachment($filename, $userid)){
		$path = $baseUrl.'/uploads/users/user_'.$userid.'/benefits/'.$filename;
	} else {
		if(hasAttachment2($filename)){
			$path = $baseUrl.'/uploads/temp/'.$filename;
		}
		if(!hasAttachment2($filename) && !isEmptyString($filename)){
			// $path = $baseUrl.'/uploads/temp/'.$filename;
		}
	}
	return $path;
}
/*
 * ip_in_range.php - Function to determine if an IP is located in a
 *                   specific range as specified via several alternative
 *                   formats.
 *
 * Network ranges can be specified as:
 * 1. Wildcard format:     1.2.3.*
 * 2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
 * 3. Start-End IP format: 1.2.3.0-1.2.3.255
 *
 * Return value BOOLEAN : ip_in_range($ip, $range);
 *
 * Copyright 2008: Paul Gregg <pgregg@pgregg.com>
 * 10 January 2008
 * Version: 1.2
 *
 * Source website: http://www.pgregg.com/projects/php/ip_in_range/
 * Version 1.2
 *
 * This software is Donationware - if you feel you have benefited from
 * the use of this tool then please consider a donation. The value of
 * which is entirely left up to your discretion.
 * http://www.pgregg.com/donate/
 *
 * Please do not remove this header, or source attibution from this file.
 */


// decbin32
// In order to simplify working with IP addresses (in binary) and their
// netmasks, it is easier to ensure that the binary strings are padded
// with zeros out to 32 characters - IP addresses are 32 bit numbers
function decbin32 ($dec) {
  return str_pad(decbin($dec), 32, '0', STR_PAD_LEFT);
}

// ip_in_range
// This function takes 2 arguments, an IP address and a "range" in several
// different formats.
// Network ranges can be specified as:
// 1. Wildcard format:     1.2.3.*
// 2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
// 3. Start-End IP format: 1.2.3.0-1.2.3.255
// The function will return true if the supplied IP is within the range.
// Note little validation is done on the range inputs - it expects you to
// use one of the above 3 formats.
function ip_in_range($ip, $range) {
  if (strpos($range, '/') !== false) {
    // $range is in IP/NETMASK format
    list($range, $netmask) = explode('/', $range, 2);
    if (strpos($netmask, '.') !== false) {
      // $netmask is a 255.255.0.0 format
      $netmask = str_replace('*', '0', $netmask);
      $netmask_dec = ip2long($netmask);
      return ( (ip2long($ip) & $netmask_dec) == (ip2long($range) & $netmask_dec) );
    } else {
      // $netmask is a CIDR size block
      // fix the range argument
      $x = explode('.', $range);
      while(count($x)<4) $x[] = '0';
      list($a,$b,$c,$d) = $x;
      $range = sprintf("%u.%u.%u.%u", empty($a)?'0':$a, empty($b)?'0':$b,empty($c)?'0':$c,empty($d)?'0':$d);
      $range_dec = ip2long($range);
      $ip_dec = ip2long($ip);

      # Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
      #$netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));

      # Strategy 2 - Use math to create it
      $wildcard_dec = pow(2, (32-$netmask)) - 1;
      $netmask_dec = ~ $wildcard_dec;

      return (($ip_dec & $netmask_dec) == ($range_dec & $netmask_dec));
    }
  } else {
    // range might be 255.255.*.* or 1.2.3.0-1.2.3.255
    if (strpos($range, '*') !==false) { // a.b.*.* format
      // Just convert to A-B format by setting * to 0 for A and 255 for B
      $lower = str_replace('*', '0', $range);
      $upper = str_replace('*', '255', $range);
      $range = "$lower-$upper";
    }

    if (strpos($range, '-')!==false) { // A-B format
      list($lower, $upper) = explode('-', $range, 2);
      $lower_dec = (float)sprintf("%u",ip2long($lower));
      $upper_dec = (float)sprintf("%u",ip2long($upper));
      $ip_dec = (float)sprintf("%u",ip2long($ip));
      return ( ($ip_dec>=$lower_dec) && ($ip_dec<=$upper_dec) );
    }

    echo 'Range argument is not in 1.2.3.4/24 or 1.2.3.4/255.255.255.0 format';
    return false;
  }
}
/**
 * Generate a 10 digit activation key  
 * 
 * @return String An activation key
 */
function generateActivationKey() {
	return substr(md5(uniqid(mt_rand(), 1)), 0, 6);
}
function GetDirectorySize($path){
	$bytestotal = 0;
	$path = realpath($path);
	if($path!==false){
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
			$bytestotal += $object->getSize();
		}
	}
	return $bytestotal;
}
function foldersize($path) {
	$total_size = 0;
	$files = scandir($path);
	$cleanPath = rtrim($path, '/'). '/';

	foreach($files as $t) {
		if ($t<>"." && $t<>"..") {
			$currentFile = $cleanPath . $t;
			if (is_dir($currentFile)) {
				$size = foldersize($currentFile);
				$total_size += $size;
			}
			else {
				$size = filesize($currentFile);
				$total_size += $size;
			}
		}
	}
	return $total_size;
}
function getFolderSize($dir){
	$size = 0;
	if(is_dir($dir)){
		$files  = scandir($dir);
		foreach($files as $file) {
			if($file != '.' && $file != '..'){
				if(filetype($dir.DIRECTORY_SEPARATOR.$file) == 'dir') {
					$size += getFolderSize($dir.DIRECTORY_SEPARATOR.$file);
				} else {
					$size += filesize($dir.DIRECTORY_SEPARATOR.$file);
				}
			}
		}
	}
	return $size;
}

function format_size($size) {
	global $units;

	$mod = 1024;
	for ($i = 0; $size > $mod; $i++) {
		$size /= $mod;
	}
	$endIndex = strpos($size, ".")+3;

	return substr( round($size), 0, $endIndex).' '.$units[$i];
}
function human_filesize($bytes, $decimals = 0) {
	$size = array('Bytes','KB','MB','GB','TB','PB','EB','ZB','YB');
	$factor = floor((strlen($bytes) - 1) / 3);
	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .' '.@$size[$factor];
}
function rmdirr($dirname){
	// Sanity check
	if (!file_exists($dirname)) {
		return false;
	}

	// Simple delete for a file
	if (is_file($dirname)) {
		return unlink($dirname);
	}

	// Loop through the folder
	$dir = dir($dirname);
	while (false !== $entry = $dir->read()) {
		// Skip pointers
		if ($entry == '.' || $entry == '..') {
			continue;
		}

		// Recurse
		rmdirr("$dirname/$entry");
	}

	// Clean up
	$dir->close();
	return rmdir($dirname);
}
function is_dir_empty($dir) {
	if (!is_readable($dir)) {
		return NULL;
	}
	$handle = opendir($dir);
	while (false !== ($entry = readdir($handle))) {
		if ($entry != "." && $entry != "..") {
			return FALSE;
		}
	}
	return TRUE;
}
function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (is_dir($dir."/".$object)){
					rrmdir($dir."/".$object);
				}
			} else {
				unlink($dir."/".$object);
			}
		}
		rmdir($dir);
	}
}
function rrmdir2($dir, $ignorearray=array()) {
	if (is_dir($dir)) {
		$files = scandir($dir);

		foreach ($files as $file){
			if ($file != "." && $file != "..") {
				if(count($ignorearray) == 0){
					rrmdir2($file);
				} else {
					if(!in_array($dir.DIRECTORY_SEPARATOR.$file, $ignorearray)){
						debugMessage('deleting '.$dir.DIRECTORY_SEPARATOR.$file);
						rrmdir2($dir.DIRECTORY_SEPARATOR.$file, $ignorearray);
					} else {
						debugMessage('ignored '.$file);
					}
				}
			}
		}
		if(count($ignorearray) == 0){
			rmdir($dir);
		} else {
			if(!in_array($dir, $ignorearray)){
				rmdir($dir);
			} else {
				debugMessage('ignored '.$dir);
			}
		}

	} else if (file_exists($dir)){
		unlink($dir);
	}
}
function full_copy( $source, $target ){
	if ( is_dir( $source )) {
		mkdir( $target, 0777 );
		$d = dir( $source );

		while( FALSE !== ( $entry = $d->read())){
			if ( $entry == '.' || $entry == '..' )
			{
				continue;
			}

			$Entry = $source . '/' . $entry;
			if ( is_dir( $Entry ) )
			{
				full_copy( $Entry, $target . '/' . $entry );
				continue;
			}
			copy( $Entry, $target . '/' . $entry );
		}
		$d->close();

	} else {
		copy( $source, $target );
	}
}
/**
 * Simple PHP age Calculator
 *
 * Calculate and returns age based on the date provided by the user.
 * @param   date of birth('Format:yyyy-mm-dd').
 * @return  age based on date of birth
 */
function ageCalculator($dob){
	if(!empty($dob)){
		$birthdate = new DateTime($dob);
		$today   = new DateTime('today');
		$age = $birthdate->diff($today)->y;
		return $age;
	}else{
		return 0;
	}
}
function isConnected(){
	$connected = @fsockopen("www.google.com", 80);
	//website, port  (try 80 or 443)
	if ($connected){
		$is_conn = true; //action when connected
		fclose($connected);
	}else{
		$is_conn = false; //action in connection failure
	}
	return $is_conn;
}
/**
 * Same as implode, but escape the separator when it exists in the array elements being imploded
* If the separator is longer than 1 character, only the first character need be escaped (not every character of the delimiter).
*
* @param	string	$sep	delimiter to stich the array together with
* @param	array	$arr	array to combine
* @param	char	$escape	escape character to escape any found delimiters with
* @return	imploded string
*
* @author	Sam Pospischil <pospi@spadgos.com>
*/
function escapedimplode($sep, $arr, $escape = '\\') {
	if (!is_array($arr)) {
		return false;
	}
	foreach ($arr as $k => $v) {
		$arr[$k] = str_replace($sep, $escape . $sep, $v);
	}
	return implode($sep, $arr);
}

/**
 * same as explode, but don't treat delimiters preceded by an escaped character as delimiters, and convert them in the output
 * If the separator is longer than 1 character, only the first character need be escaped (not every character of the delimiter).
 *
 * @param	string	$sep	boundary delimiter to split the input string on
 * @param	string	$str	string to explode
 * @param	char	$escape	escape character used to escape delimiters in the string
 * @return	exploded array, with delimiter escape characters removed
 *
 * @author	Sam Pospischil <pospi@spadgos.com>
 */
function escapedexplode($sep, $str, $escape = '\\') {
	$astr = str_split($str);

	$result = array();			// final result array
	$buffer = '';				// buffer for current array item match
	$matchedBuffer = '';		// buffer for storing matched delimiters in case of a mismatch part-way
	$wasEsc = false;			// flag to ignore the current character if the previous one was an escape char
	$sepLen = strlen($sep);		// precomputed length of separator string, for efficiency
	$matchNext = 0;				// character index within separator to attempt to match next

	foreach ($astr as $char) {
		if ($matchNext >= $sepLen) {			// full separator has been matched, store and continue
			$result[] = str_replace($escape . $sep, $sep, $buffer);
			$buffer = '';
			$matchedBuffer = '';
			$matchNext = 0;
		}

		$nextChar = substr($sep, $matchNext, 1);	// get next character to match

		if ($char == $nextChar && !$wasEsc) {	// partial separator match, keep matching
			$matchNext++;
			$matchedBuffer .= $char;
		} else if ($matchNext > 0) {			// matched partially but not completely, add matched chars to buffer
			$buffer .= $matchedBuffer;
			$matchNext = 0;
		} else {
			$buffer .= $char;					// this isnt a separator, add to buffer
		}

		if ($char == $escape && $matchNext == 0) {	// detect escape characters and flag that the next character *cannot* match
			$wasEsc = true;
			continue;
		}
		$wasEsc = false;						// otherwise reset the escape character flag
	}

	// add any remaining buffered chars as the last element
	if ($buffer || $matchedBuffer) {
		$result[] = str_replace($escape . $sep, $sep, $buffer . $matchedBuffer);
	}

	return $result;
}
/**
 * Convert a multi-dimensional, associative array to CSV data
 * @param  array $data the array of data
 * @return string       CSV text
 */
function str_putcsv($data) {
	# Generate CSV data from array
	$fh = fopen('php://temp', 'rw'); # don't create a file, attempt
	# to use memory instead

	# write out the headers
	fputcsv($fh, array_keys(current($data)));

	# write out the data
	foreach ( $data as $row ) {
		fputcsv($fh, $row);
	}
	rewind($fh);
	$csv = stream_get_contents($fh);
	fclose($fh);

	return $csv;
}
function parse_csv ($csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = true){
	$enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string);
	$enc = preg_replace_callback(
			'/"(.*?)"/s',
			function ($field) {
		return urlencode(utf8_encode($field[1]));
	},
	$enc
	);
	$lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $enc);
	return array_map(
			function ($line) use ($delimiter, $trim_fields) {
		$fields = $trim_fields ? array_map('trim', explode($delimiter, $line)) : explode($delimiter, $line);
		return array_map(
				function ($field) {
			return str_replace('!!Q!!', '"', utf8_decode(urldecode($field)));
		},
		$fields
		);
	},
	$lines
	);
}
function build_table($array){
	// start table
	$html = '<table style="width: 100%; border-collapse: collapse; padding: 5px; margin: 0px; font-family: Arial; font-size: 12px;">';
	// header row
	$html .= '<tr>';
	foreach($array[0] as $key=>$value){
		$html .= '<th style="max-width:250px; border: solid 1px #666666; text-align: left; padding: 5px;">' . htmlspecialchars($value) . '</th>';
	}
	$html .= '</tr>';

	// data rows
	foreach( $array as $key=>$value){
		if($key > 0){
		$html .= '<tr>';
		foreach($value as $key2=>$value2){
			$html .= '<td style="min-width:100px; border: solid 1px #666666; text-align: left; padding: 5px;">' . htmlspecialchars($value2) . '</td>';
		}
		$html .= '</tr>';
		}
	}

	// finish table and return it

	$html .= '</table>';
	return $html;
}
function getRelativePath($id, $type, $base =''){
	$breadcrumb = '/'.getLibDir().'/';
	$ftype = new File(); $isfile = false;
	$dtype = new Folder(); $isdir = false;
	if($type == 'file'){
		$ftype->populate($id);
		$ishome = false;
		if($ftype->isInHome()){
			$ishome = true;
		}
		if($ishome){
			return $breadcrumb;
		} else {
			$pathstr = stripUrl($ftype->getFolder()->getPath());
			if($ftype->isTrashed()){
				$pathstr = stripUrl($ftype->getRestore()->getPath());
			}
			$pathidsarr = array_remove_empty(explode('/', $pathstr));
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				// $breadcrumb .= $fold->getName().'/ ';
				if(!$fold->isHome()){
					$breadcrumb .= $fold->getName().'/';
				}
			}
			if($ftype->isTrashed() && !$ftype->getRestore()->isHome()){
				$breadcrumb .= $ftype->getRestore()->getName().'/';
			}
			return $breadcrumb;
		}
	}
	if($type == 'dir'){
		$dtype->populate($id);
		$ishome = false;
		if($dtype->isInHome()){
			$ishome = true;
		}
		if($ishome){
			return $breadcrumb;
		} else {
			$pathstr = stripUrl($dtype->getPath()); // debugMessage($pathstr);
			if($dtype->isTrashed()){
				$pathstr = stripUrl($dtype->getRestore()->getPath());
			}
				
			$pathidsarr = array_remove_empty(explode('/', $pathstr));
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				if(!$fold->isHome()){
					$breadcrumb .= $fold->getName().'/';
				}

			}
			if($dtype->isTrashed() && !$dtype->getRestore()->isHome()){
				$breadcrumb .= $dtype->getRestore()->getName().'/';
			}
			return $breadcrumb;
		}
	}
}
function getAbsolutePath($id, $type, $base =''){
	if(isEmptyString($base)){
		$base = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
	}
	$path = stripUrl($base).DIRECTORY_SEPARATOR;
	$ftype = new File(); $isfile = false;
	$dtype = new Folder(); $isdir = false;
	if($type == 'file'){
		$ftype->populate($id);
		$ishome = false;
		if($ftype->isInHome()){
			$ishome = true;
		}
		if($ishome){
			return $path.$ftype->getFolder()->getName().DIRECTORY_SEPARATOR.$ftype->getName();
		} else {
			$pathstr = stripUrl($ftype->getFolder()->getPath());
			$pathidsarr = array_remove_empty(explode('/', $pathstr));
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				// $breadcrumb .= $fold->getName().'/ ';
				if(!$fold->isHome()){
					$path .= $fold->getName().DIRECTORY_SEPARATOR;
				}
			}
			return $path.$ftype->getFolder()->getName().DIRECTORY_SEPARATOR.$ftype->getName();
		}
	}
	if($type == 'dir'){
		$dtype->populate($id);
		$ishome = false;
		if($dtype->isInHome()){
			$ishome = true;
		}
		if($ishome){
			$home = $dtype->getHomeFolder();
			$path = $base;
			if(isEmptyString($path)){
				$path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR.$home->getName();
			}
			return stripUrl($path).DIRECTORY_SEPARATOR.$dtype->getName();
		} else {
			$pathstr = stripUrl($dtype->getParent()->getPath());
			$pathidsarr = array_remove_empty(explode('/', $pathstr));
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				if(!$fold->isHome()){
					$path .= $fold->getName().DIRECTORY_SEPARATOR;
				}
			}
			// debugMessage($path);
			return $path.$dtype->getParent()->getName().DIRECTORY_SEPARATOR.$dtype->getName();
		}
	}
}
//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a'){
	$datetime1 = date_create($date_1);
	$datetime2 = date_create($date_2);

	$interval = date_diff($datetime1, $datetime2);
	return $interval->format($differenceFormat);
}
function getSeasonForDate($date = ''){
	if(isEmptyString($date)){
		$date = date('Y-m-d');
	}
	$month = abs(date('m', strtotime($date)));
	$season = 2;
	if($month >= 7 && $month <= 12){
		$season = 1;
	}	
	return $season;
}
function isMobile() {
	return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
function rearrange( $arr ){
	foreach( $arr as $key => $all ){
		foreach( $all as $i => $val ){
			$new[$i][$key] = $val;
		}
	}
	return $new;
}
function reArrayFiles(&$file_post) {

	$file_ary = array();
	$file_count = count($file_post['name']);
	$file_keys = array_keys($file_post);

	for ($i=0; $i<$file_count; $i++) {
		foreach ($file_keys as $key) { // debugMessage($key);
			//if(!isArrayKeyAnEmptyString('filename', $file_post[$key][$i])){
				$file_ary[$i][$key] = $file_post[$key][$i]['filename'];
			// }
		}
	}
	foreach ($file_ary as $key => $value) {
		if(isEmptyString($value['name'])){
			unset($file_ary[$key]);
		} else {
			$file_ary[$key]['ext'] = findExtension($value['name']);
		}
	}

	return $file_ary;
}
function calcutateAge($dob){
	$dob = date("Y-m-d",strtotime($dob));

	$dobObject = new DateTime($dob);
	$nowObject = new DateTime();

	$diff = $dobObject->diff($nowObject);

	return $diff->y;

}
function in_array_all($needles, $haystack) {
   return !array_diff($needles, $haystack);
}
function in_array_any($needles, $haystack) {
   return !!array_intersect($needles, $haystack);
}
function make_list($arr){
	$return = '<ul>';
	foreach ($arr as $item) {
		$return .= '<li id="'.$item['id'].'">'.(!isArrayKeyAnEmptyString('children', $item) ? $item['title'].' '.make_list($item['children']) : '<a class="gonowhere">'.$item['title'].'</a>').'</li>';
	}
	
	$return .= '</ul>';
	return $return;
}
function trimArray($array){
	$trimmed_array = array_map('trim',$array);
	return $trimmed_array;
}
function stripSpaces($str){
	return str_replace(' ', '', trim($str));
}
function stripMultipleSpaces($str){
	return preg_replace('/\s+/', ' ', trim($str));
}
function stripArraySpaces($item, $key){
	return preg_replace('/\s+/', ' ', trim($item));
}
function stripSpacesFromArray($array){
	// array_walk_recursive($array, 'stripArraySpaces');
	return $array;
}
function sortArrayByArray(array $array, array $orderArray) {
	$ordered = array();
	foreach ($orderArray as $key) {
		if (array_key_exists($key, $array)) {
			$ordered[$key] = $array[$key];
			unset($array[$key]);
		}
	}
	return $ordered + $array;
}
function commaStringToArray($str = ''){
	if(isEmptyString($str)){
		return array();
	}
	return array_remove_empty(explode(',', stripSpaces($str)));
}
function arrayToCommaString($arr = ''){
	if(!is_array($arr) || count($arr) == 0){
		return '';
	}
	return implode(',', $arr);
}
function getCommaListFromValues($currentvalue, $allvalues){
	$units = ''; $units_array = array();
	if(!isEmptyString($currentvalue)){
		$content_array = commaStringToArray($currentvalue); // debugMessage($units_array);
		foreach($content_array as $unitid){
			if(!isArrayKeyAnEmptyString($unitid, $allvalues)){
				$units_array[] = $allvalues[$unitid];
			}
		}
		$units = createHTMLCommaListFromArray($units_array, ', ');
	}
	return $units;
}
function getArrayOfListValues($currentvalue, $allvalues){
	$units = ''; $units_array = array();
	if(!isEmptyString($currentvalue)){
		$content_array = commaStringToArray($currentvalue); // debugMessage($units_array);
		foreach($content_array as $unitid){
			if(!isArrayKeyAnEmptyString($unitid, $allvalues)){
				$units_array[$unitid] = $allvalues[$unitid];
			}
		}
		$units = $units_array;
	}
	return $units;
}
function getCurrentValueFromArray($currentvalue, $allvalues){
	$name = '';
	if(!isArrayKeyAnEmptyString($currentvalue, $allvalues)){
		$name = $allvalues[$currentvalue];
	}
	return $name;
}
/**
 * Inserts any number of scalars or arrays at the point
 * in the haystack immediately after the search key ($needle) was found,
 * or at the end if the needle is not found or not supplied.
 * Modifies $haystack in place.
 * @param array &$haystack the associative array to search. This will be modified by the function
 * @param string $needle the key to search for
 * @param mixed $stuff one or more arrays or scalars to be inserted into $haystack
 * @return int the index at which $needle was found
 */
function array_insert_after(&$haystack, $needle = '', $stuff){
	if (! is_array($haystack) ) return $haystack;

	$new_array = array();
	for ($i = 2; $i < func_num_args(); ++$i){
		$arg = func_get_arg($i);
		if (is_array($arg)) $new_array = array_merge($new_array, $arg);
		else $new_array[] = $arg;
	}

	$i = 0;
	foreach($haystack as $key => $value){
		++$i;
		if ($key == $needle) break;
	}

	$haystack = array_merge(array_slice($haystack, 0, $i, true), $new_array, array_slice($haystack, $i, null, true));

	return $i;
}
function getValueFromString($url, $parameter_name){
	$parts = parse_url($url); // debugMessage($parts);
	if(isset($parts['query'])){
		parse_str($parts['query'], $query);
		if(isset($query[$parameter_name])){
			return $query[$parameter_name];
		} else {
			return null;
		}
	} else {
		return null;
	}
}
?>
