<?php
	include APPLICATION_PATH.'/includes/header.php';
	
	$title = "Dashboard";
	
	$description = 'Welcome to '.getAppName();
	$pageid = "dashboard_index";
	$conn = Doctrine_Manager::connection();
	
	$initconfig  = '{}'; 
	$session = SessionWrapper::getInstance();
	if(!isEmptyString($session->getVar('serialconfig'))){
		$initconfig = $session->getVar('serialconfig');
	}
	$urlconfig = '{}';
	if(!isEmptyString($session->getVar('urlconfig'))){
		$urlconfig = $session->getVar('urlconfig');
	}
	
	$dashwidgets_query  = "select * from user_dashboard d where d.userid = '".getUserID()."' OR FIND_IN_SET(".getUserID().", d.shareids) > 0 ";
	$dashwidgets = $conn->fetchAll($dashwidgets_query);
	
	$this->headTitle($title.$browserappend);
?>
<!--<script type="text/javascript" src="/assets/js/plugins/custom/flexmonster/flexmonster.js"></script>-->
<script>
$(document).ready(function() {
	$('.page-title').html('<?php echo $title; ?>');
	$('.titlebreadcrumb').html('<?php echo $homedir.$title; ?>');
});
</script>

<?php require_once APPLICATION_PATH.'/views/scripts/index/messages.phtml'; ?>
<div class="row">
 	<div class="xcol-sm-12" style="min-width:1000px; max-width:2500px;">
       	<form class="form-horizontal" action="<?php echo $this->baseUrl("dashboard/savestate"); ?>" method="get" id="indexform">
            <div class="panel">
                <input type="hidden" name="id" id="id" value="<?php echo getUserID(); ?>" />
                <textarea id="dashbordconfig" name="dashbordconfig" class="form-control expanding hide" style="min-height:60px;"></textarea>
                <div class="panel-body minheight400" style="padding:10px 0;">
                    <div class="grid-stack" id="grid" data-gs-width="12">
                        <?php if(count($dashwidgets) == 0){ ?>
                            <div class="grid-stack-item" data-gs-auto-position="true" data-gs-min-width="6" data-gs-min-height="5">
                                <div class="grid-stack-item-content">
                                    <span class="blocked maxwidth centeralign" style="margin-top: 20%;">No content available</span>
                                    <div class="divider10"></div>
                                    <span style="display: block; text-align: center;"><a href="<?php echo $this->baseUrl('report/results/addwidgets/1'); ?>" class="btn btn-xs btn-default hide">Add Widget</a></span>
                                </div>
                            </div>
                            <div class="grid-stack-item" data-gs-auto-position="true" data-gs-min-width="6" data-gs-min-height="5">
                                <div class="grid-stack-item-content">
                                    <span class="blocked maxwidth centeralign" style="margin-top: 20%;">No content available</span>
                                    <div class="divider10"></div>
                                    <span style="display: block; text-align: center;"><a href="<?php echo $this->baseUrl('report/results/addwidgets/1'); ?>" class="btn btn-xs btn-default">Add Widget</a></span>
                                </div>
                            </div>
                        <?php } else { ?>
                            <?php foreach($dashwidgets as $widget){ 
								$reportdata = objectToArray(json_decode($widget['serialconfig'])); // debugMessage($reportdata['dataSource']['filename']);
								$config = array(); 
								$gs_width = 'data-gs-width="6" data-gs-min-width="3"'; $gs_height = 'data-gs-height="5" data-gs-min-height="3"';
								$gs_x = ''; $gs_y = ''; $gx_auto = 'data-gs-auto-position="true"';
								if(!isEmptyString($widget['gridconfig'])){
									$config = objectToArray(json_decode($widget['gridconfig'])); // debugMessage($config);
									if(!isArrayKeyAnEmptyString('width', $config)){
										$gs_width = 'data-gs-width="'.$config['width'].'" data-gs-min-width="3"';
									}
									if(!isArrayKeyAnEmptyString('height', $config)){
										$gs_height = 'data-gs-height="'.$config['height'].'" data-gs-min-height="3"';
									}
									if(!isArrayKeyAnEmptyString('x', $config)){
										$gs_x = 'data-gs-x="'.$config['x'].'"';
									} else {
										$gx_auto = 'data-gs-auto-position="false"';
									}
									if(!isArrayKeyAnEmptyString('y', $config)){
										$gs_y = 'data-gs-y="'.$config['y'].'"';
									} else {
										$gx_auto = 'data-gs-auto-position="false"';
									}
								}
							?>
                                <div class="grid-stack-item makerelative" id="grid_<?php echo $widget['id']; ?>" <?php echo $gs_width.' '.$gs_height.' '.$gs_x.' '.$gs_y; ?> data-gs-id="<?php echo $widget['id']; ?>"  style="margin-top:15px;">
                                    <div class="makeabsolute" style="right: 25px; top: -10px;">
                                        <div class="btn-group gridactions" style="z-index:1000;">
                                            <button type="button" class="btn btn-default btn-xs dropdown-toggle xnoround" data-toggle="dropdown"><i class="fa fa-caret-down"></i> Actions</button>
                                            <ul class="dropdown-menu listtype" role="menu" style="z-index:1001; left: -66px; min-width: 120px; top: 20px;">
                                                <?php 
                                                    $interpurl = $this->baseUrl('report/widgetdetails/pgc/true/id/'.encode($widget['id']));
                                                ?>
                                                <li><a class="noreloadpopup" href="<?php echo $interpurl; ?>" title="View Interpretation Notes" rel="Interpretation">Interpretation Notes</a></li>
                                                <li><a href="<?php echo $this->baseUrl('report/results/widgetid/'.encode($widget['id'])); ?>" title="View/Edit" target="_blank">Customize</a></li>
                                                <li><a class="reload" style="cursor:pointer;" widgetid="<?php echo $widget['id']; ?>" id="reload_<?php echo $widget['id']; ?>" title="Reload Widget">Reload</a></li>
                                                <li><a class="noreloadpopup" href="<?php echo $this->baseUrl('report/sharewidget/pgc/true/id/'.encode($widget['id'])); ?>" title="Share on another user's dashboard" rel="Share with Users">Share</a></li>
                                                <li><a class="gonowhere deleteline" action="<?php echo $this->baseUrl('index/delete/id/'.encode($widget['id'])."/entityname/UserDashboard/successurl/".encode($this->viewurl)); ?>" title="Remove Widget from Dashboard" rel="Remove Widget from Dashboard" message="<?php echo $this->translate('global_delete_confirm_message'); ?>" >Remove</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="grid-stack-item-content makerelative"> 
                                        <span class="makeabsolute" style="background-color: #0072ce; border: 1px solid #fff; color: #fff; font-size: 12px; font-weight: bold; height: auto; left: 20px; padding: 5px; top: -5px; width: auto; z-index: 10000;"><?php echo isEmptyString($widget['title']) ? '<a class="noreloadpopup" href="'.stripUrl($interpurl).'/tab/edit" title="Add Title & Interpretation" rel="Add Title & Interpretation" style="color:#fff;">Add Title</a>' : '<a class="noreloadpopup" href="'.stripUrl($interpurl).'/tab/view" title="Add Title & Interpretation" rel="Add Title & Interpretation" style="color:#fff;">'.$widget['title'].'</a>'; ?></span>
                                        <div id="widget<?php echo $widget['id']; ?>" class="pivotHolder" style="padding:0;"></div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		var options = {
			animate: true,
			alwaysShowResizeHandle: false,
			resizable: {
				handles: 'e, se, s, sw, w'
			}
		};
		$('.grid-stack').gridstack(options);
			
		new function () { 
			this.grid = $('.grid-stack').data('gridstack');
			$('.grid-stack').trigger('change');
			
			this.serializedData = [];
			
			this.saveGrid = function () {
				this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
					el = $(el);
					var node = el.data('_gridstack_node'); // console.log(node );
					return {
						x: node.x,
						y: node.y,
						width: node.width,
						height: node.height,
						id: node.id
					};
				}, this);
				var griddata = JSON.stringify(this.serializedData, null, '');
				$('#dashbordconfig').val(griddata); // console.log();
				
				$('.grid-stack').on('gsresizestop', function(event, elem) {
					// console.log(elem);
					var id = $(elem).attr('data-gs-id');
					$("#reload_"+id).click();
				});
				
				// save update to the database
				var posturl = $("#indexform").attr('action');
				$.post(  
					posturl,  
					$("#indexform").serialize(),  
					function(data){
						// console.log(data);
						// data = jQuery.parseJSON(data);
					}  
				);
				
				return false;
			}.bind(this);
				
			$('.grid-stack').on('change', this.saveGrid);
		}
	});
</script>
<script type="text/javascript">
	$(function () {	
		<?php foreach($dashwidgets as $widget){ 
				$reportdata = objectToArray(json_decode($widget['serialconfig']));
		?>
			$("#widget<?php echo $widget['id']; ?>").html("<a id='loading_<?php echo $widget['id']; ?>' style='text-align:center; display:block; margin-top:20%;'><img style='width:auto;' src='/assets/images/loader.gif' /></a>").css({'display':'block','padding':'10px 0'});
		
			<?php if(!isEmptyString($widget['serialconfig'])){ ?>
				var pivot<?php echo $widget['id']; ?> = new Flexmonster({
					container: "widget<?php echo $widget['id']; ?>",
					toolbar: false,
					width: '100%',
					ready: function () {
						pivot<?php echo $widget['id']; ?>.setReport(<?php echo $widget['serialconfig']; ?>);
						pivot<?php echo $widget['id']; ?>.setOptions({
							grid: {
								showHeaders: true,
								showFilter: false								
							},
							chart: {
								showFilter: false,
								showMeasures: false
							},
							configuratorButton: false,
							configuratorActive: false,
							showDefaultSlice: false							
						});					
					},
					licenseKey: "Z7NP-XCC01R-3S5Z5E-3A175B",
					componentFolder: "/assets/js/plugins/custom/flexmonster/"			
				});
				
				$("#reload_<?php echo $widget['id']; ?>").click(function(){ // alert('refreshing');
					pivot<?php echo $widget['id']; ?>.setReport(<?php echo $widget['serialconfig']; ?>);
					pivot<?php echo $widget['id']; ?>.setOptions({
						grid: {
							showHeaders: true,
							showFilter: false								
						},
						chart: {
							showFilter: false,
							showMeasures: false
						},
						configuratorButton: false,
						configuratorActive: false,
						showDefaultSlice: false
					});
				});
				
				function updateData() {
				 /* flexmonster.updateData({
					//filename: "<?php //echo $reportdata['dataSource']['filename']; ?>"
					filename: "http://cdn.flexmonster.com/2.3/data/data.csv"
				  });*/
				  
					
				}
				
			<?php } ?>
			
		<?php } ?>
	});
	
</script>
<?php
	require_once APPLICATION_PATH.'/includes/footer.php';
?>
