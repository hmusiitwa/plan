<?php
	include APPLICATION_PATH.'/includes/header.php';
	
	$initconfig  = '{"unusedAttrsVertical": 500, "autoSortUnusedAttrs": false}';
	$session = SessionWrapper::getInstance();
	if(!isEmptyString($session->getVar('serialconfig_project'))){
		$initconfig = $session->getVar('serialconfig_project');
	}
	
	$pagetitle = "Select Parameters";
	$title = "Projects Status";
	$this->headTitle($title.$browserappend);
?>

<script type="text/javascript">
$(function () {	
	var wrappedRenderers = $.extend( {}, 
		$.pivotUtilities.renderers, 
		$.pivotUtilities.export_renderers,
		$.pivotUtilities.c3_renderers,
		$.pivotUtilities.d3_renderers
	);
	var dataClass = $.pivotUtilities.SubtotalPivotData;
    var renderers = $.pivotUtilities.subtotal_renderers;
    
	var nrecoPivotExt = new NRecoPivotTableExtensions({
		wrapWith: '<div class="pvtTableRendererHolder"></div>',  // special div is needed by fixed headers when used with pivotUI
		fixedHeaders : true
	});
	
	var stdRendererNames = ["Table","Table Barchart","Heatmap","Row Heatmap","Col Heatmap"];
	$.each(stdRendererNames, function() {
		var rName = this;
		wrappedRenderers[rName] = nrecoPivotExt.wrapPivotExportRenderer(nrecoPivotExt.wrapTableRenderer(wrappedRenderers[rName]));
	});
	
	$("#fetch").on('click', function(){
		var projectid = $("#projectid").multipleSelect('getSelects');
		//alert(projectid);
		if(!isEmptyString(projectid) || isEmptyString(projectid)){
			$("#errors").html('');
			$("#output").html("<a id='loading' style='text-align:center; display:block; margin-top:50px;'><img style='width:18px;' src='/assets/images/loader.gif' /> Please wait...</a>").css({'display':'block','padding':'10px 0'});

			var url = "<?php echo $this->baseUrl('report/projectdata/'); ?>projectid/"+projectid;
			// alert(url); 
			$.getJSON(url, function(data) {
				// these are the functions you wish to use
		        var functionsConfig = {
		            renderers:   wrappedRenderers,
		            onRefresh: function(config) {
		                var config_copy = JSON.parse(JSON.stringify(config));
		                //delete some values which are functions
		                delete config_copy["aggregators"];
		                delete config_copy["renderers"];
		                //delete some bulky default values
		                delete config_copy["rendererOptions"];
		                delete config_copy["localeStrings"];

			             // this is correct way to apply fixed headers with pivotUI
		    			nrecoPivotExt.initFixedHeaders($('#output table.pvtTable'));

		    			// apply boostrap styles to pvt UI controls
		    			$('#output select.pvtAttrDropdown:not(.form-control)').addClass('form-control input-sm');
		    			$('#output select.pvtAggregator:not(.form-control), #output select.pvtRenderer:not(.form-control)').addClass('form-control input-sm');
		    			$('#output>table:not(.table)').addClass('table');	

		    			$(".pvtTable td:contains('Powered by')").css("color", "#fff");
		    			$(".pvtTable a:contains('PivotTable.js')").css("color", "#fff");

		    			$("#serialconfig").text(JSON.stringify(config_copy, undefined, 2));
		           }
		        };
		
		        // deserialize saved state it into an object
		        // merge the deserialized object with the functions object
		        var mergedConfig = $.extend({}, functionsConfig, JSON.parse('<?php echo $initconfig; ?>'));
		       	$("#output").pivotUI(data, mergedConfig);

		       	$("#pintodash").removeClass("hide");

				/*var row1 = $("table.pvtUi tr:nth-child(1) td");
				var row2 = $("table.pvtUi tr:nth-child(2) td");
				var row3 = $("table.pvtUi tr:nth-child(3) td:nth-child(1)");
				row1.addClass('hide'); row2.addClass('hide'); row3.addClass('hide');*/
			});
		} else {
			$("#pintodash").addClass('hide');
			$("#errors").html('<div id="nodata" class="alert alert-warning">Select report parameters.</div>');
		}
	});

});
</script>
<script>
$(document).ready(function() {
	$('.page-title').html('<?php echo $title; ?>');
	$('.titlebreadcrumb').html('<?php echo $homedir.$title; ?>');

	$("#fetch").trigger('click');
		
	$("#pintodash").on("click", function(){
		$("#pintodash").html('Saving..'); 
		
		var projectid = $("#projectid").multipleSelect('getSelects');
		var url = base64_encode("<?php echo $this->baseUrl('report/pivot/projectid/'); ?>"+projectid);
		$("#urlconfig").val(url); 
		
		var posturl = "<?php echo $this->baseUrl('report/savestate'); ?>"; // alert(submiturl);
		$.post(posturl,  
			$("#indexform").serialize(),  
			function(data){
				// alert(data);
				$("#pintodash").html('Pin to Dashboard'); 
				return true;
			}  
		);
    });
    
	$('#projectid').multipleSelect({
		filter: true,
		selectAll: false,
		placeholder: "[Filter Projects]"
    });

});
</script>
	
<div class="row" id="contentarea">
    <div class="col-md-12">	
    	<form id="indexform" class="form-horizontal" action="<?php echo $this->viewurl; ?>" method="get">
        	<div class="panel panel-border panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title lineblocked text-dark"><?php echo $pagetitle; ?></h3>
                </div>
                <div class="panel-body minheight400">
                	<ul class="resetlist listactions clearfix">
	                    <li style="width:100%; display:block;">
				            <?php
				            	$projects = getProjects();
				                $dropdown = new Zend_Form_Element_Multiselect('projectid',
				                		array(
			                				'multiOptions' => $projects,
			                				'view' => new Zend_View(),
			                				'decorators' => array('ViewHelper'),
			                				'class' => array('') ,
				                			'style' => array('width:60%')                                           										)
				                );
				                $dropdown->setValue(commaStringToArray($request->getParam('projectid')));
				                echo $dropdown->render();
				            ?>
				        </li>
				        <li style="width:100%; display:block;">
				            
				        </li>
				        <li style="width:100%; display:block;">
				            <button type="button" class="btn btn-primary btn-sm blockanchor" id="fetch" title="Filter" style="xpadding: 5px 10px !important;"><i class="fa fa-filter"></i> Generate</button>
				            <a href="<?php echo $this->viewurl; ?>" class="btn btn-default btn-sm" title="Reset" style="vertical-align:top;"><i class="fa fa-refresh"></i></a> 
                            <a class="btn btn-primary btn-sm hide" id="pintodash">Pin to Dashboard</a>
				        </li>
			        </ul>
			        <div id="divider5"></div>
                	<div id="errors" class="clearfix"></div>
                    <div id="output" class="pivotHolder"></div>
                    <div class="divider5"></div>
                    <textarea class="form-control expanding hide" name="serialconfig" id="serialconfig" style="min-height:200px;"><?php echo $initconfig; ?></textarea>
                    <input type="hidden" name="urlconfig" id="urlconfig" value="<?php echo $this->viewurl; ?>" />
                </div>
          	</div>
        </form>
    </div>
</div>
<?php
	require_once APPLICATION_PATH.'/includes/footer.php';
?>
