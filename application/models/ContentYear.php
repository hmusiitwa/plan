<?php

class ContentYear extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_fyear');
		$this->hasColumn('cspid', 'integer', null, array('notblank' => true));
		$this->hasColumn('name', 'string', 50, array('notblank' => true));
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('level', 'string', 4, array('default' => 2));
		$this->hasColumn('refno', 'string', 10);
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		$this->hasColumn('status', 'string', 4, array('default' => 0));
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('level', $formvalues)){
			unset($formvalues['level']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>