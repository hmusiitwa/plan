<?php 

class FileAttribute extends BaseRecord {
	
    public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		$this->setTableName('file_attribute');
		
		$this->hasColumn('fileid', 'integer', null, array('notblank' => true));
        $this->hasColumn('attr', 'string', 50);
		$this->hasColumn('value', 'string', 500);
    }
	/**
    * Contructor method for custom functionality - add the fields to be marked as dates
    */
	public function construct() {
		parent::construct();
       
        // set the custom error messages
        $this->addCustomErrorMessages(array(
       									"fileid.notblank" => $this->translate->_("file_attribute_file_error"),
        								/* "attr.notblank" => $this->translate->_("file_attribute_attribute_error"),
        								"value.notblank" => $this->translate->_("file_attribute_value_error"), */
       								));
    }
    
    function setUp() {
    	parent::setUp();
    	
		$this->hasOne('File as file',
						 array(
								'local' => 'fileid',
								'foreign' => 'id'
							)
					);
    }
	/*
	 * Custom validation
	 */
	function validate() {
		parent::validate();
		
		$conn = Doctrine_Manager::connection();
		
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues){
		# force setting of default none string column values. enum, int and date 	
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>