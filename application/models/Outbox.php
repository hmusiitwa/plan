<?php
/**
 * Outbox
 */
class Outbox extends BaseRecord  {
	public function setTableDefinition() {
		parent::setTableDefinition();
		$this->setTableName('outbox');
		
		$this->hasColumn('id', 'integer', null, array('primary' => true, 'autoincrement' => true));
		$this->hasColumn('phone', 'string', 10000);
		$this->hasColumn('msg', 'string', 1000);
		$this->hasColumn('resultcode', 'string', 255);
		$this->hasColumn('source', 'string', 15);
		$this->hasColumn('smsid', 'string', 255);   
		$this->hasColumn('deliverstatus', 'string', 255);
		$this->hasColumn('deliverdetails', 'string', 255);
		$this->hasColumn('msgcount', 'string', 15);
		$this->hasColumn('createdby', 'integer', null, array('default' => NULL));
		$this->hasColumn('messageid', 'integer', null, array('default' => NULL));
		$this->hasColumn('country', 'string', 2, array('default' => 'UG'));
		$this->hasColumn('userid', 'string', 15);
		$this->hasColumn('scheduledate', 'date', array('default' => NULL));
		$this->hasColumn('deliverdate', 'date', array('default' => NULL));
	}
	
	public function setUp() {
		parent::setUp();
		$this->actAs('Timestampable',
				array('created' => array(
						'name' => 'datecreated',
				),
						'updated' => array(
								'name'     =>  'datecreated',
								'onInsert' => false,
								'options'  =>  array('notnull' => false)
						)
				)
		);
		
		/* $this->hasOne('ServiceBuildDetails as servicedetails',
							array('local' => 'servicedetailid',
									'foreign' => 'id'
							)
						); */
		
	}
	# preprocess model data
	function processPost($formvalues) {
		if(isArrayKeyAnEmptyString('messageid', $formvalues)){
			unset($formvalues['messageid']);
		}
		if(isArrayKeyAnEmptyString('scheduledate', $formvalues)){
			unset($formvalues['scheduledate']);
		}
		if(isArrayKeyAnEmptyString('deliverdate', $formvalues)){
			unset($formvalues['deliverdate']);
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function populateBySMSID($id) {
		$query = Doctrine_Query::create()->from('Outbox o')->where("o.smsid = '".$id."'");
		//debugMessage($query->getSQLQuery());
		$result = $query->execute();
		if(!$result){
			return new Outbox();
		}
		return $result->get(0);
	}
}