<?php

/**
 * Special file&folder Access permissions for a group or user
 */
class Access extends BaseEntity   {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
        $this->setTableName('access');
        $this->hasColumn('companyid', 'integer', null, array('default' => NULL));
        $this->hasColumn('groupid', 'integer', null, array('default' => NULL));
        $this->hasColumn('categoryid', 'integer', null, array('default' => NULL));
        $this->hasColumn('userid', 'integer', null, array('default' => NULL));
        $this->hasColumn('folderid', 'integer', null, array('default' => NULL));
        $this->hasColumn('fileid', 'integer', null, array('default' => NULL));
        
        $this->hasColumn('addfolder', 'integer', null, array('default' => 1));
        $this->hasColumn('create', 'integer', null, array('default' => 1));
        $this->hasColumn('edit', 'integer', null, array('default' => 1));
        $this->hasColumn('view', 'integer', null, array('default' => 1));
        $this->hasColumn('list', 'integer', null, array('default' => 1));
		$this->hasColumn('delete','integer', null, array('default' => 1));
		$this->hasColumn('move','integer', null, array('default' => 1));
        $this->hasColumn('share', 'integer', null, array('default' => 1));
        $this->hasColumn('archive', 'integer', null, array('default' => 1));
        $this->hasColumn('email', 'integer', null, array('default' => 1));
    }
	
    function setUp() {
    	parent::setUp();
    	// foreign key for the group
		$this->hasOne('UserAccount as user', array('local' => 'userid', 'foreign' => 'id'));
		$this->hasOne('AclGroup as group', array('local' => 'groupid', 'foreign' => 'id'));
		$this->hasOne('Folder as folder', array('local' => 'folderid', 'foreign' => 'id'));
		$this->hasOne('File as file', array('local' => 'fileid', 'foreign' => 'id'));
    }
/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); 
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('companyid', $formvalues)){
			unset($formvalues['companyid']);
		}
		if(isArrayKeyAnEmptyString('groupid', $formvalues)){
			unset($formvalues['groupid']);
		}
		if(isArrayKeyAnEmptyString('categoryid', $formvalues)){
			unset($formvalues['categoryid']);
		}
		if(isArrayKeyAnEmptyString('folderid', $formvalues)){
			unset($formvalues['folderid']);
		}
		if(isArrayKeyAnEmptyString('userid', $formvalues)){
			unset($formvalues['userid']);
		}
		if(isArrayKeyAnEmptyString('fileid', $formvalues)){
			unset($formvalues['fileid']);
		}
		if(isArrayKeyAnEmptyString('addfolder', $formvalues)){
			$formvalues['addfolder'] = 0;
		}
		if(isArrayKeyAnEmptyString('create', $formvalues)){
			$formvalues['create'] = 0; 
		}
		if(isArrayKeyAnEmptyString('edit', $formvalues)){
			$formvalues['edit'] = 0; 
		}
		if(isArrayKeyAnEmptyString('view', $formvalues)){
			$formvalues['view'] = 0;  
		}
		if(isArrayKeyAnEmptyString('list', $formvalues)){
			$formvalues['list'] = 0;  
		}
		if(isArrayKeyAnEmptyString('delete', $formvalues)){
			$formvalues['delete'] = 0; 
		}
		if(isArrayKeyAnEmptyString('move', $formvalues)){
			$formvalues['move'] = 0;
		}
		if(isArrayKeyAnEmptyString('email', $formvalues)){
			$formvalues['email'] = 0; 
		}
		if(isArrayKeyAnEmptyString('share', $formvalues)){
			$formvalues['share'] = 0;  
		}
		if(isArrayKeyAnEmptyString('archive', $formvalues)){
			$formvalues['archive'] = 0;
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>