<?php
/**
 * Model for results 
 */
class DatasetResult extends BaseEntity  {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('data_set_result_summary');
		
		$this->hasColumn('refno', 'string', 25);
		$this->hasColumn('datasetid', 'integer', null, array('notblank' => true));
		$this->hasColumn('submittedbyid', 'integer', null, array('notblank' => true));
		$this->hasColumn('projectid', 'integer', null, array('default' => NULL));
		$this->hasColumn('milestoneid', 'integer', null, array('default' => NULL));
		$this->hasColumn('quarter', 'integer', null, array('default' => NULL));
		$this->hasColumn('activityid', 'integer', null, array('default' => NULL));
		$this->hasColumn('period', 'string', 25);
		$this->hasColumn('groupattr_val', 'string', 10);
		$this->hasColumn('startdate','date', null, array('default' => NULL));
		$this->hasColumn('enddate','date', null, array('default' => NULL));
		$this->hasColumn('status', 'integer', null, array('default' => 3));
		$this->hasColumn('datesubmitted','date', null, array('default' => NULL));
		$this->hasColumn('remarks', 'string', 1000);
		$this->hasColumn('comments', 'string', 1000);
		
		$this->hasColumn('dateapproved','date', null, array('default' => NULL));
		$this->hasColumn('approvedbyid', 'integer', null, array('default' => NULL));
		$this->hasColumn('currentstep', 'string', 4, array('default' => NULL));
		$this->hasColumn('status1', 'string', 10, array('default' => NULL));
		$this->hasColumn('status2', 'string', 10, array('default' => NULL));
		$this->hasColumn('status3', 'string', 10, array('default' => NULL));
		$this->hasColumn('approvedbyid1', 'string', 10, array('default' => NULL));
		$this->hasColumn('approvedbyid2', 'string', 10, array('default' => NULL));
		$this->hasColumn('approvedbyid3', 'string', 10, array('default' => NULL));
		$this->hasColumn('dateapproved1', 'string', 25, array('default' => NULL));
		$this->hasColumn('dateapproved2', 'string', 25, array('default' => NULL));
		$this->hasColumn('dateapproved3', 'string', 25, array('default' => NULL));
		$this->hasColumn('reason1', 'string', 255, array('default' => NULL));
		$this->hasColumn('reason2', 'string', 255, array('default' => NULL));
		$this->hasColumn('reason3', 'string', 255, array('default' => NULL));
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate","datesubmitted"));
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"datasetid.notblank" => "Please specify result dataset"
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Dataset as dataset', array('local' => 'datasetid','foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid','foreign' => 'id'));
		$this->hasOne('Activity as activity', array('local' => 'activityid', 'foreign' => 'id'));
		$this->hasOne('Milestone as milestone', array('local' => 'milestoneid', 'foreign' => 'id'));
		$this->hasMany('DatasetResultDetail as resultdetails', array('local' => 'id', 'foreign' => 'resultid'));
		$this->hasMany('DatasetResultProjectCost as costdetails', array('local' => 'id', 'foreign' => 'resultid'));
		$this->hasOne('UserAccount as user', array('local' => 'submittedbyid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as approver', array('local' => 'approvedbyid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as firstapprover', array('local' => 'dateapproved1', 'foreign' => 'id'));
		$this->hasOne('UserAccount as secondapprover', array('local' => 'approvedbyid2', 'foreign' => 'id'));
		$this->hasOne('UserAccount as thirdapprover', array('local' => 'approvedbyid3', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('activityid', $formvalues)){
			unset($formvalues['activityid']);
		}
		if(isArrayKeyAnEmptyString('milestoneid', $formvalues)){
			unset($formvalues['milestoneid']);
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				$formvalues['refno'] = NULL;
			}
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("R".($this->getID() + 1000));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("R".($this->getID() + 1000));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	# send notifications
	function afterApprove($status ='', $stepuser = ''){
		// $this->sendApprovalConfirmationNotification($stepuser);
		return true;
	}
	function getUserID(){
		return $this->getSubmittedByID();
	}
	# determine if request has pending approval
	function isDraft(){
		return $this->getStatus() == 1 ? true : false;
	}
	function isSubmitted(){
		return $this->getStatus() == 2 ? true : false;
	}
	# determine if request is approved
	function isApproved(){
		return $this->getStatus() == 3 ? true : false;
	}
	# determine if request is rejected
	function isRejected(){
		return $this->getStatus() == 4 ? true : false;
	}
	function getCurrentStatus(){
		$resultstatuses = getResultStatuses();
		$statuslabel = isArrayKeyAnEmptyString($this->getStatus(), $resultstatuses) ? '' : $resultstatuses[$this->getStatus()];
		if($this->getDataset()->getHasWorkFlow() == 0 && $this->getStatus() == 3){
			$statuslabel = 'Completed';
		}
		if($this->getDataset()->getHasWorkflow() == 1){
			if($this->isMultiStep() && $this->isSubmitted()  && $this->getCurrentStep() != $this->getUSer()->getApprovalSteps()){
				$statuslabel = 'Pending - Step '.$this->getCurrentStep().' of '.$this->getDataset()->getApprovalSteps().' Completed';
				if(isEmptyString($this->getCurrentStep())){
					$statuslabel = 'Submitted for Approval';
				}
			}
		}
		
		return $statuslabel;
	}
	function canApprove($userid) {
		$isallowed = false;
	
		$multistep = $this->getDataset()->isMultiStep() ? '1' : '0';
		$maxsteps = $this->getDataset()->getApprovalSteps();
		if($this->getDataset()->getHasWorkflow() == 1 && $this->isSubmitted()){
			if($this->isApprover($userid)){ // if user is an approver
				if($this->hasNotYetApproved($userid)){ // if user has not yet approved
					$isallowed = true;
				}
			}
		}
		return $isallowed;
	}
	function canDelete($userid){
		$acl = getACLInstance();
		$allowdelete = false;
		if($acl->checkPermission('Data Entry', 'update')){
			if($this->getDataset()->getHasWorkflow() == 1){
				if($this->isDraft() && $this->isSubmitter($userid)){
					$allowdelete = true;
				}
				if($this->isSubmitted()){
					$allowdelete = false;
				}
			} else {
				if($acl->checkPermission('Data Entry', 'delete')){
					$allowdelete = false;
					if($this->getDataset()->getAudienceType() == '3'){
						$allowdelete = true;
					}
					if($this->getDataset()->getAudienceType() == '2' && $this->isSubmitter($userid)){
						$allowdelete = true;
					}
					if($this->getDataset()->getAudienceType() == '1' && $this->isSubmitter($userid)){
						$allowdelete = true;
					}
					if($this->isSubmitter($userid) || $this->getDataset()->getCreatedBy() == $userid){
						$allowdelete = true;
					}
				}
			}
		}
		return $allowdelete;
	}
	function canEdit($userid){
		$acl = getACLInstance();
		$allowedit = false;
		
		if($this->getDataset()->getHasWorkflow() == 1){
			if($this->isDraft() && $this->isSubmitter($userid)){
				$allowedit = true;
			}
			if($this->isSubmitted() && $this->isSubmitter($userid)){
				$allowedit = false;
			}
		} else {
			if($acl->checkPermission('Data Entry', 'update')){
				$allowedit = false;
				if($this->getDataset()->getAudienceType() == '3'){
					$allowedit = true;
				}
				if($this->getDataset()->getAudienceType() == '2' && $this->isSubmitter($userid)){
					$allowedit = true;
				}
				if($this->getDataset()->getAudienceType() == '1' && $this->isSubmitter($userid)){
					$allowedit = true;
				}
				if($this->isSubmitter($userid) || $this->getDataset()->getCreatedBy() == $userid){
					$allowedit = true;
				}
			}
		}
		
		return $allowedit;
	}
	function canUnsubmit($userid){
		return $this->isSubmitter($userid) && $this->isSubmitted() && isEmptyString($this->getCurrentStep() );
	}
	function canSubmit($userid){
		return $this->isSubmitter($userid) && $this->isDraft();
	}
	function hasappproval(){
		return $this->getDataset()->getHasWorkflow() == 1;
	}
	function hasNotYetApproved($userid){
		$hasnotapproved = true;
		if($this->getApprovedByID() == $userid & ($this->getStatus() == 3 || $this->getStatus() == 4)){
			$hasnotapproved = false;
		}
		if($this->getApprovedByID1() == $userid & ($this->getStatus1() == 3 || $this->getStatus1() == 4)){
			$hasnotapproved = false;
		}
		if($this->getApprovedByID2() == $userid & ($this->getStatus2() == 3 || $this->getStatus2() == 4)){
			$hasnotapproved = false;
		}
		if($this->getApprovedByID3() == $userid & ($this->getStatus3() == 3 || $this->getStatus3() == 4)){
			$hasnotapproved = false;
		}
		
		return $hasnotapproved;
	}
	function isApprover($userid){
		$isallowed = false;
		if($this->getDataset()->getFirstApprover() == $userid){
			$isallowed = true;
		}
		if($this->getDataset()->getSecondApprover() == $userid){
			$isallowed = true;
		}
		if($this->getDataset()->getThirdApprover() == $userid){
			$isallowed = true;
		}
		return $isallowed;
	}
	function isSubmitter($userid){
		return $userid == $this->getUserID() ? true : false;
	}
	# Send notification to inform user
	function sendApprovalConfirmationNotification($stepuser = '') {
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
		$session = SessionWrapper::getInstance();
	
		// assign values
		$template->assign('firstname', $this->getUser()->getFirstName());
	
		$statuslabel = $this->isApproved() ? "Approved" : "Rejected";
		$subject = "Leave ".$statuslabel;
		$steps = $this->getDataset()->getApprovalSteps();
		if($this->getUser()->isMultiStep()){
			switch ($stepuser){
				case 1:
					$statuslabel = $this->getStatus1() == 1 ? "Approved" : "Rejected";
					$subject = "Approval Step ".$stepuser."/".$steps.": ".$statuslabel;
					break;
				case 2:
					$statuslabel = $this->getStatus2() == 1 ? "Approved" : "Rejected";
					$subject = "Approval Step ".$stepuser."/".$steps.": ".$statuslabel;
					break;
				case 3:
					$statuslabel = $this->getStatus3() == 1 ? "Approved" : "Rejected";
					$subject = "Approval Step ".$stepuser."/".$steps.": ".$statuslabel;
					break;
				default:
					break;
			}
		}
		// debugMessage($statuslabel.' ~ '.$subject);
	
		$save_toinbox = true;
		$type = "dataentry";
		$subtype = "dataentry_".strtolower($statuslabel);
		$viewurl = $template->serverUrl($template->baseUrl('dataentry/view/id/'.encode($this->getID())));
	
		$rejectreason = "";
	
		$approverid = $this->getApprovedByID();
		if($this->getUser()->isMultiStep()){
			switch ($stepuser){
				case 1:
					$approverid = $this->getUser()->getFirstApprover();
					break;
				case 2:
					$approverid = $this->getUser()->getSecondApprover();
					break;
				case 3:
					$approverid = $this->getUser()->getThirdApprover();
					break;
				default:
					break;
				}
			}
			$approver = ' By '.getUserNamesFromID($approverid).' ';
	
			$message_contents = "<p>This is to confirm that your daset submission <b>tbd name</b> for period <b>tbd period </b> has been successfully ".$statuslabel.$approver.$rejectreason.".</p>
		
			<p>To view this submission online <a href='".$viewurl."'>click here<a></p>
			<br />
			<p>".$this->getApprover()->getName()."<br />
			".getAppName()."</p>
			";
			$template->assign('contents', $message_contents);
		
			$mail->clearRecipients();
			$mail->clearSubject();
			$mail->setBodyHtml('');
	
			// configure base stuff
			$mail->addTo($this->getUser()->getEmail(), $this->getUser()->getName());
			// set the send of the email address
			$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		
			$mail->setSubject($subject);
			// render the view as the body of the email
		
			$html = $template->render('default.phtml');
			$mail->setBodyHtml($html);
			// debugMessage($html); // exit();
	
			if(!isEmptyString($this->getUser()->getEmail())){
				try {
					// $mail->send();
					$session->setVar("custommessage1", "Email sent to ".$this->getUser()->getEmail());
				} catch (Exception $e) {
					debugMessage($e->getMessage());
					$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
				}
			}
	
			$mail->clearRecipients();
			$mail->clearSubject();
			$mail->setBodyHtml('');
			$mail->clearFrom();
	
			if($save_toinbox){
				# save copy of message to user's application inbox
				$message_dataarray = array(
				"senderid" => DEFAULT_ID,
				"subject" => $subject,
				"contents" => $message_contents,
				"html" => $html,
				"type" => $type,
				"subtype" => $subtype,
				"refid" => $this->getID(),
				"recipients" => array(
					md5(1) => array("recipientid" => $this->getUserID())
				)
			); // debugMessage($message_dataarray);
			// process message data
			$message = new Message();
			$message->processPost($message_dataarray);
			$message->save();
		}
	
		return true;
	}
	
	function getPeriodLabel(){
		$periodlabel = '';
		if(!isEmptyString($this->getperiod())){
			$period_array = array_remove_empty(explode(',', trim($this->getperiod())));
			// debugMessage($period_array);
			if(count($period_array) > 0){
				if(!isArrayKeyAnEmptyString(0, $period_array) && !isArrayKeyAnEmptyString(1, $period_array)){
					$start = strlen($period_array[0]) == 7 ? date("M Y", strtotime($period_array[0])) : changeMySQLDateToPageFormat($period_array[0]);
					$end = strlen($period_array[1]) == 7 ? date("M Y", strtotime($period_array[1])) : changeMySQLDateToPageFormat($period_array[1]);
						
					$periodlabel = $start.' - '.$end;
				} else {
					$start = strlen($period_array[0]) == 7 ? date("F Y", strtotime($period_array[0])) : changeMySQLDateToPageFormat($period_array[0]);
					$periodlabel = $start;
				}
			}
			
			if($this->getDataset()->getPeriodType() == 14){
				$periodlabel = $this->getperiod();
			}
		}
		return $periodlabel;
	}
}
?>