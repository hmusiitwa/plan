<?php

class ContentRiskDetail extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_risk_detail');
		
		$this->hasColumn('riskid', 'integer', null, array('default' => NULL));
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('type', 'string', 4, array('default' => 1));
		$this->hasColumn('status', 'string', 4, array('default' => 1));
		$this->hasColumn('inputdate', 'string', 25);
		$this->hasColumn('startdate', 'string', 15);
		$this->hasColumn('enddate', 'string', 15);
		$this->hasColumn('nextreviewdate', 'string', 15);
		$this->hasColumn('period', 'string', 25);
		$this->hasColumn('probability', 'string', 10);
		$this->hasColumn('impact_cost', 'string', 10);
		$this->hasColumn('impact_schedule', 'string', 10);
		
		$this->hasColumn('impactdetails', 'string', 500);
		$this->hasColumn('responsetype', 'string', 4);
		$this->hasColumn('responsedetails', 'string', 1000);
		$this->hasColumn('isbaseline', 'string', 1, array('default' => 0));
		
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		// $this->addDateFields(array("inputdate"));
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as user', array('local' => 'userid', 'foreign' => 'id'));
		$this->hasOne('ContentRisk as risk', array('local' => 'riskid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('inputdate', $formvalues)){
			// unset($formvalues['inputdate']);
			$formvalues['inputdate'] = date('Y-m-d H:i:s', time());
		} else {
			$formvalues['inputdate'] = date('Y-m-d H:i:s', strtotime($formvalues['inputdate']));
		}
		
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}
		if(isArrayKeyAnEmptyString('nextreviewdate', $formvalues)){
			unset($formvalues['nextreviewdate']);
		} else {
			$formvalues['nextreviewdate'] = date('Y-m-d', strtotime($formvalues['nextreviewdate']));
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(!isEmptyString($this->getRiskID()) && $this->getStatus() == '1' && $this->getRisk()->getReviewID() != $this->getID()){
			$this->getRisk()->setReviewID($this->getID());
			
			$resave = true;
		}
		if($this->getStatus() == '1' && $this->getRisk()->getEndDate() != $this->getNextReviewDate()){
			$resave = true;
			$this->getRisk()->setEndDate($this->getNextReviewDate());
		}
			
		if($resave){
			# initial save
			$this->getRisk()->save();
			$conn = Doctrine_Manager::connection();
			$query = "update content_risk_detail c set c.status = 0 where c.riskid = '".$this->getRiskID()."' AND c.id <> '".$this->getID()."' ";
			// debugMessage($query);
			$result = $conn->execute($query);
		}
	
		return true;
	}
	
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(!isEmptyString($this->getRiskID()) && $this->getStatus() == '1' && $this->getRisk()->getReviewID() != $this->getID()){
			$this->getRisk()->setReviewID($this->getID());
			$resave = true;
		}
		
		if($this->getStatus() == '1' && $this->getRisk()->getEndDate() != $this->getNextReviewDate()){
			$resave = true;
			$this->getRisk()->setEndDate($this->getNextReviewDate());
		}
			
		if($resave){
			$this->getRisk()->save();
			$conn = Doctrine_Manager::connection();
			if($this->getStatus() == '1'){
				$query = "update content_risk_detail c set c.status = 0 where c.riskid = '".$this->getRiskID()."' AND c.id <> '".$this->getID()."' ";
				// debugMessage($query);
				$result = $conn->execute($query);
			}
		}
	
		return true;
	}
	
	function getExposure(){
		$maxprob = 5;
		$maximpact = 5;
		$maxscore = $maxprob + $maximpact;
		$rank_value = $this->getprobability() * (($this->getimpact_schedule() + $this->getimpact_cost()) / 2);
		$rank_score = formatNum(($rank_value / ($maxprob * $maximpact)) * 100);
		
		return $rank_score;
	}
}
?>