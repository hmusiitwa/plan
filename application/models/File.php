<?php 

class File extends BaseEntity {
	
    public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		$this->setTableName('file');
		
		$this->hasColumn('companyid', 'integer', null, array('notblank' => true, 'default' => getCompanyID()));
		$this->hasColumn('folderid', 'integer', null);
        $this->hasColumn('filename', 'string', 255, array('notblank' => true));
        $this->hasColumn('refno', 'string', 25);
		$this->hasColumn('title', 'string', 255);
        $this->hasColumn('description', 'string', 500);
        $this->hasColumn('extension', 'string', 6);
        $this->hasColumn('filesize', 'string', 25);
        $this->hasColumn('type', 'integer', null, array('default' => NULL));
        $this->hasColumn('restoreid', 'integer', null, array('default' => NULL));
        $this->hasColumn('tags', 'string', 500);
        $this->hasColumn('refurl', 'string', 500);
        
        # override the not null and not blank properties for the createdby column in the BaseEntity
        $this->hasColumn('createdby', 'integer', 11, array('default' => NULL));
    }
    protected $preupdatedata;
    protected $controller;
    function getPreUpdateData(){
    	return $this->preupdatedata;
    }
    function setPreUpdateData($preupdatedata) {
    	$this->preupdatedata = $preupdatedata;
    }
    function getController(){
    	return $this->controller;
    }
    function setController($controller) {
    	$this->controller = $controller;
    }
	/**
    * Contructor method for custom functionality - add the fields to be marked as dates
    */
	public function construct() {
		parent::construct();
       
        // set the custom error messages
        $this->addCustomErrorMessages(array(
       									"filename.notblank" => $this->translate->_("file_filename_error"),
        								"filename.length" => sprintf($this->translate->_("file_filename_length_error"), 50)
       								));
    }
    
    protected $previousname;
    function getPreviousName(){
    	return $this->previousname;
    }
    function setPreviousName($previousname) {
    	$this->previousname = $previousname;
    }
    function setUp() {
    	parent::setUp();
    	# relationship to link a document to a matter
    	# Link to the matter
    	$this->hasOne('Company as company',
    			array(
    					'local' => 'companyid',
    					'foreign' => 'id'
    			)
    	);
		$this->hasOne('Folder as folder',
						 array(
								'local' => 'folderid',
								'foreign' => 'id'
							)
					);
		$this->hasOne('Folder as restore',
				array(
						'local' => 'restoreid',
						'foreign' => 'id'
				)
		);
		$this->hasMany('FileAttribute as attributelist',
				array(
						'local' => 'id',
						'foreign' => 'fileid'
				)
		);
    }
	/*
	 * Custom validation
	 */
	function validate() {
		parent::validate();
		
		$conn = Doctrine_Manager::connection();
		
		# Unique file name in same folder
		$unique_query = "SELECT id FROM file WHERE filename = '".$this->getFileName()."' AND folderid = '".$this->getFolderID()."' AND id <> '".$this->getID()."' ";
		# debugMessage($unique_query);
		$result = $conn->fetchOne($unique_query);
		if(!isEmptyString($result)){ 
			$this->getErrorStack()->add("file.unique",  "File ".$this->getFileName()." already exists in the destination");
		}
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues){
		# force setting of default none string column values. enum, int and date 	
		if(isArrayKeyAnEmptyString('createdby', $formvalues)){
			unset($formvalues['createdby']);
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']); 
		}
		if(isArrayKeyAnEmptyString('folderid', $formvalues)){
			unset($formvalues['folderid']);
		}
		if(isArrayKeyAnEmptyString('restoreid', $formvalues)){
			unset($formvalues['restoreid']);
		}
		if(!isArrayKeyAnEmptyString('previousname', $formvalues)){
			$this->setPreviousName($formvalues['previousname']);
		}
		if(!isArrayKeyAnEmptyString('filename', $formvalues)){
			$formvalues['filename'] = trim($formvalues['filename']);
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			$formvalues['refno'] = stripMultipleSpaces(trim($formvalues['refno']));
		}
		if(!isArrayKeyAnEmptyString('id', $formvalues) && !isArrayKeyAnEmptyString('extension', $formvalues)){
			if($formvalues['previousname'] != ($formvalues['filename'].$formvalues['extension'])){
				$formvalues['filename'] = $formvalues['filename'].$formvalues['extension'];
			}
		}
		if(!isArrayKeyAnEmptyString('attributelist', $formvalues)){
			foreach ($formvalues['attributelist'] as $key => $value){
				if(isArrayKeyAnEmptyString('attr', $value) || isArrayKeyAnEmptyString('value', $value)){
					unset($formvalues['attributelist'][$key]);
				}
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		
		if(is_file($this->getTempFilePath())){
			if(file_exists($this->getAbsoluteFilePath())){
				unlink($this->getAbsoluteFilePath());
			}
			rename($this->getTempFilePath(), $this->getAbsoluteFilePath());
			// debugMessage('file moved successfully '.$this->getFilename());
			$this->getFolder()->setLastUpdateDate(date('Y-m-d H:i:s'));
			$this->getFolder()->setLastUpdatedBy($session->getVar('userid'));
			$this->getFolder()->save();
		}
	
		# add log to audit trail
		$view = new Zend_View();
		// $controller = $this->getController();
		$url = $view->baseUrl('library/index/parentid/'.encode($this->getFolderID()));
		$profiletype = 'Uploaded File: <i>'.$this->getRelativePathFromHome().'</i>';
		$usecase = '2.1';
		$module = '2';
		$type = PATH_CREATE;
	
		$audit_values = getAuditInstance();
		$audit_values['companyid'] = $session->getVar('companyid');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$transactiondate = date("Y-m-d H:i:s");
		$audit_values['transactiondate'] = $transactiondate;
		$audit_values['transactiondetails'] = $profiletype;
		$audit_values['url'] = $url;
		$audit_values['prejson'] = $this->getRelativePathFromHome();
		//debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		$duplicateaudits = $this->getDuplicateAudits($type, $transactiondate);
		if($duplicateaudits->count() > 1){
			$duplicateaudits->delete();
		}
		// exit;
		return true;
	}
	# update after
	function beforeUpdate(){
		$session = SessionWrapper::getInstance();
		# set object data to class variable before update
		$folder = new Folder();
		$folder->populate($this->getID());
		$this->setPreUpdateData($folder->toArray());
		// exit;
		return true;
	}
	function afterUpdate($savetoaudit = true) {
		$session = SessionWrapper::getInstance();
		// check if name has changed to update the filesystem
		if($this->getPreviousName() != $this->getFileName()){
			$prevname = stripUrl($this->getFolder()->getAbsolutePath()).DIRECTORY_SEPARATOR.$this->getPreviousName();  //debugMessage($prevname);
			$newname = stripUrl($this->getAbsolutePath()).DIRECTORY_SEPARATOR.$this->getFileName(); //debugMessage($newname);
			rename($prevname, $newname);
		}
		
		if($savetoaudit){
			# set postupdate from class object, and then save to audit
			$prejson = json_encode($this->getPreUpdateData()); // debugMessage($prejson);
		
			$this->clearRelated(); // clear any current object relations
			$after = $this->toArray(); // debugMessage($after);
			$postjson = json_encode($after); // debugMessage($postjson);
		
			// $diff = array_diff($prejson, $postjson);  // debugMessage($diff);
			$diff = array_diff($this->getPreUpdateData(), $after);
			$jsondiff = json_encode($diff); // debugMessage($jsondiff);
		
			$view = new Zend_View();
			$controller = $this->getController();
			$url = $view->baseUrl('library/index/parentid/'.encode($this->getFolderID()));
			$profiletype = 'Updated File: <i>'.$this->getRelativePathFromHome().'</i>';
			$usecase = '2.2';
			$module = '2';
			$type = PATH_UPDATE;
		
			$audit_values = getAuditInstance();
			$audit_values['companyid'] = $session->getVar('companyid');
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['status'] = "Y";
			$audit_values['userid'] = $session->getVar('userid');
			$transactiondate = date("Y-m-d H:i:s");
			$audit_values['transactiondate'] = $transactiondate;
			$audit_values['transactiondetails'] = $profiletype;
			$audit_values['isupdate'] = 1;
			$audit_values['prejson'] = $prejson;
			$audit_values['postjson'] = $postjson;
			$audit_values['jsondiff'] = $jsondiff;
			$audit_values['url'] = $url;
			// debugMessage($audit_values);
			$this->notify(new sfEvent($this, $type, $audit_values));
			
			$duplicateaudits = $this->getDuplicateAudits($type, $transactiondate);
			if($duplicateaudits->count() > 1){
				$duplicateaudits->delete();
			}
		}
		// exit;
		return true;
	}
	function getDuplicateAudits($type, $transactiondate){
		$q = Doctrine_Query::create()->from('AuditTrail a')->where("a.transactiontype = '".$type."' AND a.transactiondate = '".$transactiondate."' AND a.id <> '".$this->getID()."' ");
		$result = $q->execute();
		return $result;
	}
	# absolute path to folder containing file
	function getAbsolutePath(){
		$basepath = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
		// debugMessage($this->getFolder()->toArray());
		if($this->getFolder()->isTrash()){
			return $this->getFolder()->getTrashAbsoluteUrl().DIRECTORY_SEPARATOR;
		} else {
			$pathstr = stripUrl($this->getFolder()->getPath());
			$pathidsarr = array_remove_empty(explode('/', $pathstr));
			$pathtoroot = ''; $fnames = array();
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				$pathtoroot .= $fold->getName().DIRECTORY_SEPARATOR;
			}
			$pathtoroot .= $this->getFolder()->getName().DIRECTORY_SEPARATOR;
			// debugMessage($pathtoroot);
		}
		return $basepath.$pathtoroot;
	}
	# absolute path to file
	function getAbsoluteFilePath(){
		return $this->getAbsolutePath().$this->getFileName();
	}
	# absolute path to file
	function getRelativePath(){
		$view = new Zend_View(); $viewpath = 'uploads/';
		$pathstr = stripUrl($this->getFolder()->getPath());
		$pathidsarr = array_remove_empty(explode('/', $pathstr));
		$pathtoroot = ''; $fnames = array();
		foreach($pathidsarr as $i => $fid){
			$fold = new Folder();
			$fold->populate($fid);
			$viewpath .= $fold->getName().'/';
		}
		$viewpath .= $this->getFolder()->getName().'/';
		$viewpath = $view->baseUrl(stripURL($viewpath));
		return $viewpath;
	}
	# relative path
	function getRelativePathFromHome(){
		$pathidsarr = array_remove_empty(explode('/', stripUrl($this->getFolder()->getPath())));
		// debugMessage($this->getFolder()->getPath());
		$viewpath = '';
		if(count($pathidsarr) == 0){
			$viewpath .= ''.getLibDir().' / ';
		} else {
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				if($fold->isHome()){
					$viewpath .= ''.getLibDir().' / ';
				} else {
					$viewpath .= $fold->getName().' / ';
				}
			}
		}
		if(!$this->getFolder()->isHome()){
			$viewpath .= $this->getFolder()->getName().' / ';
		}
		$viewpath .= $this->getName();
		return stripUrl($viewpath);
	}
	# relative path to file
	function getRelativeFilePath(){
		return $this->getRelativePath().'/'.$this->getFileName();
	}
	function isInHome(){
		return isEmptyString($this->getFolder()->getParentID()) && $this->getFolder()->getPath() == '/' ? true : false;
	}
	function isFolder(){
		return false;
	}
	function isFile(){
		return true;
	}
	function isTrashed(){
		$trashdir = $this->getFolder()->getTrashFolder($this->getCompanyID());
		return $this->getFolderID() == $trashdir->getID() ? true : false;
	}
	function getTempPath(){
		$tempdir = BASE_PATH.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$this->getFolder()->getHomeFolder()->getName().DIRECTORY_SEPARATOR."temp";
		return $tempdir;
	}
	function getTempFilePath(){
		return $this->getTempPath().DIRECTORY_SEPARATOR.$this->getFileName();
	}
	function getName(){
		return $this->getFilename();
	}
	function getArchiveAbsoluteUrl(){
		$path = stripURL($this->getAbsolutePath()).DIRECTORY_SEPARATOR."Archive";
		return $path;
	}
	function hasArchiveOnFileSystem(){
		return is_dir($this->getArchiveAbsoluteUrl()) ? true : false;
	}
	function hasArchiveOnDB(){
		return !isEmptyString($this->getArchiveFolder()->getID()) ? true : false;
	}
	function getArchiveFolder(){
		$q = Doctrine_Query::create()->from('Folder f')->where("f.parentid = '".$this->getFolderID()."' AND f.name = 'archive' ");
		$result = $q->fetchOne();
		if(!$result){
			return new Folder();
		}
		// debugMessage($result->toArray());
		return $result;
	}
	/* function getAttributes(){
		$q = Doctrine_Query::create()->from('FileAttribute f')->where("f.fileid = '".$this->getID()."' ")->orderby("f.attr asc");  debugMessage($q->getSqlQuery());
		//$result = $q->execute();
	
		return '';
	} */
}
?>