<?php
/**
 * A collection of a set of permissions resources
 * 
 */
class AclGroup extends BaseEntity implements Zend_Acl_Role_Interface {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		$this->setTableName('aclgroup');
		$this->hasColumn('name', 'string', 50, array('notblank' => true));
		$this->hasColumn('description', 'string', 255);
		$this->hasColumn('companyid', null, array('default' => NULL));
		$this->hasColumn('parentid', null, array('default' => NULL));
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(array(
       									//"name.unique" => "Role name already exists. Please specify another",
       									"name.notblank" => "Please enter Name"
       								)); 
	}
	/**
	 * Setup the permissions for the ACLGroup class
	 */
	public function setUp() {
		parent::setUp();
		// the permissions for the group
		$this->hasMany('AclPermission as permissions', 
						array('local' => 'id', 
								'foreign' => 'groupid',
						));		
		$this->hasOne('Company as company', array('local' => 'companyid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "Role <b>".$this->getName()."</b> already exists. Please specify another.");
		}
	}
	# determine if the name has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM aclgroup WHERE name = '".$name."' AND name <> '' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/**
	 * @see Zend_Acl_Role_Interface::getRoleId()
	 *
	 * @return string The ID of the role
	 */
	public function getRoleId() {
		return $this->getID();
	}
	/**
	 * Clean out the data received from the screen by:
	 * - remove empty/blank groupid  - the groupid is not required and therefore is an empty value is maintained will cause an out of range exception 
	 *
	 * @param Array $post_array
	 */
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); 
		// check if the groupid is blank then remove it
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues); 
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		# add log to audit trail
		
		$usecase = '0.4';
		$module = '0';
		$type = SYSTEM_CREATEROLE;
		$details = "Role (".$this->getName().") created";
			
		$audit_values = getAudienceTypes();
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['status'] = "Y";
		$audit_values['transactiondetails'] = $details;

		// debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
	}
}
?>
