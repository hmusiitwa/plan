<?php

class ContentComment extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_comment');
		
		$this->hasColumn('riskid', 'integer', null, array('default' => NULL));
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('type', 'string', 4, array('default' => 1)); // 1=Risk, 2
		$this->hasColumn('status', 'string', 4, array('default' => 1));
		$this->hasColumn('section', 'string', 4, array('default' => 1));
		$this->hasColumn('inputdate', 'string', 25);
		$this->hasColumn('contents', 'string', 10000);
		$this->hasColumn('files', 'string', 500);
		$this->hasColumn('subject', 'string', 255, array('default' => null));
		$this->hasColumn('notification', 'string', 10000, array('default' => null));
		$this->hasColumn('emails', 'string', 500, array('default' => null));
		
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as user', array('local' => 'userid', 'foreign' => 'id'));
		$this->hasOne('ContentRisk as risk', array('local' => 'riskid', 'foreign' => 'id'));
		$this->hasOne('MilestoneProgress as progress', array('local' => 'progressid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('inputdate', $formvalues)){
			// unset($formvalues['inputdate']);
			$formvalues['inputdate'] = date('Y-m-d H:i:s', time());
		} else {
			$formvalues['inputdate'] = date('Y-m-d H:i:s', strtotime($formvalues['inputdate']));
		}
		
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		return true;
	}
	
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		return true;
	}
}
?>