<?php

class Project extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('project');
		$this->hasColumn('title', 'string', 255, array('notblank' => true));
		$this->hasColumn('abbrv', 'string', 25);
		$this->hasColumn('description', 'string', 10000);
		$this->hasColumn('refno', 'string', 25);
		// $this->hasColumn('projectno', 'string', 50);
		$this->hasColumn('type', 'integer', null, array('default' => NULL));
		$this->hasColumn('category', 'integer', null, array('default' => NULL));
		$this->hasColumn('status', 'integer', null, array('default' => 1)); // 0-Draft, 1-Approved, 4-Cancelled, 5-Completed, 2-Submitted for Review				0	0
		$this->hasColumn('phasestatus', 'integer', null, array('default' => 1)); // 0-Not started, 2-Planning Phase, 1-Implementation Phase, 3-Monitoring & Review Phase, 4-Closure Phase				0	0
		$this->hasColumn('programid', 'string', 25, array('default' => NULL));
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		//$this->hasColumn('fyear', 'integer', null, array('default' => NULL));
		$this->hasColumn('ownerid', 'integer', null, array('default' => NULL));
		$this->hasColumn('cost', 'string', 20, array('default' => NULL));
		$this->hasColumn('currency', 'string', 4, array('default' => 'EUR'));
		$this->hasColumn('folderid', 'integer', 11, array('default' => NULL));
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as owner', array('local' => 'ownerid', 'foreign' => 'id'));
		$this->hasOne('Folder as folder', array('local' => 'folderid', 'foreign' => 'id'));
		$this->hasMany('ProjectOutline as projectoutline', array('local' => 'id', 'foreign' => 'projectid'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("project_title.unique", "Project <b>".$this->getName()."</b> already exists. Please specify another.");
		}
		if($this->refnoExists()){
			$this->getErrorStack()->add("project_refno.unique", "Project No# ".$this->getRefNo()." already exists for another project. Please specify another.");
		}
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE title = '".$name."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	function refnoExists($number =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($number)){
			$number = $this->getRefNo();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE refno = '".$number."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); // exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('category', $formvalues)){
			unset($formvalues['category']);
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('phasestatus', $formvalues)){
			unset($formvalues['phasestatus']);
		}
		if(isArrayKeyAnEmptyString('fyear', $formvalues)){
			unset($formvalues['fyear']);
		}
		if(isArrayKeyAnEmptyString('ownerid', $formvalues)){
			unset($formvalues['ownerid']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			if(isArrayKeyAnEmptyString('startdate_old', $formvalues)){
				unset($formvalues['startdate']);
			} else {
				$formvalues['startdate'] = NULL;
			}
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			if(isArrayKeyAnEmptyString('enddate_old', $formvalues)){
				unset($formvalues['enddate']);
			} else {
				$formvalues['enddate'] = NULL;
			}
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}
		if(!isArrayKeyAnEmptyString('programid', $formvalues)) {
			if(is_array($formvalues['programid'])){
				$formvalues['programid'] = implode(',', $formvalues['programid']);
			}
		} else {
			if(!isArrayKeyAnEmptyString('programid_old', $formvalues)){
				$formvalues['programid'] = NULL;
			} else {
				unset($formvalues['programid']);
			}
		}
		
		$details = array();
		if(!isArrayKeyAnEmptyString('details', $formvalues)){
			foreach ($formvalues['details'] as $key => $value){
				// debugMessage($value);
				if(!isArrayKeyAnEmptyString('refno', $value)){
					if(!isArrayKeyAnEmptyString('id', $formvalues)){
						$details[$key]['projectid'] = $formvalues['id'];
					}
					if(!isArrayKeyAnEmptyString('id', $value)){
						$details[$key]['id'] = $value['id'];
					}
					$details[$key]['refno'] = $value['refno'];
					if(!isArrayKeyAnEmptyString('startdate', $value)){
						$details[$key]['startdate'] = date('Y-m-d', strtotime($value['startdate']));
					} else {
						if(!isArrayKeyAnEmptyString('startdate_old_'.$key, $value)){
							$details[$key]['startdate'] = NULL;
						}
					}
					if(!isArrayKeyAnEmptyString('enddate', $value)){
						$details[$key]['enddate'] = date('Y-m-d', strtotime($value['enddate']));
					} else {
						if(!isArrayKeyAnEmptyString('enddate_old_'.$key, $value)){
							$details[$key]['enddate'] = NULL;
						}
					}
					if(!isArrayKeyAnEmptyString('program_unit_'.$key, $formvalues)){
						$details[$key]['program_unit'] = $formvalues['program_unit_'.$key];
					} else {
						if(!isArrayKeyAnEmptyString('program_unit_old_'.$key, $value)){
							$details[$key]['program_unit'] = NULL;
						}
					}
					if(!isArrayKeyAnEmptyString('program_locations_ids_'.$key, $formvalues)){
						$details[$key]['program_locations'] = implode(',', $formvalues['program_locations_ids_'.$key]);
					} else {
						if(!isArrayKeyAnEmptyString('program_locations_old_'.$key, $value)){
							$details[$key]['program_locations'] = NULL;
						}
					}
					if(!isArrayKeyAnEmptyString('ownerid_'.$key, $formvalues)){
						$details[$key]['ownerid'] = $formvalues['ownerid_'.$key];
					} else {
						if(!isArrayKeyAnEmptyString('ownerid_old_'.$key, $value)){
							$details[$key]['ownerid'] = NULL;
						}
					}
					if(!isArrayKeyAnEmptyString('cost', $value)){
						$details[$key]['cost'] = $value['cost'];
					}
				}
				unset($formvalues['program_unit_'.$key]);
				unset($formvalues['program_locations_ids_'.$key]);
				unset($formvalues['ownerid_'.$key]);
			}
			unset($formvalues['details']);
		}
		if(count($details) > 0){
			$formvalues['projectoutline'] = $details;
		}
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		
		$this->setupLibrary();
		
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		
		$this->setupLibrary();
		
		return true;
	}
	function setupLibrary(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		// debugMessage($this->toArray());
		
		if(isEmptyString($this->getFolderID())){
			$folder = new Folder();
			// debugMessage('setting folder now');
			$data = array(
					"name" => $this->getName(),
					"parentid" => PROJECT_DIR,
					"refno" => $this->getRefno(),
					"createdby" => getUserID()
			);
				
			$existingquery = "select id from folder where parentid = '".PROJECT_DIR."' AND (name = '".$this->getName()."' OR refno = '".$this->getrefno()."') ";
			$existingid = $conn->fetchOne($existingquery);
			$update = false;
			if(!isEmptyString($existingid)){
				$folder->populate($existingid);
				$oldfolder = new Folder();
				$oldfolder->populate($existingid);
				$data['previousname'] = $oldfolder->getName();
				$data["lastupdatedby"] = getUserID();
				$update = true;
			}
			// debugMessage($data);
				
			$folder->processPost($data);
			/* debugMessage('folder err '.$folder->getErrorStackAsString());
			 debugMessage($folder->toArray()); */ // exit;
			if(!$folder->hasError()){
				if(!$update){
					$folder->save();
					$folder->afterSave();
				} else {
					$folder->beforeUpdate();
					$folder->save();
					$folder->afterUpdate();
				}
				$this->setFolderID($folder->getID());
				try {
					$this->save();
				} catch (Exception $e) {
					debugMessage('err saving '.$e->getMessage());
				}
			}
		}
		// exit();
		
		return true;
	}
	
	function getName(){
		return $this->getTitle();
	}
	function getUnitIDs(){
		$conn = Doctrine_Manager::connection();
		$query = "select group_concat(distinct(p.program_unit) separator ',') as unitids from project_outline p where p.projectid = '".$this->getID()."' ";
		$unitids = $conn->fetchOne($query);
		return $unitids;
	}
	function getUnitsLabel($unitids =''){
		$progunits = ''; $progunits_array = array(); 
		$program_units = getProgramUnits();
		if(isEmptyString($unitids)){
			$conn = Doctrine_Manager::connection();
			$query = "select group_concat(distinct(p.program_unit) separator ',') as unitids from project_outline p where p.projectid = '".$this->getID()."' ";
			$unitids = $conn->fetchOne($query);
		}
		if(!isEmptyString($unitids)){
			$units_array = array_remove_empty(explode(',', str_replace(' ', '', $unitids))); // debugMessage($units_array);
			foreach($units_array as $unitid){
				if(!isArrayKeyAnEmptyString($unitid, $program_units)){
					$progunits_array[] = $program_units[$unitid];
				}
			}
			$progunits = createHTMLCommaListFromArray($progunits_array, ', ');
		}
		return $progunits;
	}
	function getLocationsLabel(){
		$locations = getDistricts();
		$proglocations = ''; $proglocations_array = array();
		if(!isEmptyString($this->getprogram_locations())){
			$district_array = array_remove_empty(explode(',', str_replace(' ', '', $this->getprogram_locations()))); // debugMessage($units_array);
			foreach($district_array as $did){
				if(!isArrayKeyAnEmptyString($did, $locations)){
					$proglocations_array[] = $locations[$did];
				}
			}
			$proglocations = createHTMLCommaListFromArray($proglocations_array, ', ');
		}
		return $proglocations;
	}
	function getProjectDetails(){
		$q = Doctrine_Query::create()->from('ProjectOutline p')->where("p.projectid = '".$this->getID()."'")->orderby('p.id');
		
		$result = $q->execute();
		return $result;
	}
	function getTypeLabel(){
		return $this->getType() == '2' ? 'SPAD No#' : 'FAD No#';
	}
}
?>