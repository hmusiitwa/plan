<?php

class Activity extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('activity');
		$this->hasColumn('title', 'string', 255, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15, array('notblank' => true));
		$this->hasColumn('description', 'string', 10000);
		
		$this->hasColumn('projectid','integer', null, array('default' => NULL));
		$this->hasColumn('outcomeid', 'integer', null, array('default' => NULL));
		$this->hasColumn('outputid', 'string', 255);
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('puid', 'string', 255);
		
		$this->hasColumn('status', 'integer', null, array('default' => NULL)); // 0-Draft, 1-Submitted, 2-Approved, 3-InProgress, 4-Cancelled, 5-Completed	utf8	utf8_general_ci		0	0
		$this->hasColumn('type', 'integer', null, array('default' => NULL));
		$this->hasColumn('category', 'integer', null, array('default' => NULL));
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"title.notblank" => "Please enter Activity Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('Activity as parent', array('local' => 'parentid', 'foreign' => 'id'));
		$this->hasOne('ContentFrame as outcome', array('local' => 'outcomeid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->refnoExists()){
			$this->getErrorStack()->add("refno.unique", "Entry with Code <b>".$this->getRefno()."</b> already exists. Please specify another.");
		}
		/* if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "Entry with name <b>".$this->getName()."</b> already exists. Please specify another.");
		} */
	}
	# determine if the username has already been assigned
	function refnoExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM ".$this->getTableName()." WHERE refno = '".$this->getRefNo()."' AND refno is not null AND projectid = '".$this->getProjectID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		$conn = Doctrine_Manager::connection();
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('category', $formvalues)){
			unset($formvalues['category']);
		}
		if(isArrayKeyAnEmptyString('id', $formvalues)){
			$formvalues['createdby'] = getUserID();
		}
		if(!isArrayKeyAnEmptyString('activitytype', $formvalues)){
			$formvalues['type'] = $formvalues['activitytype'];
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				// $formvalues['refno'] = NULL;
			}
		}
		if(!isArrayKeyAnEmptyString('outputids', $formvalues)){
			$formvalues['outputid'] = implode(',', $formvalues['outputids']);
		} else {
			if(!isArrayKeyAnEmptyString('outputids_old', $formvalues)){
				$formvalues['outputid'] = NULL;
			} else {
				unset($formvalues['outputid']);
			}
		}
		if(!isArrayKeyAnEmptyString('puids', $formvalues)){
			$formvalues['puid'] = implode(',', $formvalues['puids']);
		} else {
			if(!isArrayKeyAnEmptyString('puid_old', $formvalues)){
				$formvalues['puid'] = NULL;
			} else {
				unset($formvalues['puid']);
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function getName(){
		return $this->getTitle();
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		$resave = false;
		
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
		
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("A".($this->getID() + 1000));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
		
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
		$conn = Doctrine_Manager::connection();
		
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
		
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("A".($this->getID() + 1000));
			$resave = true;
		}
		if($resave){
			# initial save
			$this->save();
		}
		return true;
	}
	# find duplicates after save
	function getDuplicates(){
		$session = SessionWrapper::getInstance();
		$q = Doctrine_Query::create()->from('Activity q')->where("q.title = '".$this->getTitle()."' AND q.projectid = '".$this->getProjectID()."' ")->orderby("q.id");
	
		$result = $q->execute();
		return $result;
	}
}
?>