<?php

class Indicator extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('indicator');
		
		$this->hasColumn('title', 'string', 255, array('notblank' => true));
		$this->hasColumn('description', 'string', 10000);
		$this->hasColumn('refno', 'string', 25);
		$this->hasColumn('programid', 'integer', 10, array('default' => NULL));
		$this->hasColumn('projectid', 'integer', 10, array('default' => NULL));
		$this->hasColumn('contentid', 'integer', 10, array('default' => NULL));
		$this->hasColumn('parentid', 'integer', 10, array('default' => NULL));
		$this->hasColumn('type', 'integer', null, array('default' => 1));
		$this->hasColumn('level', 'integer', null, array('default' => NULL));
		$this->hasColumn('indicatortype', 50, array('default' => NULL));
		$this->hasColumn('uom', 'string', 50, array('default' => NULL));
		$this->hasColumn('weight', 'string', 10, array('default' => NULL));
		
		$this->hasColumn('ownertype', 'string', 4, array('default' => NULL));
		$this->hasColumn('userid', 'integer', null, array('default' => null));
		$this->hasColumn('contactid', 'integer', null, array('default' => null));
		$this->hasColumn('ownername', 'string', 255, array('default' => NULL));
		$this->hasColumn('frequency', 'string', 10, array('default' => NULL));
		$this->hasColumn('assumptions', 'string', 255, array('default' => NULL));
		$this->hasColumn('mov', 'string', 10, array('default' => NULL));
		$this->hasColumn('movdetails', 'string', 500, array('default' => NULL));
		$this->hasColumn('decimalpts', 'string', 10, array('default' => NULL));
		$this->hasColumn('categoryoption', 'string', 500, array('default' => NULL));
		$this->hasColumn('attributegroup', 'string', 500, array('default' => NULL));
		$this->hasColumn('numerator', 'string', 10000, array('default' => NULL));
		$this->hasColumn('numeratortitle', 'string', 255, array('default' => NULL));
		$this->hasColumn('numeratordetails', 'string', 10000, array('default' => NULL));
		$this->hasColumn('denominator', 'string', 10000, array('default' => NULL));
		$this->hasColumn('denominatortitle', 'string', 255, array('default' => NULL));
		$this->hasColumn('denominatordetails', 'string', 10000, array('default' => NULL));
		$this->hasColumn('baseline', 'string', 1000);
		$this->hasColumn('basemilestoneid', 'integer', null, array('default' => NULL));
		$this->hasColumn('csindicators', 'string', 255);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// $this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"title.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Program as program', array('local' => 'programid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('ContentFrame as frame', array('local' => 'contentid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as owner', array('local' => 'userid', 'foreign' => 'id'));
		$this->hasOne('Contact as contact', array('local' => 'contactid', 'foreign' => 'id'));
		$this->hasOne('Milestone as milestone', array('local' => 'basemilestoneid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		/* if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "Indicator with name <b>".$this->getName()."</b> already exists. Please specify another.");
		}
		if($this->refnoExists()){
			$this->getErrorStack()->add("refno.unique", "Indicator with Code <b>".$this->getRefno()."</b> already exists. Please specify another.");
		} */
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE title = '".$name."' AND refno = '".$this->getRefno()."' AND type = '".$this->getType()."' AND projectid = '".$this->getProjectID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	function refnoExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM ".$this->getTableName()." WHERE refno = '".$this->getRefNo()."' AND type = '".$this->getType()."' AND projectid = '".$this->getProjectID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				$formvalues['refno'] = NULL;
			}
		}
		if(isArrayKeyAnEmptyString('contentid', $formvalues)){
			unset($formvalues['contentid']);
		}
		if(isArrayKeyAnEmptyString('basemilestoneid', $formvalues)){
			unset($formvalues['basemilestoneid']);
		}
		if(isArrayKeyAnEmptyString('programid', $formvalues)){
			unset($formvalues['programid']);
		}
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(!isArrayKeyAnEmptyString('view', $formvalues)){
			if($formvalues['view'] == 'obj' || $formvalues['view'] == 'objective'){
				if(isArrayKeyAnEmptyString('programid', $formvalues)){
					$formvalues['programid'] = isArrayKeyAnEmptyString('contentid', $formvalues) ? NULL : $formvalues['contentid']; 
					$formvalues['contentid'] = NULL;
					$formvalues['level'] = 1;
				}
			}
		}
		if(isArrayKeyAnEmptyString('userid', $formvalues)){
			unset($formvalues['userid']);
		}
		if(isArrayKeyAnEmptyString('contactid', $formvalues)){
			unset($formvalues['contactid']);
		}
		if(isArrayKeyAnEmptyString('parentid', $formvalues)){
			unset($formvalues['parentid']);
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('level', $formvalues)){
			unset($formvalues['level']);
		}
		if(isArrayKeyAnEmptyString('uom', $formvalues)){
			unset($formvalues['uom']);
		}
		if(isArrayKeyAnEmptyString('indicatortype', $formvalues)){
			unset($formvalues['indicatortype']);
		}
		if(isArrayKeyAnEmptyString('weight', $formvalues)){
			unset($formvalues['weight']);
		}
		if(!isArrayKeyAnEmptyString('numerator', $formvalues)){
			stripMultipleSpaces($formvalues['numerator']);
		}
		if(!isArrayKeyAnEmptyString('numeratordetails', $formvalues)){
			stripMultipleSpaces($formvalues['numeratordetails']);
		}
		if(!isArrayKeyAnEmptyString('denominator', $formvalues)){
			stripMultipleSpaces($formvalues['denominator']);
		}
		if(!isArrayKeyAnEmptyString('denominatordetails', $formvalues)){
			stripMultipleSpaces($formvalues['denominatordetails']);
		}
		if(!isArrayKeyAnEmptyString('indicatorids', $formvalues)){
			$formvalues['csindicators'] = implode(',', $formvalues['indicatorids']);
			unset($formvalues['indicatorids']);
		} else {
			if(!isArrayKeyAnEmptyString('csindicators_old', $formvalues)){
				$formvalues['csindicators'] = NULL;
			} else {
				unset($formvalues['csindicators']);
			}
		}
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	
	function getTargets($milestoneid=''){
		$customquery = "";
		if(!isEmptyString($milestoneid)){
			$customquery .= " AND m.milestoneid = '".$milestoneid."' ";
		}
		$orderq = "m.datecreated desc";
		$q = Doctrine_Query::create()->from('ContentTarget m')->innerjoin('m.indicator i')->where("m.indicatorid = '".$this->getID()."' ".$customquery)->orderby($orderq);
		// debugMessage($q->getSqlQuery());
		$result = $q->execute();
		return $result;
	}
	function beforeSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if($this->getLevel() == 1 AND !isEmptyString($this->getProgramID()) AND isEmptyString($this->getProjectID())){
			$this->setContentID(NULL);
			$resave = true;
		}
	
		return true;
	}
	function beforeUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if($this->getLevel() == 1 AND !isEmptyString($this->getProgramID()) AND isEmptyString($this->getProjectID())){
			$this->setContentID(NULL);
			$resave = true;
		}
	
		return true;
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
		
		$conn = Doctrine_Manager::connection();
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
		
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("IND-".($this->getID() + 1000));
			$resave = true;
		}
		if($this->getLevel() == 1 AND !isEmptyString($this->getProgramID()) AND isEmptyString($this->getProjectID())){
			$this->setContentID(NULL);
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("IND-".($this->getID() + 1000));
			$resave = true;
		}
		if($this->getLevel() == 1 AND !isEmptyString($this->getProgramID()) AND isEmptyString($this->getProjectID())){
			$this->setContentID(NULL);
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function getName() {
		return $this->getTitle();
	}
	# find duplicates after save
	function getDuplicates(){
		$session = SessionWrapper::getInstance();
		$q = Doctrine_Query::create()->from('Indicator q')->where("q.title = '".$this->getName()."'  AND q.refno = '".$this->getRefno()."' AND q.type = '".$this->getType()."' AND q.projectid = '".$this->getProjectID()."' ")->orderby("q.id");
	
		$result = $q->execute();
		return $result;
	}
	function isQuantitative(){
		return $this->getIndicatorType() != '6' ? true : false;
	}
	function isQualitative(){
		return $this->getIndicatorType() == '6' ? true : false;
	}
	function getIndicatorLevel(){
		switch($this->getlevel()){
			case 1:
				$level = 'Objective';
				break;
			case 2:
				$level = 'Outcome';
				break;
			case 3:
				$level = 'Output';
				break;
			default:
				$level = '';
				break;
		}
		
		return $level;
	}
}
?>