<?php
/**
 * Model for contract
 */
class Share extends BaseEntity {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('share');
		$this->hasColumn('companyid', 'integer', null, array('notblank' => true));
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('url', 'string', 255);
		$this->hasColumn('name', 'string', 255, array('notblank' => true));
		$this->hasColumn('type', 'integer', null, array('default' => NULL));
		$this->hasColumn('folderid', 'integer', null, array('default' => NULL));
		$this->hasColumn('fileid', 'integer', null, array('default' => NULL));
		$this->hasColumn('filelist', 'string', 255);
		$this->hasColumn('dirlist', 'string', 255);
		$this->hasColumn('downloadlimit', 'string', 255);
		$this->hasColumn('status', 'integer', null, array('default' => 1));
		$this->hasColumn('startdate', 'date', null, array('default' => NULL));
		$this->hasColumn('enddate', 'date', null, array('default' => NULL));
		$this->hasColumn('issecured', 'string', 25);
		$this->hasColumn('password', 'string', 50);
		$this->hasColumn('downloadlimit', 'string', 25);
		$this->hasColumn('browsesubfoler', 'string', 25);
		$this->hasColumn('accesstype', 'string', 25, array('default' => '1')); // 1=List & Download, 2=List, upload & download, 3=Upload only
		$this->hasColumn('canaddfolder', 'string', 15, array('default' => '0'));
		$this->hasColumn('canupload', 'string', 15, array('default' => '0'));
		$this->hasColumn('canread', 'string', 15, array('default' => '1'));
		$this->hasColumn('canlist', 'string', 15, array('default' => '1'));
		$this->hasColumn('candelete', 'string', 15, array('default' => '0'));
		$this->hasColumn('notes', 'string', 1000);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"name.notblank" => $this->translate->_("share_name_error"),
										"companyid.notblank" => $this->translate->_("share_companyid_error")
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Company as company',
				array(
						'local' => 'companyid',
						'foreign' => 'id'
				)
		);
		$this->hasOne('Folder as folder',
				array(
						'local' => 'folderid',
						'foreign' => 'id'
				)
		);
		$this->hasOne('File as file',
				array(
						'local' => 'fileid',
						'foreign' => 'id'
				)
		);
		
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues);
		if(isArrayKeyAnEmptyString('userid', $formvalues)){
			unset($formvalues['userid']);
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('folderid', $formvalues)){
			unset($formvalues['folderid']);
		}
		if(isArrayKeyAnEmptyString('fileid', $formvalues)){
			unset($formvalues['fileid']);
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		}
		if(isArrayKeyAnEmptyString('accesstype', $formvalues)){
			unset($formvalues['accesstype']);
		} else {
			if($formvalues['accesstype'] == 1){
				$formvalues['canupload'] = 0;
				$formvalues['canread'] = 1;
				$formvalues['canlist'] = 1;
			}
			if($formvalues['accesstype'] == 2){
				$formvalues['canupload'] = 1;
				$formvalues['canread'] = 1;
				$formvalues['canlist'] = 1;
			}
			if($formvalues['accesstype'] == 3){
				$formvalues['canupload'] = 1;
				$formvalues['canread'] = 0;
				$formvalues['canlist'] = 0;
			}
		}
		if(isArrayKeyAnEmptyString('canaddfolder', $formvalues)){
			if(!isArrayKeyAnEmptyString('canaddfolder_old', $formvalues)){
				if($formvalues['canaddfolder_old'] == 1){
					$formvalues['canaddfolder'] = 0;
				} else {
					unset($formvalues['canaddfolder']);
				}
			} else {
				unset($formvalues['canaddfolder']);
			}
		}
		if(isArrayKeyAnEmptyString('browsesubfoler', $formvalues)){
			if(!isArrayKeyAnEmptyString('browsesubfoler_old', $formvalues)){
				if($formvalues['browsesubfoler_old'] == 1){
					$formvalues['browsesubfoler'] = 0;
				} else {
					unset($formvalues['browsesubfoler']);
				}
			} else {
				unset($formvalues['browsesubfoler']);
			}
		}
		if(isArrayKeyAnEmptyString('browsesubfoler', $formvalues)){
			unset($formvalues['browsesubfoler']);
		}
		if(isArrayKeyAnEmptyString('canread', $formvalues)){
			unset($formvalues['canread']);
		}
		if(isArrayKeyAnEmptyString('canupload', $formvalues)){
			unset($formvalues['canupload']);
		}
		if(isArrayKeyAnEmptyString('canlist', $formvalues)){
			unset($formvalues['canlist']);
		}
		if(isArrayKeyAnEmptyString('candelete', $formvalues)){
			unset($formvalues['candelete']);
		}
		
		/* debugMessage($formvalues); exit; */
		parent::processPost($formvalues);
	}
}
?>