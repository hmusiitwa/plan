<?php
/**
 * Model for company
 */
class Company extends BaseEntity {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('company');
		# override the not null and not blank properties for the createdby column in the BaseEntity
		$this->hasColumn('createdby', 'integer', 11, array('default' => NULL));
		$this->hasColumn('name', 'string', 255, array('notblank' => true));
		$this->hasColumn('status', 'integer', null, array('default' => NULL)); // 0=>'Incomplete','2'=>'Pending Approval','1'=>'Approved', '6'=>'Rejected', '3'=>'Queried', '4'=>'Expired', 5=>'Terminated', // 6*Renewed(eventtype)
		$this->hasColumn('abbrv', 'string', 15);
		$this->hasColumn('refno', 'string', 25);
		$this->hasColumn('username', 'string', 25);
		
		$this->hasColumn('contactperson', 'string', 255);
		$this->hasColumn('email', 'string', 255);
		$this->hasColumn('phone', 'string', 15);
		$this->hasColumn('fax', 'string', 15);
		$this->hasColumn('country', 'string', 2, array('default' => 'ZA'));
		$this->hasColumn('addressline1', 'string', 255);
		$this->hasColumn('addressline2', 'string', 255);
		$this->hasColumn('city', 'string', 255);
		$this->hasColumn('postalcode', 'string', 10);
		$this->hasColumn('description', 'string', 1000);
		$this->hasColumn('startdate','date', null, array('default' => NULL));
		$this->hasColumn('enddate','date', null, array('default' => NULL));
		
		// $this->hasColumn('logo', 'string', 50);
		$this->hasColumn('website', 'string', 255);
		$this->hasColumn('description', 'string', 1000);
		$this->hasColumn('notes', 'string', 1000);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"name.notblank" => $this->translate->_("company_name_error")
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as invitedby',
				array(
						'local' => 'invitedbyid',
						'foreign' => 'id'
				)
		);
		
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "The name <b>".$this->getName()."</b> already exists. Please specify another.");
		}
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// debugMessage(decode($formvalues[URL_SUCCESS]));
		// trim spaces from the name field
		if(!isArrayKeyAnEmptyString('phone', $formvalues)){
			if(strlen($formvalues['phone']) == 10){
				$formvalues['phone'] = getFullPhone($formvalues['phone']);
			}
		}
		if(!isArrayKeyAnEmptyString('phone2', $formvalues)){
			if(strlen($formvalues['phone2']) == 10){
				$formvalues['phone2'] = getFullPhone($formvalues['phone2']);
			}
		}
		if(!isArrayKeyAnEmptyString('fax', $formvalues)){
			if(strlen($formvalues['fax']) == 10){
				$formvalues['fax'] = getFullPhone($formvalues['fax']);
			}
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(!isArrayKeyAnEmptyString('currentstatus', $formvalues)){
			$formvalues['status'] = $formvalues['currentstatus'];
		}
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	/*
	 * Custom save logic
	*/
	function afterSave(){
		$session = SessionWrapper::getInstance();
		
		return true;
	}
	# update after
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		
		return true;
	}
	# determine if the name has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM company WHERE name = '".$name."' AND name <> '' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	# determine full address
	function getFullAddress(){
		$str = '';
		if(!isEmptyString($this->getAddressLine1())){
			$str .= nl2br($this->getAddressLine1());
		}
		if(!isEmptyString($this->getAddressLine2())){
			$str .= '<br>'.nl2br($this->getAddressLine2());
		}
		return $str;
	}
	function getCountryName() {
		if(isEmptyString($this->getCountry())){
			return "--";
		}
		if($this->getCountry() == 'UG'){
			return "Uganda";
		}
		$countries = getCountries();
		return $countries[$this->getCountry()];
	}
	
	# relative path to profile image
	function hasProfileImage(){
		$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."company".DIRECTORY_SEPARATOR."comp_";
		$real_path = $real_path.$this->getID().DIRECTORY_SEPARATOR."logo".DIRECTORY_SEPARATOR.$this->getLogo();
		if(file_exists($real_path) && !isEmptyString($this->getLogo())){
			return true;
		}
		return false;
	}
	
	# determine path to medium profile picture
	function getPicturePath() {
		$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
		$path = "";
		$path = $baseUrl.'/images/logo.png';
		if($this->hasProfileImage()){
			$path = $baseUrl.'/uploads/company/comp_'.$this->getID().'/logo/'.$this->getLogo();
		}
		// debugMessage($path);
		return $path;
	}
	
	function getLargePicturePath() {
		$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
		$path = "";
		$path = $baseUrl.'/images/logo.png';
		if($this->hasProfileImage()){
			$path = $baseUrl.'/uploads/company/comp_'.$this->getID().'/logo/large_'.$this->getLogo();
		}
		// debugMessage($path);
		return $path;
	}
}
?>