<?php

class Milestone extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('milestone');
		
		$this->hasColumn('programid', 'integer', null, array('default' => NULL));
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('type', 'string', 4);
		$this->hasColumn('level', 'string', 10);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('fyear', 'string', 10);
		$this->hasColumn('status', 'string', 10);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// $this->addDateFields(array("startdate","enddate"));
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"programid.notblank" => "Please specify Objective"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("milestone.unique", "Milestone with name <b>".$this->getName()."</b> already exists. Please specify another.");
		}
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE name = '".$name."'AND programid = '".$this->getProgramID()."' AND level = '".$this->getLevel()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
	
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		return true;
	}
}
?>