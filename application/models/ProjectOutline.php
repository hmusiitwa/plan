<?php

class ProjectOutline extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('project_outline');
		$this->hasColumn('projectid', 'integer', null, array('default' => NULL));
		$this->hasColumn('refno', 'string', 25, array('notblank' => true));
		$this->hasColumn('program_unit', 'string', 25, array('default' => NULL));
		$this->hasColumn('ownerid', 'integer', null, array('default' => NULL));
		$this->hasColumn('program_locations', 'string', 25, array('default' => NULL));
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		$this->hasColumn('cost', 'string', 20, array('default' => NULL));
		$this->hasColumn('currency', 'string', 4, array('default' => 'EUR'));
		$this->hasColumn('description', 'string', 10000);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"refno.notblank" => "Please enter Project No#"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('ProgramUnit as programunit', array('local' => 'program_unit', 'foreign' => 'id'));
		$this->hasOne('UserAccount as owner', array('local' => 'ownerid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		
		if(isArrayKeyAnEmptyString('program_unit', $formvalues)){
			if(isArrayKeyAnEmptyString('program_unit_old', $formvalues)){
				unset($formvalues['program_unit']);
			} else {
				$formvalues['program_unit'] = NULL;
			}
		}
		if(isArrayKeyAnEmptyString('ownerid', $formvalues)){
			if(isArrayKeyAnEmptyString('ownerid_old', $formvalues)){
				unset($formvalues['ownerid']);
			} else {
				$formvalues['ownerid'] = NULL;
			}
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			if(isArrayKeyAnEmptyString('startdate_old', $formvalues)){
				unset($formvalues['startdate']);
			} else {
				$formvalues['startdate'] = NULL;
			}
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			if(isArrayKeyAnEmptyString('enddate_old', $formvalues)){
				unset($formvalues['enddate']);
			} else {
				$formvalues['enddate'] = NULL;
			}
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}
		if(isArrayKeyAnEmptyString('cost', $formvalues)){
			if(isArrayKeyAnEmptyString('cost_old', $formvalues)){
				unset($formvalues['cost']);
			} else {
				$formvalues['cost'] = NULL;
			}
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
}
?>