<?php
/**
 * Model for results details
 */
class UserCategory extends BaseEntity  {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('user_category_group');
		
		$this->hasColumn('type', 'string', 4, array('default' => NULL));
		$this->hasColumn('name', 'string', 255, array('notblank' => true));
		$this->hasColumn('userids', 'string', 1000, array('notblank' => true));
		$this->hasColumn('projectid', 'integer', null, array('default' => NULL));
		$this->hasColumn('description', 'string', 255);
		$this->hasColumn('permissions', 'string', 255);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"name.notblank" => "Please enter group name",
										"userids.notblank" => "Please specify atleast one user"
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "Group <b>".$this->getname()."</b> already exists. Please specify another.");
		}
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(!isArrayKeyAnEmptyString('to_hidden', $formvalues)){
			$to = str_replace(' ', '', trim($formvalues['to_hidden']));
			unset($formvalues['poptionlist']);
			unset($formvalues['to_hidden']);
			$array_ids = array_unique(array_remove_empty(explode(',', $to)));
			$formvalues['userids'] = implode(',', $array_ids);
			if(isEmptyString($formvalues['userids'])){
				$formvalues['userids'] = NULL;
			}
		} else {
			if(!isArrayKeyAnEmptyString('userids_old', $formvalues)){
				$formvalues['userids'] = NULL;
			} else {
				unset($formvalues['userids']);
			}
				
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM user_category_group WHERE name = '".$name."' AND type = '".$this->getType()."' AND name <> '' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
}
?>