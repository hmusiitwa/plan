<?php
/**
 * Model for company details
 */
class Document extends BaseRecord {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('document');
		$this->hasColumn('refid', 'integer', null, array('default' => NULL));
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('type', 'integer', null, array('notblank' => true, 'default' => 1));
		/* 1=Document, 2=Text */
		$this->hasColumn('title', 'string', 255);
		$this->hasColumn('description', 'string', 65535);
		$this->hasColumn('filename', 'string', 255, array('notblank' => true));
		$this->hasColumn('oldfilename', 'string', 255);
		$this->hasColumn('path', 'string', 1024);
		$this->hasColumn('uploaddate', 'date', null, array('default' => NULL));
		$this->hasColumn('uploadedby', 'integer', null, array('default' => NULL));
		$this->hasColumn('status', 'string', 4);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("uploaddate"));
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"type.notblank" => $this->translate->_("document_type_error"),
										"filename.notblank" => $this->translate->_("document_filename_error")
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasMany('UserAccount as user',
				array(
						'local' => 'id',
						'foreign' => 'userid'
				)
		);
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues);
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('refid', $formvalues)){
			unset($formvalues['refid']);
		}
		if(isArrayKeyAnEmptyString('userid', $formvalues)){
			unset($formvalues['userid']);
		}
		if(isArrayKeyAnEmptyString('id', $formvalues)){
			if(isArrayKeyAnEmptyString('uploaddate', $formvalues)){
				$formvalues['uploaddate'] = date('Y-m-d');
			}
			if(isArrayKeyAnEmptyString('uploadedby', $formvalues)){
				$formvalues['uploadedby'] = $session->getVar('userid');
			}
		}
		// debugMessage($formvalues); // exit;
		parent::processPost($formvalues);
	}
	
	# relative path to file
	function hasFile(){
		$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
		$real_path .= DIRECTORY_SEPARATOR.$this->getFilename();
		if(file_exists($real_path) && !isEmptyString($this->getFilename())){
			return true;
		}
		return false;
	}
	
	# determine path to file
	function getFilePath() {
		$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
		$path = "";
		if($this->hasFile()){
			$path = '';
		}
		// debugMessage($path);
		return $path;
	}
	function getAbsolutePath() {
		$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
		$path = "";
		if($this->hasFile()){
			$path = '';
		}
		// debugMessage($path);
		return $path;
	}
	# determine translation for document type
	function getDocTypeText(){
		if(isEmptyString($this->getDoctype())){
			return "";
		}
		$formatedtypes = getDatavariables("DOCUMENT_TYPES");
		if(!isArrayKeyAnEmptyString($this->getDocType(), $formatedtypes)){
        	return $formatedtypes[$this->getDocType()];
		}
		// debugMessage($documenttypes);
		return "";
	}
	# after save
	function afterSave($savetoaudit = true){
		$session = SessionWrapper::getInstance();
		# check if customer is being invited during update
	
		# add log to audit trail
		$view = new Zend_View();
		$url = '';
		$profiletype = 'Document';
		$usecase = '6.1';
		$module = '6';
		$type = DOCUMENT_UPLOAD;
	
		$browser = new Browser();
		$audit_values = $session->getVar('browseraudit');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['entityid'] = '';
		$audit_values['transactiondetails'] = $profiletype." with title [".$this->getTitle()."] successfully uploaded ";
		$audit_values['url'] = $url;
		// $this->notify(new sfEvent($this, $type, $audit_values));

		return true;
	}
	# store object before save
	function beforeUpdate(){
		$session = SessionWrapper::getInstance();
		# set object data to class variable before update
		$doc = new Document();
		$doc->populate($this->getID());
		$this->setPreUpdateData($doc->toArray()); // debugMessage($customer->toArray());
		// exit;
		return true;
	}
	# update after
	function afterUpdate($savetoaudit = true){
		$session = SessionWrapper::getInstance();
		
		# set postupdate from class object, and then save to audit
		$prejson = json_encode($this->getPreUpdateData()); // debugMessage($prejson);
			
		$this->clearRelated(); // clear any current object relations
		$after = $this->toArray(); // debugMessage($after);
		$postjson = json_encode($after); // debugMessage($postjson);
			
		// $diff = array_diff($prejson, $postjson);  // debugMessage($diff);
		$diff = array_diff($this->getPreUpdateData(), $after);
		$jsondiff = json_encode($diff); // debugMessage($jsondiff);
			
		$view = new Zend_View();
		$url = "";
		$profiletype = 'Document';
		$usecase = '6.2';
		$module = '6';
		$type = DOCUMENT_UPDATE;
			
		$browser = new Browser();
		$audit_values = $session->getVar('browseraudit');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['transactiondetails'] = $profiletype." with title [".$this->getTitle()."] successfully updated ";;
		
		$audit_values['isupdate'] = 1;
		$audit_values['prejson'] = $prejson;
		$audit_values['postjson'] = $postjson;
		$audit_values['jsondiff'] = $jsondiff;
		$audit_values['url'] = $url;
		$audit_values['entityid'] = '';
		// debugMessage($audit_values);
		// $this->notify(new sfEvent($this, $type, $audit_values));
		
		return true;
	}
}

?>