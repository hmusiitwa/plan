<?php

class MilestonePeriod extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_milestone');
		
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('type', 'string', 4);
		$this->hasColumn('category', 'string', 10);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		$this->hasColumn('fyear', 'string', 10);
		$this->hasColumn('parentid', 'string', 10);
		$this->hasColumn('status', 'string', 10);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"type.notblank" => "Please specify Type"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("milestone.unique", "Milestone with name <b>".$this->getName()."</b> already exists. Please specify another.");
		}
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE name = '".$name."' AND category = '".$this->getCategory()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("M-".($this->getID()));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("M-".($this->getID()));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
}
?>