<?php
/**
 * Model for results details
 */
class DatasetResultDetail extends BaseRecord  {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('data_set_result_detail');
		
		$this->hasColumn('resultid', 'integer', null, array('default' => NULL));
		$this->hasColumn('datasetid', 'integer', null, array('notblank' => true));
		$this->hasColumn('elementid', 'integer', null, array('notblank' => true));
		$this->hasColumn('optionids', 'string', 255);
		$this->hasColumn('result', 'string', 65535);
		$this->hasColumn('isbatch', 'string', 4);
		$this->hasColumn('lineid', 'string', 15);
		$this->hasColumn('type', 'string', 15);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"datasetid.notblank" => "Please specify result dataset"
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('DatasetResult as details', array('local' => 'resultid', 'foreign' => 'id'));
		$this->hasOne('Dataset as dataset', array('local' => 'datasetid','foreign' => 'id'));
		$this->hasOne('Attribute as element', array('local' => 'elementid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('optionids', $formvalues)){
			if(!isArrayKeyAnEmptyString('optionids_old', $formvalues)){
				$formvalues['optionids'] = NULL;
			} else {
				unset($formvalues['optionids']);
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>