<?php 

class Folder extends BaseEntity {
	
    public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		$this->setTableName('folder');
        
		$this->hasColumn('companyid', 'integer', null, array('notblank' => true, 'default' => getCompanyID()));
        $this->hasColumn('name', 'string', 255, array('notblank' => true));
        $this->hasColumn('refno', 'string', 25);
        $this->hasColumn('description', 'string', 500);
        $this->hasColumn('parentid', 'integer', null, array('default' => NULL));
        $this->hasColumn('restoreid', 'integer', null, array('default' => NULL));
        $this->hasColumn('path', 'string', 50);
        $this->hasColumn('tags', 'string', 500);
        $this->hasColumn('refurl', 'string', 500);
        
        # override the not null and not blank properties for the createdby column in the BaseEntity
        $this->hasColumn('createdby', 'integer', 11, array('default' => NULL));
    }
    
    protected $previousname;
    protected $openfolder;
    function getPreviousName(){
    	return $this->previousname;
    }
    function setPreviousName($previousname) {
    	$this->previousname = $previousname;
    }
    function getOpenFolder(){
    	return $this->openfolder;
    }
    function setOpenFolder($openfolder) {
    	$this->openfolder = $openfolder;
    }
    protected $preupdatedata;
    protected $controller;
    function getPreUpdateData(){
    	return $this->preupdatedata;
    }
    function setPreUpdateData($preupdatedata) {
    	$this->preupdatedata = $preupdatedata;
    }
    function getController(){
    	return $this->controller;
    }
    function setController($controller) {
    	$this->controller = $controller;
    }
	/**
    * Contructor method for custom functionality - add the fields to be marked as dates
    */
	public function construct() {
		parent::construct();
       
        // set the custom error messages
        $this->addCustomErrorMessages(array(
       									"name.notblank" => $this->translate->_("folder_name_error"),
        								"name.length" => sprintf($this->translate->_("folder_name_length_error"), 50),
       								));
    }
    
    function setUp() {
    	parent::setUp();
    	# relationship to link a document to a matter
    	# Link to the matter
    	$this->hasOne('Company as company',
    			array(
    					'local' => 'companyid',
    					'foreign' => 'id'
    			)
    	);
		$this->hasOne('Folder as parent',
						 array(
								'local' => 'parentid',
								'foreign' => 'id'
							)
					);
		$this->hasOne('Folder as restore',
				array(
						'local' => 'restoreid',
						'foreign' => 'id'
				)
		);
		$this->hasMany('File as files', 
								array(
									'local' => 'id',
									'foreign' => 'folderid',
								)
						); 
		$this->hasMany('FolderAttribute as attributelist',
				array(
						'local' => 'id',
						'foreign' => 'folderid',
				)
		);
    }
	/*
	 * Custom validation
	 */
	function validate() {
		parent::validate();
		
		$conn = Doctrine_Manager::connection();
		
		# Unique folder name
		$unique_query = "SELECT id FROM folder WHERE companyid = '".$this->getCompanyID()."' AND name = '".$this->getName()."' AND parentid = '".$this->getParentID()."' AND id <> '".$this->getID()."' ";
		# debugMessage($unique_query);
		$result = $conn->fetchOne($unique_query);
		if(!isEmptyString($result)){ 
			$this->getErrorStack()->add("folder.unique",  "Folder <b>".$this->getName()."</b> already exists in directory <b>".$this->getParent()->getRelativePathFromHome()."</b>");
		}
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues){
		# force setting of default none string column values. enum, int and date 	
		if(isArrayKeyAnEmptyString('parentid', $formvalues)){
			unset($formvalues['parentid']); 
		}
		if(!isArrayKeyAnEmptyString('name', $formvalues)){
			$formvalues['name'] = stripMultipleSpaces(trim($formvalues['name']));
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			$formvalues['refno'] = stripMultipleSpaces(trim($formvalues['refno']));
		}
		if(!isArrayKeyAnEmptyString('previousname', $formvalues)){
			$this->setPreviousName($formvalues['previousname']);
		}
		if(isArrayKeyAnEmptyString('restoreid', $formvalues)){
			unset($formvalues['restoreid']); 
		}
		if(isArrayKeyAnEmptyString('createdby', $formvalues)){
			unset($formvalues['createdby']);
		}
		if(!isArrayKeyAnEmptyString('openfolder', $formvalues)){
			if($formvalues['openfolder'] == "1"){
				$this->setOpenFolder('1');
			}
		}
		if(!isArrayKeyAnEmptyString('name', $formvalues)){
			$formvalues['name'] = trim($formvalues['name']);
		}
		if(!isArrayKeyAnEmptyString('tags', $formvalues)){
			$formvalues['tags'] = stripMultipleSpaces(trim($formvalues['tags']));
		}
		if(!isArrayKeyAnEmptyString('attributelist', $formvalues)){
			foreach ($formvalues['attributelist'] as $key => $value){
				if(isArrayKeyAnEmptyString('attr', $value) || isArrayKeyAnEmptyString('value', $value)){
					unset($formvalues['attributelist'][$key]);
				}
			}
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	/**
	 * @see BaseRecord::afterSave()
	 */
	function afterSave() {
		$session = SessionWrapper::getInstance();
		# update path for created folder
		if(!isEmptyString($this->getParentID())){
			if(!isEmptyString($this->getParent()->getPath())){
				$pathstring .= $this->getParent()->getPath();
			}
			if($this->getParent()->isHome()){
				$pathstring .= $this->getParentID();
			} else {
				$pathstring .= '/'.$this->getParentID();
			}
			
			$this->setPath($pathstring);
			$this->save();
		}
		# set new successurl folder if specified
		if(!isEmptyString($this->getOpenFolder())){
			$session->setVar('gotofolder', $this->getID());
		}
		# create the folder
		if(!$this->folderExists()){
			mkdir($this->getAbsolutePath(), 0777);
		}
		
		# add log to audit trail
		$view = new Zend_View();
		// $controller = $this->getController();
		$url = $view->baseUrl('library/index/parentid/'.encode($this->getID()));
		$usecase = '10.1';
		$module = '10';
		$type = PATH_CREATE;
			
		$audit_values = getAuditInstance();
		$audit_values['companyid'] = $session->getVar('companyid');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$transactiondate = date("Y-m-d H:i:s");
		$audit_values['transactiondate'] = $transactiondate;
		$audit_values['status'] = "Y";
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['transactiondetails'] = 'Created Folder: <i>'.$this->getRelativePathFromHome().'</i>';
		$audit_values['url'] = $url;
		$audit_values['prejson'] = $this->getRelativePathFromHome();
		//debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		// exit;
		
		$duplicateaudits = $this->getDuplicateAudits($type, $transactiondate);
		if($duplicateaudits->count() > 1){
			$duplicateaudits->get(0)->delete();
		}
		
		return true;
	}
	# update after
	function beforeUpdate(){
		$session = SessionWrapper::getInstance();
		# set object data to class variable before update
		$folder = new Folder();
		$folder->populate($this->getID());
		$this->setPreUpdateData($folder->toArray());
		// exit;
		return true;
	}
	function afterUpdate($savetoaudit = true) {
		$session = SessionWrapper::getInstance();
		// check if name has changed to update the filesystem
		if($this->getPreviousName() != $this->getName()){
			$prevname = stripUrl($this->getParent()->getAbsolutePath()).DIRECTORY_SEPARATOR.$this->getPreviousName(); // debugMessage($prevname);
			$newname = $this->getAbsolutePath(); // debugMessage($newname);
			rename($prevname, $newname);
		}
		
		if($savetoaudit){
			# set postupdate from class object, and then save to audit
			$prejson = json_encode($this->getPreUpdateData()); // debugMessage($prejson);
			 
			$this->clearRelated(); // clear any current object relations
			$after = $this->toArray(); // debugMessage($after);
			$postjson = json_encode($after); // debugMessage($postjson);
			 
			// $diff = array_diff($prejson, $postjson);  // debugMessage($diff);
			$diff = array_diff($this->getPreUpdateData(), $after);
			$jsondiff = json_encode($diff); // debugMessage($jsondiff);
			 
			$view = new Zend_View();
			$controller = $this->getController();
			$url = $view->baseUrl('library/index/parentid/'.encode($this->getID()));
			$profiletype = 'Updated Folder: <i>'.$this->getRelativePathFromHome().'</i>';
			$usecase = '2.2';
			$module = '2';
			$type = PATH_UPDATE;
			 
			$audit_values = getAuditInstance();
			$audit_values['companyid'] = $session->getVar('companyid');
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['status'] = "Y";
			$audit_values['userid'] = $session->getVar('userid');
			$transactiondate = date("Y-m-d H:i:s");
			$audit_values['transactiondate'] = $transactiondate;
			$audit_values['transactiondetails'] = $profiletype;
			$audit_values['isupdate'] = 1;
			$audit_values['prejson'] = $prejson;
			$audit_values['postjson'] = $postjson;
			$audit_values['jsondiff'] = $jsondiff;
			$audit_values['url'] = $url;
			// debugMessage($audit_values);
			$this->notify(new sfEvent($this, $type, $audit_values));
			
			$duplicateaudits = $this->getDuplicateAudits($type, $transactiondate);
			if($duplicateaudits->count() > 1){
				$duplicateaudits->get(0)->delete();
			}
		}
		// exit;
		return true;
	}
	
	function getDuplicateAudits($type, $transactiondate){
		$q = Doctrine_Query::create()->from('AuditTrail a')->where("a.transactiontype = '".$type."' AND a.transactiondate = '".$transactiondate."' AND a.id <> '".$this->getID()."' ");
		$result = $q->execute();
		return $result;
	}
	
	function getFolderName(){
		$name = $this->getName();
		if(isEmptyString($this->getParentID())){
			$name = getLibDir();
		}
		return $name;
	}
	function getHomeFolder($companyid ='', $defaultid = ''){
		if(isEmptyString($companyid) && !isEmptyString($this->getCompanyID())){
			$companyid = $this->getCompanyID();
		} else {
			$companyid = getCompanyID();
		}
		$q = Doctrine_Query::create()->from('Folder f')->where("f.companyid = '".$companyid."' AND f.parentid is null AND (f.path is not null OR f.path <> '') ");
		$result = $q->fetchOne();
		/* if($result){
			if($result->getID() != $defaultid){
				$defaultfolder = new Folder();
			}
		} */
		// debugMessage($result->toArray());
		return $result;
	}
	function getHomeFolderID($companyid =''){
		if(isEmptyString($companyid) && !isEmptyString($this->getCompanyID())){
			$companyid = $this->getCompanyID();
		}
		$q = Doctrine_Query::create()->from('Folder f')->where("f.companyid = '".$companyid."' AND f.parentid is null AND (f.path is not null OR f.path <> '') ");
		$result = $q->fetchOne();
		// debugMessage($result->toArray());
		return $result->getID();
	}
	function getTrashFolder($companyid =''){
		if(isEmptyString($companyid) && !isEmptyString($this->getCompanyID())){
			$companyid = $this->getCompanyID();
		}
		$q = Doctrine_Query::create()->from('Folder f')->where("f.companyid = '".$companyid."' AND f.parentid is null AND (f.path is null OR f.path = '') ");
		$result = $q->fetchOne();
		// debugMessage($result->toArray());
		return $result;
	}
	function getArchiveFolder(){
		$q = Doctrine_Query::create()->from('Folder f')->where("f.parentid = '".$this->getParentID()."' AND f.name = 'archive' ");
		$result = $q->fetchOne();
		if(!$result){
			return new Folder();
		}
		// debugMessage($result->toArray());
		return $result;
	}
	function getTrashAbsoluteUrl(){
		$basepath = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
		$home = $this->getHomeFolder($this->getCompanyID())->getName();
		$basepath = $basepath.$home.DIRECTORY_SEPARATOR."Trash";
		return $basepath;
	}
	function isHome(){
		return isEmptyString($this->getParentID()) && $this->getPath() == '/' ? true : false;
	}
	function isInHome(){
		return $this->getParent()->isHome() ? true : false;
	}
	function isTrash(){
		return isEmptyString($this->getParentID()) && isEmptyString($this->getPath()) ? true : false;
	}
	function isTrashed(){
		$trashdir = $this->getTrashFolder($this->getCompanyID());
		return $this->getParentID() == $trashdir->getID() ? true : false;
	}
	function getArchiveAbsoluteUrl(){
		$path = stripURL($this->getAbsolutePath()).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Archive";
		return $path;
	}
	function hasArchiveOnFileSystem(){
		return is_dir($this->getArchiveAbsoluteUrl()) ? true : false;
	}
	function hasArchiveOnDB(){
		return !isEmptyString($this->getArchiveFolder()->getID()) ? true : false;
	}
	# absolute path to folder containing file
	function getAbsolutePath(){
		$basepath = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
		if($this->isHome()){
			return $basepath.$this->getHomeFolder()->getName().DIRECTORY_SEPARATOR;
		}
		if($this->isTrash()){
			return $basepath.$this->getHomeFolder()->getName().DIRECTORY_SEPARATOR."Trash".DIRECTORY_SEPARATOR;
		}
		$pathstr = stripUrl($this->getPath());
		$pathidsarr = array_remove_empty(explode('/', $pathstr));
		$pathtoroot = ''; $fnames = array();
		foreach($pathidsarr as $i => $fid){
			$fold = new Folder();
			$fold->populate($fid);
			$pathtoroot .= $fold->getName().DIRECTORY_SEPARATOR;
		}
		$pathtoroot .= $this->getName().DIRECTORY_SEPARATOR;
		// debugMessage($pathtoroot);
		
		return $basepath.$pathtoroot;
	}
	# relative path
	function getRelativePathFromHome(){
		$pathidsarr = array_remove_empty(explode('/', stripUrl($this->getPath())));
		// debugMessage($pathidsarr);
		$viewpath = '';
		if($this->isHome()){
			$viewpath .= '/'.getLibDir().' ';
		} else {
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				if($fold->isHome()){
					$viewpath .= '/'.getLibDir().' ';
				} else {
					$viewpath .= ' /'.$fold->getName();
				}
			}
			$viewpath .= ' /'.$this->getName();
		}
		return stripUrl($viewpath);
	}
	function getRelativePathWithLinks($type = 'library', $userid = ''){
		$view = new Zend_View();
		$session = SessionWrapper::getInstance();
		if(isEmptyString($userid)){
			$userid = $session->getVar('userid');
		}
		$pathstr = 'library/index/parentid/';
		if($type == 'access'){
			$pathstr = 'access/index/userid/'.$userid.'/id/';
		}
		$pathidsarr = array_remove_empty(explode('/', stripUrl($this->getPath())));
		// debugMessage($pathidsarr);
		$viewpath = '';
		if($this->isHome()){
			$viewpath .= '/<a href="'.$view->baseUrl($pathstr.encode($this->getHomeFolderID())).'">'.getLibDir().'</a> ';
		} else {
			foreach($pathidsarr as $i => $fid){
				$fold = new Folder();
				$fold->populate($fid);
				if($fold->isHome()){
					$viewpath .= '/<a href="'.$view->baseUrl($pathstr.encode($this->getHomeFolderID())).'">'.getLibDir().'</a> ';
				} else {
					$viewpath .= ' /<a href="'.$view->baseUrl($pathstr.encode($fold->getID())).'">'.$fold->getName().'</a>';
				}
			}
			$viewpath .= ' /'.$this->getName();
		}
		return stripUrl($viewpath);
	}
	function folderExists(){
		if(isEmptyString($this->getID())){
			return false;
		}
		return is_dir($this->getAbsolutePath()) ? true : false;
	}
	function isFolder(){
		return true;
	}
	function isFile(){
		return false;
	}
	function getTotalFolderSize(){
		return getFolderSize($this->getAbsolutePath());
	}
	function getFolders($companyid ='', $parentid = ''){
		if(isEmptyString($companyid) && !isEmptyString($this->getCompanyID())){
			$companyid = $this->getCompanyID();
		}
		if(isEmptyString($parentid)){
			$parentid = $this->getID();
		}
		$q = Doctrine_Query::create()->from('Folder f')->where("f.companyid = '".$companyid."' AND f.parentid = '".$parentid."' ")->orderby("f.name asc"); // debugMessage($q->getSqlQuery());
		$result = $q->execute();
		
		return $result;
	}
	
	function getSystemStructure($currentid =''){
		// debugMessage($system);
		return $this->getTree($currentid);
	}
	function getTree($current = ''){
		$conn = Doctrine_Manager::connection();
		$rows = $conn->fetchAll("select t.id, t.parentid, t.name as text from folder t where t.parentid <> '' order by t.parentid "); // debugMessage($rows);
		$tree = $this->buildTree($rows, $this->getID(), $current); // debugMessage($tree);
		$data = array('id' => $this->getID(), 'text' => $this->getFolderName(), 'state' => array('opened' => true, 'selected' => true), 'children' => $tree);
		return $data;
	}
	function buildTree(array $elements, $parentId = 1, $currentid = '') {
		$branch = array();
	
		foreach ($elements as $element) {
			if ($element['parentid'] == $parentId) {
				$children = $this->buildTree($elements, $element['id']);
				if ($children) {
					$element['children'] = $children;
				}
				if($element['id'] == $currentid){
					$element['state'] = array('disabled' => true);
				}
				$branch[] = $element;
			}
		}
	
		return $branch;
	}
	function getFoldersOutsideScope($fid, $data){
		if(isEmptyString($fid)){
			return $data;
		} else {
			return $this->getFoldersInsideScope();
		}
	}
	function getFoldersInsideScope($fid, $data){
		if(isEmptyString($fid)){
			return $data;
		} else {
			return $this->getFoldersInsideScope();
		}
	}
	function getImmediateChildren(){
		$session = SessionWrapper::getInstance();
		$f = new Folder();
		$id = $this->getID();
		if(isEmptyString($id)){
			$id = $f->getHomeFolderID($session->getVar('userid'));
		}
		$q = Doctrine_Query::create()->from('Folder f')->where("f.parentid = '".$id."' ");
		$result = $q->execute();
		return $result;
	}
}
?>