<?php

class ProjectPartner extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('project_partner');
		$this->hasColumn('projectid', 'integer', null, array('default' => NULL));
		$this->hasColumn('directoryid', 'integer', null, array('default' => NULL));
		$this->hasColumn('role', 'string', 50);
		$this->hasColumn('description', 'string', 255);
		$this->hasColumn('permissions', 'string', 255);
	}
	public function setUp() {
		parent::setUp();
		$this->hasOne('Contact as directory', array('local' => 'directoryid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('directoryid', $formvalues)){
			unset($formvalues['directoryid']);
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>