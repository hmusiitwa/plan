<?php

class IndicatorGroup extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('indicator_group');
		
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('indicatorids', 'string', 500);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
		$this->getErrorStack()->add("name.unique", "Group with name <b>".$this->getName()."</b> already exists. Please specify another.");
	}
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE name = '".$name."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(!isArrayKeyAnEmptyString('to', $formvalues)){
			unset($formvalues['optionlist']);
			$formvalues['indicatorids'] = implode(',', array_remove_empty(array_unique($formvalues['to'])));
		} else {
			$formvalues['indicatorids'] = NULL;
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				$formvalues['refno'] = NULL;
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
		
		$conn = Doctrine_Manager::connection();
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
		
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("IG-".($this->getID() + 100));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("IG-".($this->getID() + 100));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	# find duplicates after save
	function getDuplicates(){
		$session = SessionWrapper::getInstance();
		$q = Doctrine_Query::create()->from('IndicatorGroup q')->where("q.name = '".$this->getName()."'  ")->orderby("q.id");
	
		$result = $q->execute();
		return $result;
	}
}
?>