<?php

class DatasetResultProjectCost extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('data_set_result_projectcost');
		$this->hasColumn('resultid', 'integer', null, array('default' => NULL));
		$this->hasColumn('datasetid', 'integer', null, array('default' => NULL));
		$this->hasColumn('projectid', 'integer', null, array('default' => NULL));
		$this->hasColumn('poid', 'integer', null, array('default' => NULL));
		$this->hasColumn('currency', 'string', 4, array('default' => 'EUR'));
		$this->hasColumn('annualbudget', 'string', 15, array('default' => NULL));
		$this->hasColumn('periodbudget', 'string', 15, array('default' => NULL));
		$this->hasColumn('cost_1', 'string', 15, array('default' => NULL));
		$this->hasColumn('cost_2', 'string', 15, array('default' => NULL));
		$this->hasColumn('cost_3', 'string', 15, array('default' => NULL));
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('DatasetResult as details', array('local' => 'resultid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('ProgramUnit as programunit', array('local' => 'poid', 'foreign' => 'id'));
		$this->hasOne('Dataset as dataset', array('local' => 'datasetid', 'foreign' => 'id'));
		$this->hasOne('Milestone as milestone', array('local' => 'milestoneid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
}
?>