<?php

class Contact extends BaseEntity {
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('directory');
		$this->hasColumn('typeids', 'string', 50, array('default' => NULL)); 
		// 1 farmer, 2 supplier, 3 local govt, 4 naads staff, 5 maaif, 6 farm group, 7 other
		$this->hasColumn('contacttype', 'integer', array('notblank' => true, 'default' => NULL));
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('categoryid', 'integer', null, array('default' => NULL));
		$this->hasColumn('category', 'string', 255);
		$this->hasColumn('department', 'string', 255);
		
		$this->hasColumn('firstname', 'string', 100);
		$this->hasColumn('lastname', 'string', 100);
		$this->hasColumn('orgname', 'string', 100);
		$this->hasColumn('contactperson', 'string', 255);
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('idtype', 'string', 4, array('default' => '1'));
		$this->hasColumn('idnumber', 'string', 25);
		$this->hasColumn('title', 'string', 255);
		$this->hasColumn('gender', 'integer', null, array('default' => NULL)); # 1=Male, 2=Female, 3=Unknown
		$this->hasColumn('phone', 'string', 25);
		$this->hasColumn('phone2', 'string', 25);
		$this->hasColumn('email', 'string', 255);
		$this->hasColumn('email2', 'string', 255);
		
		$this->hasColumn('address', 'string', 255);
		$this->hasColumn('website', 'string', 500);
		$this->hasColumn('country', 'string', 2, array('default' => 'UG'));
		$this->hasColumn('districtid', 'integer', null, array('default' => NULL));
		$this->hasColumn('countyid', 'integer', null, array('default' => NULL));
		$this->hasColumn('subcountyid', 'integer', null, array('default' => NULL));
		$this->hasColumn('parishid', 'integer', null, array('default' => NULL));
		$this->hasColumn('villageid', 'integer', null, array('default' => NULL));
		$this->hasColumn('city', 'string', 50);
		$this->hasColumn('town', 'string', 50);
		$this->hasColumn('postalcode', 'string', 50);
		$this->hasColumn('gpslat', 'string', 15);
		$this->hasColumn('gpslng', 'string', 15);
		
		$this->hasColumn('notes', 'string', 1000);
		$this->hasColumn('description', 'string', 1000);
		$this->hasColumn('services', 'service', 1000);	
		$this->hasColumn('tags', 'service', 1000);	
		$this->hasColumn('profilephoto', 'string', 255);
		
		$this->hasColumn('dateofbirth',  'string', 50, array('default' => NULL));
		$this->hasColumn('birthtype',  'string', 4, array('default' => NULL));
		$this->hasColumn('age',  'string', 4, array('default' => NULL));
		$this->hasColumn('nextofkin_name', 'string', 255);
		$this->hasColumn('nextofkin_phone', 'string', 50);
		$this->hasColumn('nextofkin_email', 'string', 50);
		
		$this->hasColumn('status', 'integer', null, array('default' => 1)); //
		$this->hasColumn('regdate', 'date', null, array('default' => NULL));
		$this->hasColumn('startdate', 'date', null, array('default' => NULL));
		$this->hasColumn('enddate', 'date', null, array('default' => NULL));
		$this->hasColumn('profiledbyid', 'string', 255);
		
		$this->hasColumn('noofmembers', 'string', 10);
		$this->hasColumn('noofmale', 'string', 10);
		$this->hasColumn('nooffemale', 'string', 10);
		$this->hasColumn('noofchildren', 'string', 10);
		$this->hasColumn('totalhousehold', 'string', 10);
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('groupid', 'integer', null, array('default' => NULL));
		$this->hasColumn('group', 'string', 50);
		$this->hasColumn('fyear', 'string', 25);
		$this->hasColumn('department', 'string', 50);
		
	}
	
	protected $contactname;
	function getContactName(){
		return $this->contactname;
	}
	function setContactName($contactname) {
		$this->contactname = $contactname;
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// define the date fields
		$this->addDateFields(array("regdate"));
		// set the default contract type
		// set the custom error messages
       	$this->addCustomErrorMessages(array(
       									"contacttype.notblank" => "Please select a Type"								
       	       						));
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as user', array('local' => 'userid', 'foreign' => 'id')); 
		$this->hasOne('Location as district', array('local' => 'districtid','foreign' => 'id')); 
		$this->hasOne('Location as county', array('local' => 'countyid', 'foreign' => 'id')); 
		$this->hasOne('Location as subcounty', array('local' => 'subcountyid', 'foreign' => 'id')); 
		$this->hasOne('Location as parish', array('local' => 'parishid', 'foreign' => 'id'));
		$this->hasOne('Location as village', array('local' => 'villageid', 'foreign' => 'id'));
		$this->hasOne('Contact as contact', array('local' => 'parentid', 'foreign' => 'id'));
	}
	/*
	 * 
	 */
	function validate() {
		parent::validate();
		
		$conn = Doctrine_Manager::connection();
		
		# Validate that the contact firstname,lastname,othernames are unique for person contacttype
		if($this->getContactType() == "1"){
			# Check that the firstname been specified
			if(isEmptyString($this->getFirstName())){
				$this->getErrorStack()->add("firstname.notblank", "Please enter First Name");
			}
			# Check that the lastname been specified
			if(isEmptyString($this->getLastName())){
				$this->getErrorStack()->add("lastname.notblank", "Please enter Last Name");
			}

			# Validate that the person firstname, lastname and othernames are unique for person contacttype 
			$person_query = "SELECT id FROM directory WHERE firstname = '".$this->getFirstName()."' AND lastname = '".$this->getLastName()."' AND districtid = '".$this->getDistrictID()."' AND id <> '".$this->getID()."' ";		
			$person_result = $conn->fetchOne($person_query);
			if(!isEmptyString($person_result)){ 
				$this->getErrorStack()->add("directory.unique", "Contact with Name ".$this->getName()." already exists");
			}
		}
		
		# Custom validation for Company contact type 
		if($this->getContactType() == "2"){
			# Check that the company name has been specified
			if(isEmptyString($this->getOrgName())){
				$this->getErrorStack()->add("orgname.notblank", "Please enter Organization Name");
			}
			
			# Validate that the company name is unique for company contacttype
			$company_query = "SELECT id FROM directory WHERE orgname = '".$this->getOrgName()."' AND contacttype = '2'  AND districtid = '".$this->getDistrictID()."' AND id <> '".$this->getID()."'";
			$company_result = $conn->fetchOne($company_query);
			if(!isEmptyString($company_result)){ 
				$this->getErrorStack()->add("company.unique",  "Contact with Name ".$this->getName()." already exists");
			}
		}
		
		// check that either phone or email provided
		if(isEmptyString($this->getPhone()) && isEmptyString($this->getEmail())){
			$this->getErrorStack()->add("contact.notblank", "Please specify atleast one Contact (Phone or Email)");
		}
		
		# validate that phone is unique
		if(!isEmptyString($this->getPhone()) && $this->phoneExists()){
			$this->getErrorStack()->add("phone.unique", 'Profile with Phone '.$this->getPhone().' already exists for '.$this->getContactName());
		}
		# validate that refno is unique
		if(!isEmptyString($this->getRefNo()) && $this->refExists()){
			$this->getErrorStack()->add("refno.unique", 'Profile with ID '.$this->getRefNo().' already exists for '.$this->getContactName());
		}
	}
	/*
	 * 
	 */
	function processPost($formvalues){
		// check if the locationid is specified
		if(isArrayKeyAnEmptyString('userid', $formvalues)){
			unset($formvalues['userid']); 
		}
		if(isArrayKeyAnEmptyString('parentid', $formvalues)){
			unset($formvalues['parentid']);
		}
		if (isArrayKeyAnEmptyString('categoryid', $formvalues)) {
			unset($formvalues['categoryid']);
		}
		if (isArrayKeyAnEmptyString('profiledbyid', $formvalues)) {
			unset($formvalues['profiledbyid']);
		}
		if (isArrayKeyAnEmptyString('profiledbyid_x', $formvalues)) {
			unset($formvalues['profiledbyid_x']);
		} else {
			$formvalues['profiledbyid'] = $formvalues['profiledbyid_x'];
		}
		if(isArrayKeyAnEmptyString('districtid', $formvalues)){
			if(!isArrayKeyAnEmptyString('districtid_old', $formvalues)){
				$formvalues['districtid'] = NULL; 
			} else {
				unset($formvalues['districtid']); 
			}
		}
		if(!isArrayKeyAnEmptyString('districtid_x', $formvalues) && isArrayKeyAnEmptyString('districtid', $formvalues)){
			$formvalues['districtid'] = $formvalues['districtid_x'];
		}
		if(isArrayKeyAnEmptyString('countyid', $formvalues)){
			if(!isArrayKeyAnEmptyString('countyid_old', $formvalues)){
				$formvalues['countyid'] = NULL; 
			} else {
				unset($formvalues['countyid']); 
			}
		}
		if(isArrayKeyAnEmptyString('subcountyid', $formvalues)){
			if(!isArrayKeyAnEmptyString('subcountyid_old', $formvalues)){
				$formvalues['subcountyid'] = NULL; 
			} else {
				unset($formvalues['subcountyid']); 
			} 
		}
		if(isArrayKeyAnEmptyString('parishid', $formvalues)){
			if(!isArrayKeyAnEmptyString('parishid_old', $formvalues)){
				$formvalues['parishid'] = NULL; 
			} else {
				unset($formvalues['parishid']); 
			}
		}
		if(isArrayKeyAnEmptyString('villageid', $formvalues)){
			if(!isArrayKeyAnEmptyString('villageid_old', $formvalues)){
				$formvalues['villageid'] = NULL; 
			} else {
				unset($formvalues['villageid']); 
			}
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				$formvalues['refno'] = '';
			}
		}
		if(isArrayKeyAnEmptyString('gender', $formvalues)){
			unset($formvalues['gender']); 
		}
		if(!isArrayKeyAnEmptyString('age', $formvalues)){
			if(isArrayKeyAnEmptyString('dateofbirth', $formvalues)){
				$formvalues['dateofbirth'] = date("Y-m-d", strtotime(date("Y-m-d") . " - ".$formvalues['age']." years "));
				$formvalues['dateofbirth'] = getFirstDayOfMonth('1', date('Y', strtotime($formvalues['dateofbirth'])));
			}
		}
		if(!isArrayKeyAnEmptyString('firstname', $formvalues)) {
			$formvalues['firstname'] = ucwords(strtolower(stripMultipleSpaces($formvalues['firstname'])));
		}
		if(!isArrayKeyAnEmptyString('lastname', $formvalues)) {
			$formvalues['lastname'] = ucwords(stripMultipleSpaces(trim($formvalues['lastname'])));
		}
		if(!isArrayKeyAnEmptyString('orgname', $formvalues)) {
			$formvalues['orgname'] = ucwords(stripMultipleSpaces(trim($formvalues['orgname'])));
		}
		if(isArrayKeyAnEmptyString('regdate', $formvalues)) {
			$formvalues['regdate'] = date('Y-m-d');
		} else {
			$formvalues['regdate'] = date('Y-m-d', strtotime($formvalues['regdate']));
		}
		if(!isArrayKeyAnEmptyString('dateofbirth', $formvalues)) {
			$formvalues['dateofbirth'] = date('Y-m-d', strtotime($formvalues['dateofbirth']));
		}
		if(!isArrayKeyAnEmptyString('contactphoto', $formvalues)) {
			$formvalues['profilephoto'] = $formvalues['contactphoto'];
		}
		if(isArrayKeyAnEmptyString('country', $formvalues)){
			unset($formvalues['country']);
		}
		
		// debugMessage($formvalues['country']);
		if(!isArrayKeyAnEmptyString('phone', $formvalues)){
			if(stringContains('-', $formvalues['phone'])){
				$formvalues['phone'] = str_replace('-', '', $formvalues['phone']);
			}
		}
		if(!isArrayKeyAnEmptyString('phone2', $formvalues)){
			if(stringContains('-', $formvalues['phone2'])){
				$formvalues['phone2'] = str_replace('-', '', $formvalues['phone2']);
			}
		}
		if(!isArrayKeyAnEmptyString('nextofkin_phone', $formvalues)){
			if(stringContains('-', $formvalues['nextofkin_phone'])){
				$formvalues['nextofkin_phone'] = str_replace('-', '', $formvalues['nextofkin_phone']);
			}
		}
		if(!isArrayKeyAnEmptyString('categoryids', $formvalues)){
			$formvalues['category'] = implode(',', $formvalues['categoryids']);
		}
		debugMessage($formvalues); // exit();		
		parent::processPost($formvalues);
	}
	# determine if phone number has already been assigned 
	function phoneExists($phone =''){
		$conn = Doctrine_Manager::connection();
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query_custom = '';
		if(isEmptyString($phone)){
			$phone = $this->getPhone();
		}
	
		# unique phone
		$phone_query = "SELECT id FROM directory WHERE phone = '".$phone."' AND typeids = '".$this->getTypeIDs()."' ".$id_check;
		// debugMessage($phone_query);
		$result = $conn->fetchOne($phone_query);
		// debugMessage($result);exit();
		if(isEmptyString($result)){
			return false;
		} else {
			$contact = new Contact();
			$contact->populate($result);
			$contact->setContactName($contact->getName());
		}
		return true;
	}
	# determine if the refno has already been assigned to another organisation
	function refExists($ref =''){
		$conn = Doctrine_Manager::connection();
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query_custom = '';
		if(isEmptyString($ref)){
			$ref = $this->getRefNo();
		}
	
		# unique phone
		$ref_query = "SELECT id FROM directory WHERE refno = '".$ref."' AND idtype = '".$this->getIDType()."' AND typeids = '".$this->getTypeIDs()."' ".$id_check;
		// debugMessage($phone_query);
		$result = $conn->fetchOne($ref_query);
		// debugMessage($result);exit();
		if(isEmptyString($result)){
			return false;
		} else {
			$contact = new Contact();
			$contact->populate($result);
			$contact->setContactName($contact->getName());
		}
		return true;
	} 
	/**
	 * Returns the full name of the Contact. If the contact is a person, return a concatination on the firstname and the lastname. 
	 * Else if a Company, return the organization name
	 *
	 * @return String
	 */
	function getName() {
		if ($this->isPerson()) {
			$name = '';
			if(!isEmptyString($this->getFirstName())){
				$name .= ucwords(strtolower($this->getFirstName())).' ';
			}
			if(!isEmptyString($this->getLastName())){
				$name .= ucwords(strtolower($this->getLastName())).' ';
			}
			return ($name);
		} else {
			return ucwords(strtolower($this->getOrgName()));
		}
	}
	/**
     * Determine the gender strinig of a person
     * @return String the gender
     */
    function getGenderLabel(){
    	return $this->getGender() == '1' ? 'Male' : 'Female'; 
    }
    # Determine gender text depending on the gender
    function getGenderText(){
    	if($this->isMale()){
    		return 'Male';
    	}
    	if($this->isFemale()){
    		return 'Female';
    	}
    	if($this->isCompany()){
    		return 'Organisation';
    	}
    	return '';
    }
 	/**
     * Determine if a person is male
     * @return bool
     */
    function isMale(){
    	return $this->getGender() == '1' ? true : false; 
    }
	/**
     * Determine if a person is female
     * @return bool
     */
    function isFemale(){
    	return $this->getGender() == '2' ? true : false; 
    }
	/**
     * Return an array containing the IDs of the categories that the contacts belongs to
     *
     * @return Array of the IDs of the categories that the contacts belongs to
     */
    function getSubCategoryIDs() {
        $ids = array();
        $subcategories = $this->getSubCategories();
        //debugMessage($categories->toArray());
        foreach($subcategories as $thecategories) {
            $ids[] = $thecategories->getCategoryID();
        }
        return $ids;
    }
	/**
     * Display a list of categories that the contact belongs
     *
     * @return String HTML list of the categories that the contact belongs to
     */
    function displayCategories() {
        $categories = $this->getSubCategories();
        $str = "";
        if ($categories->count() == 0) {
            return $str;
        }
        $str .= '<ul class="list">';
        foreach($categories as $thecategories) {
            $str .= "<li>".$thecategories->getBusinessDirectoryCategory()->getName()."</li>"; 
        }
        $str .= "</ul>";
        return $str; 
    }
	# determine list of sub categories as comma separated
	function getCategoryList() {
		$categories = $this->getSubCategories();
		// debugMessage($categories->toArray());
       	$str = "";
        $names = array();
        if ($categories->count() > 0) {
	        foreach($categories as $category) {
	        	$names[] = $category->getBusinessDirectoryCategory()->getName();
	        }
	       	$str = implode(', ', $names);
        } else {
        	$str = "--";
        }
        
        return $str; 
	}
    # determine the category name
    function getCategoryName() {
    	return $this->getCategory()->getName();
    }
	# determine the category id
    function getTheCategoryID() {
    	return $this->getCategoryID();
    }
	# determine the sub categories for a category
	function getSubCategories() {
		$q = Doctrine_Query::create()
		->from('ContactCategory c')->innerJoin('c.category b')
		->where("c.contactid = '".$this->getID()."' AND b.parentid IS NOT NULL ")
		->orderby("b.name ASC");
		return $q->execute();
	}
	# determine if contact is a person
    function isPerson(){
    	return $this->getContactType() == '1' ? true : false; 
    }
	# determine if contact is a company
    function isCompany(){
    	return $this->getContactType() == '2' ? true : false; 
    }
    function isActive(){
    	return $this->getStatus() == '1' ? true : false;
    }
    function getStatusLabel(){
    	return $this->getStatus() == '1' ? 'Verified' : 'Inactive';
    }
    
	# relative path to profile image
    function hasProfileImage(){
    	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."directory".DIRECTORY_SEPARATOR."";
    	$real_path = $real_path.$this->getID().DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$this->getProfilePhoto();
    	// debugMessage($real_path);
    	if(file_exists($real_path) && !isEmptyString($this->getProfilePhoto())){
    		return true;
    	}
    	return false;
    }
    # determine if person has profile image
    function getRelativeProfilePicturePath(){
	    $real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."directory".DIRECTORY_SEPARATOR."";
	    $real_path = $real_path.$this->getID().DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$this->getProfilePhoto();
	    if(file_exists($real_path) && !isEmptyString($this->getProfilePhoto())){
	    	return $real_path;
	    }
	    $real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."default".DIRECTORY_SEPARATOR."default_medium_male.jpg";
	    if($this->isFemale()){
	    	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."default".DIRECTORY_SEPARATOR."default_medium_female.jpg";
	    }
	    if($this->isCompany() || !$this->isPerson()){
	    	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."default".DIRECTORY_SEPARATOR."medium_group.jpg";
	    }
	    return $real_path;
    }
    # determine path to medium profile picture
    function getMediumPicturePath() {
	    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	    $path = "";
	    $path = $baseUrl.'/uploads/default/default_medium_male.jpg';
	    if($this->isFemale()){
	   		$path = $baseUrl.'/uploads/default/default_medium_female.jpg';
	    }
	    if($this->isCompany() || !$this->isPerson()){
	    	$real_path = $photo_path = $baseUrl."/uploads/default/medium_group.jpg";
	    }
	    if($this->hasProfileImage()){
	    	$path = $baseUrl.'/uploads/directory/'.$this->getID().'/avatar/medium_'.$this->getProfilePhoto();
	    }
	    // debugMessage($path);
	    return $path;
    }
    # determine path to large profile picture
    function getLargePicturePath() {
	    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	    $path = $baseUrl.'/uploads/default/default_large_male.jpg';
	    if($this->isFemale()){
	    	$path = $baseUrl.'/uploads/default/default_large_female.jpg';
	    }
	    if($this->isCompany() || !$this->isPerson()){
	    	$real_path = $photo_path = $baseUrl."/uploads/default/medium_group.jpg";
	    }
	    if($this->hasProfileImage()){
	    	$path = $baseUrl.'/uploads/directory/'.$this->getID().'/avatar/large_'.$this->getProfilePhoto();
	    }
    	# debugMessage($path);
    	return $path;
    }   
	/**
	 * Update the folder and file name for the uploaded images 
	 */
	function afterSave(){
		# set refno
		if(isEmptyString($this->getRefNo())){
			$this->setRefNo($this->getID() + 100000);
			$this->save();
		}
		return true;
	}
	function afterUpdate(){
		# set refno
		if(isEmptyString($this->getRefNo())){
			$this->setRefNo($this->getID() + 100000);
			$this->save();
		}
		
		// exit;
		// update fyear details
		return true;
	}
	# determine if pending approval
	function isPending(){
		return $this->getStatus() == 2 ? true : false;
	}
	# determine if approved
	function isApproved(){
		return $this->getStatus() == 3 ? true : false;
	}
	function isSaved(){
		return $this->getStatus() == 1 ? true : false;
	}
	function isRejected(){
		return $this->getStatus() == 4 ? true : false;
	}
	function isDeactivated(){
		return $this->getStatus() == 5 ? true : false;
	}
	function isStaff(){
		return in_array(3, explode(',', $this->getTypeIDs())) ? true : false;
	}
	# radom featured contact
	function fetchRandomContact(){
		$q = Doctrine_Query::create()
		->from('Contact c')
		->where("c.description <> '' AND c.orgname <> '' AND c.website <> '' AND c.contacttype = 2 AND c.status = 3 ")
		->limit(1)->orderBy('rand()');
		return $q->execute();
	}
    # determine if contact has gps location so as to plot out their data
    function hasGPSCoordinates() {
    	return !isEmptyString($this->getGpsLat()) && !isEmptyString($this->getGpsLng()) ? true : false;
    	// return true;
    }
    # determine if farmer is ugandan
    function isUgandan() {
    	return $this->getCountry() == 'UG' ? true : false;
    }
    /**
    * Get the full name of the country from the two digit code
     *
     * @return String The full name of the state
     */
	function getCountryName() {
    	if(isEmptyString($this->getCountry())){
			return "--";
     	}
     	$countries = getCountries();
     	return $countries[$this->getCountry()];
	}
}
?>
