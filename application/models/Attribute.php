<?php

class Attribute extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('data_attribute');
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('domaintype', 'string', 10);
		$this->hasColumn('valuetype', 'string', 255);
		$this->hasColumn('aggregatetype', 'string', 10);
		$this->hasColumn('categorytype', 'string', 1);
		$this->hasColumn('categorygroup', 'string', 255);
		$this->hasColumn('category', 'string', 255);
		$this->hasColumn('optiontype', 'string', 4);
		$this->hasColumn('optionid', 'integer', null, array('default' => NULL));
		$this->hasColumn('systemattr', 'string', 10);
		$this->hasColumn('systemattr_values', 'string', 255);
		$this->hasColumn('issearchable', 'string', 255);
		$this->hasColumn('optionlabel', 'string', 255);
		$this->hasColumn('min_val', 'string', 50, array('default' => NULL));
		$this->hasColumn('max_val', 'string', 50, array('default' => NULL));
		$this->hasColumn('decimals', 'string', 50, array('default' => NULL));
		$this->hasColumn('formcontrol', 'string', 50, array('default' => NULL)); // 1 short, 2 long
		$this->hasColumn('min_size', 'string', 50, array('default' => NULL)); // 1 text, 2 file
		$this->hasColumn('max_size', 'string', 50, array('default' => NULL)); // 1 text, 2 file
		$this->hasColumn('format', 'string', 50); // 1 file formats, 2 date formats, 3 time formats
		$this->hasColumn('hastipcomment', 'string', 4); // 0 disabled, 1 enabled
		$this->hasColumn('tooltip_position', 'string', 500); // 1 on value, 2 on label hover
		$this->hasColumn('hasbatchupload', 'string', 4); // 0 disabled, 1 enabled
		$this->hasColumn('hasuploadtitle', 'string', 4); // 0 disabled, 1 enabled
		$this->hasColumn('usemin', 'string', 4); // 0 disabled, 1 enabled
		$this->hasColumn('usemax', 'string', 4); // 0 disabled, 1 enabled
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Category as optioncategory', array('local' => 'optionid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "Attribute <b>".$this->getName()."</b> already exists. Please specify another name or cancel to search & apply it");
		}
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				$formvalues['refno'] = NULL;
			}
		}
		if(isArrayKeyAnEmptyString('aggregatetype', $formvalues)){
			unset($formvalues['aggregatetype']);
		}
		if(!isArrayKeyAnEmptyString('category', $formvalues)){
			$formvalues['categorytype'] = 1;
		} else {
			if(!isArrayKeyAnEmptyString('category_old', $formvalues)){
				$formvalues['category'] = NULL;
			} else {
				unset($formvalues['category']);
			}
		}
		if(isArrayKeyAnEmptyString('optionid', $formvalues)){
			if(!isArrayKeyAnEmptyString('optionid_old', $formvalues)){
				$formvalues['optionid'] = NULL;
			} else {
				unset($formvalues['optionid']);
			}
		}
		if(!isArrayKeyAnEmptyString('categorygroup', $formvalues)){
			$formvalues['categorytype'] = 2;
			if(!isArrayKeyAnEmptyString('category', $formvalues)){
				$formvalues['category'] = NULL;
			}
		} else {
			if(!isArrayKeyAnEmptyString('categorygroup_old', $formvalues)){
				$formvalues['categorygroup'] = NULL;
			} else {
				unset($formvalues['categorygroup']);
			}
		}
		if(!isArrayKeyAnEmptyString('domaintype', $formvalues)){
			if($formvalues['domaintype'] == 2){
				$formvalues['categorygroup'] = NULL;
				$formvalues['category'] = NULL;
				$formvalues['categorytype'] = NULL;
			}
		}
		if(isArrayKeyAnEmptyString('systemattr_values_ids', $formvalues)){
			if(!isArrayKeyAnEmptyString('systemattr_values_old', $formvalues)){
				$formvalues['systemattr_values'] = NULL;
			} else {
				unset($formvalues['systemattr_values']);
			}
			if(!isArrayKeyAnEmptyString('limit_systemattr_old', $formvalues)){
				$formvalues['systemattr_values'] = NULL;
			}
		} else {
			$formvalues['systemattr_values'] = implode(',', $formvalues['systemattr_values_ids']);
		}
		if(!isArrayKeyAnEmptyString('optiontype', $formvalues)){
			if($formvalues['optiontype'] == 1){
				$formvalues['systemattr'] = NULL;
				$formvalues['systemattr_values'] = NULL;
			}
			if($formvalues['optiontype'] == 2){
				$formvalues['optionid'] = NULL;
			}
		}
		if(!isArrayKeyAnEmptyString('valuetype', $formvalues)){
			if($formvalues['valuetype'] == '6'){
				if(!isArrayKeyAnEmptyString('max_date', $formvalues)){
					$formvalues['max_val'] = date('Y-m-d', strtotime($formvalues['max_date']));
				}
				if(!isArrayKeyAnEmptyString('min_date', $formvalues)){
					$formvalues['min_val'] = date('Y-m-d', strtotime($formvalues['min_date']));
				}
				if(isArrayKeyAnEmptyString('usemin', $formvalues)){
					$formvalues['usemin'] = 0;
				} else {
					$formvalues['min_val'] = NULL;
				}
				if(isArrayKeyAnEmptyString('usemax', $formvalues)){
					$formvalues['usemax'] = 0;
				} else {
					$formvalues['max_val'] = NULL;
				}
			}
			if(isArrayKeyAnEmptyString('format_ids', $formvalues)){
				if(!isArrayKeyAnEmptyString('format_old', $formvalues)){
					$formvalues['format'] = NULL;
				} else {
					unset($formvalues['format']);
				}
			} else {
				$formvalues['format'] = implode(',', $formvalues['format_ids']);
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE name = '".$name."' AND valuetype = '".$this->getvaluetype()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		$resave = false;
		
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
		
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("ATT-".($this->getID() + 1000));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
		
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("ATT-".($this->getID() + 1000));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	# find duplicates after save
	function getDuplicates(){
		$session = SessionWrapper::getInstance();
		$q = Doctrine_Query::create()->from('Attribute q')->where("q.name = '".$this->getName()."' AND q.valuetype = '".$this->getvaluetype()."' ")->orderby("q.id");
	
		$result = $q->execute();
		return $result;
	}
	function getFormatText(){
		$list = '';
		$names = array();
		$formats = getFileFormats();
		if(!isEmptyString($this->getFormat())){
			$userlist = array_remove_empty(explode(',', $this->getFormat())); // debugMessage($userlist);
			foreach($userlist as $id){
				if(!isArrayKeyAnEmptyString($id, $formats)){
					$names[] = $formats[$id];
				}
			}
			$list = createHTMLCommaListFromArray($names,  ', ');
		}
		return $list;
	}
	function getCategoryOptionList(){
		$list = array();
		$alloptions = array(); $currentoptions = array();
		if($this->getcategorytype() == 1 && !isEmptyString($this->getcategory())){
			$alloptions = $currentoptions = getDataCategoryOptions(false, getOptionsForID($this->getcategory()));
		}
		if($this->getcategorytype() == 2 && !isEmptyString($this->getcategorygroup())){
			
		}
		return $currentoptions;
	}
	function getConfigOptionName($optionid){
		$list = $this->getConfigOptionList();
		return isArrayKeyAnEmptyString($optionid, $this->getConfigOptionList()) ? '' : $list[$optionid];
	}
	function getConfigOptionList(){
		$list = array();
		$alloptions = array(); $currentoptions = array();
		if($this->getoptiontype() == 1 && !isEmptyString($this->getoptionid())){
			$alloptions = $currentoptions = getDataCategoryOptions(false, getOptionsForID($this->getoptionid()));
		}
		if($this->getoptiontype() == 2){
			if($this->getsystemattr() == '9'){
				$alloptions = $currentoptions = getDistricts();
			}
			if($this->getsystemattr() == '1'){ // projects
				$alloptions = $currentoptions = getProjects();
			}
			if($this->getsystemattr() == '2'){ // activities
				$alloptions = $currentoptions = getProjectActivities();
			}
			if($this->getsystemattr() == '3'){ // objectives
				$alloptions = $currentoptions = getObjectives();
			}
			if($this->getsystemattr() == '4'){ // indicators
				$alloptions = $currentoptions = getDataIndicators();
			}
			if($this->getsystemattr() == '5'){ // project outcomes
				$alloptions = $currentoptions = getOutcomes(4);
			}
			if($this->getsystemattr() == '6'){ // project outputs
				$alloptions = $currentoptions = getOutputs(4, '', '', 1);
			}
			if($this->getsystemattr() == '8'){ // programme units
				$alloptions = $currentoptions = getProgramUnits();
			}
			if($this->getsystemattr() == '9'){ // district locations
				$alloptions = $currentoptions = getDistricts();
			}
			if($this->getsystemattr() == '10'){ // system users
				$alloptions = $currentoptions = getDataUsers();
			}
			if($this->getsystemattr() == '11'){ // country outcomes
				$alloptions = $currentoptions = getOutcomes(1);
			}
			if($this->getsystemattr() == '12'){ // Locations: Constituency
				$alloptions = $currentoptions = getCounties();
			}
			if($this->getsystemattr() == '13'){ // Locations: Subcounties
				$alloptions = $currentoptions = getSubCounties();
			}
			if($this->getsystemattr() == '14'){ // Datasets
				$alloptions = $currentoptions = getDatasets('', 1);
			}
			if($this->getsystemattr() == '15'){ // Attributes
				$alloptions = $currentoptions = getDataAttributes();
			}
			if($this->getsystemattr() == '16'){ // Frameworks: SDG Objectives
				$alloptions = $currentoptions = getPrograms(6);
			}
			if($this->getsystemattr() == '18'){ // Frameworks: NDP Objectives
				$alloptions = $currentoptions = getPrograms(5);
			}
			if($this->getsystemattr() == '19'){ // Frameworks: Organization  Objectives
				$alloptions = $currentoptions = getPrograms(2);
			}
			if($this->getsystemattr() == '20'){ // Frameworks: Organization  Outcomes
				$alloptions = $currentoptions = getOutcomes(2);
			}
			if($this->getsystemattr() == '17'){ // Frameworks: Global Objectives
				$alloptions = $currentoptions = getPrograms(3);
			}
			if($this->getsystemattr() == '21'){ // Frameworks: Global Outcomes
				$alloptions = $currentoptions = getOutcomes(3);
			}
			if($this->getsystemattr() == '22'){ // Indicator units
				$alloptions = $currentoptions = getDatavariables('INDICATOR_UNITS');
			}
			if($this->getsystemattr() == '23'){ // activity types
				$alloptions = $currentoptions = getDatavariables('ACTIVITY_TYPES');
			}
			if($this->getsystemattr() == '24'){ // Sub Impact Areas
				$alloptions = $currentoptions = getDatavariables('PROGRAM_SUBIMPACT_AREAS');
			}
			if($this->getsystemattr() == '25'){ // Milestones: Level 1
				$alloptions = $currentoptions = getMilestonePeriods(1);
			}
			if($this->getsystemattr() == '26'){ // Milestones: Level 2
				$alloptions = $currentoptions = getMilestonePeriods(2);
			}
			if(!isEmptyString($this->getsystemattr_values())){
				$currentoptions = getArrayOfListValues($this->getsystemattr_values(), $alloptions);
			}
		}
		return $currentoptions;
	}
}
?>