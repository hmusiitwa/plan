<?php

class ContentRisk extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_risk');
		$this->hasColumn('type', 'string', 4);
		$this->hasColumn('name', 'string', 255);
		$this->hasColumn('description', 'string', 1000);
		$this->hasColumn('programid', 'integer', null, array('default' => NULL));
		$this->hasColumn('projectid','integer', null, array('default' => NULL));
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('refno', 'string', 10);
		$this->hasColumn('category', 'string', null, array('default' => NULL));
		
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('reportedbyid', 'integer', null, array('default' => NULL));
		$this->hasColumn('frequency', 'string', 10);
		$this->hasColumn('startdate', 'string', 15);
		$this->hasColumn('enddate', 'string', 15);
		$this->hasColumn('status', 'string', 4);
		$this->hasColumn('reviewid','integer', null, array('default' => NULL));
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		// $this->addDateFields(array("startdate","enddate"));
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Program as program', array('local' => 'programid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as owner', array('local' => 'userid', 'foreign' => 'id'));
		$this->hasOne('ContentRisk as parent', array('local' => 'parentid', 'foreign' => 'id'));
		$this->hasOne('ContentRiskDetail as latestreview', array('local' => 'reviewid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}	

		if(isArrayKeyAnEmptyString('id', $formvalues)){
			$reviewdetails = array();
			if(!isArrayKeyAnEmptyString('probability', $formvalues)){
				$reviewdetails['probability'] = $formvalues['probability'];
			}
			if(!isArrayKeyAnEmptyString('impact_cost', $formvalues)){
				$reviewdetails['impact_cost'] = $formvalues['impact_cost'];
			}
			if(!isArrayKeyAnEmptyString('impact_schedule', $formvalues)){
				$reviewdetails['impact_schedule'] = $formvalues['impact_schedule'];
			}
			if(!isArrayKeyAnEmptyString('impactdetails', $formvalues)){
				$reviewdetails['impactdetails'] = $formvalues['impactdetails'];
			}
			if(!isArrayKeyAnEmptyString('responsetype', $formvalues)){
				$reviewdetails['responsetype'] = $formvalues['responsetype'];
			}
			if(!isArrayKeyAnEmptyString('responsedetails', $formvalues)){
				$reviewdetails['responsedetails'] = $formvalues['responsedetails'];
			}
			if(!isArrayKeyAnEmptyString('enddate', $formvalues)){
				$reviewdetails['nextreviewdate'] = $formvalues['enddate'];
			}
			$reviewdetails['inputdate'] = date("Y-m-d H:i:s", time());
			$reviewdetails['isbaseline'] = 1;
			
			$reviewdetails['type'] = 1;
			$reviewdetails['status'] = 1;
			$reviewdetails['userid'] = getUserID();
			$formvalues['latestreview'] = $reviewdetails;
		}
		
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo('R'.($this->getID() + 1000));
			$resave = true;
		}
		if(!isEmptyString($this->getReviewID()) && (isEmptyString($this->getlatestreview()->getRiskID())) || $this->getlatestreview()->getRiskID() != $this->getID()){
			$this->getlatestreview()->setRiskID($this->getID());
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo('R'.($this->getID() + 1000));
			$resave = true;
		}
		if(!isEmptyString($this->getReviewID()) && (isEmptyString($this->getlatestreview()->getRiskID())) || $this->getlatestreview()->getRiskID() != $this->getID()){
			$this->getlatestreview()->setRiskID($this->getID());
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function getStatusUpdates($query = ""){
		$customquery = "";
		if(!isEmptyString($query)){
			$customquery .= $query;
		}
		$q = Doctrine_Query::create()->from('ContentRiskDetail c')->where("c.riskid = '".$this->getID()."' ".$customquery)->orderby("c.inputdate desc, c.id desc");
	
		$result = $q->execute();
		return $result;
	}
	function getComments($query = ""){
		$customquery = "";
		if(!isEmptyString($query)){
			$customquery .= $query;
		}
		$q = Doctrine_Query::create()->from('ContentComment c')->where("c.riskid = '".$this->getID()."' ".$customquery)->orderby("c.id asc");
	
		$result = $q->execute();
		return $result;
	}
	
	function isNotClosed(){
		return $this->getStatus() != 2;
	}
	function isClosed(){
		return $this->getStatus() == 2;
	}
	
	function updateStatus($status = 1){
		$session = SessionWrapper::getInstance();
			
		# set active to true and blank out activation key
		$this->setStatus($status);
		$this->save();
	
		// $this->sendStatusChangeEmail(true);
	
		$module = '1';
		if($status == 2){
			$usecase = '1.6';
			$type = USER_DEACTIVATE;
			$details = "Risk ".$this->getName()." (Ref ".$this->getRefNo().")  closed ";
		}
		if($status == 1){
		$usecase = '1.7';
			$type = USER_REACTIVATE;
			$details = "Risk ".$this->getName()." (Ref ".$this->getRefNo().") re-opened ";
		}
	
		$audit_values = getAuditInstance();
		$audit_values['actionid'] = 18;
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['transactiondetails'] = $details;
		$audit_values['status'] = "Y";
		// $this->notify(new sfEvent($this, $type, $audit_values));

		return true;
	}
}
?>