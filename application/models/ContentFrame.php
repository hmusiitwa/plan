<?php

class ContentFrame extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_logframe');
		$this->hasColumn('programid', 'integer', null, array('default' => NULL));
		$this->hasColumn('projectid','integer', null, array('default' => NULL));
		$this->hasColumn('activityid', 'integer', null, array('default' => NULL));
		$this->hasColumn('type', 'integer', null, array('default' => null));
		$this->hasColumn('title', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15, array('notblank' => true));
		$this->hasColumn('description', 'string', 1000);
		
		$this->hasColumn('level', 'string', 4, array('default' => NULL));
		$this->hasColumn('ownerid', 'integer', null, array('default' => NULL));
		$this->hasColumn('ordering', 'string', 4);
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		
		$this->hasColumn('globoutcomeids', 'string', 50);
		$this->hasColumn('progoutcomeids', 'string', 50);
		$this->hasColumn('ndpoutcomeids', 'string', 50);
		$this->hasColumn('sdgoutcomeids', 'string', 50);
		$this->hasColumn('impactids', 'string', 50);
		$this->hasColumn('cspid', 'integer', null, array('default' => 1));
		$this->hasColumn('mov', 'string', 500);
		$this->hasColumn('assumptions', 'string', 500);
		$this->hasColumn('uom', 'string', 50);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"title.notblank" => "Please enter Name",
       			"title.length" => "Maximum characters for Title should be 500",
       			"refno.notblank" => "Please enter Code",
       			"refno.length" => "Maximum characters for Result Code should be 15",
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Program as program', array('local' => 'programid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('Activity as activity', array('local' => 'activityid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as owner', array('local' => 'userid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		/* if($this->refnoExists()){
			$this->getErrorStack()->add("refno.duplicate", "Entry with code <b>".$this->getRefno()."</b> already exists. Please specify another.");
		}
		if($this->nameExists()){
			$this->getErrorStack()->add("title.unique", "Entry with name <b>".$this->getTitle()."</b> already exists. Please specify another.");
		} */
	}
	# determine if the username has already been assigned
	function refnoExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM ".$this->getTableName()." WHERE refno = '".$this->getRefno()."' AND type = '".$this->getType()."' AND level = '".$this->getLevel()."' AND projectid = '".$this->getProjectID()."' AND programid = '".$this->getProgramID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	function nameExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM ".$this->getTableName()." WHERE title = '".$this->getTitle()."' AND type = '".$this->getType()."' AND level = '".$this->getLevel()."' AND projectid = '".$this->getProjectID()."' AND programid = '".$this->getProgramID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance();  // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('category', $formvalues)){
			unset($formvalues['category']);
		}
		if(!isArrayKeyAnEmptyString('title', $formvalues)){
			stripMultipleSpaces($formvalues['title']);
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			stripMultipleSpaces($formvalues['refno']);
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('programid', $formvalues)){
			unset($formvalues['programid']);
		}
		if(isArrayKeyAnEmptyString('parentid', $formvalues)){
			unset($formvalues['parentid']);
		}
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('activityid', $formvalues)){
			unset($formvalues['activityid']);
		}
		if(isArrayKeyAnEmptyString('level', $formvalues)){
			unset($formvalues['level']);
		}
		if(isArrayKeyAnEmptyString('fyear', $formvalues)){
			unset($formvalues['fyear']);
		}
		if(isArrayKeyAnEmptyString('cspid', $formvalues)){
			unset($formvalues['cspid']);
		}
		if(isArrayKeyAnEmptyString('ownerid', $formvalues)){
			unset($formvalues['ownerid']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		}
		if(!isArrayKeyAnEmptyString('globoutcomeids', $formvalues)) {
			$formvalues['globoutcomeids'] = implode(',', $formvalues['globoutcomeids']);
		} else {
			if(!isArrayKeyAnEmptyString('globoutcomeids_old', $formvalues)){
				if(isArrayKeyAnEmptyString('globoutcomeids', $formvalues)) {
					$formvalues['globoutcomeids'] = NULL;
				}
			} else {
				unset($formvalues['globoutcomeids']);
			}
		}
		if(!isArrayKeyAnEmptyString('progoutcomeids', $formvalues)) {
			$formvalues['progoutcomeids'] = implode(',', $formvalues['progoutcomeids']);
		} else {
			if(!isArrayKeyAnEmptyString('progoutcomeids_old', $formvalues)){
				if(isArrayKeyAnEmptyString('progoutcomeids', $formvalues)) {
					$formvalues['progoutcomeids'] = NULL;
				}
			} else {
				unset($formvalues['progoutcomeids']);
			}
		}
		if(!isArrayKeyAnEmptyString('ndpoutcomeids', $formvalues)) {
			$formvalues['ndpoutcomeids'] = implode(',', $formvalues['ndpoutcomeids']);
		} else {
			if(!isArrayKeyAnEmptyString('ndpoutcomeids_old', $formvalues)){
				if(isArrayKeyAnEmptyString('ndpoutcomeids', $formvalues)) {
					$formvalues['ndpoutcomeids'] = NULL;
				}
			} else {
				unset($formvalues['ndpoutcomeids']);
			}
		}
		if(!isArrayKeyAnEmptyString('sdgoutcomeids', $formvalues)) {
			$formvalues['sdgoutcomeids'] = implode(',', $formvalues['sdgoutcomeids']);
		} else {
			if(!isArrayKeyAnEmptyString('sdgoutcomeids_old', $formvalues)){
				if(isArrayKeyAnEmptyString('sdgoutcomeids', $formvalues)) {
					$formvalues['sdgoutcomeids'] = NULL;
				}
			} else {
				unset($formvalues['sdgoutcomeids']);
			}
		}
		if(!isArrayKeyAnEmptyString('impactids', $formvalues)) {
			$formvalues['impactids'] = implode(',', $formvalues['impactids']);
		} else {
			if(!isArrayKeyAnEmptyString('impactids_old', $formvalues)){
				if(isArrayKeyAnEmptyString('impactids', $formvalues)) {
					$formvalues['impactids'] = NULL;
				}
			} else {
				unset($formvalues['impactids']);
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function getName(){
		return $this->getTitle();
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
	
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
	
		return true;
	}
	# find duplicates after save
	function getDuplicates(){
		$session = SessionWrapper::getInstance();
		$q = Doctrine_Query::create()->from('ContentFrame q')->where("q.title = '".$this->getTitle()."' AND q.refno = '".$this->getRefno()."' AND q.type = '".$this->getType()."'  AND q.level = '".$this->getLevel()."' AND q.projectid = '".$this->getProjectID()."' ")->orderby("q.id");
		
		$result = $q->execute();
		return $result;
	}
}
?>