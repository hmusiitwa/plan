<?php

class ContentCsp extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_csp');
		$this->hasColumn('title', 'string', 255, array('notblank' => true));
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('startyear', 'integer', null, array('default' => NULL));
		$this->hasColumn('endyear', 'integer', null, array('default' => NULL));
		$this->hasColumn('status', 'integer', null, array('default' => 0));
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		$this->hasColumn('vision', 'string', 500);
		
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('startyear', $formvalues)){
			unset($formvalues['startyear']);
		}
		if(isArrayKeyAnEmptyString('endyear', $formvalues)){
			unset($formvalues['endyear']);
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>