<?php

class Dataset extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('data_set');
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('expirydays', 'string', 10, array('default' => 0));
		$this->hasColumn('openperiods', 'string', 10, array('default' => 0));
		$this->hasColumn('minsubmitdays', 'string', 10);
		$this->hasColumn('periodtype', 'string', 10);
		$this->hasColumn('formtype', 'string', 10); // 1-normal, 2-section
		$this->hasColumn('periodids', 'string', 255);
		$this->hasColumn('notifyapprovalrequired', 'string', 4);
		$this->hasColumn('notifyuseroncomplete', 'string', 4);
		$this->hasColumn('notifyusers', 'string', 255);
		
		$this->hasColumn('sendremainder', 'string', 1);
		$this->hasColumn('remainderusers', 'string', 255);
		$this->hasColumn('remainderfreq', 'string', 4);
		$this->hasColumn('remainderfreqtype', 'string', 4);
		$this->hasColumn('viewconfig', 'string', 4, array('default' => 1));
		
		$this->hasColumn('allmandatory', 'string', 1);
		$this->hasColumn('elementids', 'string', 255);
		$this->hasColumn('groupattr', 'string', 10);
		$this->hasColumn('indicatorids', 'string', 255);
		$this->hasColumn('userids', 'string', 255);
		$this->hasColumn('groupids', 'string', 255);
		$this->hasColumn('audiencetype', 'string', 4, array('default' => 0)); // 1 = view&modify only own, // 2 view/modify own plus only view other, 3 => view/modify any 
		$this->hasColumn('projectid', 'integer', null, array('default' => NULL));
		$this->hasColumn('activityid', 'integer', null, array('default' => NULL));
		$this->hasColumn('startdate', 'date', array('default' => NULL));
		$this->hasColumn('enddate', 'date', array('default' => NULL));
		
		$this->hasColumn('allowdrafts', 'string', 4, array('default' => 0));
		$this->hasColumn('hasworkflow', 'string', 4, array('default' => 0));
		$this->hasColumn('multistep', 'string', 4, array('default' => 0));
		$this->hasColumn('maxsteps', 'string', 4, array('default' => 1));
		$this->hasColumn('approvaltype', 'string', 4, array('default' => 5));
		$this->hasColumn('step1approver', 'string', 10, array('default' => NULL));
		$this->hasColumn('step2approver', 'string', 10, array('default' => NULL));
		$this->hasColumn('step3approver', 'string', 10, array('default' => NULL));
		$this->hasColumn('columns', 'string', 10, array('default' => 2));
		$this->hasColumn('folderid', 'integer', 11, array('default' => NULL));
		$this->hasColumn('ispreconfigured', 'string', 1, array('default' => 0));
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('Activity as activity', array('local' => 'activityid', 'foreign' => 'id'));
		$this->hasMany('DatasetSection as sections', array('local' => 'id', 'foreign' => 'datasetid'));
		$this->hasOne('UserAccount as approver', array('local' => 'approvedbyid', 'foreign' => 'id'));
		$this->hasOne('Folder as folder', array('local' => 'folderid', 'foreign' => 'id'));
		$this->hasOne('Attribute as groupattribute', array('local' => 'groupattr', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->nameExists()){
			$this->getErrorStack()->add("dataset.unique", "Dataset with name <b>".$this->getName()."</b> already exists. Please specify another.");
		}
	}	
	# determine if the username has already been assigned
	function nameExists($name =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		if(isEmptyString($name)){
			$name = $this->getName();
		}
		$query = "SELECT id FROM ".$this->getTableName()." WHERE name = '".$name."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); // exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('activityid', $formvalues)){
			unset($formvalues['activityid']);
		}
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}
		if(!isArrayKeyAnEmptyString('to', $formvalues)){
			// $to = str_replace(' ', '', trim($formvalues['to']));
			$formvalues['elementids'] = implode(',', $formvalues['to']);
			unset($formvalues['optionlist']);
			unset($formvalues['to']);
		} else {
			if(!isArrayKeyAnEmptyString('elementids_old', $formvalues)){
				$formvalues['elementids'] = NULL;
			} else {
				unset($formvalues['elementids']);
			}
		}
		if(!isArrayKeyAnEmptyString('to2', $formvalues)){
			// $to2 = str_replace(' ', '', trim($formvalues['to2']));
			$formvalues['indicatorids'] = implode(',', $formvalues['to2']);
			unset($formvalues['optionlist2']);
			unset($formvalues['to2']);
		} else {
			if(!isArrayKeyAnEmptyString('indicatorids_old', $formvalues)){
				$formvalues['indicatorids'] = NULL;
			} else {
				unset($formvalues['indicatorids']);
			}
		}
		
		if(isArrayKeyAnEmptyString('allowdrafts', $formvalues)){
			if(!isArrayKeyAnEmptyString('allowdrafts_old', $formvalues)){
				$formvalues['allowdrafts'] = 0;
			} else {
				//unset($formvalues['allowdrafts']);
				unset($formvalues['allowdrafts']);
			}
		}
		if(isArrayKeyAnEmptyString('hasworkflow', $formvalues)){
			if(!isArrayKeyAnEmptyString('hasworkflow_old', $formvalues)){
				$formvalues['hasworkflow'] = 0;
			} else {
				// unset($formvalues['hasworkflow']);
				unset($formvalues['hasworkflow']);
			}
		}
		if(isArrayKeyAnEmptyString('groupattr', $formvalues)){
			if(!isArrayKeyAnEmptyString('groupattr_old', $formvalues)){
				$formvalues['groupattr'] = NULL;
			} else {
				// unset($formvalues['hasworkflow']);
				unset($formvalues['groupattr']);
			}
		}
		if(isArrayKeyAnEmptyString('hasgroupattr', $formvalues)){
			$formvalues['groupattr'] = NULL;
		}
		if(!isArrayKeyAnEmptyString('hasworkflow', $formvalues)){
			if($formvalues['hasworkflow'] == 0){
				$formvalues['hasworkflow'] = 0;
				$formvalues['multistep'] = 0;
				$formvalues['maxsteps'] = NULL;
				$formvalues['approvaltype'] = NULL;
				$formvalues['step1approver'] = NULL;
				$formvalues['step2approver'] = NULL;
				$formvalues['step3approver'] = NULL;
			} else {
				if($formvalues['multistep'] == 0 || isArrayKeyAnEmptyString('multistep', $formvalues)){
					$formvalues['maxsteps'] = 1;
					$formvalues['step2approver'] = NULL;
					$formvalues['step3approver'] = NULL;
				}
				if($formvalues['multistep'] == 1){
					if($formvalues['maxsteps'] == 2){
						$formvalues['step3approver'] = NULL;
					}
				}
			}
		} else {
			$formvalues['notifyapprovalrequired'] = 0;
		}
		
		if(isArrayKeyAnEmptyString('audiencetype', $formvalues)){
			if(!isArrayKeyAnEmptyString('notifyuseroncomplete_old', $formvalues)){
				$formvalues['userlistids'] = NULL;
				$formvalues['grouplistids'] = NULL;
			}
		} else {
			if($formvalues['audiencetype'] == 1){
				$formvalues['groupids'] = NULL;
				if(isArrayKeyAnEmptyString('userlistids', $formvalues)){
					if(!isArrayKeyAnEmptyString('userids_old', $formvalues)){
						$formvalues['userids'] = NULL;
					} else {
						unset($formvalues['userids']);
					}
				} else {
					$formvalues['userids'] = implode(',', $formvalues['userlistids']);
				}
			}
			if($formvalues['audiencetype'] == 2){
				$formvalues['userids'] = NULL;
				if(isArrayKeyAnEmptyString('grouplistids', $formvalues)){
					if(!isArrayKeyAnEmptyString('groupids_old', $formvalues)){
						$formvalues['groupids'] = NULL;
					} else {
						unset($formvalues['groupids']);
					}
				} else {
					$formvalues['groupids'] = implode(',', $formvalues['grouplistids']);
				}
			}
		}
		
		if(!isArrayKeyAnEmptyString('notifyuseroncomplete', $formvalues)){
			if(!isArrayKeyAnEmptyString('notifyusers_ids', $formvalues)){
				$formvalues['notifyusers'] = implode(',', $formvalues['notifyusers_ids']);
			} else {
				$formvalues['notifyusers'] = NULL;
			}
		} else {
			if(!isArrayKeyAnEmptyString('notifyuseroncomplete_old', $formvalues)){
				$formvalues['notifyuseroncomplete'] = 0;
				$formvalues['notifyusers'] = NULL;
			}
		}
		if(!isArrayKeyAnEmptyString('sendremainder', $formvalues)){
			if(!isArrayKeyAnEmptyString('remainderusers_ids', $formvalues)){
				$formvalues['remainderusers'] = implode(',', $formvalues['remainderusers_ids']);
			} else {
				$formvalues['remainderusers'] = NULL;
			}
		} else {
			if(!isArrayKeyAnEmptyString('sendremainder_old', $formvalues)){
				$formvalues['sendremainder'] = 0;
				$formvalues['remainderusers'] = NULL;
			}
		}
		if(!isArrayKeyAnEmptyString('refno', $formvalues)){
			if($formvalues['refno'] == 'Auto'){
				$formvalues['refno'] = NULL;
			}
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
		
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("DST-".($this->getID() + 100));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
		
		$this->setupLibrary();
		
		return true;
	}
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo("DST-".($this->getID() + 100));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
		
		$this->setupLibrary();
		
		return true;
	}
	function setupLibrary(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
		// debugMessage($this->toArray());
	
		if(isEmptyString($this->getFolderID())){
			$folder = new Folder();
			// debugMessage('setting folder now');
			$data = array(
					"name" => $this->getName(),
					"parentid" => DATASET_DIR,
					"refno" => $this->getRefNo(),
					"createdby" => getUserID()
			);
	
			$existingquery = "select id from folder where parentid = '".DATASET_DIR."' AND (name = '".$this->getName()."' OR refno = '".$this->getRefNo()."') ";
			$existingid = $conn->fetchOne($existingquery);
			$update = false;
			if(!isEmptyString($existingid)){
				$folder->populate($existingid);
				$oldfolder = new Folder();
				$oldfolder->populate($existingid);
				$data['previousname'] = $oldfolder->getName();
				$data["lastupdatedby"] = getUserID();
				$update = true;
			}
			debugMessage($data);
	
			$folder->processPost($data);
			debugMessage('folder err '.$folder->getErrorStackAsString());
			debugMessage($folder->toArray()); // exit;
			if(!$folder->hasError()){
				if(!$update){
					$folder->save();
					$folder->afterSave();
				} else {
					$folder->beforeUpdate();
					$folder->save();
					$folder->afterUpdate();
				}
				$this->setFolderID($folder->getID());
				try {
					$this->save();
				} catch (Exception $e) {
					debugMessage('err saving '.$e->getMessage());
				}
			}
		}
		// exit();
	
		return true;
	}
	function getSectionDetails($query = "", $orderquery = 'd.position asc'){
		$customquery = "";
		if(!isEmptyString($query)){
			$customquery .= $query;
		}
		$q = Doctrine_Query::create()->from('DatasetSection d')->where("d.datasetid = '".$this->getID()."' ".$customquery)->orderby($orderquery);
	
		$result = $q->execute();
		return $result;
	}
	function getElementIDsFromSections(){
		$conn = Doctrine_Manager::connection();
		$query = "select group_concat(s.elementids order by s.elementids separator ',') from data_set_section s where s.datasetid = '".$this->getID()."' "; //debugMessage($query);
		$data = $conn->fetchOne($query); //debugMessage($data);
		return $data;
	}
	function getUserList(){
		$list = ''; 
		$names = array();
		$users = getUsers();
		if(!isEmptyString($this->getUserIDs())){ 
			$userlist = array_remove_empty(explode(',', $this->getUserIDs())); // debugMessage($userlist);
			foreach($userlist as $id){
				if(!isArrayKeyAnEmptyString($id, $users)){
					$names[] = $users[$id];
				}
			}
			$list = createHTMLCommaListFromArray($names,  ', ');
		}
		return $list;
	}
	function getGroupList(){
		$conn = Doctrine_Manager::connection();
		$list = ''; 
		$names = array();
		$groups = getResultGroups();
		if(!isEmptyString($this->getGroupIDs())){ 
			$grplist = array_remove_empty(explode(',', $this->getGroupIDs())); // debugMessage($userlist);
			foreach($grplist as $id){
				$user_array = array();
				if(!isArrayKeyAnEmptyString($id, $groups)){
					$groupname = $groups[$id];
					$alertmsg = 'Users in '.$groups[$id].'<br><br>';
					$user_query = "select userids from user_category_group where id = '".$id."' ";
					$userids = $conn->fetchOne($user_query);
					if(!isEmptyString($userids)){
						$user_array = getDataUsers(false, $userids);
						$user_list = count($user_array) > 0 ? createHTMLListFromArray($user_array,  'blog-categories-list list-unstyled customlist noboldlist') : 'None';
						$alertmsg .= $user_list;
					} else {
						$alertmsg .= 'None';
					}
					
					$names[] = '<a class="gonowhere alert-dialog" title="Preview Users" message="'.$alertmsg.'" containerstyle="text-align:left !important;">'.$groupname.'</a>';
				}
			}
			$list = createHTMLCommaListFromArray($names,  ', ');
		}
		return $list;
	}
	function getPublicUrl(){
		$view = new Zend_View();
		$url = $view->serverUrl($view->baseUrl('dataentry/index/src/pub/datasetid/'.$this->getID()));
		return '<a href="'.$url.'" target="_blank">'.$url.'</a>';
	}
	function isSingleStep(){
		$status = false;
		if($this->getmultistep() == '0'){
			$status = true;
		}
		return $status;
	}
	function isMultiStep(){
		$status = false;
		if($this->getmultistep() == '1' || $this->getApprovalSteps() > 1){
			$status = true;
		}
		return $status;
	}
	function isAutoApprove(){
		$status = false;
		if($this->getmultistep() == '2'){
			$status = true;
		}
		return $status;
	}
	function getApprovalSteps(){
		$steps = 1;
		if(!isEmptyString($this->getmaxsteps())){
			$steps = $this->getmaxsteps();
		}
		return $steps;
	}
	function getFirstApprover(){
		$step1approver = $this->getStep1Approver();
		return $step1approver;
	}
	function getSecondApprover(){
		$step2approver = $this->getStep2Approver();
		return $step2approver;
	}
	function getThirdApprover(){
		$step3approver = $this->getStep3Approver();
		return $step3approver;
	}
	function isSingleEntry(){
		return $this->getPeriodType() == 14 ? false : true; 
	}
	function isUserAllowed($datasetid, $userid){
		if(isEmptyString($datasetid)){
			$datasetid = $this->getID();
		}
		$conn = Doctrine_Manager::connection();
		$allowed = false;
		$datasetquery = "SELECT * FROM data_set as q WHERE q.id = '".$datasetid."' ";
		$datasetdetails = $conn->fetchRow($datasetquery); // debugMessage($datasetdetails);
		if($datasetdetails['audiencetype'] == '1'){
			$userlist = array_remove_empty(explode(',', $datasetdetails['userids']));
			if(in_array($userid, $userlist)){
				$allowed = true;
			}
		}
		if($datasetdetails['audiencetype'] == '2'){
			if(!isEmptyString($datasetdetails['groupids'])){
				$grplist = array_remove_empty(explode(',', $datasetdetails['groupids'])); // debugMessage($userlist);
				foreach($grplist as $gid){
					$user_query = "select userids from user_category_group where id = '".$gid."' ";
					$userids = $conn->fetchOne($user_query);
					if(!isEmptyString($userids)){
						$userlist = array_remove_empty(explode(',', $userids));
						if(in_array($userid, $userlist)){
							$allowed = true;
						}
					}
				}
			}
		}
		if($datasetdetails['audiencetype'] == '3'){
			$allowed = true;
		}
		if($userid == $datasetdetails['createdby']){
			$allowed = true;
		}
		return $allowed;
	}
	function hasGroupAttr(){
		return !isEmptyString($this->getGroupAttr()) ? true : false;
	}
	function isPreBuilt(){
		return $this->getispreconfigured() == 1 ? true : false;
	}
}
?>