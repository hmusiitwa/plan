<?php

/**
 * Access actions for for a resource
 */
class AclAction extends BaseEntity   {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
        $this->setTableName('aclaction');
        $this->hasColumn('resourceid', 'integer', null, array('notblank' => true));
        $this->hasColumn('slug', 'string', 50, array('notblank' => true));
        $this->hasColumn('name', 'string', 255, array('notblank' => true));
        $this->hasColumn('controller', 'string', 50);
        $this->hasColumn('key', 'string', 15);
        $this->hasColumn('sortorder', 'string', 4);
    }
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(array(
       									"resourceid.notblank" => $this->translate->_("permission_resourceid_error")
       								)); 
		
	}
	
    function setUp() {
    	parent::setUp();
    	// foreign key for the group
		$this->hasOne('AclResource as resource', array(
							'local' => 'resourceid',
							'foreign' => 'id')
					);
    }
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); 
		// trim spaces from the name field
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>