<?php

class SystemAttribute extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('data_category_system');
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('query', 'string', 1000);
		$this->hasColumn('procedure', 'string', 50);
		$this->hasColumn('param1', 'string', 500);
		$this->hasColumn('param2', 'string', 500);
		$this->hasColumn('param3', 'string', 500);
	}
}
?>