<?php

// include 'UserNotifications.php';

class UserAccount extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('useraccount');
		$this->hasColumn('type', 'string', 11);
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('typeids', 'string', 255, array('notblank' => true));
		$this->hasColumn('firstname', 'string', 255, array('notblank' => true));
		$this->hasColumn('lastname', 'string', 255, array('notblank' => true));
		
		$this->hasColumn('email', 'string', 50); // only required during activation
		$this->hasColumn('email2', 'string', 50);
		$this->hasColumn('phone', 'string', 15);
		$this->hasColumn('phone2', 'string', 15);
		$this->hasColumn('username', 'string', 15); // only required during activation
		$this->hasColumn('password', 'string', 255); // only required during activation
		$this->hasColumn('trx', 'string', 255);
		$this->hasColumn('status', 'integer', null, array('default' => '0')); # 0=Pending, 1=Active, 2=Deactivated
		$this->hasColumn('isinvited', 'string', 4, array('default' => NULL));
		$this->hasColumn('logintries', 'string', 4, array('default' => NULL));
		$this->hasColumn('blockdate', 'date', array('default' => NULL));
		
		$this->hasColumn('activationkey', 'string', 15);
		$this->hasColumn('activationdate', 'date', array('default' => NULL));
		$this->hasColumn('gender', 'integer', null, array('default' => 1)); # 1=Male, 2=Female, 3=Unknown
		$this->hasColumn('profilephoto', 'string', 50);
		$this->hasColumn('forcechangepasswd', 'string', 1);
		$this->hasColumn('districtid', 'string', 11);
		$this->hasColumn('companyid', 'integer', 11, array('default' => getCompanyID()));
		
		# override the not null and not blank properties for the createdby column in the BaseEntity
		$this->hasColumn('createdby', 'integer', 11);
		$this->hasColumn('layout', 'string', 1, array('default' => '1'));
		$this->hasColumn('sidebar', 'string', 1, array('default' => '1'));
		$this->hasColumn('defaultsort', 'string', 11, array('default' => NULL));
		$this->hasColumn('defaultfolderid', 'string', 11, array('default' => NULL));
		$this->hasColumn('lastsortupdate','date', null, array('default' => NULL));
		$this->hasColumn('dashbordconfig', 'string', 10000);
		$this->hasColumn('resetexpiry', 'string', 20, array('default' => NULL));
	}
	
	protected $oldpassword;
	protected $newpassword;
	protected $confirmpassword;
	protected $oldemail;
	protected $changeemail;
	protected $isinvited;
	protected $isphoneinvited;
	protected $preupdatedata;
	protected $controller;
	protected $sendpasswdemail;
	protected $isbeinginvited;
	
	function getOldPassword(){
		return $this->oldpassword;
	}
	function setOldPassword($oldpassword) {
		$this->oldpassword = $oldpassword;
	}
	function getNewPassword(){
		return $this->newpassword;
	}
	function setNewPassword($newpassword) {
		$this->newpassword = $newpassword;
	}
	function getConfirmPassword(){
		return $this->confirmpassword;
	}
	function setConfirmPassword($confirmpassword) {
		$this->confirmpassword = $confirmpassword;
	}
	function getOldEmail(){
		return $this->oldemail;
	}
	function setOldEmail($oldemail) {
		$this->oldemail = $oldemail;
	}
	function getChangeEmail(){
		return $this->changeemail;
	}
	function setChangeEmail($changeemail) {
		$this->changeemail = $changeemail;
	}
	function getIsBeinginvited(){
		return $this->isbeinginvited;
	}
	function setIsBeingInvited($isbeinginvited) {
		$this->isbeinginvited = $isbeinginvited;
	}
	function getNameofUser(){
		return $this->nameofuser;
	}
	function setNameofUser($nameofuser) {
		$this->nameofuser = $nameofuser;
	}
	function getPreUpdateData(){
		return $this->preupdatedata;
	}
	function setPreUpdateData($preupdatedata) {
		$this->preupdatedata = $preupdatedata;
	}
	function getController(){
		return $this->controller;
	}
	function setController($controller) {
		$this->controller = $controller;
	}
	function getSendPasswdEmail(){
		return $this->sendpasswdemail;
	}
	function setSendPasswdEmail($sendpasswdemail) {
		$this->sendpasswdemail = $sendpasswdemail;
	}
	function getDefaultPermissions(){
		return $this->defaultpermissions;
	}
	function setDefaultPermissions($defaultpermissions) {
		$this->defaultpermissions = $defaultpermissions;
	}
	function getFolderAccess(){
		return $this->folderaccess;
	}
	function setFolderAccess($folderaccess) {
		$this->folderaccess = $folderaccess;
	}
	
	# Contructor method for custom initialization
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("dateofbirth","activationdate","dateinvited"));
		
		# set the custom error messages
       	$this->addCustomErrorMessages(array(
       									"typeids.notblank" => "User role not specified",
       									"firstname.notblank" => $this->translate->_("profile_firstname_error"),
       									"lastname.notblank" => $this->translate->_("profile_lastname_error")
       	       						));
	}
	
	# Model relationships
	public function setUp() {
		parent::setUp(); 
		# copied directly from BaseEntity since the createdby can be NULL when a user signs up 
		# automatically set timestamp column values for datecreated and lastupdatedate 
		$this->actAs('Timestampable', 
						array('created' => array(
												'name' => 'datecreated'
											),
							 'updated' => array(
												'name'     =>  'lastupdatedate',    
												'onInsert' => false,
												'options'  =>  array('notnull' => false)
											)
						)
					);
		
		$this->hasOne('UserAccount as creator', array('local' => 'createdby', 'foreign' => 'id'));
		$this->hasOne('Location as district', array('local' => 'districtid', 'foreign' => 'id'));
		$this->hasOne('Folder as defaultfolder', array('local' => 'defaultfolderid', 'foreign' => 'id'));
		$this->hasMany('Access as permissions', array('local' => 'id', 'foreign' => 'userid'));
		$this->hasOne('Company as company', array('local' => 'companyid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation 
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->usernameExists()){
			$this->getErrorStack()->add("username.unique", sprintf($this->translate->_("profile_username_unique_error"), $this->getUsername()));	
		}
		# validate that email is unique
		if($this->emailExists()){
			$this->getErrorStack()->add("email.unique", sprintf($this->translate->_("profile_email_unique_error"), $this->getEmail()));
		}
		
		if(!isEmptyString($this->getName()) && $this->nameExists()){
			$this->getErrorStack()->add("name.unique", "User with name <b>".$this->getName()."</b> already exists");
		}
		
		
		# validate attempt to change password with an invalid current password
		if(!isEmptyString($this->getNewPassword())){
			if(!isEmptyString($this->getOldPassword()) && sha1($this->getOldPassword()) != $this->getTrx()){
				$this->getErrorStack()->add("oldpassword", $this->translate->_("profile_oldpassword_invalid_error"));
			} else {
				$this->setPassword(sha1($this->getNewPassword()));
			}
		}
	}
	# determine if name has already been assigned
	function nameExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM useraccount WHERE 
		(
		concat_ws(' ', firstname, lastname) =  '".$this->getName()."' OR 
		concat_ws(' ', lastname, firstname) = '".$this->getName()."' 
		) 
		AND companyid = '".$this->getCompanyID()."' AND type <>  '4' ".$id_check;
		// debugMessage($query); exit;
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	# determine if the username has already been assigned
	function usernameExists($username =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		
		if(isEmptyString($username)){
			$username = $this->getUsername();
		}
		$query = "SELECT id FROM useraccount WHERE username = '".$username."' AND username <> '' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	# determine if the email has already been assigned
	function emailExists($email =''){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		if(isEmptyString($email)){
			$email = $this->getEmail();
		}
		$query = "SELECT id FROM useraccount WHERE email = '".$email."' AND email <> '' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	# determine if the refno has already been assigned to another organisation
	function phoneExists($phone =''){
		$conn = Doctrine_Manager::connection();
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query_custom = '';
		if(isEmptyString($phone)){
			$phone = $this->getPhone();
		}
		
		# unique phone
		$phone_query = "SELECT id FROM useraccount WHERE phone = '".$phone."' ".$id_check;
		// debugMessage($phone_query);
		$result = $conn->fetchOne($phone_query);
		// debugMessage($result);exit();
		if(isEmptyString($result)){
			return false;
		} else {
			$user = new UserAccount();
			$user->populate($result);
			$this->setNameofUser($user->getfirstname().' '.$user->getlastname());
		}
		return true;
	}
	# check for user using password and phone number
	function validateUserUsingPhone($password, $phone){
		$formattedphone = getFullPhone($phone);
		$conn = Doctrine_Manager::connection();
		$query = "SELECT * from useraccount as m where (m.phone = '".$formattedphone."' || m.phone = '".$phone."') AND (m.password = '".sha1($password)."' || m.trx = '".sha1($password)."') ";
		// debugMessage($query);
		$result = $conn->fetchRow($query);
		// debugMessage($result);
		return $result;
	}
	# check for user using password and phone number
	function validateExistingPhone($phone){
		$formattedphone = getFullPhone($phone);
		$conn = Doctrine_Manager::connection();
		$query = " ";
		// debugMessage($query);
		$result = $conn->fetchRow($query);
		// debugMessage($result);
		return $result;
	}
	# Preprocess model data
	function processPost($formvalues){
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues);  
		if(isArrayKeyAnEmptyString('companyid', $formvalues)){
			unset($formvalues['companyid']);
		}
		if(!isArrayKeyAnEmptyString('firstname', $formvalues)){
			// $formvalues['firstname'] = ucwords(strtolower($formvalues['firstname']));
			$formvalues['firstname'] = strtoupper($formvalues['firstname']);
		}
		if(!isArrayKeyAnEmptyString('lastname', $formvalues)){
			$formvalues['lastname'] = strtoupper($formvalues['lastname']);
		}
		# if the passwords are not changed , set value to null
		if(!isArrayKeyAnEmptyString('password', $formvalues)){
			$formvalues['trx'] = encode(strrev($formvalues['password']));
			$formvalues['password'] = sha1($formvalues['password']);
		} else {
			unset($formvalues['password']);
		}
		if(!isArrayKeyAnEmptyString('oldpassword', $formvalues)){
			$this->setoldpassword($formvalues['oldpassword']);
		}
		if(!isArrayKeyAnEmptyString('confirmpassword', $formvalues)){
			$this->setconfirmpassword($formvalues['confirmpassword']);
		}
		if(!isArrayKeyAnEmptyString('newpassword', $formvalues)){
			$this->setNewPassword($formvalues['newpassword']);
			$formvalues['trx'] = encode(strrev($formvalues['newpassword']));
			$formvalues['password'] = sha1($formvalues['newpassword']);
		}

		if(!isArrayKeyAnEmptyString('phone', $formvalues)){
			if(stringContains('-', $formvalues['phone'])){
				$formvalues['phone'] = str_replace('-', '', $formvalues['phone']);
			}
		}
		if(!isArrayKeyAnEmptyString('dateofbirth', $formvalues)){
			unset($formvalues['dateofbirth']);
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']); 
		}
		if(isArrayKeyAnEmptyString('agreedtoterms', $formvalues)){
			unset($formvalues['agreedtoterms']); 
		}
		if(isArrayKeyAnEmptyString('gender', $formvalues)){
			unset($formvalues['gender']); 
		}
		if(isArrayKeyAnEmptyString('activationdate', $formvalues)){
			unset($formvalues['activationdate']); 
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']); 
		}
		if(!isArrayKeyAnEmptyString('isinvited', $formvalues)){
			debugMessage('invited '.$formvalues['isinvited']);
			if($formvalues['isinvited'] == 1){
				$this->setIsBeingInvited($formvalues['isinvited']);
				$session->setVar('invitedata', array('invitedbyid' => $session->getVar('userid'), 'dateinvited' => date('Y-m-d')));
			}
			if($formvalues['isinvited'] == 2){
				if(!isArrayKeyAnEmptyString('emailpassword', $formvalues)){
					$this->setSendPasswdEmail('1');
				}
			}
		} else {
			$formvalues['isinvited'] = NULL;
		}
		
		# process the user roles if sent as arrays
		if(isArrayKeyAnEmptyString('id', $formvalues)) {
			if(!isArrayKeyAnEmptyString('type', $formvalues)) {
				if(isArrayKeyAnEmptyString('createdby', $formvalues)) {
					$formvalues['createdby'] = DEFAULT_ID;
				}
				$formvalues['activationkey'] = $this->generateActivationKey();
			}
		}
		if(!isArrayKeyAnEmptyString('typeids', $formvalues)) {
			$formvalues['type'] = $formvalues['typeids'];
		}
		if(!isArrayKeyAnEmptyString('type', $formvalues)) {
			$formvalues['typeids'] = $formvalues['type'];
		}
		
		if(isArrayKeyAnEmptyString('defaultsort', $formvalues)){
			unset($formvalues['defaultsort']);
		}
		if(isArrayKeyAnEmptyString('defaultfolderid', $formvalues)){
			unset($formvalues['defaultfolderid']);
		}
		if(isArrayKeyAnEmptyString('lastsortupdate', $formvalues)){
			unset($formvalues['lastsortupdate']);
		}
		if(isArrayKeyAnEmptyString('defaultsort', $formvalues)){
			$formvalues['defaultsort'] = 2;
		}
		
		if(!isArrayKeyAnEmptyString('folders', $formvalues)){
			$this->setFolderAccess($formvalues['folders']);
		}
		if(!isArrayKeyAnEmptyString('permissions', $formvalues)){
			$this->setDefaultPermissions($formvalues['permissions']);
			unset($formvalues['permissions']);
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	
	# Custom save logic
	function transactionSave(){
		$conn = Doctrine_Manager::connection();
		$session = SessionWrapper::getInstance();
		
		# begin transaction to save
		try {
			$conn->beginTransaction();
			# initial save
			$this->save();
			
			# commit changes
			$conn->commit();
			
			$resave = false;
			if(isEmptyString($this->getCreatedBy()) || ($this->getCreatedBy() == 0)){
				$this->setCreatedBy($this->getID());
				$resave = true;
			}
			if(isEmptyString($this->getRefNo())){
				$this->setRefNo(($this->getID() + 1000));
				$resave = true;
			}
			
			if($resave){
				# initial save
				$this->save();
			}
			
			# invite via email
			if($this->getIsInvited() == 1){
				$this->inviteViaEmail();
			}
			# activate account immediately and send  alert to user if configured
			if($this->getIsInvited() == 2){
				$this->activateAccount('', false);
				# send email to user if option selected
				if($this->getSendPasswdEmail() == '1'){
					$this->sendProfilePasswordNotification();
				}
			}
			
			# send signup notification to email for public registration
			if($this->isSelfRegistered() || ($this->getCreatedBy() == 0)){
				$this->setCreatedBy($this->getID());
				$this->save();
				
				$this->sendSignupNotification(); // exit;
			}
			
			# add log to audit trail

			$controller = $this->getController();

			$profiletype = 'User Profile ';
			$usecase = '1.3';
			$module = '1';
			$type = USER_CREATE;
			$details = $profiletype." ".$this->getName()."  created";
			
			$audit_values = getAuditInstance();
			$audit_values['module'] = $module;
			$audit_values['actionid'] = '10';
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['status'] = "Y";
			$audit_values['transactiondetails'] = $details; 
			$this->notify(new sfEvent($this, $type, $audit_values));
			
			return true;
			
		} catch(Exception $e){
			// debugMessage('Error is '.$e->getMessage()); exit;
			$conn->rollback();
			// debugMessage('Error is '.$e->getMessage());
			throw new Exception($e->getMessage());
			return false;
		}
	}
	# update after
	function beforeUpdate(){
		$session = SessionWrapper::getInstance();
		# set object data to class variable before update
		$user = new UserAccount();
		$user->populate($this->getID());
		$this->setPreUpdateData($user->toArray());
		// exit;
		return true;
	}
	# update after
	function afterUpdate($savetoaudit = true, $section = 'overview'){
		$session = SessionWrapper::getInstance();
		
		$resave = false;
		if(isEmptyString($this->getCreatedBy()) || ($this->getCreatedBy() == 0)){
			$this->setCreatedBy($this->getID());
			$resave = true;
		}
		if(isEmptyString($this->getRefNo())){
			$this->setRefNo(($this->getID() + 1000));
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
		
		# check if user is being invited during update
		if($this->getIsBeingInvited() == 1){
			if(isEmptyString($this->getEmail())){
				$session->setVar('warningmessage', "Activation not sent. No email available on profile");
			} else {
		 		$this->inviteViaEmail();
			}
        }
        
        # activate account immediately and send  alert to user if configured
        if($this->getIsInvited() == 2){
        if(!$this->isActivated()){
        		$this->activateAccount('', false);
        
        		# add log to audit trail
        		$audit_values = getAuditInstance();
        		$audit_values['module'] = '1';
        		$audit_values['usecase'] = '1.15';
        		$audit_values['transactiontype'] = USER_INVITE;
        		$audit_values['status'] = "Y";
        		$audit_values['companyid'] = $this->getCompanyID();
        		$audit_values['transactiondetails'] = "User ".$this->getName()."  activated";
        		$this->notify(new sfEvent($this, USER_INVITE, $audit_values));
        	}
        	# send email to user if option selected
        	if($this->getSendPasswdEmail() == '1'){
        		$this->sendProfilePasswordNotification();
        	}
        }
        
        if($savetoaudit){
	        # set postupdate from class object, and then save to audit
	        $prejson = json_encode($this->getPreUpdateData()); // debugMessage($prejson);
	        
	        $this->clearRelated(); // clear any current object relations
	        $after = $this->toArray(); // debugMessage($after);
	        $postjson = json_encode($after); // debugMessage($postjson);
	        
	        // $diff = array_diff($prejson, $postjson);  // debugMessage($diff);
	        $diff = array_diff($this->getPreUpdateData(), $after);
	        $jsondiff = json_encode($diff); // debugMessage($jsondiff);
	        
	        $location = "About"; $actionid = "11";
	        if($section == "tbd"){
	        	$location = "TBD";
	        	$actionid = "";
	        }
	        
        	$profiletype = 'User Profile ';
        	$usecase = '1.4';
        	$module = '1';
        	$type = USER_UPDATE;
        	$details = "User Profile for ".$this->getName()." updated";
	        
	       	$audit_values = getAuditInstance();
	        $audit_values['module'] = $module;
	        $actionid = "11";
	        $audit_values['usecase'] = $usecase;
	        $audit_values['transactiontype'] = $type;
	        $audit_values['status'] = "Y";
	        $audit_values['companyid'] = $this->getCompanyID();
	        $audit_values['transactiondetails'] = $details;
	        $audit_values['isupdate'] = 1;
	        $audit_values['prejson'] = $prejson;
	        $audit_values['postjson'] = $postjson;
	        $audit_values['jsondiff'] = $jsondiff;
	       	$this->notify(new sfEvent($this, $type, $audit_values));
        }
        
        return true;
	}
	# find duplicates after save
	function getDuplicates(){
		$q = Doctrine_Query::create()->from('UserAccount u')->where("u.email = '".$this->getEmail()."' AND u.username = '".$this->getUserName()."' AND u.id <> '".$this->getID()."' ");
		
		$result = $q->execute();
		return $result;
	}
	# Send notification to invite person to create an account
	function sendProfileInvitationNotification($sendmail = true) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
		
		// assign values
		$template->assign('inviter', $this->getName());
		$template->assign('firstname', isEmptyString($this->getFirstName()) ? 'Friend' : $this->getFirstName());
		$template->assign('inviter', $this->getCreator()->getName());
		// the actual url will be built in the view
		$invitelink = $template->serverUrl($template->baseUrl('signup/index/id/'.encode($this->getID())."/"));
		$body = '<p>You have been invited to activate your '.getAppName().' Account.</p>
		<p>To activate your account, <a href="'.$invitelink.'>">click here</a> or alternatively copy the activation link below and paste it in the address window of the browser.<br />'.$invitelink.'</p>
		<p>After activating your account, you will be able to login using the your email/username and password.</p>';
		$template->assign('contents', $body);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/1/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
	
		// configure base stuff
		$mail->addTo($this->getEmail());
		// set the send of the email address
		$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$subject = $this->translate->_('profile_email_subject_invite_user');
		$template->assign('subject', $subject);
		$mail->setSubject($subject);
		
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('emailbody.phtml'));
		// debugMessage($template->render('emailbody.phtml')); // exit();
	
		if($sendmail){
			if(isEmptyString($this->getEmail())){
				$session->setVar('warningmessage', "Invitation not sent. No email available on profile");
			} else {
				try {
					$mail->send();
					debugMessage('mail sent '.$this->getEmail());
				} catch (Exception $e) {
					$session->setVar("warningmessage", 'Email notification not sent! '.$e->getMessage());
					debugMessage('Email notification not sent! '.$e->getMessage());
				}
			}
		} else {
			echo $template->render('emailbody.phtml');
		}
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
	
		return true;
	}
	
	# Send notification to invite person to create an account
	function sendCredentialsViaEmail($sendmail = true) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
	
		// assign values
		$template->assign('inviter', $this->getName());
		$template->assign('firstname', isEmptyString($this->getFirstName()) ? 'Friend' : $this->getFirstName());
		$template->assign('inviter', $this->getCreator()->getName());
		// the actual url will be built in the view
		$viewurl = $template->serverUrl($template->baseUrl('user/login'));
		$template->assign('invitelink', $viewurl);
		
		$passtxt = $this->getforcechangepasswd() ? 'temporary' : '';
		$passtxt2 = $this->getforcechangepasswd() ? '<p><b>Note:</b> This password is case sensitive and you will be required to change it at the next logon</p>' : '<p><b>Note</b>: This password is case sensitive.</p>';
		
		$message_contents = "<p>Your <b>".getAppName()."</b> Account has been successfully activated. </p>
		<p>You can now login at ".$view->serverUrl($view->baseUrl('user/login'))." using the ".$passtxt." password and username below; </p>
		<p><b>Username: </b>".$this->getUsername()." <br><b>Password:</b> ".strrev(decode($this->getTrx()))."</p> ".$passtxt2." <p>If you any other issues, questions or feedback, please feel free to contact us.</p>";
		$template->assign('contents', $message_contents);
		
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/2/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
	
		// configure base stuff
		$mail->addTo($this->getEmail());
		// set the send of the email address
		$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$subject = "Account Activation";
		$template->assign('subject', $subject);
		$mail->setSubject($subject);
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('emailbody.phtml'));
		// debugMessage($template->render('emailbody.phtml')); exit();
	
		if($sendmail){
			if(isEmptyString($this->getEmail())){
				$session->setVar('warningmessage', "Email not sent. No email available on profile");
			} else {
				try {
					$mail->send();
					debugMessage('mail sent '.$this->getEmail());
				} catch (Exception $e) {
					$session->setVar("warningmessage", 'Email notification not sent! '.$e->getMessage());
					debugMessage('Email notification not sent! '.$e->getMessage());
				}
			}
		} else {
			echo $template->render('emailbody.phtml');
		}
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
	
		return true;
	}
	
	# Send a notification to agent that their account will be approved shortly
	function sendSignupNotification($sendmail = true) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
	
		# assign values
		$template->assign('firstname', $this->getFirstName());
		$subject = "Account Activation";
		$save_toinbox = false;
		$type = "";
		$subtype = "";
		
		$activationurl = $template->serverUrl($template->baseUrl('signup/activate/id/'.encode($this->getID())."/actkey/".$this->getActivationKey()."/"));
		$body = '<p>You are most welcome to <b>'.getAppFullName().'</b></p><p>To activate your account, <a href="'.$activationurl.'" style="display:inline;">click here</a> or alternatively copy the activation link below and paste it in the address window of the browser.<br />'.$activationurl.' </p><p>After activation, you will be able to login using the your username or email and the password you provided during registration.</p>';
		
		$template->assign('contents', $body);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/4/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);

		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');

		# configure base stuff
		$mail->addTo($this->getEmail(), $this->getName());
		$mail->setFrom(getDefaultAdminEmail(true), getDefaultAdminName(true));
		$code = "";
		$mail->setSubject($subject.$code);
		# render the view as the body of the email
		$html = $template->render('emailbody.phtml');
		$mail->setBodyHtml($html);
		// echo ($html); // exit();
			
		if($sendmail){
			if(isEmptyString($this->getEmail())){
				$session->setVar('warningmessage', "Email notification not sent! No email available on profile");
			} else {
				try {
					$mail->send();
					$session->setVar("invitesuccess", 'Email sent to '.$this->getEmail());
				} catch (Exception $e) {
					$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
				}
			}
		} else {
			echo ($html); // exit();
		}
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();

		if($save_toinbox && $sendmail){
			# save copy of message to user's application inbox
			$message_dataarray = array(
					"senderid" => DEFAULT_ID,
					"subject" => $subject,
					"contents" => $body,
					"html" => $html,
					"type" => $type,
					"subtype" => $subtype,
					"refid" => $this->getID(),
					"recipients" => array(
							md5(1) => array("recipientid" => $this->getID())
					)
			); // debugMessage($message_dataarray);
			// process message data
			$message = new Message();
			$message->processPost($message_dataarray);
			$message->save();
		}

		return true;
	}
	# Send notification to inform user that profile has been activated
	function sendActivationConfirmationNotification($sendmail = true) {
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
		$session = SessionWrapper::getInstance();
	
		// assign values
		$template->assign('firstname', $this->getFirstName());
		$code = "";
		$subject = "Account Activation ";
		$save_toinbox = true;
		$type = "useraccount";
		$subtype = "profile_activated";
		$message_contents = "<p>This is to confirm that your Account has been successfully activated. </p>
		<p>You can login anytime at ".$view->serverUrl($view->baseUrl('user/login'))." using any of your identities(email or username) and the password you provided during registration. </p>
		<p>If you happen to forget your login credentials, go to ".$view->serverUrl($view->baseUrl('user/recoverpassword')).". For any other issues, questions or feedback, please feel free to contact us.</p>";
		$template->assign('contents', $message_contents);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/3/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
	
		// configure base stuff
		$mail->addTo($this->getEmail(), $this->getName());
		// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());
	
		$subject = sprintf($this->translate->_('profile_email_subject_invite_confirmation'), getAppName());
		$mail->setSubject($subject);
		// render the view as the body of the email
	
		$html = $template->render('default.phtml');
		$mail->setBodyHtml($html);
		// debugMessage($html); // exit();
			
		if($sendmail){
			if(isEmptyString($this->getEmail())){
				$session->setVar('warningmessage', "Invitation not sent. No email available on profile");
			} else {
				try {
			$mail->send();
			
			$usecase = '1.16';
			$module = '1';
			$type = USER_ACTIVATE;
			$details = "Account activation confirmation for ".$this->getName()." sent to ".$this->getEmail();
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 16;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "Y";
			$audit_values['companyid'] = $this->getCompanyID();
			$this->notify(new sfEvent($this, $type, $audit_values));
			
		} catch (Exception $e) {
			$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
			
			$usecase = '1.16';
			$module = '1';
			$type = USER_ACTIVATE;
			$details = "Account activation confirmation for ".$this->getName()." not delivered to ".$this->getEmail().". Error: ".$e->getMessage();
				
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 16;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "N";
			$audit_values['companyid'] = $this->getCompanyID();
			$this->notify(new sfEvent($this, $type, $audit_values));
		}
			}
		} else {
			echo $template->render('emailbody.phtml');
		}
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
	
		if($save_toinbox){
			# save copy of message to user's application inbox
			$message_dataarray = array(
					"senderid" => DEFAULT_ID,
					"subject" => $subject,
					"contents" => $message_contents,
					"html" => $html,
					"type" => $type,
					"subtype" => $subtype,
					"refid" => $this->getID(),
					"recipients" => array(
							md5(1) => array("recipientid" => $this->getID())
					)
			); // debugMessage($message_dataarray);
			// process message data
			$message = new Message();
			$message->processPost($message_dataarray);
			$message->save();
		}
	
		return true;
	}
	# set activation code to change user's email
	function triggerEmailChange($newemail) {
		$this->setActivationKey($this->generateActivationKey());
		$this->setTempEmail($newemail);
		$this->save();
		$this->sendNewEmailActivation();
		return true;
	}
	# send new email change confirmation
	function sendNewEmailActivation($sendmail = true) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
		
		// assign values
		$template->assign('firstname', $this->getFirstName());
		$template->assign('newemail', $this->getTempEmail());
		$activationurl = $template->serverUrl($template->baseUrl('profile/newemail/id/'.encode($this->getID()).'/actkey/'.$this->getActivationKey()));
		$message_contents = '<p>You recently requested to change your <b><?php echo $this->appname; ?></b> Email Address to <b><?php echo $this->toemail; ?></b>.</p><p>To confirm this request, click on the activation link below:<br />'.$activationurl.'</p><p>If the page fails to open, copy the activation link and paste it in the address window of the browser.</p><p>On confirmation, you will no longer be able to login using <b><?php echo $this->fromemail; ?></b>.</p><p>If you need any further assistance or have any question, please feel free to <a href="'.$template->serverUrl($template->baseUrl('contactus')).'" title="Contact us">Contact us</a></p>';
		$template->assign('contents', $message_contents);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/7/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		
		// configure base stuff
		$mail->addTo($this->getEmail(), $this->getName());
		// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());
		
		$mail->setSubject($this->translate->_('profile_email_subject_changeemail'));
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('emailbody.phtml'));
		// debugMessage($template->render('emailbody.phtml')); exit();
		if($sendmail){
			try {
				$mail->send();
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
			}
		} else {
			echo $template->render('emailbody.phtml');
		}
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
	
		return true;
	}
	# Send a notification to agent that their account will be approved shortly
	function sendDeactivateNotification($sendmail = true) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		
		// assign values
		$template->assign('firstname', $this->getFirstName());
		$message_contents = '<p>This email confirms that your account on '.getAppName().' has been deactivated.</p><p>Unfortunately you will no longer be able to login.</p><p>If you need any further assistance or have any question or feedback, please feel free to <a href="'.$template->serverUrl($template->baseUrl('contactus')).'" title="Contact us">Contact us</a></p>';
		$template->assign('contents', $message_contents);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/8/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		
		// configure base stuff
		$mail->addTo($this->getEmail(), $this->getName());
		// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());
		
		$mail->setSubject("Account Deactivation");
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('emailbody.phtml'));
		// debugMessage($template->render('emailbody.phtml')); // exit();
		if(true){
			try {
				$mail->send();

$usecase = '1.6';
			$module = '1';
			$type = USER_DEACTIVATE;
			$details = "Deactivation notification for User ".$this->getName()."  sent to ".$this->getEmail();
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 18;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "Y";
			$audit_values['companyid'] = $this->getCompanyID();
			$this->notify(new sfEvent($this, $type, $audit_values));
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
$usecase = '1.6';
			$module = '1';
			$type = USER_DEACTIVATE;
			$details = "Deactivation notification for User ".$this->getName()."  not delivered to ".$this->getEmail().". Error: ".$e->getMessage();
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 18;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "N";
			$audit_values['companyid'] = $this->getCompanyID();
			$this->notify(new sfEvent($this, $type, $audit_values));
			}
		} else {
			echo $template->render('emailbody.phtml'); 
		}
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
		
		return true;
	}
	# Send a notification to agent that their account will be approved shortly
	function sendBlockedNotification() {
		$session = SessionWrapper::getInstance($sendmail = true);
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
	
		// assign values
		$template->assign('firstname', $this->getFirstName());
		$message_contents = '<p>Unfortunately your account on '.getAppName().' has been temporally blocked for security reasons and you will not be able to login currently.</p><p>If you have any issues, please send an email to admin to resolve by <a href="'.$template->serverUrl($template->baseUrl('contactus')).'" title="Contact us">clicking here </a></p>';
		$template->assign('contents', $message_contents);
		//$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/8/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		// $template->assign('viewonline', $onlineview);
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
	
		// configure base stuff
		$mail->addTo($this->getEmail(), $this->getName());
		// set the send of the email address
		$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
	
		$mail->setSubject("Account Blocked");
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('emailbody.phtml'));
		// debugMessage($template->render('emailbody.phtml')); // exit();
		if(true){
			try {
				$mail->send();
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
			}
		} else {
			echo $template->render('emailbody.phtml');
		}
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
		
		$view = new Zend_View();
		$usecase = '1.6';
		$module = '1';
		$type = USER_DEACTIVATE;
		$details = "Account for User ID ".$this->getID()." blocked for security purposes";
			
		$browser = new Browser();
		$audit_values = $session->getVar('browseraudit');
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['userid'] = $session->getVar('userid');
		$audit_values['url'] = '';
		$audit_values['transactiondetails'] = $details;
		$audit_values['status'] = "Y";
		// debugMessage($audit_values);
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		return true;
	}
	# change email notification to new address
	function sendNewEmailNotification($newemail) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();

		// assign values
		$template->assign('firstname', $this->getFirstName());
		$template->assign('fromemail', $this->getEmail());
		$template->assign('toemail', $newemail);
		$template->assign('code', $this->getActivationKey());
		$viewurl = $template->serverUrl($template->baseUrl('profile/changeemail/id/'.encode($this->getID())."/actkey/".$this->getActivationKey()."/ref/".encode($newemail)."/"));
				$template->assign('activationurl', $viewurl);

		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');

		// configure base stuff
		$mail->addTo($newemail, $this->getName());
		// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());

		$mail->setSubject("Email Change Request");
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('changeemail_newnotification.phtml'));
		// debugMessage($template->render('changeemail_newnotification.phtml')); exit();
		try {
			$mail->send();
		} catch (Exception $e) {
			$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
		}

		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();

		return true;
	}
	# change email notification to old address
	function sendOldEmailNotification($newemail, $sendmail = true) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();

		// assign values
		$template->assign('firstname', $this->getFirstName());
		$template->assign('fromemail', $this->getEmail());
		$template->assign('toemail', $newemail);
		
		$message_contents = '<p>If you need any further assistance or have any question, please feel free to <a href="'.$template->serverUrl($template->baseUrl('contactus')).'" title="Contact us">Contact us</a></p>';
		$template->assign('contents', $message_contents);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/9/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);

		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');

		// configure base stuff
		$mail->addTo($this->getEmail(), $this->getName());
		// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());

		$mail->setSubject("Email Change Request");
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('emailbody.phtml'));
		// debugMessage($template->render('emailbody.phtml')); //exit();
		
		if($sendmail){
			try {
				$mail->send();
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
			}
		} else {
			echo $template->render('emailbody.phtml');
		}

		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();

		return true;
	}
	# invite one user to join. already existing persons
	function inviteOne() {
		$session = SessionWrapper::getInstance();
		
		$this->setIsInvited('1');
		
		// debugMessage($this->toArray()); exit();
		$this->save();

		// send email
		$this->sendProfileInvitationNotification();

		return true;
	}
	# Send contact us notification
	function sendContactNotification($dataarray, $sendmail = true) {
		$session = SessionWrapper::getInstance();
		// debugMessage($dataarray);
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		

		$mail->setSubject("[".getAppName()." Contact Message] ".$dataarray['subject']);
		// set the send of the email address
		$mail->setFrom($dataarray['email'], $dataarray['name']);
		
		$html = "";
		$subject = "New Contact Message: ".$dataarray['subject'];
		$message_contents = nl2br($dataarray['message']);
		$type = "contactus";
		$subtype = "newmessage";
		# save copy of message to user's application inbox
		$message_dataarray = array(
				"sendername" => $dataarray['name'],
				"senderemail" => $dataarray['email'],
				"subject" => $subject,
				"contents" => $message_contents,
				"html" => $html,
				"type" => $type,
				"subtype" => $subtype,
				"recipients" => array(
						md5(1) => array("recipientid" => DEFAULT_ID)
				)
		); // debugMessage($message_dataarray);
		// process message data
		$message = new Message();
		$message->processPost($message_dataarray); // debugMessage($message->toArray()); debugMessage('error is '.$message->getErrorStackAsString());
		if($sendmail){
			$message->save(); // send the message
		}	
		// assign values
		$viewurl = $template->serverUrl($template->baseUrl('notifications/view/id/'.encode($message->getID())));
		$message_contents = '<p>Dear Admin,</p> <p>A New <?php echo $this->appname; ?> Contact Form message has been received with the following details;</p> 
		<p>
		<b>Sender Name: </b>'.$dataarray['name'].'<br />
		<b>Sender Email: </b>'.$dataarray['email'].'<br />
		<b>Subject: </b>'.$dataarray['subject'].'<br />
		<b>Message: </b>'.$dataarray['message'].'</p> <p>To view this message online, <a href="'.$viewurl.'">click here</a></p><br />';
		$template->assign('contents', $message_contents);
		$onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/6/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
		$template->assign('viewonline', $onlineview);
		
		// configure base stuff
		$mail->addTo(getSupportEmailAddress());
		$mail->addCc(getAltSupportEmailAddress());
		// render the view as the body of the email
		$html = $template->render('emailbody.phtml');
		$mail->setBodyHtml($html);
		// debugMessage($html); // exit();
		
		if($sendmail){
			try {
				$mail->send();
			} catch (Exception $e) {
				$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
			}
		} else {
			echo $html;
		}

		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
		
		return true;
	}
	// TODO log to audit trail
	function sendContactConfirmation($dataarray) {
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
	
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		
		$template->assign('firstname', $dataarray['name']);
		$template->assign('name', $dataarray['name']);
		$template->assign('email', $dataarray['email']);
		$template->assign('subject', $dataarray['subject']);
		$template->assign('message', nl2br($dataarray['message']));
		$template->assign('showsubject', 1);
		
		$viewurl = $template->serverUrl($template->baseUrl('message/view/id/'.encode($this->getID())));
		$template->assign('viewurl', $viewurl);
		// debugMessage($first);
		// assign values
		$template->assign('message', nl2br($dataarray['message']));
		$message_contents = "<p>This is an auto generated email to acknowledge receipt of your message via ".getAppName()." contact form with the following details; </p>
		<p><b>Subject:</b> ".$dataarray['subject']."</p>
		<p><b>Message:</b> ".$dataarray['message']."</p>
		<p>Thank you for contacting us! We shall now respond to you shortly.</p>";
		$template->assign('contents', $message_contents);
		
		$mail->setSubject("Contact Confirmation");
		// set the send of the email address
		// $mail->setFrom($dataarray['email'], $dataarray['name']);
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());
	
		// configure base stuff
		$mail->addTo($dataarray['email']);
		// render the view as the body of the email
		$html = $template->render('default.phtml');
		$mail->setBodyHtml($html);
		// debugMessage($template->render('default.phtml')); // exit();
		try {
			$mail->send();
		} catch (Exception $e) {
			$session->setVar(ERROR_MESSAGE, 'Email notification not sent! '.$e->getMessage());
		}
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
	
		return true;
	}
	# invite user to activate via email
	function inviteViaEmail(){
		$session = SessionWrapper::getInstance();
		if($this->sendProfileInvitationNotification()){
			$session->setVar('invitesuccess', "Email Invitation sent to ".$this->getEmail());
		}
	
		return true;
	}
	
	# send login credentials to email
	function sendProfilePasswordNotification(){
		$session = SessionWrapper::getInstance();
		if($this->sendCredentialsViaEmail()){
			$session->setVar('invitesuccess', "Email sent to ".$this->getEmail());
		}
		
		return true;
	}
	# Reset the password for  the user. All checks and validations have been completed
	function resetPassword($newpassword = "") {
	 	# check if the password is empty 
	 	if (isEmptyString($newpassword)) {
	 		# generate a new random password
	 		$newpassword = $this->generatePassword(); 
	 	}
	 	return $this->changePassword($newpassword); 
	}
	# Change the password for the user. All checks and validations have been completed
	function changePassword($newpassword){
		$session = SessionWrapper::getInstance();
		
		// now change the password
		$this->setTrx(encode(strrev($newpassword)));
		$this->setPassword(sha1($newpassword));
		$this->setActivationKey('');
		if($this->getStatus() != 1){
			$this->setStatus(1);
		}
		if($this->getForceChangePasswd() == 1){
			$this->setForceChangePasswd('0');
		}
		if(isEmptyString($this->getActivationDate())){
			$startdate = date("Y-m-d H:i:s", time());
			$this->setActivationDate($startdate);
		}
		
		try {
			$this->save();
			
			$view = new Zend_View();
			$url = $view->serverUrl($view->baseUrl('profile/view/id/'.encode($this->getID())));
			$usecase = '1.8';
			$module = '1';
			$type = USER_CHANGE_PASSWORD;
			$details = "Password Change for ".$this->getName()."   successfull";
			
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 3;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['userid'] = $session->getVar('userid');
			$audit_values['url'] = $url;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "Y";
			// debugMessage($audit_values);
			$this->notify(new sfEvent($this, $type, $audit_values));
			
			return true;
		} catch (Exception $e) {
			$err = $e->getMessage();
			$session->setVar(ERROR_MESSAGE, "Error in changing Password. ".$e->getMessage());
			
			$usecase = '1.8';
			$module = '1';
			$type = USER_CHANGE_PASSWORD;
			$details = "Error in changing Password for ".$this->getName()." . Error: ".$err;
				
			$audit_values = getAuditInstance();
			$audit_values['actionid'] = 3;
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "N";
			$this->notify(new sfEvent($this, $type, $audit_values));
			return false;
		}
	}
	/*
	 * Reset the user's password and send a notification to complete the recovery  
	 *
	 * @return Boolean TRUE if resetting is successful and FALSE if emailaddress security questions and answer is invalid or has no record in the database
	 */
	function recoverPassword() {
		$session = SessionWrapper::getInstance();
		# reset the password and set the next password change date
		$this->setActivationKey($this->generateActivationKey()); // debugMessage($this->toArray());
		$expiry = date("Y-m-d H:i:s", strtotime("now + ".getExpiryDuration()." minutes")); // debugMessage('expiry on '.$expiry); exit;
		$this->setResetexpiry($expiry);
		
		# save the activation key for the user 
		$this->save();
		
		# Send the user the reset password email
		$this->sendRecoverPasswordEmail();
		

		return true;
	}
	/**
	 * Send an email with a link to activate the users' account
	 */
	function sendRecoverPasswordEmail($sendmail = true) {
		$session = SessionWrapper::getInstance(); 
		$template = new EmailTemplate(); 
		// create mail object
		$mail = getMailInstance(); 

		// assign values
		$template->assign('firstname', $this->getFirstName());
		// just send the parameters for the activationurl, the actual url will be built in the view 
		// $template->assign('resetpasswordurl', array("controller"=> "user","action"=> "resetpassword", "actkey" => $this->getActivationKey(), "id" => encode($this->getID())));
		$viewurl = $template->serverUrl($template->baseUrl('user/resetpassword/id/'.encode($this->getID())."/actkey/".$this->getActivationKey()."/")); 
		$template->assign('resetpasswordurl', $viewurl);
		$message_contents = '<p>A request to reset your '.getAppName().' Account has been received. </p><p>You can set a new password by <a href="'.$viewurl.'">clicking here</a> or follow the link below:<br />'.$viewurl.'</p><p>Note: Your reset code will expire in '.getExpiryDuration().' minutes ('.formatDateAndTime($this->getResetExpiry(), false).').</p>';
		$template->assign('contents', $message_contents );
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		
		// configure base stuff
		$mail->addTo($this->getEmail());
		// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());
		
		$mail->setSubject($this->translate->_('profile_email_subject_recoverpassword'));
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('default.phtml'));
		// debugMessage($template->render('default.phtml')); 
		try {
			$mail->send();
			
			$audit_values['actionid'] = 15;
			$usecase = '1.14';
			$module = '1';
			$type = USER_RECOVER_PASSWORD;
			$details = "Recover password request for ".$this->getName()."  sent to ".$this->getEmail();
			
			$audit_values = getAuditInstance();
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "Y";
			$audit_values['companyid'] = $this->getCompanyID();
			$this->notify(new sfEvent($this, $type, $audit_values));
			
		} catch (Exception $e) {
			$msg = 'Email notification not sent! '.$e->getMessage();
			$session->setVar(ERROR_MESSAGE, $msg);
			
			$audit_values['actionid'] = 15;
			$usecase = '1.14';
			$module = '1';
			$type = USER_RECOVER_PASSWORD;
			$details = "Recover password email for ".$this->getName()."  not sent to ".$this->getEmail();
				
			$audit_values = getAuditInstance();
			$audit_values['module'] = $module;
			$audit_values['usecase'] = $usecase;
			$audit_values['transactiontype'] = $type;
			$audit_values['transactiondetails'] = $details;
			$audit_values['status'] = "N";
			$audit_values['companyid'] = $this->getCompanyID();
			$this->notify(new sfEvent($this, $type, $audit_values));
		}
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
		
		return true;
   }
   /**
    * Process the activation key from the activation email
    * 
    * @param $actkey The activation key 
    * 
    * @return bool TRUE if the signup process completes successfully, false if activation key is invalid or save fails
    */
   function activateAccount($actkey, $checkkey = true) {
	   	# validate the activation key
	   	if($this->getActivationKey() != $actkey && $checkkey){
		   	$this->getErrorStack()->add("user.activationkey", $this->translate->_("profile_invalid_actkey_error"));
		   	return false;
	   	}
		
		# set active to true and blank out activation key
		$this->setStatus(1);
		$this->setActivationKey("");
		if($this->getforcechangepasswd() == 1){
			$this->setActivationKey($this->generateActivationKey());
		}
		$startdate = date("Y-m-d H:i:s");
		$this->setActivationDate($startdate);
	
		# save
		try {
			$this->save();
			return true;
			
		} catch (Exception $e){
			$this->getErrorStack()->add("user.activation", $this->translate->_("profile_activation_error"));
			$this->logger->err("Error activating User ".$this->getEmail()." ".$e->getMessage());
			
			return false;
		}
   	}
   
	/**
    * Process the deactivation for an agent
    * 
    * @param $actkey The activation key 
    * 
    * @return bool TRUE if the signup process completes successfully, false if activation key is invalid or save fails
    */
   function deactivateAccount($status = 0) {
   		$session = SessionWrapper::getInstance(); 
   		
		# set active to true and blank out activation key
		$this->setStatus($status);
   		if($this->getStatus() == 1){
   			// $this->setforcechangepasswd(1);
   			$this->setActivationKey($this->generateActivationKey());
   		}		
		
		$this->save();
		
		$this->sendAccountStatusChangeEmail(true); 
		
		$module = '1';
		if($status == 2){
			$usecase = '1.6';
			$type = USER_DEACTIVATE;
			$details = "User ".$this->getName()."  deactivated ";
		}
		if($status == 1){
			$usecase = '1.7';
			$type = USER_REACTIVATE;
			$details = "User ".$this->getName()."  Activated ";
		}
		
		$audit_values = getAuditInstance();
		$audit_values['actionid'] = 18;
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['transactiondetails'] = $details;
		$audit_values['companyid'] = $this->getCompanyID();
		$audit_values['status'] = "Y";
		$this->notify(new sfEvent($this, $type, $audit_values));
		
		return true;
   }
   /**
    * Send an email with a link to activate the users' account
    */
   function sendAccountStatusChangeEmail($sendmail = true) {
	   	$session = SessionWrapper::getInstance();
	   	$template = new EmailTemplate();
	   	// create mail object
	   	$mail = getMailInstance();
	   
	   	// assign values
	   	$template->assign('firstname', $this->getFirstName());
	   	$message_contents = '<p>Unfortunately your account on <b>'.getAppFullName().'</b> has been disabled/deactivated by the Admin. You will now no longer be able to login going forward.</p><p>If you need any further assistance or have any question/feedback, please feel free to <a href="'.$template->serverUrl($template->baseUrl('contactus')).'" title="Contact us">Contact us</a></p>';
	   	if($this->getStatus() == 1){
	   		$message_contents = '<p>Your account on <b>'.getAppFullName().'</b> has been activated by Admin. You can now login.</p><p>If you need any further assistance or have any question/feedback, please feel free to <a href="'.$template->serverUrl($template->baseUrl('contactus')).'" title="Contact us">Contact us</a></p>';
	   	}
	   
	   	$template->assign('contents', $message_contents);
	   	// $onlineview = '<a href="'.$template->serverUrl($template->baseUrl('user/viewemail/type/10/id/'.encode($this->getID())."/")).'" style="text-align:center; font-size:10px;">Click here to view this email in a browser</a>';
	   	// $template->assign('viewonline', $onlineview);
	   
	   	$mail->clearRecipients();
	   	$mail->clearSubject();
	   	$mail->setBodyHtml('');
	   
	   	// configure base stuff
	   	$mail->addTo($this->getEmail());
	   	// set the send of the email address
		$mail->clearFrom();
		$mail->clearReplyTo();
	   	$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName());
		$mail->setReplyTo(getSupportEmailAddress());
	   
	   	if($this->getStatus() == 2){
	   		$mail->setSubject("Account Deactivated");
	   	}
	   	if($this->getStatus() == 1){
	   		$mail->setSubject("Account Activated");
	   	}
	   	// render the view as the body of the email
	   	$mail->setBodyHtml($template->render('emailbody.phtml'));
	   	// debugMessage($template->render('emailbody.phtml')); exit;
	   
   		if($sendmail){
			if(isEmptyString($this->getEmail())){
				$session->setVar('warningmessage', "Invitation not sent. No email available on profile");
			} else {
				try {
					$mail->send();
$usecase = '1.6';
	   				$module = '1';
	   				$type = USER_DEACTIVATE;
	   				$details = "Deactivation notification for ".$this->getName()."  sent to ".$this->getEmail();
	   				
	   				$audit_values = getAuditInstance();
	   				$audit_values['actionid'] = 18;
	   				$audit_values['module'] = $module;
	   				$audit_values['usecase'] = $usecase;
	   				$audit_values['transactiontype'] = $type;
	   				$audit_values['transactiondetails'] = $details;
	   				$audit_values['status'] = "Y";
	   				$audit_values['companyid'] = $this->getCompanyID();
	   				$this->notify(new sfEvent($this, $type, $audit_values));
					// debugMessage('mail sent '.$this->getEmail());
					$session->setVar("invitesuccess", 'Email alert sent to '.$this->getEmail());
				} catch (Exception $e) {
					$session->setVar("warningmessage", 'Email notification not sent! '.$e->getMessage());
	   				// debugMessage('Email notification not sent! '.$e->getMessage());
	   				
	   				$usecase = '1.6';
	   				$module = '1';
	   				$type = USER_DEACTIVATE;
	   				$details = "Deactivation notification for ".$this->getName()."  not delivered to ".$this->getEmail().". Error: ".$e->getMessage();
	   				
	   				$audit_values = getAuditInstance();
	   				$audit_values['actionid'] = 18;
	   				$audit_values['module'] = $module;
	   				$audit_values['usecase'] = $usecase;
	   				$audit_values['transactiontype'] = $type;
	   				$audit_values['transactiondetails'] = $details;
	   				$audit_values['status'] = "N";
	   				$audit_values['companyid'] = $this->getCompanyID();
	   				$this->notify(new sfEvent($this, $type, $audit_values));
	   			}
			}
		} else {
			echo $template->render('emailbody.phtml');
		}
	   
	   	$mail->clearRecipients();
	   	$mail->clearSubject();
	   	$mail->setBodyHtml('');
	   	$mail->clearFrom();
	   
	   	return true;
   }
	/**
	 * Generate a new password incase a user wants a new password
	 * 
	 * @return String a random password 
	 */
    function generatePassword() {
		return $this->generateRandomString($this->config->password->passwordminlength);
    }
	/**
	 * Generate a 10 digit activation key  
	 * 
	 * @return String An activation key
	 */
    function generateActivationKey() {
		return substr(md5(uniqid(mt_rand(), 1)), 0, 6);
    }
   /**
    * Find a user account either by their email address 
    * 
    * @param String $email The email
    * @return UserAccount or FALSE if the user with the specified email does not exist 
    */
	function findByEmail($email) {
		# query active user details using email
		$q = Doctrine_Query::create()->from('UserAccount u')->where('u.email = ?', $email);
		$result = $q->fetchOne(); 
		
		# check if the user exists 
		if(!$result){
			# user with specified email does not exist, therefore is valid
			$this->getErrorStack()->add("user.invalid", $this->translate->_("profile_user_invalid_error"));
			return false;
		}
		
		$data = $result->toArray(); 

		# merge all the data including the user groups 
		$this->merge($data);
		# also assign the identifier for the object so that it can be updated
		$this->assignIdentifier($data['id']); 
		
		return true; 
	}
	# find user by email
	function populateByEmail($email) {
		# query active user details using email
		$q = Doctrine_Query::create()->from('UserAccount u')->where('u.email = ?', $email);
		$result = $q->fetchOne(); 
		
		# check if the user exists 
		if(!$result){
			$result = new UserAccount();
		}
		
		return $result; 
	}
	# find user by phone
	function populateByPhone($phone) {
		$query = Doctrine_Query::create()->from('UserAccount m')->where("m.phone = '".$phone."' || m.phone LIKE '%".getShortPhone($phone)."%'");
		//debugMessage($query->getSQLQuery());
		$result = $query->execute();
		return $result->get(0);
	}
	function findByUsername($username) {
		# query active user details using email
		$q = Doctrine_Query::create()->from('UserAccount u')->where('u.username = ?', $username);
		$result = $q->fetchOne(); 
		
		if($result){
			$data = $result->toArray(); 
		} else {
			$data = $this->toArray(); 
		}
		
		# merge all the data including the user groups 
		$this->merge($data);
		# also assign the identifier for the object so that it can be updated
		if($result){
			$this->assignIdentifier($data['id']);
		} 
		
		return true; 
	}
	/**
	 * Return the user's full names, which is a concatenation of the first and last names
	 *
	 * @return String The full name of the user
	 */
	function getName() {
		// return !isEmptyString($this->getDisplayName()) ? $this->getDisplayName() : $this->getFirstName()." ".$this->getLastName();
		return $this->getFirstName()." ".$this->getLastName();
	}
	# function to determine the user's profile path
	function getProfilePath() {
		$path = "";
		$view = new Zend_View();
		// $path = '<a href="'.$view->serverUrl($view->baseUrl('user/'.strtolower($this->getUserName()))).'">'.$view->serverUrl($view->baseUrl('user/'.strtolower($this->getUserName()))).'</a>';
		$path = '<a href="javascript: void(0)">'.$view->serverUrl($view->baseUrl('user/'.strtolower($this->getUserName()))).'</a>';
		return $path;
	}
	/*
	 * TODO Put proper comments
	 */
	function generateRandomString($length) {
		$rand_array = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9");
		$rand_id = "";
		for ($i = 0; $i <= $length; $i++) {
			$rand_id .= $rand_array[rand(0, 35)];
		}
		return $rand_id;
	}
	
	/**
     * Determine the gender strinig of a person
     * @return String the gender
     */
    function getGenderLabel(){
    	if($this->isMale()){
			return 'Male';
		}
		if($this->isFemale()){		
			return 'Female';
		}
		return '';
    }
 	/**
     * Determine if a person is male
     * @return bool
     */
    function isMale(){
    	return $this->getGender() == '1' ? true : false; 
    }
	/**
     * Determine if a person is female
     * @return bool
     */
    function isFemale(){
    	return $this->getGender() == '2' ? true : false; 
    }
	# Determine gender text depending on the gender
	function getGenderText(){
		if($this->isMale()){
			return 'Male';
		}
		if($this->isFemale()){		
			return 'Female';
		}
		return '';
	}
	# Determine if user profile has been activated
	function isActivated(){
		return $this->getStatus() == 1;
	}
	# Determine if user has accepted terms
	function hasAcceptedTerms(){
		return $this->getAgreedToTerms() == 1;
	}
    # Determine if user is active	 
	function isUserActive() {
		return $this->getStatus() == 1;
	}
	# determine text to display depending on the status of the user
	function getStatusLabel(){
		return $this->getStatus() == 1 ? 'Active' : 'Inactive';
	}
    # Determine if user is deactivated
	function isUserInActive() {
		return $this->getStatus() == 0 ? true : false;
	}
	# determine if user has been pending
	function isPending() {
		return $this->getStatus() == 1 ? true : false;
	}
	# determine if user has been deactivated
	function isDeactivated() {
		return $this->getStatus() == 2 ? true : false;
	}
	/**
	 * Display a list of groups that the user belongs
	 *
	 * @return String HTML list of the groups that the user belongs to
	 */
	function displayGroups() {
		return $this->getUserTypeText();
	}
	# function get user type
	function getUserTypeText(){
		// return getUserType($this->getType());
		if(isEmptyString($this->getTypeIDs())){
			return '';
		}
		$usergrps = '';
		$ids = $this->getUserGroupIDs(); // debugMessage($ids); debugMessage($this->getGroupIDs());
		$countgrps = count($ids);
		$groups = getUserType();
		if($countgrps > 0){
			$namearray = array();
			foreach ($ids as $key => $id){
				$namearray[] = isArrayKeyAnEmptyString($id, $groups) ? '' : $groups[$id];
			}
			$usergrps = implode(', ', array_remove_empty($namearray));
		}
		return $usergrps;
	}
	function getUserGroupIDs(){
		$ids = explode(',', str_replace(' ', '', trim($this->getTypeIDs())));
		return $ids;
	}
	function getGroupIDs() {
		return $this->getUserGroupIDs();
	}
	function belongsToGroup($groupid =""){
		if(isEmptyString($groupid)){
			return false;
		}
		$type_array = $this->getUserGroupIDs(); // debugMessage($type_array);
		if(in_array($groupid, $type_array)){
   			return true;
	    }
	    
	    return false;
	}
	# determine if loggedin user is admin
	function isAdmin() {
		$session = SessionWrapper::getInstance(); 
		$return = false;
		$type_array = $this->getUserGroupIDs();
		if(in_array_any(array(1), $type_array)){
			return true;
		}
		return $return;
	}
	function isDistrictClerk() {
		$session = SessionWrapper::getInstance(); 
		$return = false;
		$type_array = $this->getUserGroupIDs();
		if(in_array_any(array(2), $type_array)){
			return true;
		}
		return $return;
	}
	function isContractManager() {
		$session = SessionWrapper::getInstance(); 
		$return = false;
		$type_array = $this->getUserGroupIDs();
		if(in_array_any(array(6), $type_array)){
			return true;
		}
		return $return;
	}
	function isDataClerk() {
		$session = SessionWrapper::getInstance(); 
		$return = false;
		$type_array = $this->getUserGroupIDs();
		if(in_array_any(array(3), $type_array)){
			return true;
		}
		return $return;
	}
	function isContentAdmin() {
		$session = SessionWrapper::getInstance(); 
		$return = false;
		$type_array = $this->getUserGroupIDs();
		if(in_array_any(array(5), $type_array)){
			return true;
		}
		return $return;
	}
    # determine if person has not been invited
    function hasNotBeenInvited() {
    	return $this->getIsInvited() == 0 ? true : false;
    }
    # determine if person has been invited
    function hasBeenInvited() {
    	return $this->getIsInvited() == 1 ? true : false;
    }
    # determine if the farmer registered themselves
    function isSelfRegistered(){
    	return $this->getCreatedBy() == $this->getID() ? true : false;
    }
    # relative path to profile image
    function hasProfileImage(){
    	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_";
    	$real_path = $real_path.$this->getID().DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$this->getProfilePhoto();
    	// debugMessage($real_path);
    	if(file_exists($real_path) && !isEmptyString($this->getProfilePhoto())){
    		return true;
    	}
    	return false;
    }
    # determine if person has profile image
    function getRelativeProfilePicturePath(){
	    $real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_";
	    $real_path = $real_path.$this->getID().DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."medium_".$this->getProfilePhoto();
	    if(file_exists($real_path) && !isEmptyString($this->getProfilePhoto())){
	    	return $real_path;
	    }
	    $real_path = BASE_PATH.DIRECTORY_SEPARATOR."public".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_0".DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."default_medium_male.jpg";
	    if($this->isFemale()){
	    	$real_path = BASE_PATH.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR."user_0".DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR."default_medium_female.jpg";
	    }
	    return $real_path;
    }
    # determine path to small profile picture
    function getSmallPicturePath() {
	    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	    $path = $baseUrl.'/uploads/default/default_small_male.jpg';
	    if($this->isFemale()){
	    	$path = $baseUrl.'/uploads/default/default_small_female.jpg';
	    }
	    if($this->hasProfileImage()){
	    	$path = $baseUrl.'/uploads/users/user_'.$this->getID().'/avatar/small_'.$this->getProfilePhoto();
	    }
	    return $path;
    }
    # determine path to thumbnail profile picture
    function getThumbnailPicturePath() {
	    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	    $path = $baseUrl.'/uploads/default/default_thumbnail_male.jpg';
	    if($this->isFemale()){
	    	$path = $baseUrl.'/uploads/default/default_thumbnail_female.jpg';
	    }
	    if($this->hasProfileImage()){
	    	$path = $baseUrl.'/uploads/users/user_'.$this->getID().'/avatar/thumbnail_'.$this->getProfilePhoto();
	    }
	    return $path;
    }
    # determine path to medium profile picture
    function getMediumPicturePath() {
	    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	    $path = "";
	    $path = $baseUrl.'/uploads/default/default_medium_male.jpg';
	    if($this->isFemale()){
	   		$path = $baseUrl.'/uploads/default/default_medium_female.jpg';
	    }
	    if($this->hasProfileImage()){
	    	$path = $baseUrl.'/uploads/users/user_'.$this->getID().'/avatar/medium_'.$this->getProfilePhoto();
	    }
	    // debugMessage($path);
	    return $path;
    }
    # determine path to large profile picture
    function getLargePicturePath() {
	    $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
	    $path = $baseUrl.'/uploads/default/default_large_male.jpg';
	    if($this->isFemale()){
	    	$path = $baseUrl.'/uploads/default/default_large_female.jpg';
	    }
	    if($this->hasProfileImage()){
	    	$path = $baseUrl.'/uploads/users/user_'.$this->getID().'/avatar/large_'.$this->getProfilePhoto();
	    }
    	# debugMessage($path);
    	return $path;
    }   
    
	# Get the full name of the country from the two digit code
	function getCountryName() {
		if(isEmptyString($this->getCountry())){
			return "--";
		}
		if($this->getCountry() == 'UG'){
			return "Uganda";
		}
		$countries = getCountries(); 
		return $countries[$this->getCountry()];
	}
	# determine full address
	function getFullAddress(){
		$str = '';
		if(!isEmptyString($this->getAddress1())){
			$str .= nl2br($this->getAddress1());
		}
		if(!isEmptyString($this->getAddress2())){
			$str .= '<br>'.nl2br($this->getAddress2());
		}
		return $str;
	}
	function getActivationLink(){
		$view = new Zend_View();
		$viewurl = $view->serverUrl($view->baseUrl('signup/index/id/'.encode($this->getID())."/"));
		return $viewurl;
	}
	function updatePermissions(){
		$session = SessionWrapper::getInstance();
		$permissions = $this->getDefaultPermissions();
		$folders = $this->getFolderAccess();
		debugMessage($permissions);
		debugMessage($folders); // exit;
		$perms = array();
	
		$delete_array = array();
		$delete_collection = new Doctrine_Collection(Doctrine_Core::getTable("Access"));
		if(is_array($permissions)){
			foreach ($permissions as $key => $value){
				if(!in_array($value['folderid'], $folders)){
					// debugMessage($value['folderid']);
					// unset($permissions[$key]);
					if(!isArrayKeyAnEmptyString('id', $value)){
						$delete_array[$key] = $value;
					}
				} else {
					$permissions[$key]['userid'] = $this->getID();
					if(isArrayKeyAnEmptyString('addfolder', $value)){
						$permissions[$key]['addfolder'] = 0;
					}
					if(isArrayKeyAnEmptyString('id', $value)){
						$permissions[$key]['createdby'] = $session->getVar('userid');
					}
					if(isArrayKeyAnEmptyString('create', $value)){
						$permissions[$key]['create'] = 0;
					}
					if(isArrayKeyAnEmptyString('edit', $value)){
						$permissions[$key]['edit'] = 0;
					}
					if(isArrayKeyAnEmptyString('view', $value)){
						$permissions[$key]['view'] = 0;
					}
					if(isArrayKeyAnEmptyString('list', $value)){
						$permissions[$key]['list'] = 0;
					}
					if(isArrayKeyAnEmptyString('delete', $value)){
						$permissions[$key]['delete'] = 0;
					}
					if(isArrayKeyAnEmptyString('move', $value)){
						$permissions[$key]['move'] = 0;
					}
					if(isArrayKeyAnEmptyString('email', $value)){
						$permissions[$key]['email'] = 0;
					}
					if(isArrayKeyAnEmptyString('share', $value)){
						$permissions[$key]['share'] = 0;
					}
					if(isArrayKeyAnEmptyString('archive', $value)){
						$permissions[$key]['archive'] = 0;
					}
				}
			}
			// debugMessage($permissions);
				
			$access_collection = new Doctrine_Collection(Doctrine_Core::getTable("Access"));
			foreach ($permissions as $key => $value){
				$access = new Access();
				if(!isArrayKeyAnEmptyString('id', $value)){
					$access->populate($value['id']);
				}
				if(in_array($value['folderid'], $folders)){
					$access->processPost($value);
					if(!$access->hasError()){
						$access_collection->add($access);
					} else {
						debugMessage($access->getErrorStackAsString());
					}
				}
			}
			// debugMessage($access_collection->toArray()); exit;
			// save access collection
			if($access_collection->count() > 0){
				try {
					$access_collection->save();
					// delete denied objects
					if(count($delete_array) > 0){
						foreach ($delete_array as $k => $value){
							$delete = new Access();
							$delete->populate($value['id']);
							$delete_collection->add($delete);
						}
					}
					if($delete_collection->count() > 0){
						try {
							$delete_collection->delete();
						} catch (Exception $e) {
							debugMessage('err deleting '.$e->getMessage());
						}
					}
				} catch (Exception $e) {
					debugMessage($e->getMessage());
				}
			}
		}
	
		return true;
	}
	function getDirPermissions($folders = ''){
		# query active user details using email
		$q = Doctrine_Query::create()->from('Access a')->where("a.userid = '".$this->getID()."' AND a.folderid IN(".$folders.") ");
		$result = $q->execute();
	
		return $result;
	}
}
?>
