<?php
/**
 * Model for issue details
 */
class ContentIssueDetail extends BaseRecord  {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('content_issue_detail');
		$this->hasColumn('issueid', 'string', 10, array('notblank' => true));
		$this->hasColumn('type', 'string', 4, array('default' => 1)); // 1=Status Update, 2 => Comment
		$this->hasColumn('section', 'string', 4, array('default' => 1));
		$this->hasColumn('status', 'string', 4, array('default' => null));
		$this->hasColumn('userid', 'string', 10, array('notblank' => true));
		$this->hasColumn('inputdate', 'string', 25, array('default' => null));
		$this->hasColumn('contents', 'string', 1000, array('default' => null));
		$this->hasColumn('subject', 'string', 255, array('default' => null));
		$this->hasColumn('notification', 'string', 10000, array('default' => null));
		$this->hasColumn('emails', 'string', 500, array('default' => null));
		$this->hasColumn('files', 'string', 255, array('default' => null));
		
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"userid.notblank" => "Please specify user"
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('ContentIssue as issue', array('local' => 'issueid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as user', array('local' => 'userid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('inputdate', $formvalues)){
			// unset($formvalues['inputdate']);
			$formvalues['inputdate'] = date('Y-m-d H:i:s', time());
		} else {
			$formvalues['inputdate'] = date('Y-m-d H:i:s', strtotime($formvalues['inputdate']));
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(!isEmptyString($this->getStatus()) && $this->getStatus() != $this->getIssue()->getStatus()){
			$this->getIssue()->setStatus($this->getStatus());
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(!isEmptyString($this->getStatus()) && $this->getStatus() != $this->getIssue()->getStatus()){
			$this->getIssue()->setStatus($this->getStatus());
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
}
?>