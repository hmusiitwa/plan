<?php
/**
 * Model for results audit trail
 */
class DatasetResultAudit extends BaseRecord  {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('data_set_result_audit');
		$this->hasColumn('resultid', 'integer', null, array('notblank' => NULL));
		$this->hasColumn('userid', 'integer', null, array('notblank' => true));
		$this->hasColumn('executedbyid', 'integer', null, array('default' => NULL));
		$this->hasColumn('type', 'integer', null, array('default' => NULL)); // 1 moved to stage, 2= Disqualified/Qualified, 3=Applied/Sourced, 4=Updated, 5=Comment, 6=Event, 7=Email, 8=moved candidate, 9 = copied from another job, 10=Moved to HRMagic
		$this->hasColumn('status', 'integer', null, array('default' => NULL));
		$this->hasColumn('section', 'integer', null, array('default' => NULL));
		
		$this->hasColumn('eventdate','date', null, array('default' => NULL));
		$this->hasColumn('filename', 'string', 255);
		$this->hasColumn('remarks', 'string', 255);
		$this->hasColumn('notification', 'string', 10000);
		$this->hasColumn('subject', 'string', 255);
		$this->hasColumn('emails', 'string', 500);
		$this->hasColumn('phones', 'string', 255);
		$this->hasColumn('startdate','date', null, array('default' => NULL));
		$this->hasColumn('enddate','date', null, array('default' => NULL));
		$this->hasColumn('notes', 'string', 1000);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// $this->addDateFields(array("eventdate"));
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"resultid.notblank" => "Please specify result dataset"
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as user',
						array(
							'local' => 'userid',
							'foreign' => 'id'
						)
		);
		$this->hasOne('UserAccount as executedby',
				array(
						'local' => 'executedbyid',
						'foreign' => 'id'
				)
		);
		/* $this->hasOne('DatasetResult as result',
				array(
						'local' => 'resultid',
						'foreign' => 'id'
				)
		); */
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		$config = Zend_Registry::get("config");
		
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('executedbyid', $formvalues)){
			unset($formvalues['executedbyid']);
		}
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	# send event notification 
	function sendEventNotification($isnew = true){
		$session = SessionWrapper::getInstance();
		$template = new EmailTemplate();
		# create mail object
		$mail = getMailInstance();
		$view = new Zend_View();
		
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		
		// assign values
		$template->assign('inviter', $this->getExecutedBy()->getName());
		$template->assign('firstname', $this->getUser()->getFirstName());
		// the actual url will be built in the view
		// $viewurl = $template->serverUrl($template->baseUrl('signup/index/id/'.encode($this->getID())."/"));
		// $template->assign('invitelink', $viewurl);
		// debugMessage($this->getAttendeesList());
		$when = formatDateAndTime($this->getStartDate()).' to '.formatDateAndTime($this->getEndDate());
		if(date('Y-m-d', strtotime($this->getStartDate())) == date('Y-m-d', strtotime($this->getEndDate()))){
			$when = formatDateAndTime($this->getStartDate()).' to '.date('g:i A', strtotime($this->getEndDate()));
		}
		
		$introtext = ' has changed status of dataset ';
		$contents = '<p>'.$this->getExecutedBy()->getName().' '.$introtext.' with the following details;<br /><br />
			<b>TBD:</b> --<br />
			<b>TBD:</b> '.$when.'<br />
			<b>Attendees:</b> --<br />
			<b>Description:</b> '.$this->getNotes().'<br /><br />';
		$template->assign('contents', $contents);
		
		// configure base stuff
		$mail->addTo($this->getUser()->getEmail(), $this->getUser()->getName()); //debugMessage($this->getEmail());
		// set the send of the email address
		$mail->setFrom(getDefaultAdminEmail(), getDefaultAdminName()); //debugMessage(getDefaultAdminEmail().' ~ '.getDefaultAdminName());
		$subject = 'Dataset submission confirmation';
		$template->assign('subject', $subject); // debugMessage($subject);
		$mail->setSubject($subject);
		// render the view as the body of the email
		$mail->setBodyHtml($template->render('default.phtml'));
		// debugMessage($template->render('default.phtml')); // exit();
		
		if(isEmptyString($this->getUser()->getEmail())){
			$session->setVar('warningmessage', "Notification not sent. No email available on profile");
		} else {
			try {
				$mail->send();
				// debugMessage('sent');
				$session->setVar('invitesuccess', "Invitation sent to ".$this->getUser()->getEmail());
				$this->setStatus('1');
				$this->save();
				return 'sent';
			} catch (Exception $e) {
				$session->setVar("warningmessage", 'Email notification not sent! '.$e->getMessage());
				debugMessage($e->getMessage());
			}
		}
		// exit;
		$mail->clearRecipients();
		$mail->clearSubject();
		$mail->setBodyHtml('');
		$mail->clearFrom();
		
		return 'failed';
	}
}

?>