<?php

class ContentAssumption extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_assumption');
		$this->hasColumn('type', 'string', 4);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('programid', 'integer', null, array('default' => NULL));
		$this->hasColumn('projectid','integer', null, array('default' => NULL));
		$this->hasColumn('contentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('activityid', 'integer', null, array('default' => NULL));
		$this->hasColumn('refno', 'string', 10);
		$this->hasColumn('ordering', 'string', 4);
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('reason', 'string', 255);
		$this->hasColumn('howtovalidate', 'string', 500);
		$this->hasColumn('isvalidated', 'string', 4);
		$this->hasColumn('level', 'string', 4);
		$this->hasColumn('impactdesc', 'string', 500);
		$this->hasColumn('responsedetails', 'string', 1000);
		$this->hasColumn('ownertype', 'string', 4);
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('contactid', 'integer', null, array('default' => NULL));
		$this->hasColumn('ownername', 'string', 255);
		$this->hasColumn('opendate', 'string', 15);
		$this->hasColumn('closedate', 'string', 15);
		$this->hasColumn('status', 'string', 4);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		$this->addDateFields(array("opendate","closedate"));
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Program as program', array('local' => 'programid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('Activity as activity', array('local' => 'activityid', 'foreign' => 'id'));
		$this->hasOne('ContentFrame as frame', array('local' => 'contentid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as owner', array('local' => 'userid', 'foreign' => 'id'));
		$this->hasOne('Contact as contact', array('local' => 'contactid', 'foreign' => 'id'));
		$this->hasOne('ContentAssumption as parent', array('local' => 'parentid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('activityid', $formvalues)){
			unset($formvalues['activityid']);
		}
		if(isArrayKeyAnEmptyString('contentid', $formvalues)){
			unset($formvalues['contentid']);
		}
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('programid', $formvalues)){
			unset($formvalues['programid']);
		}
		if(isArrayKeyAnEmptyString('contactid', $formvalues)){
			unset($formvalues['contactid']);
		}
		if(isArrayKeyAnEmptyString('userid', $formvalues)){
			unset($formvalues['userid']);
		}
		if(isArrayKeyAnEmptyString('parentid', $formvalues)){
			unset($formvalues['parentid']);
		}
		if(isArrayKeyAnEmptyString('status', $formvalues)){
			unset($formvalues['status']);
		}
		if(isArrayKeyAnEmptyString('opendate', $formvalues)){
			unset($formvalues['opendate']);
		}
		if(isArrayKeyAnEmptyString('closedate', $formvalues)){
			unset($formvalues['closedate']);
		}
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
}
?>