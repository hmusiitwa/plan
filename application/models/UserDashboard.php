<?php
/**
 * Model for user dashboard details
 */
class UserDashboard extends BaseEntity  {
	
	public function setTableDefinition() {
		parent::setTableDefinition();
		
		$this->setTableName('user_dashboard');
		
		$this->hasColumn('userid', 'string', 10, array('notblank' => true));
		$this->hasColumn('title', 'string', 255);
		$this->hasColumn('shareids', 'string', 500);
		$this->hasColumn('notes', 'string', 1000);
		$this->hasColumn('serialconfig', 'string', 10000, array('default' => null));
		$this->hasColumn('gridconfig', 'string', 255, array('default' => null));
		$this->hasColumn('urlconfig', 'string', 500);
		$this->hasColumn('type', 'string', 4, array('default' => 1)); // 1=Dashboard, 2=>My Reports
		$this->hasColumn('sortorder', 'string', 10);
		$this->hasColumn('status', 'string', 4, array('default' => null));
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
		$this->addCustomErrorMessages(array(
										"userid.notblank" => "Please specify user"
       	       						));     
	}
	/*
	 * Relationships for the model
	 */
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('UserAccount as user', array('local' => 'userid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	 */
	function processPost($formvalues) {
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		if(isArrayKeyAnEmptyString('serialconfig', $formvalues)){
			unset($formvalues['serialconfig']);
		}
		if(!isArrayKeyAnEmptyString('userids', $formvalues)){
			$formvalues['shareids'] = implode(',', $formvalues['userids']);
			unset($formvalues['userids']);
		} else {
			if(!isArrayKeyAnEmptyString('shareids_old', $formvalues)){
				$formvalues['shareids'] = NULL;
			}
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
}
?>