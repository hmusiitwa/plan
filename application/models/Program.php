<?php

class Program extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('program');
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('code', 'string', 15, array('notblank' => true));
		$this->hasColumn('description', 'string', 1000);
		$this->hasColumn('abbrv', 'string', 15);
		$this->hasColumn('type', 'string', 4, array('notblank' => true, 'default' => 1));
		
		$this->hasColumn('managerid', 'string', 15, array('default' => NULL));
		$this->hasColumn('cspid', 'integer', null, array('default' => 1));
		$this->hasColumn('projectid', 'string', 15, array('default' => NULL));
		$this->hasColumn('parentid', 'string', 15, array('default' => NULL));
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name",
       			"code.notblank" => "Please enter Code"
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		/* if($this->refnoExists()){
			$this->getErrorStack()->add("refno.unique", "Entry with Code <b>".$this->getCode()."</b> already exists. Please specify another.");
		}
		if($this->nameExists()){
			$this->getErrorStack()->add("name.unique", "Entry with name <b>".$this->getName()."</b> already exists. Please specify another.");
		} */
	}
	# determine if the username has already been assigned
	function refnoExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM ".$this->getTableName()." WHERE code = '".$this->getCode()."' AND type = '".$this->getType()."' AND projectid = '".$this->getProjectID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	function nameExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
	
		$query = "SELECT id FROM ".$this->getTableName()." WHERE name = '".$this->getname()."' AND type = '".$this->getType()."' AND projectid = '".$this->getProjectID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('managerid', $formvalues)){
			$formvalues['managerid'] = NULL;
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			$formvalues['type'] = NULL;
		}
		if(isArrayKeyAnEmptyString('cspid', $formvalues)){
			$formvalues['cspid'] = NULL;
		}
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			$formvalues['projectid'] = NULL;
		}
		if(isArrayKeyAnEmptyString('parentid', $formvalues)){
			$formvalues['parentid'] = NULL;
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$conn = Doctrine_Manager::connection();
	
		$dups = $this->getDuplicates();
		if($dups->count() > 0){
			$i = 1;
			foreach ($dups as $aduplicate){
				if($i > 1){
					$aduplicate->delete();
					$conn->execute("ALTER TABLE ".$this->getTableName()." AUTO_INCREMENT = 1");
				}
				$i++;
			}
		}
	
		return true;
	}
	# find duplicates after save
	function getDuplicates(){
		$session = SessionWrapper::getInstance();
		$q = Doctrine_Query::create()->from('Program p')->where("p.type = '".$this->getType()."' AND p.code = '".$this->getCode()."' AND p.projectid = '".$this->getProjectID()."' ")->orderby("p.id");
	
		$result = $q->execute();
		return $result;
	}
}
?>