<?php

class MilestoneProgress extends BaseRecord {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('milestone_progress');
		
		$this->hasColumn('milestoneid', 'integer', null, array('default' => NULL));
		$this->hasColumn('period', 'string', 25);
		$this->hasColumn('summary', 'string', 500, array('notblank' => true));
		$this->hasColumn('details', 'string', 10000);
		$this->hasColumn('status', 'string', 10);
		$this->hasColumn('files', 'string', 255, array('default' => null));
		$this->hasColumn('inputdate', 'string', 25, array('default' => null));
		$this->hasColumn('inputby', 'string', 10, array('notblank' => true));
		
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('inputdate', $formvalues)){
			// unset($formvalues['inputdate']);
			$formvalues['inputdate'] = date('Y-m-d H:i:s', time());
		} else {
			$formvalues['inputdate'] = date('Y-m-d H:i:s', strtotime($formvalues['inputdate']));
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
}
?>