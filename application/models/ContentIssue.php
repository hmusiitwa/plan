<?php

class ContentIssue extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_issue');
		$this->hasColumn('type', 'string', 4);
		$this->hasColumn('name', 'string', 255);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('projectid','integer', null, array('default' => NULL));
		$this->hasColumn('activityid', 'integer', null, array('default' => NULL));
		$this->hasColumn('refno', 'string', 10);
		$this->hasColumn('parentid', 'integer', null, array('default' => NULL));
		$this->hasColumn('category', 'string', 25);
		$this->hasColumn('priority', 'string', 4);
		
		$this->hasColumn('participants', 'string', 255, array('default' => NULL));
		$this->hasColumn('userid', 'integer', null, array('default' => NULL));
		$this->hasColumn('startdate', 'string', 15);
		$this->hasColumn('enddate', 'string', 15);
		$this->hasColumn('status', 'string', 4);
		
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		// $this->addDateFields(array("startdate","enddate"));
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
		$this->hasOne('Activity as activity', array('local' => 'activityid', 'foreign' => 'id'));
		$this->hasOne('UserAccount as owner', array('local' => 'userid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('startdate', $formvalues)){
			unset($formvalues['startdate']);
		} else {
			$formvalues['startdate'] = date('Y-m-d', strtotime($formvalues['startdate']));
		}
		if(isArrayKeyAnEmptyString('enddate', $formvalues)){
			unset($formvalues['enddate']);
		} else {
			$formvalues['enddate'] = date('Y-m-d', strtotime($formvalues['enddate']));
		}
		if(!isArrayKeyAnEmptyString('participants_ids', $formvalues)){
			$formvalues['participants'] = implode(',', $formvalues['participants_ids']);
		} else {
			$formvalues['participants'] = NULL;
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
	
	function afterSave(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo($this->getID() + 10000);
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	
	function afterUpdate(){
		$session = SessionWrapper::getInstance();
		$resave = false;
	
		if(isEmptyString($this->getRefNo()) || $this->getRefNo() == 'Auto'){
			$this->setRefNo($this->getID() + 10000);
			$resave = true;
		}
			
		if($resave){
			# initial save
			$this->save();
		}
	
		return true;
	}
	function getStatusUpdates($query = "", $limit = ""){
		$customquery = "";
		if(!isEmptyString($query)){
			$customquery .= $query;
		}
		$q = Doctrine_Query::create()->from('ContentIssueDetail c')->where("c.issueid = '".$this->getID()."' ".$customquery)->orderby("c.inputdate asc, c.id asc");
		if(!isEmptyString($limit) && is_numeric($limit)){
			$q = Doctrine_Query::create()->from('ContentIssueDetail c')->where("c.issueid = '".$this->getID()."' ".$customquery)->limit($limit)->orderby("c.inputdate desc, c.id desc");
		}
	
		$result = $q->execute();
		return $result;
	}
	
	function isNotClosed(){
		return $this->getStatus() != 2;
	}
	function isClosed(){
		return $this->getStatus() == 2;
	}
	
	function updateStatus($status = 1){
		$session = SessionWrapper::getInstance();
		 
		# set active to true and blank out activation key
		$this->setStatus($status);
		$this->save();
		
		// $this->sendStatusChangeEmail(true);
		
		$module = '1';
		if($status == 2){
			$usecase = '1.6';
			$type = USER_DEACTIVATE;
			$details = "Issue ".$this->getName()." (Ref ".$this->getRefNo().")  closed ";
		}
		if($status == 1){
			$usecase = '1.7';
			$type = USER_REACTIVATE;
			$details = "Issue ".$this->getName()." (Ref ".$this->getRefNo().") re-opened ";
		}
		
		$audit_values = getAuditInstance();
		$audit_values['actionid'] = 18;
		$audit_values['module'] = $module;
		$audit_values['usecase'] = $usecase;
		$audit_values['transactiontype'] = $type;
		$audit_values['transactiondetails'] = $details;
		$audit_values['status'] = "Y";
		// $this->notify(new sfEvent($this, $type, $audit_values));
		
		return true;
	}
}
?>