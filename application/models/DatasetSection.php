<?php

class DatasetSection extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('data_set_section');
		
		$this->hasColumn('datasetid','integer', null, array('notblank' => true));
		$this->hasColumn('name', 'string', 500, array('notblank' => true));
		$this->hasColumn('refno', 'string', 15);
		$this->hasColumn('description', 'string', 500);
		$this->hasColumn('showrowtotal', 'string', 1, array('default' => NULL));
		$this->hasColumn('showcolumntotal', 'string', 1, array('default' => NULL));
		$this->hasColumn('elementids', 'string', 255, array('default' => NULL));
		$this->hasColumn('indicatorids', 'string', 255, array('default' => NULL));
		$this->hasColumn('categorytype', 'string', 1, array('default' => NULL));
		$this->hasColumn('categorygroup', 'string', 255, array('default' => NULL));
		$this->hasColumn('category', 'string', 255, array('default' => NULL));
		$this->hasColumn('greyelements', 'string', 255);
		$this->hasColumn('position', 'string', 4, array('default' => NULL));
		$this->hasColumn('rowmax', 'string', 10, array('default' => 5));
		$this->hasColumn('columns', 'string', 10, array('default' => 2));
		$this->hasColumn('groupattr_val', 'string', 10);
	}
	
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"name.notblank" => "Please enter Name"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Dataset as dataset', array('local' => 'datasetid', 'foreign' => 'id'));
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); // exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('position', $formvalues)){
			unset($formvalues['position']);
		}
		
		/* if(!isArrayKeyAnEmptyString('to_hidden', $formvalues)){
			$to = str_replace(' ', '', trim($formvalues['to_hidden']));
			unset($formvalues['optionlist']);
			unset($formvalues['to_hidden']);
			$formvalues['elementids'] = $to;
		} else {
			if(!isArrayKeyAnEmptyString('elementids_old', $formvalues)){
				$formvalues['elementids'] = NULL;
			} else {
				unset($formvalues['elementids']);
			}
		}
		if(!isArrayKeyAnEmptyString('to2_hidden', $formvalues)){
			$to2 = str_replace(' ', '', trim($formvalues['to2_hidden']));
			unset($formvalues['optionlist2']);
			unset($formvalues['to2_hidden']);
			$formvalues['indicatorids'] = $to2;
		} else {
			unset($formvalues['indicatorids']);
		}
		unset($formvalues['to']); */
		if(!isArrayKeyAnEmptyString('to', $formvalues)){
			// $to = str_replace(' ', '', trim($formvalues['to']));
			$formvalues['elementids'] = implode(',', $formvalues['to']);
			unset($formvalues['optionlist']);
			unset($formvalues['to']);
		} else {
			if(!isArrayKeyAnEmptyString('elementids_old', $formvalues)){
				$formvalues['elementids'] = NULL;
			} else {
				unset($formvalues['elementids']);
			}
		}
		if(!isArrayKeyAnEmptyString('to2', $formvalues)){
			// $to2 = str_replace(' ', '', trim($formvalues['to2']));
			$formvalues['indicatorids'] = implode(',', $formvalues['to2']);
			unset($formvalues['optionlist2']);
			unset($formvalues['to2']);
		} else {
			if(!isArrayKeyAnEmptyString('indicatorids_old', $formvalues)){
				$formvalues['indicatorids'] = NULL;
			} else {
				unset($formvalues['indicatorids']);
			}
		}
		
		if(!isArrayKeyAnEmptyString('category', $formvalues)){
			$formvalues['categorytype'] = 1;
		} else {
			if(!isArrayKeyAnEmptyString('category_old', $formvalues)){
				$formvalues['category'] = NULL;
			} else {
				unset($formvalues['category']);
			}
		}
		if(!isArrayKeyAnEmptyString('categorygroup', $formvalues)){
			$formvalues['categorytype'] = 2;
			if(!isArrayKeyAnEmptyString('category', $formvalues)){
				$formvalues['category'] = NULL;
			}
		} else {
			if(!isArrayKeyAnEmptyString('categorygroup_old', $formvalues)){
				$formvalues['categorygroup'] = NULL;
			} else {
				unset($formvalues['categorygroup']);
			}
		}
		
		if(!isArrayKeyAnEmptyString('categorytype', $formvalues)){
			if($formvalues['categorytype'] == '0'){
				$formvalues['category'] = NULL;
				$formvalues['categorygroup'] = NULL;
			}
			if($formvalues['categorytype'] == '1'){
				$formvalues['categorygroup'] = NULL;
			}
			if($formvalues['categorytype'] == '2'){
				$formvalues['category'] = NULL;
			}
		}
		
		if(!isArrayKeyAnEmptyString('showrowtotal', $formvalues)){
			$formvalues['showrowtotal'] = 1;
		} else {
			if(!isArrayKeyAnEmptyString('showrowtotal_old', $formvalues)){
				$formvalues['showrowtotal'] = NULL;
			} else {
				unset($formvalues['showrowtotal']);
			}
		}
		if(!isArrayKeyAnEmptyString('showcolumntotal', $formvalues)){
			$formvalues['showcolumntotal'] = 1;
		} else {
			if(!isArrayKeyAnEmptyString('showcolumntotal_old', $formvalues)){
				$formvalues['showcolumntotal'] = NULL;
			} else {
				unset($formvalues['showcolumntotal']);
			}
		}
		// debugMessage($formvalues); // exit();
		parent::processPost($formvalues);
	}
}
?>