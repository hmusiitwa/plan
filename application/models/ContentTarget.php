<?php

class ContentTarget extends BaseEntity {
	
	public function setTableDefinition() {
		#add the table definitions from the parent table
		parent::setTableDefinition();
		
		$this->setTableName('content_target');
		
		$this->hasColumn('type', 'integer', null, array('default' => 1)); // 1 - Quartely 2 - Monthly
		$this->hasColumn('projectid','integer', null, array('default' => NULL));
		$this->hasColumn('indicatorid', 'integer', null, array('default' => NULL));
		$this->hasColumn('attributeid', 'integer', null, array('default' => NULL));
		$this->hasColumn('notes', 'string', 1000, array('default' => NULL));
		$this->hasColumn('milestoneid', 'integer', null, array('default' => NULL));
		
		$this->hasColumn('baseline', 'string', 1000);
		$this->hasColumn('target1', 'string', 1000);
		$this->hasColumn('target2', 'string', 1000);
		$this->hasColumn('target3', 'string', 1000);
		$this->hasColumn('target4', 'string', 1000);
		$this->hasColumn('target5', 'string', 1000);
		$this->hasColumn('target6', 'string', 1000);
		$this->hasColumn('target7', 'string', 1000);
		$this->hasColumn('target8', 'string', 1000);
		$this->hasColumn('target9', 'string', 1000);
		$this->hasColumn('target10', 'string', 1000);
		$this->hasColumn('target11', 'string', 1000);
		$this->hasColumn('target12', 'string', 1000);
		
		$this->hasColumn('date1', 'string', 50);
		$this->hasColumn('date2', 'string', 50);
		$this->hasColumn('date3', 'string', 50);
		$this->hasColumn('date4', 'string', 50);
		$this->hasColumn('date5', 'string', 50);
		$this->hasColumn('date6', 'string', 50);
		$this->hasColumn('date7', 'string', 50);
		$this->hasColumn('date8', 'string', 50);
		$this->hasColumn('date9', 'string', 50);
		$this->hasColumn('date10', 'string', 50);
		$this->hasColumn('date11', 'string', 50);
		$this->hasColumn('date12', 'string', 50);
		
		$this->hasColumn('comment1', 'string', 1000);
		$this->hasColumn('comment2', 'string', 1000);
		$this->hasColumn('comment3', 'string', 1000);
		$this->hasColumn('comment4', 'string', 1000);
		$this->hasColumn('comment5', 'string', 1000);
		$this->hasColumn('comment6', 'string', 1000);
		$this->hasColumn('comment7', 'string', 1000);
		$this->hasColumn('comment8', 'string', 1000);
		$this->hasColumn('comment9', 'string', 1000);
		$this->hasColumn('comment10', 'string', 1000);
		$this->hasColumn('comment11', 'string', 1000);
		$this->hasColumn('comment12', 'string', 1000);
		
		$this->hasColumn('cost1', 'string', 10);
		$this->hasColumn('cost2', 'string', 10);
		$this->hasColumn('cost3', 'string', 10);
		$this->hasColumn('cost4', 'string', 10);
		$this->hasColumn('cost5', 'string', 10);
		$this->hasColumn('cost6', 'string', 10);
		$this->hasColumn('cost7', 'string', 10);
		$this->hasColumn('cost8', 'string', 10);
		$this->hasColumn('cost9', 'string', 10);
		$this->hasColumn('cost10', 'string', 10);
		$this->hasColumn('cost11', 'string', 10);
		$this->hasColumn('cost12', 'string', 10);
	}
	/**
	 * Contructor method for custom functionality - add the fields to be marked as dates
	 */
	public function construct() {
		parent::construct();
		
		$this->addDateFields(array("startdate","enddate"));
		
		// set the custom error messages
       	$this->addCustomErrorMessages(
       		array(
       			"type.notblank" => "Please specify Type"								
       		)
       	);
	}
	
	public function setUp() {
		parent::setUp(); 
		
		$this->hasOne('Milestone as milestone', array('local' => 'milestoneid', 'foreign' => 'id'));
		$this->hasOne('Indicator as indicator', array('local' => 'indicatorid', 'foreign' => 'id'));
		$this->hasOne('Project as project', array('local' => 'projectid', 'foreign' => 'id'));
	}
	/**
	 * Custom model validation
	 */
	function validate() {
		# execute the column validation
		parent::validate();
		// debugMessage($this->toArray(true));
		# validate that username is unique
		if($this->targetExists()){
		$this->getErrorStack()->add("period.unique", "Targets for Milestone <b>".$this->getMilestone()->getName()."</b> already exist.");
		}
	}
	# determine if the username has already been assigned
	function targetExists(){
		$conn = Doctrine_Manager::connection();
		# validate unique username and email
		$id_check = "";
		if(!isEmptyString($this->getID())){
			$id_check = " AND id <> '".$this->getID()."' ";
		}
		
		$query = "SELECT id FROM ".$this->getTableName()." WHERE indicatorid = '".$this->getIndicatorID()."' AND milestoneid = '".$this->getMilestoneID()."' ".$id_check;
		// debugMessage($query);
		$result = $conn->fetchOne($query);
		// debugMessage($result);
		if(isEmptyString($result)){
			return false;
		}
		
		return true;
	}
	/*
	 * Pre process model data
	*/
	function processPost($formvalues) {
		$session = SessionWrapper::getInstance(); // debugMessage($formvalues); exit;
		// trim spaces from the name field
		if(isArrayKeyAnEmptyString('projectid', $formvalues)){
			unset($formvalues['projectid']);
		}
		if(isArrayKeyAnEmptyString('indicatorid', $formvalues)){
			unset($formvalues['indicatorid']);
		}
		if(isArrayKeyAnEmptyString('attributeid', $formvalues)){
			unset($formvalues['attributeid']);
		}
		if(isArrayKeyAnEmptyString('type', $formvalues)){
			unset($formvalues['type']);
		}
		
		// debugMessage($formvalues); exit();
		parent::processPost($formvalues);
	}
	function getName(){
		return $this->getIndicator()->getRefno();
	}
	function afterSave(){
		$resave = false;
		if(!isEmptyString($this->getBaseline()) && $this->getBaseline() != $this->getIndicator()->getBaseline()){
			$this->getIndicator()->setBaseline($this->getBaseline());
			$resave = true;
		}
		if($resave){
			$this->getIndicator()->save();
		}
		
		return true;
	}
	function afterUpdate(){
		$resave = false;
		if(!isEmptyString($this->getBaseline()) && $this->getBaseline() != $this->getIndicator()->getBaseline()){
			$this->getIndicator()->setBaseline($this->getBaseline());
			$resave = true;
		}
		if($resave){
			$this->getIndicator()->save();
		}
		
		return true;
	}
}
?>