
//Use window.load and not document.ready for effective equal heights
$(document).ready(function() {
	
	$(".hastooltip:not(.disabledtip,.tipadded)").each(function(){
		var title = $(this).attr('title');
		var titleattr = $(this).attr('tiptitle');
		if (typeof titleattr !== typeof undefined && titleattr !== false) {
			var tiptitle = titleattr;
		} else {
			var tiptitle = 'Tooltip Help';
		}
		
		var contentattr = $(this).attr('tipbody');
		if (typeof contentattr !== typeof undefined && contentattr !== false) {
			var tipbody = contentattr;
		} else {
			var tipbody = "And here's some amazing help. It's very engaging yeah right...";
		}
		
		if($(this).hasClass('floattip')){
			$('<button type="button" tabindex="-1" class="btn btn-default btn-xs tooltp inline" data-toggle="popover" data-trigger="focus" style="margin-left:2px; position:absolute; right:13px; top:2px; vertical-align:top;"><i class="fa fa-question"></i></button>').insertAfter(this);
		} else if($(this).hasClass('datetip')){
			$('<button type="button" tabindex="-1" class="btn btn-default btn-xs tooltp inline" data-toggle="popover" data-trigger="focus" style="margin-left:25px; position:absolute; vertical-align:top;"><i class="fa fa-question"></i></button>').insertAfter(this);
		} else if($(this).hasClass('linetip')){
			$('<button type="button" tabindex="-1" class="btn btn-default btn-xs tooltp inline" data-toggle="popover" data-trigger="focus" style="margin-left:5px; position:absolute; vertical-align:top;"><i class="fa fa-question"></i></button>').insertAfter(this);
		} else if($(this).hasClass('tipmargin')){
			var tipmargin = $(this).attr('tipmargin');
			$('<button type="button" tabindex="-1" class="btn btn-default btn-xs tooltp inline" data-toggle="popover" data-trigger="focus" style="margin-left:'+tipmargin+'px; position:absolute; vertical-align:top;"><i class="fa fa-question"></i></button>').insertAfter(this);
		} else if($(this).hasClass('hovertip')){
			
			
		} else {
			$('<button type="button" tabindex="-1" class="btn btn-default btn-xs tooltp inline" data-toggle="popover" data-trigger="focus" style="margin-left:2px; vertical-align:top;"><i class="fa fa-question"></i></button>').insertAfter(this);
		}
		$(this).append();
		$('.tooltp').tooltipster({
			contentAsHTML: true,
			interactive: true,
			trigger: 'click',
			position: 'right',
			title: tiptitle,
			content: tipbody,
			theme: 'tooltipster-light',
			contentCloning: true,
			maxWidth: 300
		});
		
		$('.hovertip').tooltipster({
			title: title
		});
		
		$(this).addClass('tipadded');
	});
	
	// save dialog
	$('.reloadtrigger').on('click', function(e){ // alert('clicked');
		var obj = $('.reloadtrigger');
		var selectid = $(this).attr('selectid');
		var url = $(this).attr('dataurl');
		$(this).html('<img src="/assets/images/loader.gif" />');
		$('select#'+selectid).css({'background-color':'#eee'});
		$('#'+selectid+"_chosen .chosen-single span").css({'background-color':'#a7a6a2'});
		$('#'+selectid+"_chosen ul.chosen-choices").css({'background-color':'#a7a6a2'});
		
		$.getJSON(url, function (data) {
			selectValues = data;
			$("select#"+selectid).find('option[value!=""]').remove().trigger("change").trigger("chosen:updated");
			$.each(selectValues, function(key, value) { 
				$('select#'+selectid).append($("<option></option>").attr("value",key).text(value).attr("title", value));
			});
			sortSelectOptions('select#'+selectid, true);
			$('select#'+selectid).css({'background':'none'}).trigger('change').trigger("chosen:updated");
			obj.html('<i class="fa fa-refresh"></i>');
			$('#'+selectid+"_chosen .chosen-single span").css({'background':'none'});
			$('#'+selectid+"_chosen ul.chosen-choices").css({'background':'none'});
		});
	});	
	
	
	$("div.hasinputclear#selectarea input, div.hasinputclear input, div.hasinputclear#selectarea2 input, div.hasinputclear#selectarea_selected input, div.hasinputclear#selectarea2_selected input").addClass('addclear normalwidth');
	$("div.hasinputclear").append('<a class="gonowhere clear hidden makeabsolute" style="right:15px; top:8px; color:#0072ce; font-weight:normal; font-size:11px; cursor:pointer;"><i class="fa fa-remove" style=""></i> clear</a>');
	$("div.hasinputclear .clear").on('click', function(e){ // e.preventDefault(); 
		$("div.hasinputclear input.addclear").val('').trigger('keyup');
	}); 
	$("div.hasinputclear input.addclear").on('keyup', function(e){ // e.preventDefault(); 
		var searchval = $(this).val();
		if(isEmptyString(searchval)){
			$("div.hasinputclear .clear").addClass('hidden');
		} else {
			$("div.hasinputclear .clear").removeClass('hidden');
		}
	});
});